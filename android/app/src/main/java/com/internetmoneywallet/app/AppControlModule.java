package com.internetmoneywallet.app;

import android.app.Activity;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import java.util.Map;
import java.util.HashMap;

public class AppControlModule extends ReactContextBaseJavaModule {
  AppControlModule(ReactApplicationContext context) {
    super(context);
  }

  public String getName() {
    return "AppControlModule";
  }

  @ReactMethod
  public void moveToBackground() {
    Activity activity = getCurrentActivity();
    if (activity != null) {
      activity.moveTaskToBack(true);
    }
  }
}