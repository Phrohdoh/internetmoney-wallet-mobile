package com.internetmoneywallet.app;

import com.facebook.react.modules.network.OkHttpClientFactory;
import com.facebook.react.modules.network.OkHttpClientProvider;
import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;

public class SSLPinningFactory implements OkHttpClientFactory {
   private static String hostname = "api.internetmoney.io";

   public OkHttpClient createNewNetworkModuleClient() {
      CertificatePinner certificatePinner = new CertificatePinner.Builder()
        .add(hostname, "sha256/LzAv9WjT/WpmyjeMP1JWcScGJtgjbLJTgNHDzzA3KgU=") // Heroku primary public key fingerprint
        .add(hostname, "sha256/B6NnRxuiIisRQyf9ilbdpnXi1W6yYoA6/Wt4hfDWAL4=") // Backup public key fingerprint
        .build();
      OkHttpClient.Builder clientBuilder = OkHttpClientProvider.createClientBuilder();
      return clientBuilder.certificatePinner(certificatePinner).build();
  }
}