import AsyncStorage from '@react-native-async-storage/async-storage';

export const StorageOperation = {
  // Store data into async storage
  /**
   * Function to store single item into Secure storage
   * @param key: String
   */
  setData: async function (key, value) {
    try {
      await AsyncStorage.setItem(key, value);
      return {error: false};
    } catch (error) {
      return {error: true};
    }
  },

  /**
   * Function to store multiple item into Secure storage
   * @param obj: [{key,value}]
   */
  setMultiData: async function (obj) {
    try {
      await AsyncStorage.multiSet(obj);
      return {error: false};
    } catch (error) {
      return {error: true};
    }
  },

  // Get Data from async Storge
  /**
   * Function to retrieve single item from Secure storage
   * @param key: String
   */
  getData: async function (key) {
    try {
      const value = await AsyncStorage.getItem(key);
      return {data: value, error: value === null ? true : false};
    } catch (error) {
      return {data: null, error: true};
    }
  },

  /**
   * Function to retrieve multiple item from Secure storage
   * @param keys:[String,String]
   */
  getMultiData: async function (keys) {
    try {
      const values = await AsyncStorage.multiGet(keys);
      return {data: values, error: values === null ? true : false};
    } catch (e) {
      return {data: null, error: true};
    }
  },

  // Remove Data from async Storage
  /**
   * Function to remove single item from Secure storage
   * @param key: String
   * @param completionHandler: func
   */
  removeData: async function (key, completionHandler) {
    await AsyncStorage.removeItem(key, completionHandler);
  },

  /**
   * Function to remove multiple item from Secure storage
   * @param keys:[String,String]
   * @param completionHandler: func
   */
  removeMultiData: async function (keys, completionHandler) {
    await AsyncStorage.multiRemove(keys, completionHandler);
  },
};
