import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { CachedImage } from '@components';
import { useSelector } from 'react-redux';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// import languages
import { localize } from '@languages';

// import Screens
import * as Screen from '@screens';

import { IMAGES, COMMON_STYLE, COLORS } from '@themes';

import { Responsive } from '@helpers';
// import utils
import { useTokenIcons } from '@utils';
import { getNetwork } from '../redux';

const Tab = createBottomTabNavigator();

// render custom component
function renderHeaderButton (icon, onPress) {
  return (
    <TouchableOpacity
      testID={ 'navigation-header-back-button' }
      style={ styles.headerBtnStyle }
      onPress={ onPress }>
      <CachedImage source={ icon } style={ COMMON_STYLE.imageStyle(6) } />
    </TouchableOpacity>
  );
}

function createSupportHeaderObject (props, title) {
  const { navigation } = props;
  return {
    headerTitle: localize(title),
    headerRight: () => (
      <View style={ styles.headerRightViewStyle }>
        {renderHeaderButton(IMAGES.SUPPORT_ICON, () =>
          navigation.navigate('SUPPORT_SCREEN', {
            supportType: 'contactus',
          }),
        )}
      </View>
    ),
  };
}

function renderTabItem (route, focused) {
  return (
    <View style={ styles.tabIconStyle(focused) }>
      <CachedImage source={ IMAGES[route.name] } style={ styles.tabIcon(focused) } />
    </View>
  );
}
function renderNetworkTabItem (route, focused, network, getTokenIcon) {
  return (
    <View style={ styles.tabIconStyle(focused) }>
      <CachedImage
        source={ getTokenIcon('', network.chainId) }
        style={ COMMON_STYLE.imageStyle(7) }
      />
    </View>
  );
}

function renderClaimTabItem (route, focused) {
  return (
    <View style={ styles.tabIconStyle(focused) }>
      <CachedImage
        source={ focused ? IMAGES[route.name] : IMAGES.CLAIM_DASHBOARD_TAB_GRAY }
        style={ COMMON_STYLE.imageStyle(7) }
      />
    </View>
  );
}
function renderCenterTabItem (route, focused) {
  return (
    <View style={ styles.centerTabIconStyle(focused) }>
      <CachedImage source={ IMAGES[route.name] } style={ styles.centerTabIcon } />
    </View>
  );
}

const TabNavigator = props => {
  const getTokenIcon = useTokenIcons();
  const networks = useSelector(state => state.networks);
  const selectedNetwork = useSelector(getNetwork);
  const swapEnabled = networks?.wdSupportObject?.success
    && networks?.wdSupportObject?.swapRouterV3;

  return (
    <Tab.Navigator
      initialRouteName="WALLET_DASHBOARD_TAB"
      screenOptions={ ({ route }) => ({
        tabBarIcon: ({ focused }) => {
          return renderTabItem(route, focused);
        },
        tabBarLabel: ({ focused }) => {
          return (
            <Text style={ styles.tabBarLabelStyle(focused) }>
              {localize(route.name)}
            </Text>
          );
        },
        tabBarStyle: styles.tabBarStyle,
        tabBarBackground: () => <View style={ styles.tabbarViewStyle } />,
        headerStyle: styles.headerStyle,
        headerTransparent: false,
        headerTitleAlign: 'center',
        headerTitleStyle: styles.headerTitleStyle,
        headerTitle: '',
        headerShown: false,
        activeTintColor: '#000',
        inactiveTintColor: '#fff',
      }) }>
      <Tab.Screen
        name="NETWORK_DASHBOARD_TAB"
        component={ Screen.NetworksListScreen }
        options={ ({ route }) => {
          return {
            headerShown: true,
            ...createSupportHeaderObject(props, 'NETWORK_UPPERCASE'),
            tabBarIcon: ({ focused }) => {
              return renderNetworkTabItem(
                route,
                focused,
                selectedNetwork,
                getTokenIcon,
              );
            },
          };
        } }
      />
      <Tab.Screen
        name="CLAIM_DASHBOARD_TAB"
        component={ Screen.WdClaimListScreen }
        options={ ({ route }) => {
          return {
            tabBarIcon: ({ focused }) => {
              return renderClaimTabItem(route, focused);
            },
          };
        } }
      />
      <Tab.Screen
        name="WALLET_DASHBOARD_TAB"
        component={ Screen.WalletDashboardScreen }
        options={ ({ route }) => {
          return {
            tabBarIcon: ({ focused }) => {
              return renderCenterTabItem(route, focused);
            },
          };
        } }
      />
      <Tab.Screen
        name="SWAP_DASHBOARD_TAB"
        component={ Screen.SwapTokenSelectToken }
        listeners={({ navigation }) => ({
          tabPress: e => {
            e.preventDefault();
            if (swapEnabled) {
              navigation.navigate('SWAP_TOKEN_SELECT_TOKEN_SCREEN');
            }
          },
        })}
        options={ () => {
          return {
            tabBarIcon: ({ focused }) => (
              <View style={ styles.tabIconStyle(focused) }>
                <CachedImage
                  source={ IMAGES.SWAP_DASHBOARD_TAB_GRAY }
                  style={ COMMON_STYLE.imageStyle(7) }
                />
              </View>
            )
          };
        } }
      />

      <Tab.Screen
        name="SETTING_DASHBOARD_TAB"
        component={ Screen.SettingScreen }
        options={ {
          headerShown: true,
          ...createSupportHeaderObject(props, 'SETTINGS_UPPERCASE'),
        } }
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  tabBarStyle: {
    borderTopWidth: 0,
    position: 'absolute',
    height: Responsive.getHeight(13),
  },
  tabbarViewStyle: {
    backgroundColor: COLORS.BLACK,
    borderTopLeftRadius: Responsive.getWidth(5),
    borderTopRightRadius: Responsive.getWidth(5),
    borderColor: COLORS.GRAY18,
    borderTopWidth: 3,
    borderLeftWidth: 0.3,
    borderRightWidth: 0.3,
    height: '100%',
  },
  centerTabIconStyle: focused => {
    return {
      width: Responsive.getWidth(15),
      height: Responsive.getWidth(15),
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: Responsive.getWidth(7.5),
      backgroundColor: focused ? COLORS.YELLOWFB : COLORS.GRAY6D,
      marginTop: -Responsive.getHeight(3),
    };
  },
  centerTabIcon: {
    ...COMMON_STYLE.imageStyle(10, COLORS.BLACK),
  },
  tabIconStyle: () => {
    return {
      width: Responsive.getWidth(8),
      height: Responsive.getWidth(8),
      justifyContent: 'center',
      alignItems: 'center',
    };
  },

  tabIcon: focused => {
    return {
      ...COMMON_STYLE.imageStyle(7, focused ? COLORS.YELLOWFB : COLORS.GRAY6D),
    };
  },
  tabBarLabelStyle: focused => {
    return {
      ...COMMON_STYLE.textStyle(
        9,
        focused ? COLORS.WHITE : COLORS.GRAY6D,
        'BOLD',
        'center',
      ),
    };
  },
  headerStyle: {
    shadowOpacity: 0,
    shadowOffset: { height: 0 },
    elevation: 0,
    backgroundColor: COLORS.BLACK,
    height: 100,
  },

  headerTitleStyle: {
    ...COMMON_STYLE.textStyle(15, COLORS.GRAYB5, 'BOLD', 'center'),
  },
  headerRightViewStyle: {
    flexDirection: 'row',
  },
  headerBtnStyle: {
    width: Responsive.getWidth(10),
    height: Responsive.getWidth(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default TabNavigator;
