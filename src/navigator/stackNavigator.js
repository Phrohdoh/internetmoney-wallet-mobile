import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import TabNavigator from './TabNavigator';

// imoprt constants
import { COMMON_STYLE, COLORS, IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

import { Responsive } from '@helpers';
import {navigationRef} from '@utils/navigation';
import { JailBreakError, CachedImage } from '@components';


// import Screens
import * as Screen from '@screens';

const Stack = createNativeStackNavigator();

// render custom component
function renderHeaderButton (icon, onPress, tintColor) {
  return (
    <TouchableOpacity
      testID={ 'navigation-header-back-button' }
      style={ styles.headerBtnStyle }
      onPress={ onPress }>
      <CachedImage source={ icon } style={ COMMON_STYLE.imageStyle(6, tintColor) } />
    </TouchableOpacity>
  );
}

function supportHeaderObject (props, title, extraProps) {
  const { navigation } = props;
  return {
    headerTitle: localize(title),
    headerRight: () =>
      renderHeaderButton(IMAGES.SUPPORT_ICON, () =>
        navigation.navigate('SUPPORT_SCREEN', {
          supportType: 'contactus',
        }),
      ),
    ...extraProps,
  };
}

function createSupportHeaderObject (props, title) {
  const { navigation } = props;
  return {
    headerTitle: title ? localize(title) : title,
    headerRight: () => (
      <View style={ styles.headerRightViewStyle }>
        {renderHeaderButton(IMAGES.SUPPORT_ICON, () =>
          navigation.navigate('SUPPORT_SCREEN', {
            supportType: 'contactus',
          }),
        )}
      </View>
    ),
  };
}

function AppContainer () {
  
  return (
    <NavigationContainer ref={ navigationRef }>
      <Stack.Navigator
        screenOptions={ ({ navigation }) => ({
          headerStyle: styles.headerStyle,
          headerTransparent: true,
          headerTitleAlign: 'center',
          headerTitleStyle: styles.headerTitleStyle,
          headerTitle: '',
          headerShown: true,
          headerLeft: () =>
            renderHeaderButton(IMAGES.BACK_ARROW, () => navigation.goBack()),
        }) }>
        <Stack.Screen
          name="SPLASH_SCREEN"
          component={ Screen.SplashScreen }
          options={ { headerShown: false } }
        />

        <Stack.Screen
          name="JAILBREAK_ERROR_SCREEN"
          component={ JailBreakError }
          options={ { headerShown: false } }
        />


        <Stack.Screen
          name="START_SCREEN"
          component={ Screen.StartScreen }
          options={ { headerShown: false } }
        />
        <Stack.Screen
          name="EXPERIENCED_CRYPTO_SCREEN"
          component={ Screen.ExperiencedCryptoScreen }
        />
        <Stack.Screen
          name="LEARN_ONE_SCREEN"
          component={ Screen.LearnBasicOne }
          options={ props => {
            return {
              headerLeft: () =>
                renderHeaderButton(
                  IMAGES.BACK_ARROW,
                  () => props.navigation.goBack(),
                  COLORS.BLACK,
                ),
            };
          } }
        />
        <Stack.Screen
          name="LEARN_TWO_SCREEN"
          component={ Screen.LearnBasicTwo }
          options={ props => {
            return {
              headerLeft: () =>
                renderHeaderButton(
                  IMAGES.BACK_ARROW,
                  () => props.navigation.goBack(),
                  COLORS.BLACK,
                ),
            };
          } }
        />
        <Stack.Screen
          name="LEARN_THREE_SCREEN"
          component={ Screen.LearnBasicThree }
        />
        <Stack.Screen
          name="LEARN_FOUR_SCREEN"
          component={ Screen.LearnBasicFour }
        />
        <Stack.Screen
          name="LEARN_FIVE_SCREEN"
          component={ Screen.LearnBasicFive }
          options={ props => {
            return {
              headerLeft: () =>
                renderHeaderButton(
                  IMAGES.BACK_ARROW,
                  () => props.navigation.goBack(),
                ),
            };
          } }
        />
        <Stack.Screen
          name="PASS_SETUP_SCREEN"
          component={ Screen.PasswordSetupScreen }
        />
        <Stack.Screen
          name="PASSCODE_SETUP_SCREEN"
          component={ Screen.PasscodeSetupScreen }
        />
        <Stack.Screen
          name="WALLET_CREATE_SCREEN"
          component={ Screen.WalletCreateScreen }
        />
        <Stack.Screen
          name="WALLET_IMPORT_SCREEN"
          component={ Screen.ImportWalletScreen }
        />

        <Stack.Screen
          name="SECRET_REVOVERY_PHRASE_SCREEN"
          component={ Screen.SecretRecoveryPhraseScreen }
        />

        <Stack.Screen
          name="LOGIN_PASSWORD_VERIFICATION_SCREEN"
          component={ Screen.LoginPasswordVerificationScreen }
          options={ { headerShown: false } }
        />

        <Stack.Screen
          name="YOUR_SECRET_REVOVERY_PHRASE_SCREEN"
          component={ Screen.YourSecretRecoveryPhraseScreen }
        />
        <Stack.Screen
          name="GET_START_INFO_SCREEN"
          component={ Screen.GetStartInfoScreen }
          options={ { headerShown: false } }
        />

        <Stack.Screen
          name="DASHBOARD_TAB_NAVIGATOR"
          component={ TabNavigator }
          options={ { headerShown: false } }
        />
        <Stack.Screen
          name="ACCOUNT_DETAIL_SCREEN"
          component={ Screen.AccountDetailScreen }
          options={ { headerShown: false } }
        />
        <Stack.Screen
          name="SEND_TOKEN_SELECT_ACCOUNT_SCREEN"
          component={ Screen.SendTokenSelectAccount }
          options={ props => supportHeaderObject(props, 'SEND_TOKEN_UPPERCASE') }
        />
        <Stack.Screen
          name="SEND_TOKEN_ENTER_AMOUNT_SCREEN"
          component={ Screen.SendTokenEnterAmount }
          options={ props => supportHeaderObject(props, 'SEND_TOKEN_UPPERCASE') }
        />
        <Stack.Screen
          name="SEND_TOKEN_CONFIRMATION_SCREEN"
          component={ Screen.SendTokenConfirmationScreen }
          options={ props => supportHeaderObject(props, 'SEND_TOKEN_UPPERCASE') }
        />
        <Stack.Screen
          name="SEND_TOKEN_TRANSFER_BETWEEN_ACCOUNT_SCREEN"
          component={ Screen.SendTokenTransferBetweenAccount }
          options={ props => supportHeaderObject(props, 'SEND_TOKEN_UPPERCASE') }
        />
        <Stack.Screen
          name="RECEIVE_TOKEN_SCREEN"
          component={ Screen.ReceiveTokenScreen }
          options={ props =>
            createSupportHeaderObject(props, 'RECEIVE_TOKENS_UPPERCASE')
          }
        />
        <Stack.Screen
          name="SWAP_TOKEN_SELECT_TOKEN_SCREEN"
          component={ Screen.SwapTokenSelectToken }
          options={ props => supportHeaderObject(props, 'SWAP_TOKEN_UPPERCASE') }
        />
        <Stack.Screen
          name="SWAP_TOKEN_GET_QUOTE_SCREEN"
          component={ Screen.SwapTokenGetQuote }
          options={ props => supportHeaderObject(props, 'SWAP_TOKEN_UPPERCASE') }
        />
        <Stack.Screen
          name="SWAP_TOKEN_CONFIRMATION_SCREEN"
          component={ Screen.SwapTokenConfirmationScreen }
          options={ props => supportHeaderObject(props, 'SWAP_TOKEN_UPPERCASE') }
        />
        <Stack.Screen
          name="SWAP_APPROVE_CONFIRMATION_SCREEN"
          component={ Screen.SwapApproveConfirmationScreen }
          options={ props => supportHeaderObject(props, 'SWAP_TOKEN_UPPERCASE') }
        />
        <Stack.Screen
          name="RENAME_ACCOUNT_SCREEN"
          component={ Screen.RenameAccountScreen }
          options={ { headerTitle: localize('ACCOUNT_SETTING_UPPERCASE') } }
        />
        <Stack.Screen
          name="REVEAL_KEY_VERIFY_SCREEN"
          component={ Screen.RevealKeyVerificationScreen }
        />
        <Stack.Screen
          name="REVEAL_KEY_SCREEN"
          component={ Screen.RevealKeyScreen }
        />
        <Stack.Screen
          name="TRANSACTION_DETAIL_SCREEN"
          component={ Screen.TransactionDetailScreen }
          options={ props => createSupportHeaderObject(props, '') }
        />
        <Stack.Screen
          name="ADD_NETWORK_SCREEN"
          component={ Screen.AddNetworkScreen }
          options={ props => createSupportHeaderObject(props, 'ADD_NETWORK_UPPERCASE') }
        />
        <Stack.Screen
          name="CHANGE_PASSWORD_INFO_SCREEN"
          component={ Screen.ChangePasswordInfoScreen }
          options={ props =>
            supportHeaderObject(props, 'PASSWORD_SETTING_UPPERCASE')
          }
        />
        <Stack.Screen
          name="CHANGE_PASSWORD_SCREEN"
          component={ Screen.ChangePasswordScreen }
          options={ props =>
            supportHeaderObject(props, 'CHANGE_PASSWORD_UPPERCASE')
          }
        />
        <Stack.Screen
          name="SETTING_SECRET_REVOVERY_PHRASE_INFO_SCREEN"
          component={ Screen.SettingSecretRecoveryPhraseInfoScreen }
        />
        <Stack.Screen
          name="SETTING_RECOVERY_PASSWORD_VERIFICATION_SCREEN"
          component={ Screen.SettingRecoveryPasswordVerificationScreen }
        />
        <Stack.Screen
          name="SETTING_SECRET_REVOVERY_PHRASE_SCREEN"
          component={ Screen.SettingSecretRecoveryPhraseScreen }
          options={ props => {
            return {
              headerLeft: () =>
                renderHeaderButton(IMAGES.BACK_ARROW, () =>
                  props.navigation.navigate('SETTING_DASHBOARD_TAB'),
                ),
            };
          } }
        />
        <Stack.Screen
          name="ABOUT_SCREEN"
          component={ Screen.AboutScreen }
          options={ props => supportHeaderObject(props, 'ABOUT_UPPERCASE') }
        />
        <Stack.Screen
          name="CHANGE_LANGUAGE_SCREEN"
          component={ Screen.ChangeLanguageScreen }
          options={ props =>
            supportHeaderObject(props, 'CHANGE_LANGUAGE_UPPERCASE')
          }
        />
        <Stack.Screen
          name="DATA_COLLECTION_SCREEN"
          component={ Screen.DataCollectionScreen }
          options={ props =>
            supportHeaderObject(props, 'SENDING_LOGS_UPPERCASE')
          }
        />
        <Stack.Screen
          name="WALLET_CONNECT_SCREEN"
          component={ Screen.WalletConnectScreen }
          options={ props => supportHeaderObject(props, '') }
        />
        <Stack.Screen name="SUPPORT_SCREEN" component={ Screen.SupportScreen } />
        <Stack.Screen
          name="WD_CLAIM_SCREEN"
          component={ Screen.WdClaimScreen }
          options={ props =>
            supportHeaderObject(props, 'CLAIM_DIVIDENDS_UPPERCASE')
          }
        />
        {/* <Stack.Screen
          name="WD_CLAIM_LIST_SCREEN"
          component={ Screen.WdClaimListScreen }
          options={ props =>
            supportHeaderObject(props, 'CLAIM_DIVIDENDS_UPPERCASE')
          }
        /> */}
        <Stack.Screen
          name="SIGN_TRANSACTION_CONFIRMAITON_SCREEN"
          component={ Screen.SignTransactionConfirmationScreen }
          options={ props =>
            supportHeaderObject(props, 'SIGN_REQUEST_UPPERCASE')
          }
        />
        <Stack.Screen
          name="SEND_TRANSACTION_CONFIRMAITON_SCREEN"
          component={ Screen.SendTransactionConfirmationScreen }
          options={ props =>
            supportHeaderObject(props, 'DAPP_TRANSACTION_UPPERCASE')
          }
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

AppContainer.propTypes = {
  navigation: PropTypes.any,
};
export default AppContainer;

const styles = StyleSheet.create({
  headerStyle: {
    shadowOpacity: 0,
    shadowOffset: { height: 0 },
    elevation: 0,
    backgroundColor: COLORS.TRANSPARENT,
  },

  headerTitleStyle: {
    ...COMMON_STYLE.textStyle(15, COLORS.GRAYB5, 'BOLD', 'center'),
  },
  headerRightViewStyle: {
    flexDirection: 'row',
  },
  headerBtnStyle: {
    width: Responsive.getWidth(10),
    height: Responsive.getWidth(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
