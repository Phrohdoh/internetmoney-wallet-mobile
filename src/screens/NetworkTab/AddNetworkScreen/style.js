import {StyleSheet} from 'react-native';

import {COMMON_STYLE, COLORS} from '@themes';
import {Responsive} from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  headerTopMarginStyle: {
    backgroundColor: COLORS.BLACK,
  },
  containerStyle: {
    backgroundColor: COLORS.GRAY11,
    borderRadius: Responsive.getWidth(5),
    borderWidth: 1,
    borderColor: COLORS.GRAY1F,
    marginTop: Responsive.getWidth(5),
    padding: Responsive.getWidth(5),
    flex: 1,
  },
  inputContainerStyle: {
    marginTop: Responsive.getHeight(3),
  },
  addNetworkStyle: {
    ...COMMON_STYLE.roundedBtnMargin(2),
    marginTop: Responsive.getHeight(2),
  },
});
