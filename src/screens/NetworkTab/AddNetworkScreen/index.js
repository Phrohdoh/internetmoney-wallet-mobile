import React, { useState } from 'react';
import { View, Text } from 'react-native';

import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';

import { GlobalAction, NetworksAction } from '@redux';

// import Utils
import { Validation } from '@utils';

// import components
import {
  SafeAreaWrapper,
  RoundedButton,
  TitleTextInput,
  KeyboardAvoidScrollView,
} from '@components';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

import * as Web3Layer from '@web3';

const AddNetworkScreen = props => {
  const { navigation } = props;
  const dispatch = useDispatch();

  const networks = useSelector(state => state.networks);

  const [isLoading, setIsLoading] = useState(false);

  const [networkName, onChangeNetworkName] = useState({
    id: 'network_name',
    value: '',
    title: 'NETWORK_NAME',
    placeholder: 'NETWORK_NAME',
    extraProps: {
      isError: false,
      errorText: localize('ENTER_NETWORK_NAME_MSG'),
    },
  });

  const [rpcUrl, onChangeRpcUrl] = useState({
    id: 'rpc_url',
    value: '',
    title: 'RPC_URL',
    placeholder: 'NEW_RPC_NETWORK',
    extraProps: {
      isError: false,
      errorText: localize('ENTER_RPC_URL_MSG'),
    },
  });
  const [chainId, onChangeChainId] = useState({
    id: 'chain_id',
    value: '',
    title: 'CHAIN_ID',
    placeholder: 'CHAIN_ID',
    extraProps: {
      isError: false,
      errorText: localize('ENTER_CHAIN_ID_MSG'),
    },
  });
  const [symbol, onChangeSymbol] = useState({
    id: 'symbol',
    value: '',
    title: 'SYMBOL',
    placeholder: 'SYMBOL',
    extraProps: {
      isError: false,
      errorText: localize('ENTER_SYMBOL_NAME_MSG'),
    },
  });
  const [blockUrl, onChangeBlockUrl] = useState({
    id: 'block_explorer_url',
    value: '',
    title: 'BLOCK_EXPLOR_URL',
    placeholder: 'BLOCK_EXPLOR_URL_OPTIONAL',
    extraProps: {
      isError: false,
      errorText: localize('enterBlockUrlMsg'),
    },
  });

  // Action Methods
  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.value = text;
    tempDict.extraProps = { ...state.extraProps, isError: false };
    setFunction(tempDict);
  };

  const changeObj = (obj, state, setFunction) => {
    const tmpObj = { ...state, ...obj };
    setFunction(tmpObj);
  };

  const checkValidation = () => {
    const isEmptyNetworkName = Validation.isEmpty(networkName.value);
    const isEmptyRpcUrl = Validation.isEmpty(rpcUrl.value);
    const isEmptyChainId = Validation.isEmpty(chainId.value);
    const isEmptySymbol = Validation.isEmpty(symbol.value);
    const isEmptyBlockUrl = Validation.isEmpty(blockUrl.value);

    const isValidRpcUrl = Validation.isValidURL(rpcUrl.value);
    const isValidBlockUrl = Validation.isValidURL(blockUrl.value);

    const isRcpExist = networks.networks.find(
      network => network.rpc === rpcUrl.value.trim(),
    );

    const isChainIdExist = networks.networks.find(
      network => network.chainId + '' === chainId.value.trim() + '',
    );

    changeObj(
      {
        extraProps: { ...networkName.extraProps, isError: isEmptyNetworkName },
      },
      networkName,
      onChangeNetworkName,
    );

    changeObj(
      {
        extraProps: {
          ...rpcUrl.extraProps,
          isError: isEmptyRpcUrl,
          errorText: localize('ENTER_RPC_URL_MSG'),
        },
      },
      rpcUrl,
      onChangeRpcUrl,
    );
    changeObj(
      { extraProps: { ...chainId.extraProps, isError: isEmptyChainId } },
      chainId,
      onChangeChainId,
    );
    changeObj(
      { extraProps: { ...symbol.extraProps, isError: isEmptySymbol } },
      symbol,
      onChangeSymbol,
    );

    /* istanbul ignore else */
    if (isEmptyRpcUrl || !isValidRpcUrl) {
      changeObj(
        {
          extraProps: {
            ...rpcUrl.extraProps,
            isError: true,
            errorText: localize('ENTER_VALID_RPC_MSG'),
          },
        },
        rpcUrl,
        onChangeRpcUrl,
      );
    }

    /* istanbul ignore else */
    if (!isEmptyBlockUrl && !isValidBlockUrl) {
      changeObj(
        {
          extraProps: {
            ...blockUrl.extraProps,
            isError: true,
            errorText: localize('ENTER_VALID_BLOCK_URL_MSG'),
          },
        },
        blockUrl,
        onChangeBlockUrl,
      );
    }

    if (isRcpExist) {
      changeObj(
        {
          extraProps: {
            ...rpcUrl.extraProps,
            isError: true,
            errorText: localize('RPC_ALREADY_EXIST_MSG'),
          },
        },
        rpcUrl,
        onChangeRpcUrl,
      );
    }

    if (isChainIdExist) {
      changeObj(
        {
          extraProps: {
            ...chainId.extraProps,
            isError: true,
            errorText: localize('NETWORK_ALREADY_EXIST_MSG'),
          },
        },
        chainId,
        onChangeChainId,
      );
    }

    if (
      !isEmptyNetworkName &&
      !isEmptyRpcUrl &&
      !isEmptyChainId &&
      !isEmptySymbol &&
      (isEmptyBlockUrl || isValidBlockUrl) &&
      !isRcpExist &&
      !isChainIdExist &&
      isValidRpcUrl
    ) {
      return true;
    } else {
      return false;
    }
  };
  const _onPressAddNetwork = async () => {
    if (checkValidation()) {
      setIsLoading(true);
      const networkValidation = await Web3Layer.runNetworkValidation(
        rpcUrl.value,
        chainId.value,
      );

      if (networkValidation.success) {
        const addNetworkObject = await Web3Layer.addNewBlockchainNetwork(
          networkName.value,
          rpcUrl.value,
          chainId.value,
          symbol.value,
          blockUrl.value,
        );

        setIsLoading(false);

        if (addNetworkObject.success) {
          const newNetworks = [
            ...networks.networks,
            addNetworkObject.networkDetails,
          ];
          await StorageOperation.setData(
            ASYNC_KEYS.NETWORKS,
            JSON.stringify(newNetworks),
          );
          dispatch(NetworksAction.saveNetworks(newNetworks));

          navigation.goBack();
        } else {
          dispatch(
            GlobalAction.showAlert(localize('ERROR'), addNetworkObject.error),
          );
        }
      } else {
        dispatch(
          GlobalAction.showAlert(localize('ERROR'), networkValidation.error),
        );
        setIsLoading(false);
      }
    }
  };

  // Render Custom components
  const renderInputComponent = (state, setStateFunction) => {
    return (
      <TitleTextInput
        testID={ `add-network-${state.id}` }
        inputContainerStyle={ styles.inputContainerStyle }
        value={ state.value }
        title={ localize(state.title) }
        placeholder={ localize(state.placeholder) }
        onChangeText={ text => changeData(text, state, setStateFunction) }
        { ...state.extraProps }
      />
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <KeyboardAvoidScrollView>
        <View
          style={ [
            COMMON_STYLE.borderedContainerViewStyle(0),
            styles.headerTopMarginStyle,
          ] }>
          <View style={ styles.containerStyle }>
            <Text
              style={ COMMON_STYLE.textStyle(
                10,
                COLORS.LIGHT_OPACITY(0.6),
                'BASE',
                'center',
              ) }>
              {localize('ADD_NETWORK_MSG')}
            </Text>
            {renderInputComponent(networkName, onChangeNetworkName)}
            {renderInputComponent(rpcUrl, onChangeRpcUrl)}
            {renderInputComponent(chainId, onChangeChainId)}
            {renderInputComponent(symbol, onChangeSymbol)}
            {renderInputComponent(blockUrl, onChangeBlockUrl)}
            <RoundedButton
              testID="add-network-button"
              isLoading={ isLoading }
              style={ styles.addNetworkStyle }
              title={ localize('ADD_NETWORK_UPPERCASE') }
              onPress={ () => _onPressAddNetwork() }
            />
          </View>
        </View>
      </KeyboardAvoidScrollView>
    </SafeAreaWrapper>
  );
};

AddNetworkScreen.propTypes = {
  navigation: PropTypes.any,
};
export default AddNetworkScreen;
