import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  headerTopMarginStyle: {
    backgroundColor: COLORS.BLACK,
  },
  flatlistStyle: {
    backgroundColor: COLORS.GRAY11,
    borderRadius: Responsive.getWidth(5),
    borderWidth: 2,
    borderColor: COLORS.GRAY1F,
    flexGrow: 0,
    marginTop: Responsive.getHeight(2),
  },
  sepratorStyle: {
    height: 1,
    backgroundColor: COLORS.GRAY1F,
  },
  editBtnContainreStyle: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
  },
  addTokenStyle: {
    height: Responsive.getHeight(4),
    width: Responsive.getWidth(40),
    // marginBottom: Responsive.getHeight(2),
    marginTop: Responsive.getHeight(2),
  },
});
