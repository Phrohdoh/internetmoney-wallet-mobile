import React, { useState } from 'react';
import { View, Text, FlatList, SafeAreaView } from 'react-native';

import PropTypes from 'prop-types';

import { useDispatch, useSelector, useStore } from 'react-redux';

import { WalletAction, GlobalAction, NetworksAction, getNetworks } from '@redux';

// import components
import { RoundedButton, NetworkListCell, AlertActionModel } from '@components';

// import constants
import { ASYNC_KEYS } from '@constants';

// import themes
import { IMAGES, COLORS, COMMON_STYLE, bottomTabPadding } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import storage functions
import { StorageOperation } from '@storage';
import { getNetwork } from '../../../redux';

const NetworksListScreen = props => {
  const { navigation } = props;
  const dispatch = useDispatch();
  const store = useStore();

  const selectedNetwork = useSelector(getNetwork);
  const networksList = useSelector(getNetworks);
  const [choosedNetwork, setChoosedNetwork] = useState({});
  const [isShowConfirmationPopup, setIsShowConfirmationPopup] = useState(false);
  const [isRemoveEnable, setIsRemoveEnable] = useState(false);

  const changeNetwork = async () => {
    setIsShowConfirmationPopup(false);
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData());
    await StorageOperation.setData(
      ASYNC_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          localize('NETWORK_SWITCH_TITLE'),
          localize('NETWORK_CHANGE_SUCCESS_MSG') + choosedNetwork.networkName,
        ),
      );
    }, 200);
  };

  // Remove Token
  const removeNetwork = async item => {
    const tempNetworksList = [...networksList];
    if (tempNetworksList.length > 1) {
      const idx = tempNetworksList.findIndex(x => x.chainId === item.chainId);

      tempNetworksList.splice(idx, 1);

      await StorageOperation.setData(
        ASYNC_KEYS.NETWORKS,
        JSON.stringify(tempNetworksList),
      );
      dispatch(NetworksAction.saveNetworks(tempNetworksList));

      if (item.chainId === selectedNetwork.chainId) {
        setIsShowConfirmationPopup(false);
        dispatch(NetworksAction.saveSelectedNetworkObject(tempNetworksList[0]));
        dispatch(WalletAction.resetData());
        await StorageOperation.setData(
          ASYNC_KEYS.SELECTED_NETWORK,
          JSON.stringify(tempNetworksList[0]),
        );
      }

      if (tempNetworksList.length <= 1) {
        setIsRemoveEnable(false);
      }
    }
    if (item.isDefault) {
      dispatch(NetworksAction.removeDefaultNetwork(item.chainId));
      await StorageOperation.setData(
        ASYNC_KEYS.REMOVED_DEFAULT_NETWORKS,
        JSON.stringify(store.getState().networks.removedDefaultNetworks)
      );
    }
  };

  return (
    <SafeAreaView style={ COMMON_STYLE.safeAreaViewStyle }>
      <View
        style={ [
          COMMON_STYLE.borderedContainerViewStyle(bottomTabPadding),
          styles.headerTopMarginStyle,
        ] }>
        <View style={ styles.editBtnContainreStyle }>
          <RoundedButton
            testID="add-network-button"
            style={ styles.addTokenStyle }
            leftImage={ IMAGES.ADD_ICON }
            leftImageStyle={ COMMON_STYLE.imageStyle(5) }
            titleStyle={ COMMON_STYLE.textStyle(11, COLORS.BLACK, 'BOLD') }
            title={ localize('ADD_NETWORK_UPPERCASE') }
            onPress={ () => navigation.navigate('ADD_NETWORK_SCREEN') }
          />
          <RoundedButton
            testID="remove-network-button"
            style={ styles.addTokenStyle }
            titleStyle={ COMMON_STYLE.textStyle(11, COLORS.BLACK, 'BOLD') }
            title={ localize(
              !isRemoveEnable ? 'REMOVE_NETWORK_UPPERCASE' : 'DONE_UPPERCASE',
            ) }
            onPress={ () => {
              if (networksList.length > 1) {
                setIsRemoveEnable(!isRemoveEnable);
              }
            } }
          />
        </View>
        <FlatList
          style={ styles.flatlistStyle }
          data={ networksList }
          renderItem={ ({ item, index }) => (
            <NetworkListCell
              item={ item }
              index={ index }
              isRemoveEnable={ isRemoveEnable && networksList.length > 1 }
              selectedNetwork={ selectedNetwork }
              onSelectNetwork={ item => {
                if (item.chainId !== selectedNetwork.chainId) {
                  setChoosedNetwork(item);
                  setIsShowConfirmationPopup(true);
                }
              } }
              onPressDelete={ item => {
                removeNetwork(item);
              } }
            />
          ) }
          ItemSeparatorComponent={ () => <View style={ styles.sepratorStyle } /> }
          keyExtractor={ (item, index) => index + item }
        />
      </View>
      <AlertActionModel
        alertTitle={ localize('CHANGE_NETWORK') }
        isShowAlert={ isShowConfirmationPopup }
        successBtnTitle={ localize('YES') }
        onPressSuccess={ () => changeNetwork() }
        onPressCancel={ () => setIsShowConfirmationPopup(false) }>
        <Text
          style={ COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BASE', 'center') }>
          {localize('CHANGE_NETWORK_TO')}
          <Text
            style={ COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BOLD', 'center') }>
            {`${choosedNetwork.networkName}?`}
          </Text>
        </Text>
      </AlertActionModel>
    </SafeAreaView>
  );
};

NetworksListScreen.propTypes = { navigation: PropTypes.any };
export default NetworksListScreen;
