import {StyleSheet} from 'react-native';

import {COMMON_STYLE, COLORS} from '@themes';
import {Responsive} from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,

  selectedNetworkStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  networkNameStyle: {
    ...COMMON_STYLE.textStyle(13, COLORS.WHITE, 'BOLD', 'center'),
  },

  titleStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)),
    marginTop: Responsive.getHeight(2),
    marginBottom: Responsive.getHeight(1),
  },
  fromAccountViewStyle: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    borderRadius: 25,
    alignItems: 'center',
    borderWidth: 2,
    borderColor: COLORS.GRAY1F,
    paddingHorizontal: Responsive.getWidth(4),
  },
  inputViewStyle: {
    borderColor: COLORS.GRAY1F,
  },
  accountDetailStyle: {flex: 1},
  accountNameStyle: {...COMMON_STYLE.textStyle(11, COLORS.GRAYDF, 'BOLD')},
  balenceStyle: {...COMMON_STYLE.textStyle(10, COLORS.GRAY94)},
  balanceValueStyle: {...COMMON_STYLE.textStyle(10, COLORS.WHITE)},
  flatlistStyle: {
    borderRadius: Responsive.getWidth(5),
    borderColor: COLORS.GRAY41,
    borderWidth: 2,
    marginBottom: Responsive.getHeight(2),
  },
  sepratorStyle: {height: 1, backgroundColor: COLORS.LIGHT_OPACITY(0.1)},
  listSepratorStyle: {height: Responsive.getHeight(1)},
});
