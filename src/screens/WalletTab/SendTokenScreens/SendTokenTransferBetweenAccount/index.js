import React, { useState } from 'react';

import {
  View,
  Text,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';
import {
  NetworksAction,
  WalletAction,
  GlobalAction,
  getAccountList,
  getTokensByAddress,
} from '@redux';

// import components
import {
  SafeAreaWrapper,
  RoundedButton,
  ModalContainerView,
  SendTokenAccountListCell,
  StepsListView,
  NetworkSelectionModalView,
  CachedImage,
} from '@components';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { IMAGES, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';
import { getNetwork } from '../../../../redux';

const SendTokenTransferBetweenAccount = props => {
  const { navigation } = props;
  const params = props.route.params;

  const accountList = useSelector(getAccountList);

  const selectedNetwork = useSelector(getNetwork);
  const [selectedAccount, setSelectedAccount] = useState(params.accountDetail);
  const [selectedToAccount, setSelectedToAccount] = useState(
    params.accountDetail,
  );

  const [isSelectToAccount, setIsSelectToAccount] = useState(false);
  const [isShowAccountList, setIsShowAccountList] = useState(false);
  const [isShowNetworkList, setIsShowNetworkList] = useState(false);

  const dispatch = useDispatch();

  const tokens = useSelector(getTokensByAddress)[selectedAccount.publicAddress.toLowerCase()];
  const nativeToken = tokens?.[0];

  const _selecteAccount = item => {
    setIsShowAccountList(false);

    if (isSelectToAccount) {
      setSelectedToAccount(item);
    } else {
      setSelectedAccount(item);
    }
  };

  const goNext = async () => {
    if (
      selectedAccount.publicAddress.toLowerCase() ===
      selectedToAccount.publicAddress.toLowerCase()
    ) {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('CHOOSE_DIFFERENT_ACCOUNT_MSG'),
        ),
      );
    } else {
      navigation.navigate('SEND_TOKEN_ENTER_AMOUNT_SCREEN', {
        fromAccount: selectedAccount,
        fromAccountName: selectedAccount.name,
        toAccountAddress: selectedToAccount.publicAddress,
        toAccountName: selectedToAccount.name,
      });
    }
  };

  const changeNetwork = async choosedNetwork => {
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData());
    await StorageOperation.setData(
      ASYNC_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          localize('NETWORK_SWITCH_TITLE'),
          localize('NETWORK_CHANGE_SUCCESS_MSG') + choosedNetwork.networkName,
        ),
      );
    }, 200);
  };

  // Render custom component
  const renderFromAccountView = () => {
    return (
      <TouchableOpacity
        testID={ 'send-token-choose-account-button' }
        style={ styles.fromAccountViewStyle }
        onPress={ () => {
          setIsShowAccountList(true);
          setIsSelectToAccount(false);
        } }>
        <View style={ styles.accountDetailStyle }>
          <Text style={ styles.accountNameStyle }>{selectedAccount.name}</Text>
          <Text style={ styles.balenceStyle }>
            {localize('BALANCE:')}
            <Text style={ styles.balanceValueStyle }>
              {`${nativeToken.balance?.value?.toString(6, true) ?? ''} ${nativeToken.symbol}`}
            </Text>
          </Text>
        </View>
        <CachedImage source={ IMAGES.DOWN_ARROW } style={ COMMON_STYLE.imageStyle(6) } />
      </TouchableOpacity>
    );
  };

  const renderToTransferView = () => {
    return (
      <TouchableOpacity
        testID={ 'enter-the-account-to-transfer-to' }
        style={ styles.fromAccountViewStyle }
        onPress={ () => {
          setIsShowAccountList(true);
          setIsSelectToAccount(true);
        } }>
        <View style={ styles.accountDetailStyle }>
          <Text style={ styles.accountNameStyle }>{selectedToAccount.name}</Text>
        </View>
        <CachedImage source={ IMAGES.DOWN_ARROW } style={ COMMON_STYLE.imageStyle(6) } />
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <TouchableOpacity onPress={ () => setIsShowNetworkList(true) }>
        <Text style={ styles.selectedNetworkStyle }>
          {localize('CURRENT_NETWORK')}
        </Text>
        <Text style={ styles.networkNameStyle }>
          {selectedNetwork.networkName}
        </Text>
      </TouchableOpacity>
      <StepsListView selectedSteps={ 1 } />
      <View style={ COMMON_STYLE.borderedContainerViewStyle() }>
        <Text style={ styles.titleStyle }>
          {localize('CHOOSE_SEND_FROM_ACCOUNT')}
        </Text>
        {renderFromAccountView()}
        <Text style={ styles.titleStyle }>
          {localize('CHOOSE_TRANSFER_TO_ACCOUNT')}
        </Text>
        {renderToTransferView()}
        <View style={ { flex: 1 } } />
        <RoundedButton
          testID="send-token_transferbetween-next-button"
          style={ COMMON_STYLE.roundedBtnMargin() }
          title={ localize('NEXT_UPPERCASE') }
          onPress={ () => goNext() }
        />
      </View>
      <NetworkSelectionModalView
        isVisible={ isShowNetworkList }
        onPressClose={ () => setIsShowNetworkList(false) }
        onSelectNetwork={ item => {
          setIsShowNetworkList(false);
          if (item.chainId !== selectedNetwork.chainId) {
            changeNetwork(item);
          }
        } }
      />
      <ModalContainerView
        testID={ 'modal-send-token' }
        isVisible={ isShowAccountList }
        onPressClose={ () => setIsShowAccountList(false) }>
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('CHOOSE_ACCOUNTS_UPPERCASE')}
          </Text>
          <ScrollView
            horizontal
            contentContainerStyle={ { width: '100%' } }
            scrollEnabled={ false }>
            <FlatList
              data={ accountList }
              bounces={ false }
              renderItem={ ({ item, index }) => (
                <SendTokenAccountListCell
                  item={ item }
                  index={ index }
                  onSelectAccount={ item => _selecteAccount(item) }
                />
              ) }
              ItemSeparatorComponent={ () => (
                <View style={ styles.listSepratorStyle } />
              ) }
              keyExtractor={ (item, index) => index + item }
            />
          </ScrollView>
        </React.Fragment>
      </ModalContainerView>
    </SafeAreaWrapper>
  );
};

SendTokenTransferBetweenAccount.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default SendTokenTransferBetweenAccount;
