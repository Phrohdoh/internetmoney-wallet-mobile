import React, { useEffect, useState } from 'react';

import {
  View,
  Text,
  ScrollView,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';
import {
  NetworksAction,
  WalletAction,
  GlobalAction,
  getAccountList,
} from '@redux';

// import components
import {
  SafeAreaWrapper,
  RoundedButton,
  ModalContainerView,
  TitleTextInput,
  SendTokenAccountListCell,
  RecentSentCell,
  StepsListView,
  QRScannerModal,
  NetworkSelectionModalView,
  CachedImage,
} from '@components';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { IMAGES, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import { Validation } from '@utils';

import * as Web3Layer from '@web3';
import { getTokensByAddress } from '@redux';
import { getNetwork } from '../../../../redux';

const SendTokenSelectAccountScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const dispatch = useDispatch();

  const [recentSentList, setRecentSentList] = useState([]);

  const networks = useSelector(state => state.networks);
  const accounts = useSelector(state => state.accounts);
  const transactions = useSelector(state => state.transactions);
  const accountList = useSelector(getAccountList);

  const selectedNetwork = useSelector(getNetwork);

  const [selectedAccount, setSelectedAccount] = useState(params.accountDetail);
  const [isValidatingEns, setIsValidatingEns] = useState(false);
  const [isAddressValid, setIsAddressValid] = useState(false);
  const [toAccountAddress, setToAccountAddress] = useState('');
  const [isShowAccountList, setIsShowAccountList] = useState(false);
  const [isShowQrScanner, setIsShowQrScanner] = useState(false);
  const [isShowNetworkList, setIsShowNetworkList] = useState(false);
  const [publicAddress, onChangePublicAddress] = useState({
    id: 'public-address',
    value: '',
    placeholder: 'ENTER_PUBLIC_ADDRESS_PLACEHOLDER',
    extraProps: {
      isError: false,
      errorText: localize('ENTER_PUBLIC_ADDRESS_MSG'),
    },
  });

  const tokens = useSelector(getTokensByAddress)[selectedAccount.publicAddress.toLowerCase()];
  const nativeToken = tokens?.[0];

  useEffect(() => {
    filterRecentAccountList();
  }, [networks]);

  // Action Methods
  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.value = text;
    tempDict.extraProps = { ...state.extraProps, isError: false };
    setFunction(tempDict);
  };
  const changeObj = (obj, state, setFunction) => {
    const tmpObj = { ...state, ...obj };
    setFunction(tmpObj);
  };

  const filterRecentAccountList = () => {
    const uniqueToAddress = [
      ...new Set(transactions.recentAddresses.map(item => item)),
    ];

    setRecentSentList(uniqueToAddress.filter(n => n));
  };

  const _selectedAccount = item => {
    setIsShowAccountList(false);
    setSelectedAccount(item);
  };

  // Validate to address Or ENS
  const validateAddress = async text => {
    /* istanbul ignore else */
    if (!Validation.isEmpty(text)) {
      setIsValidatingEns(true);
      const isValidENS = await Web3Layer.checkValidENSOrToAddress(text);
      setIsValidatingEns(false);
      setIsAddressValid(isValidENS.success);
      if (!isValidENS.success) {
        changeObj(
          {
            extraProps: {
              ...publicAddress.extraProps,
              isError: true,
              errorText: isValidENS.error,
            },
          },
          publicAddress,
          onChangePublicAddress,
        );
      } else {
        setToAccountAddress(isValidENS.address);
      }
    }
  };

  const goNext = async () => {
    if (!Validation.isEmpty(publicAddress.value)) {
      if (isAddressValid) {
        navigation.navigate('SEND_TOKEN_ENTER_AMOUNT_SCREEN', {
          fromAccount: selectedAccount,
          fromAccountName: selectedAccount.name,
          toAccountAddress: toAccountAddress,
        });
      }
    } else {
      changeObj(
        {
          extraProps: {
            ...publicAddress.extraProps,
            isError: true,
            errorText: localize('ENTER_PUBLIC_ADDRESS_MSG'),
          },
        },
        publicAddress,
        onChangePublicAddress,
      );
    }
  };

  const transferBetweenAccount = async () => {
    navigation.navigate('SEND_TOKEN_TRANSFER_BETWEEN_ACCOUNT_SCREEN', {
      accountDetail: selectedAccount,
    });
  };

  const _selectRecentAccount = item => {
    if (selectedAccount.publicAddress.toLowerCase() !== item.toLowerCase()) {
      navigation.navigate('SEND_TOKEN_ENTER_AMOUNT_SCREEN', {
        fromAccount: selectedAccount,
        fromAccountName: selectedAccount.name,
        toAccountAddress: item,
      });
    }
  };

  const changeNetwork = async choosedNetwork => {
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData());
    await StorageOperation.setData(
      ASYNC_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          localize('NETWORK_SWITCH_TITLE'),
          localize('NETWORK_CHANGE_SUCCESS_MSG') + choosedNetwork.networkName,
        ),
      );
    }, 200);
  };

  // render custom component
  const renderFromAccountView = () => {
    return (
      <TouchableOpacity
        testID={ 'send-token-choose-account-button' }
        style={ styles.fromAccountViewStyle }
        onPress={ () => setIsShowAccountList(true) }>
        <View style={ styles.accountDetailStyle }>
          <Text style={ styles.accountNameStyle }>{selectedAccount.name}</Text>
          <Text style={ styles.balenceStyle }>
            {localize('BALANCE:')}
            <Text style={ styles.balanceValueStyle }>
              {`${nativeToken?.balance?.value?.toString(6, true) ?? ''} ${nativeToken?.symbol}`}
            </Text>
          </Text>
        </View>
        <CachedImage source={ IMAGES.DOWN_ARROW } style={ COMMON_STYLE.imageStyle(6) } />
      </TouchableOpacity>
    );
  };

  // Custom Render Components
  const renderQRButton = () => {
    return (
      <TouchableOpacity
        testID="send-token-show-qr-code"
        onPress={ () => setIsShowQrScanner(true) }>
        <CachedImage source={ IMAGES.QR_ICON } style={ COMMON_STYLE.imageStyle(7) } />
      </TouchableOpacity>
    );
  };

  const renderLoading = () => {
    return <ActivityIndicator />;
  };

  const renderInputComponent = (state, setStateFunction) => {
    return (
      <TitleTextInput
        testID={ `send-token-screen-${state.id}` }
        inputViewStyle={ styles.inputViewStyle }
        value={ state.value }
        title={ localize(state.title) }
        placeholder={ localize(state.placeholder) }
        onChangeText={ text => changeData(text, state, setStateFunction) }
        rightComponent={ isValidatingEns ? renderLoading() : renderQRButton() }
        onEndEditing={ text => validateAddress(text.nativeEvent.text) }
        { ...state.extraProps }
      />
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <TouchableOpacity onPress={ () => setIsShowNetworkList(true) }>
        <Text style={ styles.selectedNetworkStyle }>
          {localize('CURRENT_NETWORK')}
        </Text>
        <Text style={ styles.networkNameStyle }>
          {selectedNetwork.networkName}
        </Text>
      </TouchableOpacity>
      <StepsListView selectedSteps={ 1 } />
      <View style={ COMMON_STYLE.borderedContainerViewStyle() }>
        <Text style={ styles.titleStyle }>
          {localize('CHOOSE_SEND_FROM_ACCOUNT')}
        </Text>
        {renderFromAccountView()}
        <Text style={ styles.titleStyle }>
          {localize('ENTER_SEND_TO_ACCOUNT')}
        </Text>
        {renderInputComponent(publicAddress, onChangePublicAddress)}
        <Text style={ styles.titleStyle }>{localize('RECENT_ACCOUNT')}</Text>
        <FlatList
          style={ styles.flatlistStyle }
          data={ recentSentList }
          renderItem={ ({ item, index }) => (
            <RecentSentCell
              item={ item }
              index={ index }
              accounts={ accounts }
              onSelectRecentAccount={ item => _selectRecentAccount(item) }
            />
          ) }
          ItemSeparatorComponent={ () => <View style={ styles.sepratorStyle } /> }
          keyExtractor={ (item, index) => index + item }
        />

        <RoundedButton
          testID="send-token-transfer-button"
          isBordered
          style={ COMMON_STYLE.roundedBtnMargin() }
          title={ localize('TRANSFER_BETWEEN_ACCOUNTS_UPPERCASE') }
          onPress={ () => transferBetweenAccount() }
        />
        <RoundedButton
          testID="send-token-next-button"
          style={ COMMON_STYLE.roundedBtnMargin() }
          title={ localize('NEXT_UPPERCASE') }
          onPress={ () => goNext() }
        />
      </View>
      <ModalContainerView
        testID={ 'modal-send-token' }
        isVisible={ isShowAccountList }
        onPressClose={ () => setIsShowAccountList(false) }>
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('CHOOSE_ACCOUNTS_UPPERCASE')}
          </Text>
          <ScrollView
            horizontal
            contentContainerStyle={ { width: '100%' } }
            scrollEnabled={ false }>
            <FlatList
              data={ accountList }
              bounces={ false }
              renderItem={ ({ item, index }) => (
                <SendTokenAccountListCell
                  item={ item }
                  index={ index }
                  onSelectAccount={ item => _selectedAccount(item) }
                />
              ) }
              ItemSeparatorComponent={ () => (
                <View style={ styles.listSepratorStyle } />
              ) }
              keyExtractor={ (item, index) => index + item }
            />
          </ScrollView>
        </React.Fragment>
      </ModalContainerView>
      <QRScannerModal
        isVisible={ isShowQrScanner }
        isValidateAddress={ true }
        scanedValue={ (value, toAddress) => {
          setIsAddressValid(true);
          setIsShowQrScanner(false);
          setToAccountAddress(toAddress);
          changeData(value, publicAddress, onChangePublicAddress);
        } }
        onPressClose={() => setIsShowQrScanner(false)}
      />
      <NetworkSelectionModalView
        isVisible={ isShowNetworkList }
        onPressClose={ () => setIsShowNetworkList(false) }
        onSelectNetwork={ item => {
          setIsShowNetworkList(false);
          if (item.chainId !== selectedNetwork.chainId) {
            changeNetwork(item);
          }
        } }
      />
    </SafeAreaWrapper>
  );
};

SendTokenSelectAccountScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default SendTokenSelectAccountScreen;
