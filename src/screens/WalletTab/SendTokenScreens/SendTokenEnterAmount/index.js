import React, { useState } from 'react';

import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';
import { NetworksAction, WalletAction, GlobalAction } from '@redux';

// import components
import {
  SafeAreaWrapper,
  RoundedButton,
  ModalContainerView,
  SegmentController,
  CurrencyInputField,
  SelectTokenListCell,
  StepsListView,
  KeyboardAvoidScrollView,
  NetworkSelectionModalView,
  CachedImage,
} from '@components';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { IMAGES, COLORS, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import {
  Validation,
  useTokenIcons,
  removeCommas,
  sanitizeFloatingPointString,
  IntegerUnits,
} from '@utils';

import * as Web3Layer from '@web3';
import { getTokensByAddress } from '@redux';
import { getNetwork } from '../../../../redux';

const SendTokenEnterAmountScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const getTokenIcon = useTokenIcons();

  const selectedNetwork = useSelector(getNetwork);

  const accountDetail = params.fromAccount;
  const [isShowTokenList, setIsShowTokenList] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [isShowNetworkList, setIsShowNetworkList] = useState(false);
  const [maxAmountErrorMessage, setMaxAmountErrorMessage] = useState('');
  const tokens = useSelector(getTokensByAddress)[accountDetail.publicAddress.toLowerCase()];
  const [selectedToken, setSelectedToken] = useState(tokens?.[0]);

  const tokenBalance = selectedToken?.balance?.value;
  const displayedBalance = tokenBalance === undefined
    ? ''
    : tokenBalance.toString(6, true);

  const dispatch = useDispatch();

  const usdValue = selectedToken?.usdPrice?.value;
  const isSupportUSD = usdValue !== undefined;

  const [amount, onChangeAmount] = useState({
    id: 'amount',
    value: '',
    displayValue: '',
    placeholder: 'AMOUNT',
    extraProps: {
      textAlign: 'right',
      keyboardType: 'numeric',
    },
  });

  const isUSD = currentIndex !== 0;

  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.displayValue = text;
    // remove commas and potential '$'
    tempDict.value = removeCommas(text)?.replace('$', '') 
    tempDict.extraProps = { ...state.extraProps, isError: false };
    setFunction(tempDict);
  };

  const getConvertedValue = () => {
    try {
      const value = IntegerUnits.fromFloatingPoint(amount.value);
      if (!isUSD) {
        return `${localize('USD_UPPERCASE')} ${localize('VALUE')}: $${value.multiply(usdValue).toString(2)}`;
      }
      return `${selectedToken.symbol} ${localize('VALUE')}: ${value.divide(usdValue).toString(6)}`;
    } catch (e) {
      // Error
    }
  };

  const selectToken = token => {
    setIsShowTokenList(false);
    setSelectedToken(token);
  };

  const goNext = async () => {
    try {
      if (!Validation.isEmpty(amount.value) && parseFloat(amount.value) > 0) {
        let sendAmount = IntegerUnits.fromFloatingPoint(amount.value);
        if (isUSD) {
          sendAmount = sendAmount.divide(usdValue, true);
        }
        sendAmount = sendAmount.toPrecision(selectedToken.decimals, true);

        if (sendAmount.gt(tokenBalance)) {
          dispatch(
            GlobalAction.showAlert(
              localize('ERROR'),
              localize('SEND_AMOUNT_ERROR_MSG'),
            ),
          );
        } else {
          navigation.navigate('SEND_TOKEN_CONFIRMATION_SCREEN', {
            fromAccount: accountDetail,
            fromAccountName: params.fromAccountName,
            toAccountAddress: params.toAccountAddress,
            toAccountName: params.toAccountName,
            selectedToken,
            transferAmount: sendAmount,
            isSupportUSD: isSupportUSD,
            usdValue: isSupportUSD ? usdValue : '',
          });
        }
      } else {
        dispatch(
          GlobalAction.showAlert(
            localize('ERROR'),
            localize('ENTER_VALID_AMOUNT_MSG'),
          ),
        );
      }
    } catch (e) {
      // error
    }
  };

  const changeNetwork = async choosedNetwork => {
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData());
    await StorageOperation.setData(
      ASYNC_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          localize('NETWORK_SWITCH_TITLE'),
          localize('NETWORK_CHANGE_SUCCESS_MSG') + choosedNetwork.networkName,
        ),
      );
    }, 200);
  };

  const getMaxAmount = async () => {
    const maxAmount = await Web3Layer.getMaxAmount(
      1,
      selectedToken.address,
      selectedToken.decimals,
      tokenBalance,
      selectedNetwork,
    );

    if (maxAmount.success) {
      const baseMax = maxAmount.maxBalance;

      if (!isUSD) {
        changeData(
          baseMax.toString(6, true),
          amount,
          onChangeAmount,
        );
      } else {
        changeData(
          baseMax.multiply(usdValue).toString(6, true),
          amount,
          onChangeAmount,
        );
      }

      setMaxAmountErrorMessage('');
    } else {
      setMaxAmountErrorMessage(maxAmount.error);
    }
  };

  // custom components
  const renderTokenSelectionView = () => {
    return (
      <TouchableOpacity
        testID={ 'choose-token-to-send-button' }
        style={ styles.fromAccountViewStyle }
        onPress={ () => setIsShowTokenList(true) }>
        <CachedImage
          source={ getTokenIcon(
            selectedToken.address,
            selectedNetwork.chainId,
          ) }
          style={ styles.tokenIconStyle }
        />
        <View style={styles.accountDetailStyle}>
          <Text style={styles.accountNameStyle}>
          {tokenBalance?.toString(6) ?? ''} {selectedToken.symbol}
          </Text>
          {
            isSupportUSD
              ? (
                <Text style={styles.balenceStyle}>
                  <Text style={styles.orangeUsdValue}>${tokenBalance?.multiply?.(usdValue)?.toString(2) ?? ''}</Text>
                </Text>
              )
              : null
          }
        </View>
        <CachedImage source={ IMAGES.DOWN_ARROW } style={ COMMON_STYLE.imageStyle(6) } />
      </TouchableOpacity>
    );
  };

  const renderAmountView = () => {
    const segmentList = [{ title: selectedToken.symbol }];
    if (isSupportUSD) {
      segmentList.push({ title: localize('USD_UPPERCASE') });
    }

    return (
      <View style={ styles.amountViewStyle }>
        <SegmentController
          style={ styles.segmentContainerStyle(segmentList.length) }
          segments={ segmentList }
          currentIndex={ currentIndex }
          onChangeIndex={ index => {
            changeData('', amount, onChangeAmount);
            setCurrentIndex(index);
          } }
        />
        {renderInputComponent(amount, onChangeAmount)}
      </View>
    );
  };
  const renderInputComponent = (state, setStateFunction) => {
    return (
      <CurrencyInputField
        inputContainerStyle={ styles.inputContainerStyle }
        inputViewStyle={ styles.inputViewStyle }
        testID={ `send-token-enter-amount-screen-${state.id}` }
        value={ state.displayValue }
        title={ localize(state.title) }
        isUSD={ isUSD }
        placeholder={ localize(state.placeholder) }
        onChangeText={ (text) => {
          if(!text){
            changeData('', state, setStateFunction);
          } else {
            const sanitized = sanitizeFloatingPointString(text, true, isUSD ? 6 : selectedToken.decimals);
            changeData(sanitized, state, setStateFunction);
          }
        }}
        decimalsLimit={ isUSD ? 6 : selectedToken.decimals }
        { ...state.extraProps }
      />
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <TouchableOpacity onPress={ () => setIsShowNetworkList(true) }>
        <Text style={ styles.selectedNetworkStyle }>
          {localize('CURRENT_NETWORK')}
        </Text>
        <Text style={ styles.networkNameStyle }>
          {selectedNetwork.networkName}
        </Text>
      </TouchableOpacity>
      <StepsListView selectedSteps={ 2 } />

      <View style={ COMMON_STYLE.borderedContainerViewStyle() }>
        <KeyboardAvoidScrollView>
          <Text style={ styles.titleStyle }>{localize('CHOOSE_TOKEN')}</Text>
          {renderTokenSelectionView()}
          <View style={styles.labelAndMax}>
            <Text style={ styles.amountTitleStyle }>{localize('ENTER_AMOUNT')}</Text>
            <RoundedButton
              testID="max-button"
              style={ styles.maxBtnStyle }
              title={ localize('MAX_UPPERCASE') }
              onPress={ () => getMaxAmount() }
            />
          </View>
          {renderAmountView()}
          {isSupportUSD && (
            <Text
              style={ COMMON_STYLE.textStyle(
                10,
                COLORS.LIGHT_OPACITY(0.6),
                'BASE',
                'right',
              ) }>
              {getConvertedValue()}
            </Text>
          )}
          <Text style={ styles.maxErrorMessageStyle }>
            {maxAmountErrorMessage}
          </Text>
        </KeyboardAvoidScrollView>
        <RoundedButton
          testID="enter-amount-next-button"
          style={ COMMON_STYLE.roundedBtnMargin() }
          title={ localize('NEXT_UPPERCASE') }
          onPress={ () => goNext() }
        />
      </View>

      <ModalContainerView
        testID={ 'modal-send-token' }
        isVisible={ isShowTokenList }
        onPressClose={ () => setIsShowTokenList(false) }>
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('CHOOSE_TOKEN_UPPERCASE')}
          </Text>
          <ScrollView
            horizontal
            contentContainerStyle={ { width: '100%' } }
            scrollEnabled={ false }>
            <FlatList
              data={ [...tokens] }
              bounces={ false }
              renderItem={ ({ item, index }) => (
                <SelectTokenListCell
                  item={ item }
                  index={ index }
                  networkObj={ selectedNetwork }
                  accountAddress={ accountDetail }
                  onSelectToken={ item => selectToken(item) }
                />
              ) }
              ItemSeparatorComponent={ () => (
                <View style={ styles.listSepratorStyle } />
              ) }
              keyExtractor={ (item, index) => index + item }
            />
          </ScrollView>
        </React.Fragment>
      </ModalContainerView>

      <NetworkSelectionModalView
        isVisible={ isShowNetworkList }
        onPressClose={ () => setIsShowNetworkList(false) }
        onSelectNetwork={ item => {
          setIsShowNetworkList(false);
          if (item.chainId !== selectedNetwork.chainId) {
            changeNetwork(item);
          }
        } }
      />
    </SafeAreaWrapper>
  );
};

SendTokenEnterAmountScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default SendTokenEnterAmountScreen;
