import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { GlobalAction, TransactionsAction } from '@redux';

// import components
import {
  GasFeeRangeView,
  ModalContainerView,
  AlertActionModel,
  SafeAreaWrapper,
  StepsListView,
  CachedImage,
} from '@components';

import SwipeVerifyController from '@components/SwipeVerifyController';
// import constants
import { ASYNC_KEYS } from '@constants';

import { IntegerUnits } from '@utils';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import { Validation, useTokenIcons } from '@utils';

import {
  WalletTransactionTypes,
  calculateEstimateFee,
  calculateMaxFee,
  createAndSendTxn,
  getSendTxnObject,
  getSingleAccount,
  updateTxnLists
} from '@web3';
import { getUserFriendlyErrorMessage } from '../../../../utils/errors';
import { getNetwork } from '../../../../redux';

const SendTokenConfirmationScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const getTokenIcon = useTokenIcons();
  const wallet = useSelector(state => state.wallet);
  const transactions = useSelector(state => state.transactions);
  const selectedNetwork = useSelector(getNetwork);
  const [selectedRange, setSelectedRange] = useState(1);
  const [txnObject, setTxnObject] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [isShowGasFeeView, setIsShowGasFeeView] = useState(false);
  const [gasRange, setGasRange] = useState({});
  const selectedToken = params.selectedToken;
  const [accountBalance, setAccountBalance] = useState(new IntegerUnits(0));
  const [isGetTxnObject, setIsGetTxnObject] = useState(false);
  const [isShowError, setIsShowError] = useState(false);
  const [error, setError] = useState('');

  const dispatch = useDispatch();

  useEffect(() => {
    getTxnObject();
    getAccountBalance();
  }, []);

  // Action Methods

  const getAccountBalance = async () => {
    const balance = await getSingleAccount(
      params.fromAccount.publicAddress,
      selectedNetwork,
    );

    setAccountBalance(balance.account.value);
  };

  const getTxnObject = async () => {
    setIsLoading(true);
    const isNetworkTokenMatch = Validation.isEmpty(selectedToken.address);

    const parm = [
      !isNetworkTokenMatch,
      !isNetworkTokenMatch ? selectedToken.address : '',
      params.fromAccount.publicAddress,
      params.toAccountAddress,
      params.transferAmount.toPrecision(selectedToken.decimals),
      selectedNetwork,
    ];

    const txObject = await getSendTxnObject(...parm);

    if (txObject.success) {
      setGasRange(txObject.range);
      setIsLoading(false);
      setTxnObject(txObject.txnObject);
      setIsGetTxnObject(true);
    } else {
      navigation.navigate('ACCOUNT_DETAIL_SCREEN', {
        accountDetail: params.fromAccount,
      });
      dispatch(GlobalAction.showAlert(localize('ERROR'), txObject.error));
    }
  };

  const updateTxnObjectTypeTwo = (gas, maxPrioFee, maxFeePerGas) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.maxPriorityFeePerGas = maxPrioFee;
    tempTxObj.maxFeePerGas = maxFeePerGas;
    setTxnObject(tempTxObj);
  };

  const updateTxnObjectTypeOne = (gas, gasPrice) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.gasPrice = gasPrice;
    setTxnObject(tempTxObj);
  };

  const getEstimateFee = () => {
    if (!txnObject) return '';
    const estimateFee = calculateEstimateFee(txnObject, selectedNetwork.txnType);
    return `${estimateFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getMaxFee = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getTotal = () => {
    return `${params.transferAmount.toString(6)} ${selectedToken.symbol} + ${getEstimateFee()}`;
  };

  const getMaxTotal = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${params.transferAmount.toString(6)} ${selectedToken.symbol} + ${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const sendTokens = async () => {
    try {
      const txnResult = await createAndSendTxn(
        WalletTransactionTypes.SEND,
        txnObject,
        wallet,
        params.fromAccount,
        accountBalance,
        selectedNetwork,
        {
          address: selectedToken.address,
          transferAmount: params.transferAmount
        }
      );

      if (txnResult.success) {
        const { tempTxns, tempTxnsList } = await updateTxnLists(
          IntegerUnits.serialize({
            type: WalletTransactionTypes.SEND,
            transactionDetail: {
              accountName: params.fromAccountName,
              toAccountName: params.toAccountName,
              isSupportUSD: params.isSupportUSD,
              displayAmount: params.transferAmount,
              displayDecimal: selectedToken.decimals,
              transferedAmount: params.transferAmount,
              usdValue: params.usdValue,
              toAddress: params.toAccountAddress,
              fromAddress: params.fromAccount.publicAddress,
              networkDetail: selectedNetwork,
              fromToken: selectedToken,
              utcTime: moment.utc().format(),
            },
            receipt: txnResult.receipt,
            blockNumber: txnResult.blockNumber,
          }),
          transactions,
          selectedNetwork
        );

        const newRecentAddresses = [
          ...transactions.recentAddresses,
          params.toAccountAddress
        ];

        dispatch(TransactionsAction.saveTransactions(tempTxnsList));
        dispatch(TransactionsAction.saveRecentAddresses(newRecentAddresses));

        await StorageOperation.setMultiData([
          [ASYNC_KEYS.TRANSACTIONS, JSON.stringify(tempTxns)],
          [ASYNC_KEYS.RECENT_TRANSACTIONS, JSON.stringify(newRecentAddresses)],
        ]);

        setIsLoading(false);
        dispatch(
          GlobalAction.showAlert(
            localize('SUCCESS'),
            localize('SUBMIT_SUCCESS'),
          ),
        );
        navigation.navigate('ACCOUNT_DETAIL_SCREEN', {
          accountDetail: params.fromAccount,
        });
      } else {
        setIsLoading(false);
        if (txnResult.error.toLowerCase().includes('insufficient funds')) {
          setError(localize('INSUFFICIENT_FUNDS', [fromToken.symbol]));
        } else {
          setError(txnResult.error);
        }
        setIsShowError(true);
      }
    } catch (err) {
      console.error(err);
      setIsLoading(false);
    }
  };

  const slideToSendTokens = () => {
    setIsLoading(true);
    setTimeout(sendTokens, 0);
  }

  // render custom component
  const renderFromAccountView = () => {
    return (
      <View style={ styles.fromViewStyle }>
        <View style={ { flex: 1 } }>
          <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
            {localize('FROM:')}
          </Text>
          <Text style={ COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD') }>
            {params.fromAccountName}
          </Text>
        </View>
        <View style={ { flex: 1 } }>
          <Text
            style={ COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD', 'right') }>
            {selectedNetwork.sym + ' '}
          </Text>
          <Text
            numberOfLines={ 1 }
            style={ COMMON_STYLE.textStyle(11, COLORS.GRAY94, 'BASE', 'right') }>
            {localize('BALANCE:')}
            <Text
              style={ COMMON_STYLE.textStyle(11, COLORS.WHITE, 'BASE', 'right') }>
              {accountBalance.toString(6, true)}
            </Text>
          </Text>
        </View>
        <CachedImage
          source={ getTokenIcon('', selectedNetwork.chainId) }
          style={ styles.tokenIconStyle }
        />
      </View>
    );
  };

  const renderToAccountView = () => {
    return (
      <View style={ styles.toViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('TO:')}
        </Text>
        <Text
          numberOfLines={ 1 }
          adjustsFontSizeToFit={ true }
          style={ styles.toAddressTextStyle }>
          {params.toAccountName ? params.toAccountName : params.toAccountAddress} 
        </Text>
      </View>
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <Text style={ styles.selectedNetworkStyle }>
        {localize('CURRENT_NETWORK')}
      </Text>
      <Text style={ styles.networkNameStyle }>{selectedNetwork.networkName}</Text>

      <StepsListView selectedSteps={ 3 } />
      <View style={ { flex: 1 } }>
        <CachedImage source={ IMAGES.CONFIRMATION_BG } style={ styles.bgStyle } />

        <View style={ styles.detailContainerStyle }>
          <ScrollView bounces={ false }>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <Text style={ styles.confirmStyle }>
              {localize('CONFIRM_TRANSACTION_UPPERCASE')}
            </Text>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />

            <View style={ styles.accountDetailViewStyle }>
              <View style={ styles.lineViewStyle }>
                <CachedImage
                  source={ IMAGES.VERTICAL_DOTTET_LINE }
                  style={ styles.verticalLineStyle }
                />
                <View style={ styles.dotContainStyle }>
                  <View style={ styles.dotViewStyle } />
                  <CachedImage
                    style={ COMMON_STYLE.imageStyle(4) }
                    source={ IMAGES.DOWN_ARROW }
                  />
                  <View style={ styles.dotViewStyle } />
                </View>
              </View>
              <View style={ { flex: 1 } }>
                {renderFromAccountView()}
                {renderToAccountView()}
              </View>
            </View>
            <Text style={ styles.amountTitleStyle }>{localize('AMOUNT')}</Text>
            <View style={ styles.borderedViewStyle }>
              <View style={ styles.amountContainerViewStyle }>
                <Text style={ styles.amountStyle } adjustsFontSizeToFit={ true } numberOfLines={ 1 }>
                  {params.transferAmount.toString(6, true)} {selectedToken.symbol}
                </Text>
                <CachedImage
                  source={ getTokenIcon(
                    selectedToken.address,
                    selectedNetwork.chainId,
                  ) }
                  style={ COMMON_STYLE.imageStyle(7) }
                />
              </View>

              {params.isSupportUSD && (
                <Text
                  style={ COMMON_STYLE.textStyle(
                    12,
                    COLORS.LIGHT_OPACITY(0.6),
                    'BASE',
                    'center',
                  ) }>
                  ${params.usdValue.multiply(params.transferAmount).toString(2)}
                </Text>
              )}
            </View>

            <View style={ styles.bottomBorderStyle }>
              <View style={ styles.rowViewStyle }>
                <Text
                  style={ styles.estimateStyle }
                  adjustsFontSizeToFit={ true }
                  numberOfLines={ 1 }>
                  {localize('ESTIMATE_GAS_FEE')}
                </Text>
                <TouchableOpacity
                  testID={ 'gas-fee-manage-btn' }
                  style={ styles.estimateFeeBtnStyle }
                  onPress={ () => {
                    if (isGetTxnObject) {
                      setIsShowGasFeeView(true);
                    }
                  } }>
                  <Text
                    style={ styles.estimateFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {getEstimateFee()}
                  </Text>
                  <CachedImage
                    style={ COMMON_STYLE.imageStyle(4, COLORS.YELLOWFB) }
                    source={ IMAGES.DOWN_ARROW }
                  />
                </TouchableOpacity>
              </View>
              <View style={ styles.rowViewStyle }>
                <Text style={ styles.secondTextStyle }>
                  {localize('likely_second')}
                </Text>
                {selectedNetwork.txnType == 2 && (
                  <Text
                    style={ styles.maxFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {localize('MAX_FEE:')}
                    <Text style={ styles.feeStyle }>{getMaxFee()}</Text>
                  </Text>
                )}
              </View>
            </View>
            <View>
              <View style={ styles.rowViewStyle }>
                <Text style={ styles.estimateStyle }>{localize('TOTAL')}</Text>
                <Text
                  style={ styles.totalValueStyle }
                  adjustsFontSizeToFit={ true }
                  numberOfLines={ 1 }>
                  {getTotal()}
                </Text>
              </View>
              {selectedNetwork.txnType == 2 && (
                <View style={ styles.rowViewStyle }>
                  <Text
                    style={ styles.maxFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {localize('MAX_FEE:')}
                    <Text
                      adjustsFontSizeToFit={ true }
                      numberOfLines={ 1 }
                      style={ styles.feeStyle }>
                      {getMaxTotal()}
                    </Text>
                  </Text>
                </View>
              )}
            </View>
          </ScrollView>
        </View>
      </View>
      {!isLoading ? (
        <SwipeVerifyController
          style={ styles.swiperStyle }
          isSetReset={ !txnObject }
          onVerified={ () => slideToSendTokens() }>
          <Text
            style={ [
              COMMON_STYLE.textStyle(14, COLORS.LIGHT_OPACITY(0.3), 'BOLD'),
            ] }>
            {localize('SLICE_TO_SEND_TOKEN_UPPERCASE')}
          </Text>
        </SwipeVerifyController>
      ) : (
        <CachedImage source={ IMAGES.ROUNDED_LOADING } style={ styles.loadingStyle } />
      )}
      <ModalContainerView
        testID={ 'modal-gas-fee-manage' }
        isVisible={ isShowGasFeeView }
        onPressClose={ () => setIsShowGasFeeView(false) }>
        <GasFeeRangeView
          selectedRange={ selectedRange }
          gasRange={ gasRange }
          txnObject={ txnObject }
          network={ selectedNetwork }
          onPressSave={ updatedObject => {
            setIsShowGasFeeView(false);
            if (selectedNetwork.txnType != 0) {
              updateTxnObjectTypeTwo(
                updatedObject.gas,
                updatedObject.maxPrioFee,
                updatedObject.maxFeePerGas,
              );
              setSelectedRange(selectedRange);
            } else {
              updateTxnObjectTypeOne(updatedObject.gas, updatedObject.gasPrice);
            }
          } }
        />
      </ModalContainerView>

      {/* Error Message Modal */}
      <AlertActionModel
        alertTitle={ localize('ERROR') }
        isShowAlert={ isShowError }
        successBtnTitle={ localize('OK') }
        onPressSuccess={ () => {
          setIsShowError(false);
          navigation.navigate('ACCOUNT_DETAIL_SCREEN', {
            accountDetail: params.fromAccount,
          });
        } }>
        <Text
          style={ COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BASE', 'center') }>
          {getUserFriendlyErrorMessage(error)}
        </Text>
      </AlertActionModel>
    </SafeAreaWrapper>
  );
};

SendTokenConfirmationScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default SendTokenConfirmationScreen;
