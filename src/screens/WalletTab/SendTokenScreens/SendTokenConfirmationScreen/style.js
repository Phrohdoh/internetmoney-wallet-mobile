import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,

  selectedNetworkStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  networkNameStyle: {
    ...COMMON_STYLE.textStyle(13, COLORS.WHITE, 'BOLD', 'center'),
  },

  bgStyle: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
  },
  swiperStyle: {
    marginVertical: Responsive.getHeight(2),
    marginHorizontal: Responsive.getWidth(5),
  },
  confirmStyle: {
    ...COMMON_STYLE.textStyle(16, COLORS.WHITE, 'BOLD', 'center'),
    marginVertical: Responsive.getHeight(0.5),
  },
  detailContainerStyle: {
    left: '12%',
    right: '12%',
    top: '6%',
    bottom: '5%',
    position: 'absolute',
    alignSelf: 'center',
    paddingTop: Responsive.getHeight(1.5),
  },
  dottedLineStyle: {
    width: '100%',
    height: 1,
    marginVertical: Responsive.getHeight(0.5),
  },
  fromViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: Responsive.getHeight(1.5),
    borderBottomWidth: 1,
    borderColor: COLORS.GRAY27,
    flex: 1,
  },
  toViewStyle: {
    paddingVertical: Responsive.getHeight(1.5),
  },
  toAddressTextStyle: {
    ...COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD'),
  },
  tokenIconStyle: {
    ...COMMON_STYLE.imageStyle(5),
    marginLeft: Responsive.getWidth(3),
  },
  accountDetailViewStyle: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: COLORS.GRAY27,
    flex: 1,
  },
  lineViewStyle: {
    width: '10%',
    marginVertical: '8%',
  },
  verticalLineStyle: {
    width: '100%',
    flex: 1,
    resizeMode: 'contain',
  },
  dotContainStyle: {
    height: '100%',
    width: '100%',
    position: 'absolute',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dotViewStyle: {
    width: Responsive.getWidth(3),
    height: Responsive.getWidth(3),
    borderRadius: Responsive.getWidth(1.5),
    backgroundColor: COLORS.GRAY50,
  },
  amountTitleStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  borderedViewStyle: {
    borderBottomWidth: 1,
    borderColor: COLORS.GRAY27,
    paddingBottom: Responsive.getHeight(1.3),
  },
  amountContainerViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    flexDirection: 'row',
  },

  amountStyle: {
    ...COMMON_STYLE.textStyle(18, COLORS.GRAYDF, 'BOLD', 'center'),
    marginRight: Responsive.getWidth(2),
  },
  rowViewStyle: {
    flexDirection: 'row',
    flex: 1,
    marginTop: Responsive.getHeight(1),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  estimateStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BOLD'),
  },
  estimateFeeStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.YELLOWFB, 'BOLD', 'right'),
    flex: 1,
  },
  secondTextStyle: { ...COMMON_STYLE.textStyle(10, COLORS.GREEN6D) },
  maxFeeStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'right'),
    flex: 1,
  },
  feeStyle: { ...COMMON_STYLE.textStyle(10, COLORS.GRAYDF, 'BOLD') },
  totalValueStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BOLD', 'right'),
    flex: 1,
    marginLeft: Responsive.getWidth(1),
  },
  bottomBorderStyle: {
    borderBottomWidth: 1,
    borderColor: COLORS.GRAY27,
    paddingBottom: Responsive.getHeight(2),
    flex: 1,
  },
  loadingStyle: {
    ...COMMON_STYLE.imageStyle(10),
    alignSelf: 'center',
    marginVertical: Responsive.getHeight(2),
  },
  estimateFeeBtnStyle: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginLeft: Responsive.getWidth(2),
  },
});
