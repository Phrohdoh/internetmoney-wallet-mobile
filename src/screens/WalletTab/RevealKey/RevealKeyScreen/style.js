import {StyleSheet} from 'react-native';

import {COMMON_STYLE, COLORS} from '@themes';
import {Responsive} from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  privateKeyContainerStyle: {
    padding: Responsive.getWidth(5),
    borderWidth: 3,
    borderColor: COLORS.GRAY1F,
    borderRadius: Responsive.getWidth(5),
    backgroundColor: COLORS.GRAY15,
    marginTop: Responsive.getHeight(5),
  },
});
