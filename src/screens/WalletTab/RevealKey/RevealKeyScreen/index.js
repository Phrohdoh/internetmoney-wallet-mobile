import React, { useEffect, useRef, useState } from 'react';
import { View, Text, AppState } from 'react-native';
import PropTypes from 'prop-types';
import Toast from 'react-native-toast-message';

import Clipboard from '@react-native-clipboard/clipboard';

import { useSelector } from 'react-redux';

// import components
import {
  SafeAreaWrapper,
  KeyboardAvoidScrollView,
  RoundedButton,
  AlertActionModel,
} from '@components';

// import constants

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

import * as Web3Layer from '@web3';
import RNScreenshotPrevent, {
  addListener,
} from 'react-native-screenshot-prevent';
import { BlurView } from '@react-native-community/blur';

// local constants
const PRIVATE_KEY_PLACEHOLDER = '********************************';
const ROUNDED_BUTTON_MARGIN = 12;

const RevealKeyScreen = props => {
  const params = props.route.params;
  const wallet = useSelector(state => state.wallet);

  const [privateKey, setPrivateKey] = useState('');
  const [isHidden, setIsHidden] = useState(true);

  const [showScreenShotAlert, setShowScreenShotAlert] = useState(false);
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  useEffect(() => {
    getAccountPrivateKey();

    if (Platform.OS === 'ios') {
      RNScreenshotPrevent && RNScreenshotPrevent.enabled(true);
    }

    const screenshotSubscription = addListener(() => {
      setShowScreenShotAlert(true);
    });
    const appStateSubscription = AppState.addEventListener(
      'change',
      nextAppState => {
        appState.current = nextAppState;
        setAppStateVisible(appState.current);
      },
    );
    return () => {
      RNScreenshotPrevent && RNScreenshotPrevent.enabled(false);
      screenshotSubscription.remove();
      appStateSubscription.remove();
    };
  }, []);

  const getAccountPrivateKey = async () => {
    const privateKeyObj = await Web3Layer.retrievePrivateKeyOf(
      params.accountDetail.publicAddress,
      wallet.walletDetail.walletObject,
      params.password,
    );

    /* istanbul ignore else */
    if (privateKeyObj.success) {
      setPrivateKey(privateKeyObj.privateKey);
    }
  };

  const renderScreenShotTakenMessage = () => (
    <AlertActionModel
      isShowAlert={showScreenShotAlert}
      alertTitle={localize('SCREENSHOT_PRIVATE_KEY_WARNING')}
      successBtnTitle={localize('UNDERSTAND')}
      bigSuccess={true}
      onPressSuccess={() => {
        setShowScreenShotAlert(false);
      }}
    />
  );

  return (
    <SafeAreaWrapper>
      <KeyboardAvoidScrollView>
        <Text style={COMMON_STYLE.infoTitleStyle()}>
          {localize('ACCOUNT_PRIVATE_KEY_UPPERCASE', [
            params.accountDetail.name,
          ])}
        </Text>
        <Text style={COMMON_STYLE.scamAlertMsgStyle}>
          {localize('PRIVATE_KEY_SCAM_MSG_UPPERCASE')}
        </Text>
        <View style={styles.privateKeyContainerStyle}>
          <Text
            style={COMMON_STYLE.textStyle(
              14,
              COLORS.LIGHT_OPACITY(0.3),
              'BOLD',
            )}>
            {!isHidden ? privateKey : PRIVATE_KEY_PLACEHOLDER}
          </Text>
        </View>
      </KeyboardAvoidScrollView>
      {renderScreenShotTakenMessage()}
      <RoundedButton
        testID="reveal-key-copy-button"
        style={COMMON_STYLE.roundedBtnMargin(ROUNDED_BUTTON_MARGIN)}
        title={localize('COPY_CLIPBOARD_UPPERCASE')}
        onPress={() => {
          Clipboard.setString(privateKey);
          Toast.show({
            text1: localize('COPIED_SUCCESS'),
            text2: localize('PRIVATE_KEY_COPIED_SUCCESS'),
          });
        }}
      />
      <RoundedButton
        isBordered
        testID="reveal-key-hide-button"
        style={COMMON_STYLE.roundedBtnMargin(ROUNDED_BUTTON_MARGIN)}
        title={localize(isHidden ? 'SHOW_KEY_UPPERCASE' : 'HIDE_KEY_UPPERCASE')}
        onPress={() => setIsHidden(!isHidden)}
      />
      <RoundedButton
        isBordered
        testID="reveal-key-hide-button"
        style={COMMON_STYLE.roundedBtnMargin(ROUNDED_BUTTON_MARGIN)}
        title={localize('GO_BACK_BUTTON_UPPERCASE')}
        onPress={() =>
          props.navigation.navigate('ACCOUNT_DETAIL_SCREEN', {
            accountDetail: params.accountDetail,
          })
        }
      />
      {
        (appStateVisible !== 'active' && Platform.OS === 'ios') && (
          <BlurView
            style={ styles.absolute }
            blurType="dark"
            blurAmount={ 10 }
          />
        )
      }
    </SafeAreaWrapper>
  );
};

RevealKeyScreen.propTypes = { navigation: PropTypes.any, route: PropTypes.any };
export default RevealKeyScreen;
