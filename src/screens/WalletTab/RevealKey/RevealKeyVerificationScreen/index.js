import React, { useState } from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';
import { GlobalAction } from '@redux';

import * as Web3Layer from '@web3';

// import components
import {
  SafeAreaWrapper,
  KeyboardAvoidScrollView,
  RoundedButton,
  PasscodeComponent,
  TitleTextInput,
} from '@components';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import { Validation } from '@utils';

const RevealKeyVerificationScreen = props => {
  const dispatch = useDispatch();

  const { navigation } = props;
  const params = props.route.params;
  const wallet = useSelector(state => state.wallet);
  const { isPasscode } = wallet;
  const [passcode, setPasscode] = useState('');

  const [password, onChangePassword] = useState({
    id: 'password',
    value: '',
    title: 'PASSWORD',
    placeholder: 'ENTER_PASSWORD',
    extraProps: {
      isPassword: true,
    },
  });

  // Action Methods
  const handlePasscode = item => {
    const tempCode = passcode.split('');
    if (item.value === 'remove') {
      if (tempCode.length) {
        tempCode.pop();
      }
    } else {
      if (tempCode.length < 6) {
        tempCode.push(item.value);
      }
    }
    const strPasscode = tempCode.join('');
    setPasscode(strPasscode);
  };

  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.value = text;
    setFunction(tempDict);
  };

  // check all validation
  const checkPasswordValidation = () => {
    const isValidPassword = Validation.isValidPassword(password.value);

    const matchPassword = password.value === wallet.password;
    if (isValidPassword && matchPassword) {
      return true;
    } else {
      let msg = '';

      /* istanbul ignore else */
      if (Validation.isEmpty(password.value)) {
        msg = 'ENTER_PASS_MSG';
      } else if (!isValidPassword) {
        msg = 'ENTER_VALID_PASS_MSG';
      } else if (!matchPassword) {
        msg = 'PASS_NOT_MATCH_MSG';
      }

      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize(msg),
          localize('TRY_AGAIN'),
        ),
      );
      return false;
    }
  };

  const checkPasscodeValidation = () => {
    if (passcode.length < 6) {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('ENTER_VALID_PASSCODE_MSG'),
          localize('TRY_AGAIN'),
        ),
      );
      return false;
    } else {
      return true;
    }
  };

  const checkPassword = async (walletObj, password) => {
    const checkPass = await Web3Layer.checkPasswordSecurely(
      password,
      walletObj.passwordHash,
    );
    return checkPass;
  };

  const _onPressContinue = () => {
    if (isPasscode ? checkPasscodeValidation() : checkPasswordValidation()) {
      dispatch(GlobalAction.isShowLoading(true));

      setTimeout(() => {
        callWeb3();
      }, 300);
    }
  };

  const callWeb3 = async () => {
    const checkPass = await checkPassword(
      wallet.walletDetail,
      isPasscode ? passcode : password.value,
    );

    if (checkPass) {
      dispatch(GlobalAction.isShowLoading(false));
      navigation.navigate('REVEAL_KEY_SCREEN', {
        password: isPasscode ? passcode : password.value,
        accountDetail: params.accountDetail,
      });
    } else {
      dispatch(GlobalAction.isShowLoading(false));
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize(
            isPasscode
              ? 'LOGIN_VERIFY_PASSCODE_NOT_MATCH_MSG'
              : 'LOGIN_VERIFY_PASSWORD_NOT_MATCH_MSG',
          ),
          localize('TRY_AGAIN'),
        ),
      );
    }
  };

  // Custom Render Components
  const renderPasswordVerification = () => (
    <React.Fragment>
      <KeyboardAvoidScrollView>
        <Text style={ COMMON_STYLE.infoTitleStyle() }>
          {localize('PASSWORD_VERIFICATION_UPPERCASE')}
        </Text>
        <Text style={COMMON_STYLE.scamAlertMsgStyle}>
          {localize('PRIVATE_KEY_SCAM_MSG_UPPERCASE')}
        </Text>
        <Text style={ COMMON_STYLE.infoDescStyle(COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('PRIVATE_KEY_PASSWORD_VERIFICATION_MSG')}
        </Text>
        <TitleTextInput
          testID={ `password-verification-screen-${password.id}` }
          inputContainerStyle={ styles.inputContainerStyle }
          value={ password.value }
          placeholder={ localize(password.placeholder) }
          onChangeText={ text => changeData(text, password, onChangePassword) }
          { ...password.extraProps }
        />
      </KeyboardAvoidScrollView>
    </React.Fragment>
  );

  const renderPasscodeVerification = () => (
    <React.Fragment>
      <Text style={ COMMON_STYLE.infoTitleStyle() }>
        {localize('PASSCODE_VERIFICATION_UPPERCASE')}
      </Text>
      <Text style={ COMMON_STYLE.infoDescStyle(COLORS.LIGHT_OPACITY(0.6)) }>
        {localize('PRIVATE_KEY_PASSCODE_VERIFICATION_MSG')}
      </Text>
      <PasscodeComponent
        title={ localize('PASSCODE') }
        passcode={ passcode }
        onPressKey={ item => handlePasscode(item) }
      />
    </React.Fragment>
  );

  return (
    <SafeAreaWrapper>
      {isPasscode ? renderPasscodeVerification() : renderPasswordVerification()}
      <RoundedButton
        testID="pass-verification-continue-button"
        style={ COMMON_STYLE.roundedBtnMargin() }
        title={ localize('REVEAL_PRIVATE_KEY_UPPERCASE') }
        onPress={ () => _onPressContinue() }
      />
    </SafeAreaWrapper>
  );
};

RevealKeyVerificationScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default RevealKeyVerificationScreen;
