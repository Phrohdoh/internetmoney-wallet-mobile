import {StyleSheet} from 'react-native';

import {COMMON_STYLE} from '@themes';
import {Responsive} from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  segmentContainerStyle: {marginVertical: Responsive.getHeight(2)},
  segmentTabsStyle: {
    flex: 1,
  },
  loadingStyle: {
    ...COMMON_STYLE.imageStyle(12),
    flex: 1, 
    justifyContent: 'center',
    alignSelf: 'center',
  },
});
