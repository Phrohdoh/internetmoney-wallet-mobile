import React, { useState, useEffect } from 'react';

import { View } from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';

import { GlobalAction, TransactionsAction, NetworksAction, getTotalValue, getTransactionsList } from '@redux';

import moment from 'moment';
// import components
import {
  SafeAreaWrapper,
  TabHeaderView,
  SegmentController,
  AccountListComponent,
  TransactionListComponent,
  CachedImage,
} from '@components';

// import constants
import { ASYNC_KEYS } from '@constants';

// import languages
import { localize } from '@languages';

// import storage functions
import { StorageOperation } from '@storage';

// import style
import { styles } from './style';

// import themes
import { COMMON_STYLE, bottomTabPadding, IMAGES } from '@themes';

import { parseObject, log } from '@utils';

import * as Web3Layer from '@web3';
import { getNetwork } from '../../../redux';

const WalletDashboardScreen = props => {
  const { navigation } = props;

  const segmentList = [
    { title: localize('ACCOUNTS_UPPERCASE') },
    { title: localize('TRANSACTION_UPPERCASE') },
  ];

  const dispatch = useDispatch();

  const accounts = useSelector(state => state.accounts);
  const transactionsList = useSelector(getTransactionsList);
  const networks = useSelector(state => state.networks);
  const selectedNetwork = useSelector(getNetwork);
  const totalValue = useSelector(getTotalValue);

  const [currentIndex, setCurrentIndex] = useState(0);

  const displayAccountsData = totalValue !== undefined;

  const refreshAccountsData = () => {
    dispatch(NetworksAction.refreshBalances());
  };

  const checkReceiveCallEligible = async () => {
    const receiveSupportNetwork = ['1', '56', '97', '137', '250', '43114'];

    if (receiveSupportNetwork.includes(selectedNetwork.chainId.toString())) {
      {
        const { error, data } = await StorageOperation.getMultiData([
          ASYNC_KEYS.LAST_RECEIVE_CALL_TIME,
          ASYNC_KEYS.LAST_RECEIVE_CALL_COUNT,
          ASYNC_KEYS.BLOCK_NO,
        ]);

        if (!error) {
          const parseTime = parseObject(data[0][1]);
          const parseCount = parseObject(data[1][1]);

          return {
            isEligible: true,
            date: moment().format('YYYY-MM-DD'),
            count: parseCount.data,
          };
        }
      }
      return {
        isEligible: true,
        date: moment().format('YYYY-MM-DD'),
        count: 0,
      };
    } else {
      return { isEligible: false };
    }
  };

  const filterReceiveTransactionObject = async () => {
    try {
      const checkEligbility = await checkReceiveCallEligible();
      log('isEligible >>>', checkEligbility);
      if (checkEligbility.isEligible) {
        dispatch(GlobalAction.isShowLoading(true));
        const { error, data } = await StorageOperation.getData(
          ASYNC_KEYS.BLOCK_NO,
        );

        let tempBlockNo = {};

        if (!error) {
          const parseBlock = parseObject(data);
          if (parseBlock.success) {
            tempBlockNo = { ...parseBlock.data };
          }
        }

        const accountListObj = accounts.accountList.map(
          item => item.publicAddress,
        );
        const tokenArray = [
          ...new Set(
            networks.tokens
              .map(value => value['tokens'].map(value => value['address']))
              .flat(1),
          ),
        ];
      
        let currentBlockNo = 0;
        if (tempBlockNo[selectedNetwork.chainId]) {
          currentBlockNo = tempBlockNo[selectedNetwork.chainId];
        }

        const receiveObj = [
          selectedNetwork.chainId,
          currentBlockNo,
          accountListObj,
          tokenArray,
        ];
        log('receiveObj >>>', receiveObj);
        const receiveTransactions = await Web3Layer.getUsersReceiveTransactions(
          ...receiveObj,
        );

        log('receiveTransactions >>>', receiveTransactions);

        if (receiveTransactions.aReceiveTransactions) {
          saveReceiveTransactions(receiveTransactions.aReceiveTransactions);
        }

        dispatch(GlobalAction.isShowLoading(false));
        if (receiveTransactions.success) {
          tempBlockNo[selectedNetwork.chainId] =
            receiveTransactions.nLatestBlockNumber;
          await StorageOperation.setMultiData([
            [
              ASYNC_KEYS.LAST_RECEIVE_CALL_TIME,
              JSON.stringify(checkEligbility.date),
            ],
            [
              ASYNC_KEYS.LAST_RECEIVE_CALL_COUNT,
              JSON.stringify(checkEligbility.count + 1),
            ],
            [ASYNC_KEYS.BLOCK_NO, JSON.stringify(tempBlockNo)],
          ]);
        }
      }
    } catch (e) {
      dispatch(GlobalAction.isShowLoading(false));
      log('get receive transaction error >>>', e);
    }
  };

  const saveReceiveTransactions = async receiveTrans => {
    try {
      const tempTransList = [...transactionsList, ...receiveTrans];

      const { data } = await StorageOperation.getData(ASYNC_KEYS.TRANSACTIONS);
      let parseTransactions;

      const parseObj = parseObject(data);
      /* istanbul ignore else */
      if (parseObj.success) {
        parseTransactions = parseObj.data;
      }
      let tempTransactions = [];
      /* istanbul ignore else */
      if (parseTransactions) {
        tempTransactions = parseTransactions;
      }
      const transObj = tempTransactions.find(
        chain => chain.chainId === selectedNetwork.chainId,
      );
      if (transObj) {
        const transIndex = tempTransactions.indexOf(transObj);
        transObj.transactions = tempTransList;
        tempTransactions[transIndex] = transObj;
      } else {
        tempTransactions.push({
          chainId: selectedNetwork.chainId,
          transactions: tempTransList,
        });
      }

      dispatch(TransactionsAction.saveTransactions(tempTransList));
      await StorageOperation.setMultiData([
        [ASYNC_KEYS.TRANSACTIONS, JSON.stringify(tempTransactions)],
      ]);
      refreshAccountsData()
    } catch (e) {
      log('e >>>', e);
    }
  };

  return (
    <SafeAreaWrapper containerStyle={[COMMON_STYLE.tabContainerStyle]}>
      <TabHeaderView testID={'wallet-dashboard'} onPressRefresh={refreshAccountsData} {...props} />
      <View style={COMMON_STYLE.borderedContainerViewStyle(bottomTabPadding)}>
        <SegmentController
          isLineSegment
          style={ styles.segmentContainerStyle }
          segments={ segmentList }
          currentIndex={ currentIndex }
          onChangeIndex={ index => {
            setCurrentIndex(index);
            if (index === 1) {
              filterReceiveTransactionObject();
            }
          } }
        />
        <View style={ styles.segmentTabsStyle }>
          { displayAccountsData && currentIndex === 0 &&
            <AccountListComponent
              accountsList={accounts.accountList}
              navigation={navigation}
            />
          } 
          { displayAccountsData && currentIndex !== 0 &&
            <TransactionListComponent
              transactionsList={ transactionsList }
              navigation={ navigation }
            />
          }
          { !displayAccountsData &&
            // eslint-disable-next-line max-len
            <CachedImage source={IMAGES.ROUNDED_LOADING} style={styles.loadingStyle} />
          }
        </View>
      </View>
    </SafeAreaWrapper>
  );
};

WalletDashboardScreen.propTypes = { navigation: PropTypes.any };
export default WalletDashboardScreen;
