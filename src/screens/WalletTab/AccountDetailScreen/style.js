import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,

  headerViewStyle: {
    width: '100%',
    backgroundColor: COLORS.BLACK,
  },
  headerContainerStyle: {
    height: Responsive.getHeight(6),
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
  },
  headerTitleViewStyle: {
    flex: 1,
    alignItems: 'center',
    marginHorizontal: Responsive.getWidth(2),
    position: 'absolute',
    left: 0,
    right: 0,
  },
  editBtnStyle: {
    position: 'absolute',
    left: 0,
  },
  rightEditViewStyle: {
    flexDirection: 'row',
    position: 'absolute',
    right: 0,
  },
  rightEditBtnStyle: {
    marginLeft: 10,
  },
  selectedNetworkStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
  },
  networkNameStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BOLD', 'center'),
  },
  balanceViewStyle: { flex: 1, marginLeft: Responsive.getWidth(1) },
  accountDetailView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  accountNameStyle: { ...COMMON_STYLE.textStyle(22, COLORS.YELLOWFB, 'BOLD') },
  addressStyle: { ...COMMON_STYLE.textStyle(14, COLORS.WHITE) },
  valueStyle: { ...COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BASE', 'right') },
  balanceStyle: {
    ...COMMON_STYLE.textStyle(22, COLORS.YELLOWFB, 'BOLD', 'right'),
    flex: 1,
  },
  chartContainerStyle: {
    backgroundColor: COLORS.GRAY15,
    borderColor: COLORS.GRAY1F,
    borderWidth: 2,
    borderRadius: Responsive.getWidth(5),
    overflow: 'hidden',
    alignItems: 'center',
    marginTop: Responsive.getHeight(3),
  },
  hideViewStyle: {
    height: Responsive.getHeight(5),
    width: '100%',
    paddingHorizontal: Responsive.getWidth(5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: COLORS.GRAY1F,
  },
  chartStyle: {
    width: '90%',
    height: Responsive.getHeight(28),
    resizeMode: 'contain',
  },
  filterCellStyle: isSelected => {
    return {
      height: Responsive.getHeight(3),
      width: Responsive.getWidth(12),
      borderRadius: Responsive.getHeight(1.5),
      borderColor: COLORS.GRAY41,
      borderWidth: 1,
      backgroundColor: isSelected ? COLORS.YELLOWFB : COLORS.BLACK,
      justifyContent: 'center',
      alignItems: 'center',
    };
  },
  filterTitleStyle: isSelected => {
    return {
      ...COMMON_STYLE.textStyle(
        7,
        isSelected ? COLORS.BLACK : COLORS.YELLOWFB,
        'BOLD',
        'center',
      ),
    };
  },
  transectionContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginVertical: Responsive.getHeight(3),
  },
  transactionBtnStyle: {
    width: Responsive.getWidth(16),
    height: Responsive.getWidth(16),
    borderRadius: Responsive.getWidth(8),
    backgroundColor: COLORS.GRAY1E,
    borderColor: COLORS.GRAY41,
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: Responsive.getHeight(1),
  },
  weekDayFlatlistStyle: { marginBottom: Responsive.getHeight(2) },
  flatlistStyle: {
    borderRadius: Responsive.getWidth(5),
    borderColor: COLORS.GRAY41,
    borderWidth: 2,
    marginBottom: Responsive.getHeight(2),
  },
  sepratorStyle: { height: 1, backgroundColor: COLORS.LIGHT_OPACITY(0.1) },

  addTokenStyle: {
    height: Responsive.getHeight(4),
    width: Responsive.getWidth(32),
    marginBottom: Responsive.getHeight(3),
  },

  copyAddressStyle: {
    flexDirection: 'row',
    alignItems: 'center'
  },

  clipboardIconStyle: {
    ...COMMON_STYLE.imageStyle(4, COLORS.YELLOWFB),
    marginBottom: 4,
    marginRight: 2,
  },

  headerBtnStyle: { marginLeft: Responsive.getWidth(3) },
  editBtnContainreStyle: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
  },

  loadingStyle: {
    ...COMMON_STYLE.imageStyle(10),
    alignSelf: 'center',
    marginVertical: Responsive.getHeight(2),
  },

  refreshValueStyle: {
    flexDirection: 'row',
    alignItems: 'center'
  },

  refreshIconStyle: {
    ...COMMON_STYLE.imageStyle(6),
    marginRight: 5,
  },
});
