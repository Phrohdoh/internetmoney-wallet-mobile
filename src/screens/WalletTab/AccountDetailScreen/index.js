import React, { useState } from 'react';

import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';
import {
  NetworksAction,
  WalletAction,
  GlobalAction,
  getTokensByAddress,
  getTotalValueByAddress,
} from '@redux';

import * as Web3Layer from '@web3';

// import components
import {
  SafeAreaWrapper,
  RoundedButton,
  TokenListCell,
  ModalContainerView,
  AddTokenView,
  AlertActionModel,
  NetworkSelectionModalView,
  CachedImage,
} from '@components';

import Clipboard from '@react-native-clipboard/clipboard';
import Toast from 'react-native-toast-message';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { IMAGES, COLORS, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import utils
import { displayAddress, parseObject } from '@utils';
import { store } from '../../../redux/store';
import AddTokenModal from '../../../components/modalComponents/AddTokenModal';
import { getNetwork } from '../../../redux';

const AccountDetailScreen = props => {
  const { navigation, route } = props;
  const params = route.params;

  const dispatch = useDispatch();

  const networks = useSelector(state => state.networks);
  const filterList = localize('DAY_FILTER_LIST');

  const [selectedFilter, setSelectedFilter] = useState(filterList[0]);
  const selectedNetwork = useSelector(getNetwork);
  const [isShowAddToken, setIsShowAddToken] = useState(false);
  const [isHide, setIsHide] = useState(false);
  const accountDetail = params.accountDetail;
  const [isShowDeleteAlert, setIsShowDeleteAlert] = useState(false);
  const [selectedToken, setSelectedToken] = useState(undefined);
  const [isRemoveEnable, setIsRemoveEnable] = useState(false);
  const [isShowNetworkList, setIsShowNetworkList] = useState(false);
  const tokens = useSelector(getTokensByAddress)[accountDetail.publicAddress.toLowerCase()];
  const totalValue = useSelector(getTotalValueByAddress)[accountDetail.publicAddress.toLowerCase()];
  
  const displayAccountBalance = totalValue !== undefined;

  const refreshAccountBalance = () => {
    dispatch(NetworksAction.refreshBalances());
  }

  const swapEnabled = networks?.wdSupportObject?.success
    && networks?.wdSupportObject?.swapRouterV3;

  const onCopy = () => {
    Clipboard.setString(params.accountDetail.publicAddress);
    Toast.show({
      text1: localize('COPIED_SUCCESS'),
      text2: localize('ADDRESS_COPIED_SUCCESS'),
    });
  };

  // Remove Token
  const removeToken = async () => {
    if (selectedToken.isDefault) {
      dispatch(NetworksAction.removeDefaultToken({
        accountAddress: accountDetail.publicAddress,
        address: selectedToken.address,
      }));
      await StorageOperation.setData(
        ASYNC_KEYS.REMOVED_DEFAULT_TOKENS,
        JSON.stringify(store.getState().networks.removedDefaultTokens),
      );
    } else {
      // remove non-default token
      let tempTokens = [];
      if (networks.tokens) {
        tempTokens = [...networks.tokens];
      }

      const tokenObj = networks.tokens.find(
        token =>
          token.id.toLowerCase() === accountDetail.publicAddress.toLowerCase(),
      );

      if (tokenObj) {
        const tokenIndex = tempTokens.indexOf(tokenObj);
        const tokenList = [...tokenObj.tokens];
        const idx = tokenList.findIndex(
          x => x.address.toLowerCase() === selectedToken.address.toLowerCase(),
        );
        tokenList.splice(idx, 1);
        const tempTokenObj = { ...tokenObj };
        tempTokenObj.tokens = tokenList;
        tempTokens[tokenIndex] = tempTokenObj;
      }

      const { data } = await StorageOperation.getData(ASYNC_KEYS.CHAIN_TOKENS);

      const parseObj = parseObject(data);
      let tempChainTokens = [];
      if (parseObj.success) {
        tempChainTokens = parseObj.data;
      }
      const chainObj = tempChainTokens.find(
        chain => chain.chainId === selectedNetwork.chainId,
      );

      if (chainObj) {
        const chainIndex = tempChainTokens.indexOf(chainObj);
        chainObj.accountTokens = tempTokens;
        tempChainTokens[chainIndex] = chainObj;
      } else {
        tempChainTokens.push({
          chainId: selectedNetwork.chainId,
          accountTokens: tempTokens,
        });
      }

      dispatch(NetworksAction.saveTokens(tempTokens));
      await StorageOperation.setMultiData([
        [ASYNC_KEYS.CHAIN_TOKENS, JSON.stringify(tempChainTokens)],
      ]);
    }
    setIsShowDeleteAlert(false);
    setSelectedToken(undefined);
  };

  const changeNetwork = async choosedNetwork => {
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData());
    await StorageOperation.setData(
      ASYNC_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          localize('NETWORK_SWITCH_TITLE'),
          localize('NETWORK_CHANGE_SUCCESS_MSG') + choosedNetwork.networkName,
        ),
      );
    }, 200);
  };

  // render custom component
  function renderHeaderTitleView () {
    return (
      <View style={ styles.headerTitleViewStyle }>
        <TouchableOpacity onPress={ () => setIsShowNetworkList(true) }>
          <Text style={ styles.selectedNetworkStyle }>
            {localize('CURRENT_NETWORK')}
          </Text>
          <Text style={ styles.networkNameStyle }>
            {selectedNetwork.networkName}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  function renderHeaderIcon (image, onPress) {
    return (
      <TouchableOpacity
        testID={ 'account-detail-navigation-edit-button' }
        style={ styles.editBtnStyle }
        onPress={ onPress }>
        <CachedImage source={ image } style={ COMMON_STYLE.imageStyle(6) } />
      </TouchableOpacity>
    );
  }

  function renderRightHeaderIcon (image, onPress) {
    return (
      <TouchableOpacity
        testID={ 'account-detail-navigation-edit-button' }
        style={ styles.rightEditBtnStyle }
        onPress={ onPress }>
        <CachedImage source={ image } style={ COMMON_STYLE.imageStyle(6) } />
      </TouchableOpacity>
    );
  }

  const renderFilterList = (item, index) => {
    return (
      <TouchableOpacity
        key={ item + index + '-filterList' }
        testID={ `account-detail-filter-${index}-button` }
        style={ styles.filterCellStyle(selectedFilter === item) }
        onPress={ () => setSelectedFilter(item) }>
        <Text style={ styles.filterTitleStyle(selectedFilter === item) }>
          {item}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderTransactionBtn = (image, title, onPress) => {
    return (
      <TouchableOpacity
        testID={ `account-detail-${title}-button` }
        onPress={ onPress }>
        <View style={ styles.transactionBtnStyle }>
          <CachedImage source={ image } style={ COMMON_STYLE.imageStyle(8) } />
        </View>
        <Text
          style={ COMMON_STYLE.textStyle(11, COLORS.WHITE, 'BOLD', 'center') }>
          {title}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderHeaderComponent = () => {
    return (
      <View style={ styles.headerViewStyle }>
        <SafeAreaView>
          <View style={ styles.headerContainerStyle }>
            {renderHeaderTitleView()}
            {renderHeaderIcon(IMAGES.BACK_ARROW, () => {
              navigation.navigate('DASHBOARD_TAB_NAVIGATOR', {
                screen: 'WALLET_DASHBOARD_TAB',
              });
            })}
            <View style={ styles.rightEditViewStyle }>
              {renderRightHeaderIcon(IMAGES.SETTING_BORDERED_ICON, () =>
                navigation.navigate('RENAME_ACCOUNT_SCREEN', {
                  accountDetail: accountDetail,
                }),
              )}
              {renderRightHeaderIcon(IMAGES.SUPPORT_ICON, () => {
                navigation.navigate('SUPPORT_SCREEN', {
                  supportType: 'contactus',
                });
              })}
            </View>
          </View>
        </SafeAreaView>
      </View>
    );
  };

  const renderChartView = () => {
    return (
      <View style={ styles.chartContainerStyle }>
        <TouchableOpacity
          style={ styles.hideViewStyle }
          onPress={ () => setIsHide(!isHide) }>
          <Text style={ COMMON_STYLE.textStyle(12, COLORS.YELLOWFB, 'BOLD') }>
            {localize(isHide ? 'VIEW_CHART' : 'HIDE_CHART')}
          </Text>
          <CachedImage
            source={ isHide ? IMAGES.ADD_ICON : IMAGES.MINUS_ICON }
            style={ COMMON_STYLE.imageStyle(4, COLORS.YELLOWFB) }
          />
        </TouchableOpacity>
        {!isHide && (
          <React.Fragment>
            <CachedImage source={ IMAGES.CHART_IMG } style={ styles.chartStyle } />

            <FlatList
              style={ styles.weekDayFlatlistStyle }
              contentContainerStyle={ {
                justifyContent: 'space-around',
                flexGrow: 1,
              } }
              horizontal
              data={ filterList }
              renderItem={ ({ item, index }) => renderFilterList(item, index) }
              keyExtractor={ (item, index) => index + item }
            />
          </React.Fragment>
        )}
      </View>
    );
  };
  return (
    <SafeAreaWrapper>
      {renderHeaderComponent()}

      <View style={ styles.accountDetailView }>
        <View style={ { flex: 1 } }>
          <Text numberOfLines={ 1 } style={ styles.accountNameStyle }>
            {accountDetail.name}
          </Text>
          <TouchableOpacity
            style={ styles.copyAddressStyle }
            onPress={ onCopy }>
            <CachedImage
              style={ styles.clipboardIconStyle }
              source={ IMAGES.COPY_ICON }
            />
            <Text style={ styles.addressStyle }>
              {displayAddress(accountDetail.publicAddress)}
            </Text>
          </TouchableOpacity>
        </View>

      <View>
        { displayAccountBalance &&
            <View style={styles.balanceViewStyle}>
                <TouchableOpacity
                style={ styles.refreshValueStyle }
                  onPress={() => {
                      refreshAccountBalance()
                    }
                  }>
                    <CachedImage
                      style={ styles.refreshIconStyle }
                      source={ IMAGES.REFRESH_ICON }
                    />
                    <View>
                    <Text style={styles.valueStyle}>{'Value'}</Text>
                  </View>
              </TouchableOpacity>

              <Text numberOfLines={ 1 } adjustsFontSizeToFit={ true } style={ styles.balanceStyle }>
                ${totalValue.toString(2)}
              </Text>
          </View>
          }   
          {!displayAccountBalance && 
            <CachedImage source={ IMAGES.ROUNDED_LOADING } style={ styles.loadingStyle } />
          }
      </View>
   </View>

      <View style={ styles.transectionContainerStyle }>
        {renderTransactionBtn(
          IMAGES.SEND_ICON,
          localize('SEND_UPPERCASE'),
          () => {
            navigation.navigate('SEND_TOKEN_SELECT_ACCOUNT_SCREEN', {
              accountDetail: accountDetail,
            });
          },
        )}
        {renderTransactionBtn(
          IMAGES.RECEIVE_ICON,
          localize('RECEIVE_UPPERCASE'),
          () =>
            // setDisplayAccountBalance(false)
            navigation.navigate('RECEIVE_TOKEN_SCREEN', {
              accountDetail: accountDetail,
            }),
        )}
        {swapEnabled && renderTransactionBtn(
          IMAGES.SWAP_ICON,
          localize('SWAP_UPPERCASE'),
          () => {
            navigation.navigate('SWAP_TOKEN_SELECT_TOKEN_SCREEN', {
              accountDetail: accountDetail,
            });
          },
        )}
      </View>
      <View style={ styles.editBtnContainreStyle }>
        <RoundedButton
          testID="add-token-button"
          style={ styles.addTokenStyle }
          titleStyle={ COMMON_STYLE.textStyle(11, COLORS.BLACK, 'BOLD') }
          title={ localize('ADD_TOKEN_UPPERCASE') }
          onPress={ () => {
            setIsShowAddToken(true);
            setIsRemoveEnable(false);
           } }
        />
        <RoundedButton
          testID="remove-token-button"
          style={ styles.addTokenStyle }
          titleStyle={ COMMON_STYLE.textStyle(11, COLORS.BLACK, 'BOLD') }
          title={
            isRemoveEnable
              ? localize('FINISHED_UPPERCASE')
              : localize('REMOVE_TOKEN_UPPERCASE')
          }
          onPress={ () => setIsRemoveEnable(!isRemoveEnable) }
        />
      </View>
      <FlatList
        style={ styles.flatlistStyle }
        data={ tokens }
        renderItem={ ({ item, _index }) => (
          <TokenListCell
            item={ item }
            networkObj={ selectedNetwork }
            isRemoveEnable={ isRemoveEnable }
            onPressDelete={(item, index) => {
              setIsShowDeleteAlert(true);
              setSelectedToken(item);
            } }
          />
        ) }
        ItemSeparatorComponent={ () => <View style={ styles.sepratorStyle } /> }
      />

      <AlertActionModel
        alertTitle={localize('REMOVE_TOKEN')}
        isShowAlert={isShowDeleteAlert}
        successBtnTitle={localize('YES')}
        onPressSuccess={async () => {
          removeToken();
        }}
        onPressCancel={() => {
          setIsShowDeleteAlert(false);
          setSelectedToken(undefined);
        }}>
        <Text
          style={COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BOLD', 'center')}>
          {localize(
            'REMOVE_TOKEN_MSG',
            selectedToken && [selectedToken.symbol],
          )}
        </Text>
      </AlertActionModel>

      <AddTokenModal
        testID={ 'modal-account-detail' }
        isVisible={ isShowAddToken }
        onPressClose={ () => setIsShowAddToken(false) }>
        <AddTokenView
          account={ accountDetail }
          setIsShowAddToken={() => setIsShowAddToken(false) }
          onImportTokenSuccess={ () => setIsShowAddToken(false) }
        />
      </AddTokenModal>
      <NetworkSelectionModalView
        isVisible={ isShowNetworkList }
        onPressClose={ () => setIsShowNetworkList(false) }
        onSelectNetwork={ item => {
          setIsShowNetworkList(false);
          if (item.chainId !== selectedNetwork.chainId) {
            changeNetwork(item);
          }
        } }
      />
    </SafeAreaWrapper>
  );
};

AccountDetailScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};

export default AccountDetailScreen;
