import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  transactionTypeViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: Responsive.getWidth(5),
  },
  transactionDateViewStyle: {
    marginLeft: Responsive.getWidth(3),
    flex: 1,
  },
  typeStyle: {
    ...COMMON_STYLE.textStyle(18, COLORS.YELLOWFB, 'BOLD'),
  },
  dateStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)),
    marginBottom: Responsive.getHeight(1),
  },
  detailConteinarStyle: {
    ...COMMON_STYLE.borderedContainerViewStyle(),
    paddingTop: Responsive.getHeight(1),
  },
  loadingConteinarStyle: {
    ...COMMON_STYLE.borderedContainerViewStyle(),
    paddingTop: Responsive.getHeight(1),
    justifyContent: 'center',
    alignItems: 'center',
  },
  dottedLineStyle: {
    width: '100%',
    height: 1,
    marginVertical: Responsive.getHeight(0.5),
  },
  transactionStatusStyle: (color) => {
    return {
      ...COMMON_STYLE.textStyle(
        16,
        color,
        'BOLD',
        'center',
      ),
      marginVertical: Responsive.getHeight(0.5),
    };
  },
  expediteNotice: {
    ...COMMON_STYLE.textStyle(
      12,
      COLORS.YELLOWFB,
      'BOLD',
      'center',
    ),
    marginVertical: Responsive.getHeight(0.5),
  },
  accountDetailViewStyle: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: COLORS.GRAY27,
    flex: 1,
  },
  lineViewStyle: {
    width: '10%',
    marginVertical: '8%',
  },
  verticalLineStyle: {
    width: '100%',
    flex: 1,
    resizeMode: 'contain',
  },
  dotContainStyle: {
    height: '100%',
    width: '100%',
    position: 'absolute',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dotViewStyle: {
    width: Responsive.getWidth(3),
    height: Responsive.getWidth(3),
    borderRadius: Responsive.getWidth(1.5),
    backgroundColor: COLORS.GRAY50,
  },
  fromViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: Responsive.getHeight(1.5),
    borderBottomWidth: 1,
    borderColor: COLORS.GRAY27,
    flex: 1,
  },
  toViewStyle: {
    paddingVertical: Responsive.getHeight(1.5),
  },
  toAddressTextStyle: {
    ...COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD'),
    width: '80%',
  },
  tokenIconStyle: {
    ...COMMON_STYLE.imageStyle(4),
    marginLeft: Responsive.getWidth(2),
  },
  tokenViewStyle: {
    flex: 1,
  },
  amountTitleStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  amountViewContainerViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    flexDirection: 'row',
  },

  amountContainerViewStyle: {
    alignItems: 'center',
    width: '100%',
    borderBottomWidth: 1,
    borderColor: COLORS.GRAY27,
    paddingBottom: Responsive.getHeight(1.3),
  },
  amountStyle: {
    ...COMMON_STYLE.textStyle(18, COLORS.GRAYDF, 'BOLD'),
    marginRight: Responsive.getWidth(2),
  },
  transFeeViewStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: Responsive.getHeight(1.5),
    borderBottomWidth: 1,
    borderColor: COLORS.GRAY27,
  },
  accountBallanceStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  btnContainerStyle: {
    flexDirection: 'row',
    marginVertical: Responsive.getHeight(2),
    justifyContent: 'space-between',
  },
  borderedBtnStyle: {
    width: '48%',
  },
  dappDetailViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    borderBottomWidth: 1,
    borderColor: COLORS.GRAY27,
    paddingVertical: Responsive.getHeight(2),
  },
  dappTitleStyle: {
    ...COMMON_STYLE.textStyle(18, COLORS.WHITE, 'BOLD', 'center'),
  },
  peetNameStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.YELLOWFB, 'BASE', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  pendingBtnStyle: {
    marginTop: Responsive.getHeight(2),
  },
});
