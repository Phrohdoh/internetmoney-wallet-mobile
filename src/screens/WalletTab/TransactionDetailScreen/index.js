import React, { useEffect, useState } from 'react';

import { View, Text, ScrollView, Linking } from 'react-native';
import PropTypes from 'prop-types';
import Toast from 'react-native-toast-message';

import moment from 'moment';

import Clipboard from '@react-native-clipboard/clipboard';

import { useSelector } from 'react-redux';

import { getNetwork } from '@redux';

import { useWalletConnect } from '@contexts/WalletConnectContext';

// import components
import { SafeAreaWrapper, RoundedButton, CachedImage } from '@components';

// import themes
import { IMAGES, COLORS, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import {
  useTokenIcons,
  log,
  displayAddress,
  IntegerUnits,
} from '@utils';

import * as Web3Layer from '@web3';

const GWEI = new IntegerUnits(1000000000, 18);

const getTransactionStatus = (transactionObject) => {
  if (transactionObject === undefined) {
    return 'loading';
  }
  if (transactionObject.pending) {
    return 'pending';
  }
  if (!transactionObject.success) {
    return 'failed';
  }
  return 'success';
};

const TransactionDetailScreen = props => {
  const params = props.route.params;
  const { transaction } = params;
  const { transactionDetail, receipt } = transaction;

  const getTokenIcon = useTokenIcons();
  const selectedNetwork = useSelector(getNetwork);
  const { sessions } = useWalletConnect();

  let peerMeta;
  if (transaction.type === 'dapp') {
    const isV2 = typeof transactionDetail.dappTransactionDetail.topic === 'string';
    peerMeta = isV2
      ? sessions.find(s => s.topic === transactionDetail.dappTransactionDetail.topic)?.peer?.metadata
      : transactionDetail.dappTransactionDetail.connection.peerMeta;
    peerMeta = peerMeta ?? {};
  }

  const [transactionObject, setTransactionObject] = useState(undefined);

  const getTransactionObject = async () => {
    setTransactionObject(await Web3Layer.getTransactionDetails(
      receipt.transactionHash,
      selectedNetwork,
    ));
  };

  useEffect(() => {
    getTransactionObject();
  }, []);

  const transactionStatus = getTransactionStatus(transactionObject);
  const transactionDetails = transactionObject?.txnDetails ?? {};
  const exploreUrl = transactionObject?.exploreUrl ?? transactionDetails?.txnExploreUrl;

  const statusText = {
    pending: localize('PENDING_TRANSACTION_UPPERCASE'),
    failed: localize('FAILED_TRANSACTION_UPPERCASE'),
    success: localize('TRANSACTION_CONFIRM_UPPERCASE'),
  }[transactionStatus];

  const statusColor = {
    pending: COLORS.YELLOWFB,
    failed: COLORS.ERROR,
    success: COLORS.GREEN6D,
  }[transactionStatus];

  const getTransImage = () => {
    if (transaction.type === 'send') {
      return IMAGES.SEND_ICON;
    } else if (transaction.type === 'swap') {
      return IMAGES.SWAP_ICON;
    } else if (transaction.type === 'claim') {
      return IMAGES.TIME_TOKEN_ICON;
    } else if (transaction.type === 'dapp') {
      return IMAGES.DAPP_ICON;
    } else if (transaction.type === 'referral_claim') {
      return IMAGES.EARN_REFERRAL_ICON;
    } else {
      return IMAGES.RECEIVE_ICON;
    }
  };

  const getTransType = () => {
    if (transaction.type === 'send') {
      return localize('SEND');
    } else if (transaction.type === 'swap') {
      return localize('SWAP');
    } else if (transaction.type === 'claim') {
      return localize('CLAIM_DIV');
    } else if (transaction.type === 'sweep') {
      return localize('SWEEP_DIV');
    } else if (transaction.type === 'dapp') {
      return localize('DAPP');
    } else if (transaction.type === 'referral_claim') {
      return localize('REFERRAL_EARNINGS');
    } else {
      return localize('RECEIVE');
    }
  };

  const getAmountTitle = () => {
    switch (transaction.type) {
    case 'claim':
      return localize('CLAIMED_AMOUNT');
    case 'sweep':
      return localize('SWEEPED_AMOUNT');
    case 'referral_claim':
      return localize('RECEVING_REWARDS');
    default:
      return localize('AMOUNT');
    }
  };

  const renderFromAccountView = () => {
    return (
      <View style={ styles.fromViewStyle }>
        <View>
          <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
            {localize('FROM:')}
          </Text>
          <Text style={ COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD') }>
            {transactionDetail.accountName}
          </Text>
        </View>
        <View style={ { flex: 1 } }>
          <View style={ styles.accountBallanceStyle }>
            <Text
              style={ COMMON_STYLE.textStyle(12, COLORS.GRAYDF, 'BOLD', 'right') }
              numberOfLines={ 1 }>
              {`${transactionDetail.displayAmount?.toString(6)} ${transactionDetail.fromToken?.symbol}`}
            </Text>
            <CachedImage
              source={ getTokenIcon(
                transactionDetail.fromToken.address,
                selectedNetwork.chainId,
              ) }
              style={ styles.tokenIconStyle }
            />
          </View>

          {transactionDetail.isSupportUSD && (
            <Text
              style={ COMMON_STYLE.textStyle(11, COLORS.GRAY94, 'BASE', 'right') }
              numberOfLines={ 1 }>
              {localize('VALUE:')}
              <Text
                style={ COMMON_STYLE.textStyle(
                  11,
                  COLORS.WHITE,
                  'BASE',
                  'right',
                ) }>
                {`$${transactionDetail.displayAmount?.multiply(transactionDetail.usdValue).toString(2)}`}
              </Text>
            </Text>
          )}
        </View>
      </View>
    );
  };

  const renderClaimToAccountView = () => {
    return (
      <View style={ styles.toViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('TO')}
        </Text>
        <Text numberOfLines={ 1 } style={ styles.toAddressTextStyle }>
          {displayAddress(transactionDetail.toAddress)}
        </Text>
      </View>
    );
  };

  const renderReferralFromView = () => {
    return (
      <View style={ styles.fromViewStyle }>
        <View>
          <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
            {localize('FROM:')}
          </Text>
          <Text style={ COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD') }>
            {transactionDetail.accountName}
          </Text>
        </View>
        <View style={ { flex: 1 } }>
          <View style={ styles.accountBallanceStyle }>
            <Text
              style={ COMMON_STYLE.textStyle(12, COLORS.GRAYDF, 'BOLD', 'right') }
              numberOfLines={ 1 }>
              {transactionDetail.displayAmount?.toString(6)} IM
            </Text>
            <CachedImage source={ IMAGES.IM_TOKEN_ICON } style={ styles.tokenIconStyle } />
          </View>

          {transactionDetail.isSupportUSD && (
            <Text
              style={ COMMON_STYLE.textStyle(11, COLORS.GRAY94, 'BASE', 'right') }
              numberOfLines={ 1 }>
              {localize('VALUE:')}
              <Text
                style={ COMMON_STYLE.textStyle(
                  11,
                  COLORS.WHITE,
                  'BASE',
                  'right',
                ) }>
                  ${transactionDetail.displayAmount?.multiply(transactionDetail.usdValue).toString(2)}
              </Text>
            </Text>
          )}
        </View>
      </View>
    );
  };

  const renderReferralToView = () => {
    return (
      <View style={ styles.toViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('TO')}
        </Text>
        <Text numberOfLines={ 1 } style={ styles.toAddressTextStyle }>
          {displayAddress(transactionDetail.toAddress)}
        </Text>
      </View>
    );
  };

  const renderDappFrom = () => {
    return (
      <View style={ styles.toViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('FROM:')}
        </Text>
        <Text numberOfLines={ 1 } style={ styles.toAddressTextStyle }>
          {displayAddress(transactionDetail.fromAddress)}
        </Text>
      </View>
    );
  };

  const renderDappTo = () => {
    return (
      <View style={ styles.toViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('TO')}
        </Text>
        <Text numberOfLines={ 1 } style={ styles.toAddressTextStyle }>
          {displayAddress(transactionDetail.toAddress)}
        </Text>
      </View>
    );
  };

  const renderReceiveFrom = () => {
    return (
      <View style={ styles.toViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('FROM:')}
        </Text>
        <Text numberOfLines={ 1 } style={ styles.toAddressTextStyle }>
          {displayAddress(transactionDetail.fromAddress)}
        </Text>
      </View>
    );
  };

  const renderReceiveTo = () => {
    return (
      <View style={ styles.toViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('TO')}
        </Text>
        <Text numberOfLines={ 1 } style={ styles.toAddressTextStyle }>
          {displayAddress(transactionDetail.toAddress)}
        </Text>
      </View>
    );
  };

  const renderToAccountView = () => {
    return (
      <View style={ styles.toViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('TO')}
        </Text>
        <Text numberOfLines={ 1 } style={ styles.toAddressTextStyle }>
          {transactionDetail.toAccountName
            ? transactionDetail.toAccountName
            : displayAddress(transactionDetail.toAddress)}
        </Text>
      </View>
    );
  };

  const renderFromTokenView = () => {
    return (
      <View style={ styles.fromViewStyle }>
        <View>
          <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
            {localize('FROM:')}
          </Text>
          <Text style={ COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD') }>
            {transactionDetail.fromToken.symbol}
          </Text>
        </View>
        <View style={ { flex: 1 } }>
          <View style={ styles.accountBallanceStyle }>
            <Text
              style={ COMMON_STYLE.textStyle(12, COLORS.GRAYDF, 'BOLD', 'right') }
              numberOfLines={ 1 }>
              {`${transactionDetail.displayAmount?.toString(6)} ${transactionDetail.fromToken.symbol}`}
            </Text>
            <CachedImage
              source={ getTokenIcon(
                transactionDetail.fromToken.address,
                selectedNetwork.chainId,
              ) }
              style={ styles.tokenIconStyle }
            />
          </View>

          {transactionDetail.isSupportUSD && (
            <Text
              style={ COMMON_STYLE.textStyle(11, COLORS.GRAY94, 'BASE', 'right') }
              numberOfLines={ 1 }>
              {localize('VALUE:')}
              <Text
                style={ COMMON_STYLE.textStyle(
                  11,
                  COLORS.WHITE,
                  'BASE',
                  'right',
                ) }>
                {`$ ${transactionDetail.displayAmount?.multiply(transactionDetail.usdValue).toString(2)})`}
              </Text>
            </Text>
          )}
        </View>
      </View>
    );
  };

  const renderToTokenView = () => {
    return (
      <View style={ styles.fromViewStyle }>
        <View>
          <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
            {localize('TO')}
          </Text>
          <Text
            style={ COMMON_STYLE.textStyle(12, COLORS.GRAYDF, 'BOLD', 'right') }
            numberOfLines={ 1 }>
            <Text>
              {transactionDetail.quoteObj && transactionDetail.quoteObj.amountOut?.toString(6)}
            </Text>
            {' ' + transactionDetail.toToken.symbol}
          </Text>
        </View>
        <View style={ { flex: 1 } }>
          <View style={ styles.accountBallanceStyle }>
            <Text
              style={ COMMON_STYLE.textStyle(12, COLORS.GRAYDF, 'BOLD', 'right') }
              numberOfLines={ 1 }>
              {transactionDetail.toToken.symbol}
            </Text>
            <CachedImage
              source={ getTokenIcon(
                transactionDetail.toToken.address,
                selectedNetwork.chainId,
              ) }
              style={ styles.tokenIconStyle }
            />
          </View>
        </View>
      </View>
    );
  };

  const renderFeeDetailView = (title, detail) => {
    return (
      <View style={ styles.transFeeViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(12, COLORS.WHITE) }>
          {localize(title)}
        </Text>
        <Text style={ COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BOLD') }>
          {detail}
        </Text>
      </View>
    );
  };

  const renderTotalFeeDetailView = (title, detail) => {
    return (
      <View style={ styles.transFeeViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BOLD') }>
          {localize(title)}
        </Text>
        <Text style={ COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BOLD') }>
          {detail}
        </Text>
      </View>
    );
  };

  const showTransactionDetail = () => {
    return (
      <React.Fragment>
        {renderFeeDetailView('GAS_LIMIT_FEE', transactionDetails.gasLimit?.value.toString())}
        {renderFeeDetailView('GAS_USE_FEE', transactionDetails.gasUsed?.value.toString())}
        {renderFeeDetailView(
          transactionDetails.type !== 0 ? 'BASE_FEE' : 'GAS_PRICE',
          `${transactionDetails.baseFee?.divide(GWEI).toString(6)} ${localize('GWEI')}`,
        )}
        {transactionDetails.type !== 0 ? (
          <React.Fragment>
            {renderFeeDetailView(
              'PRIO_FEE',
              transactionDetails.priorityFee?.divide(GWEI).toString(6) +
                ' ' +
                localize('GWEI'),
            )}
            {renderFeeDetailView(
              'TOTAL_FEE',
              transactionDetails.totalGasFee?.multiply(transactionDetails.gasLimit).toString(6) +
                ' ' +
                selectedNetwork.sym,
            )}
            {renderFeeDetailView(
              'MAX_FEE_PER_GAS',
              transactionDetails.maxFeePerGas?.divide(GWEI).toString(6) +
                ' ' +
                localize('GWEI'),
            )}
          </React.Fragment>
        ) : (
          <React.Fragment></React.Fragment>
        )}

        {transaction.type === 'swap' &&
          renderFeeDetailView(
            'WALLET_FEE',
            `${transactionDetail.walletFee?.toString(6) ?? '0'} ${selectedNetwork.sym}`,
          )}
      </React.Fragment>
    );
  };

  const renderDappNameDetail = () => {
    return (
      transaction.type === 'dapp' && (
        <View style={ styles.dappDetailViewStyle }>
          <Text style={ styles.dappTitleStyle }>{localize('DAPP')}</Text>
          <Text style={ styles.peetNameStyle }>
            {peerMeta.name}
          </Text>
        </View>
      )
    );
  };

  const renderLoading = () => (
    <View style={ styles.loadingConteinarStyle }>
      <CachedImage
        source={ IMAGES.ROUNDED_LOADING }
        style={ COMMON_STYLE.imageStyle(15) }
      />
    </View>
  );

  const renderPending = () => {
    return (
      <View style={ styles.detailConteinarStyle }>
        <ScrollView bounces={ false } showsVerticalScrollIndicator={ false }>
          <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
          <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
          <Text style={ styles.transactionStatusStyle(statusColor) }>
            {statusText}
          </Text>
          <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
          <Text style={ styles.expediteNotice }>
            {localize('EXPEDITE_TRANSACTION_NOTICE')}
          </Text>
          <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />

          {renderDappNameDetail()}
          <View style={ styles.accountDetailViewStyle }>
            <View style={ styles.lineViewStyle }>
              <CachedImage
                source={ IMAGES.VERTICAL_DOTTET_LINE }
                style={ styles.verticalLineStyle }
              />
              <View style={ styles.dotContainStyle }>
                <View style={ styles.dotViewStyle } />
                <CachedImage
                  style={ COMMON_STYLE.imageStyle(4) }
                  source={ IMAGES.DOWN_ARROW }
                />
                <View style={ styles.dotViewStyle } />
              </View>
            </View>
            {transaction.type === 'send' && (
              <View style={ { flex: 1 } }>
                {renderFromAccountView()}
                {renderToAccountView()}
              </View>
            )}

            {transaction.type === 'receive' && (
              <View style={ { flex: 1 } }>
                {renderReceiveFrom()}
                {renderReceiveTo()}
              </View>
            )}

            {transaction.type === 'swap' && (
              <View style={ { flex: 1 } }>
                {renderFromTokenView()}
                {renderToTokenView()}
              </View>
            )}

            {transaction.type === 'dapp' && (
              <View style={ { flex: 1 } }>
                {renderDappFrom()}
                {renderDappTo()}
              </View>
            )}
            {transaction.type === 'claim' || transaction.type === 'sweep' && (
              <View style={ { flex: 1 } }>
                {renderFromAccountView()}
                {renderClaimToAccountView()}
              </View>
            )}
            {transaction.type === 'referral_claim' && (
              <View style={ { flex: 1 } }>
                {renderReferralFromView()}
                {renderReferralToView()}
              </View>
            )}
          </View>

          <Text style={ styles.amountTitleStyle }>{getAmountTitle()}</Text>
          <View style={ styles.amountContainerViewStyle }>
            <View style={ styles.amountViewContainerViewStyle }>
              <Text style={ styles.amountStyle } adjustsFontSizeToFit={ true } numberOfLines={ 1 }>
                {transactionDetail.displayAmount?.toString(6)}
              </Text>
              <Text style={ styles.amountStyle }>
                {transaction.type === 'referral_claim'
                  ? 'IM'
                  : transactionDetail.fromToken.symbol}
              </Text>
              <CachedImage
                source={
                  transaction.type === 'referral_claim'
                    ? IMAGES.IM_TOKEN_ICON
                    : getTokenIcon(
                      transactionDetail.fromToken.address,
                      selectedNetwork.chainId,
                    )
                }
                style={ COMMON_STYLE.imageStyle(7) }
              />
            </View>
            {transactionDetail.isSupportUSD && (
              <Text style={ COMMON_STYLE.textStyle(11, COLORS.GRAY94, 'BASE') }>
                {localize('VALUE:')}
                <Text style={ COMMON_STYLE.textStyle(11, COLORS.WHITE, 'BASE') }>
                  {`$${transactionDetail.displayAmount?.multiply(transactionDetail.usdValue).toString(2)}`}
                </Text>
              </Text>
            )}
          </View>
        </ScrollView>
        <View style={ styles.btnContainerStyle }>
          <RoundedButton
            testID={ 'trans-detail-block-explorer-btn' }
            style={ styles.borderedBtnStyle }
            titleStyle={ COMMON_STYLE.textStyle(9, COLORS.YELLOWFB, 'BOLD') }
            isBordered
            title={ localize('VIEW_BLOCK_EXPLORER_UPPERCASE') }
            onPress={ () => {
              try {
                Linking.openURL(transactionDetails.txnExploreUrl);
              } catch (e) {
                log('e >>>', e);
              }
            } }
          />
          <RoundedButton
            testID={ 'trans-detail-copy-btn' }
            style={ styles.borderedBtnStyle }
            titleStyle={ COMMON_STYLE.textStyle(9, COLORS.YELLOWFB, 'BOLD') }
            isBordered
            title={ localize('COPY_TRANS_ID_UPPERCASE') }
            onPress={ () => {
              Clipboard.setString(transactionDetails.txnExploreUrl);
              Toast.show({
                text1: localize('COPIED_SUCCESS'),
                text2: localize('TRANSACTION_HASH_COPIED_SUCCESS'),
              });
            } }
          />
        </View>
      </View>
    );
  };

  const renderFailed = () => {
    return (
      <View style={ styles.loadingConteinarStyle }>
        <Text style={ styles.transactionStatusStyle(statusColor) }>
          {localize('FAILED_TRANSACTION_UPPERCASE')}
        </Text>
        <RoundedButton
          testID={ 'trans-detail-view-btn' }
          style={ styles.pendingBtnStyle }
          title={ localize('VIEW') }
          onPress={ () => {
            try {
              Linking.openURL(exploreUrl);
            } catch (e) {
              log('e >>>', e);
            }
          } }
        />
      </View>
    );
  };

  const renderTransaction = () => {
    return (
      <View style={ styles.detailConteinarStyle }>
        <ScrollView bounces={ false } showsVerticalScrollIndicator={ false }>
          <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
          <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
          <Text style={ styles.transactionStatusStyle(statusColor) }>
            {statusText}
          </Text>
          <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
          <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />

          {renderDappNameDetail()}
          <View style={ styles.accountDetailViewStyle }>
            <View style={ styles.lineViewStyle }>
              <CachedImage
                source={ IMAGES.VERTICAL_DOTTET_LINE }
                style={ styles.verticalLineStyle }
              />
              <View style={ styles.dotContainStyle }>
                <View style={ styles.dotViewStyle } />
                <CachedImage
                  style={ COMMON_STYLE.imageStyle(4) }
                  source={ IMAGES.DOWN_ARROW }
                />
                <View style={ styles.dotViewStyle } />
              </View>
            </View>
            {transaction.type === 'send' && (
              <View style={ { flex: 1 } }>
                {renderFromAccountView()}
                {renderToAccountView()}
              </View>
            )}

            {transaction.type === 'receive' && (
              <View style={ { flex: 1 } }>
                {renderReceiveFrom()}
                {renderReceiveTo()}
              </View>
            )}

            {transaction.type === 'swap' && (
              <View style={ { flex: 1 } }>
                {renderFromTokenView()}
                {renderToTokenView()}
              </View>
            )}

            {transaction.type === 'dapp' && (
              <View style={ { flex: 1 } }>
                {renderDappFrom()}
                {renderDappTo()}
              </View>
            )}
            {transaction.type === 'claim' || transaction.type === 'sweep' && (
              <View style={ { flex: 1 } }>
                {renderFromAccountView()}
                {renderClaimToAccountView()}
              </View>
            )}
            {transaction.type === 'referral_claim' && (
              <View style={ { flex: 1 } }>
                {renderReferralFromView()}
                {renderReferralToView()}
              </View>
            )}
          </View>

          <Text style={ styles.amountTitleStyle }>{getAmountTitle()}</Text>
          <View style={ styles.amountContainerViewStyle }>
            <View style={ styles.amountViewContainerViewStyle }>
              <Text style={ styles.amountStyle } adjustsFontSizeToFit={ true } numberOfLines={ 1 }>
                {transactionDetail.displayAmount?.toString(6)}
              </Text>
              <Text style={ styles.amountStyle }>
                {transaction.type === 'referral_claim'
                  ? 'IM'
                  : transactionDetail.fromToken.symbol}
              </Text>
              <CachedImage
                source={
                  transaction.type === 'referral_claim'
                    ? IMAGES.IM_TOKEN_ICON
                    : getTokenIcon(
                      transactionDetail.fromToken.address,
                      selectedNetwork.chainId,
                    )
                }
                style={ COMMON_STYLE.imageStyle(7) }
              />
            </View>
            {transactionDetail.isSupportUSD && (
              <Text style={ COMMON_STYLE.textStyle(11, COLORS.GRAY94, 'BASE') }>
                {localize('VALUE:')}
                <Text style={ COMMON_STYLE.textStyle(11, COLORS.WHITE, 'BASE') }>
                  {`$${transactionDetail.displayAmount?.multiply(transactionDetail.usdValue).toString(2)}`}
                </Text>
              </Text>
            )}
          </View>
          {showTransactionDetail()}
          {renderFeeDetailView('NONCE', transactionDetails.nonce?.toString())}
          {renderTotalFeeDetailView(
            'TOTAL',
            (
              transactionDetails.type !== 0
                ? transactionDetails.totalGasFee?.multiply(transactionDetails.gasUsed)
                : transactionDetails.baseFee?.multiply(transactionDetails.gasUsed)
            )?.toString(6) +
              ' ' +
              selectedNetwork.sym,
          )}
        </ScrollView>
        <View style={ styles.btnContainerStyle }>
          <RoundedButton
            testID={ 'trans-detail-block-explorer-btn' }
            style={ styles.borderedBtnStyle }
            titleStyle={ COMMON_STYLE.textStyle(9, COLORS.YELLOWFB, 'BOLD') }
            isBordered
            title={ localize('VIEW_BLOCK_EXPLORER_UPPERCASE') }
            onPress={ () => {
              try {
                Linking.openURL(transactionDetails.txnExploreUrl);
              } catch (e) {
                log('e >>>', e);
              }
            } }
          />
          <RoundedButton
            testID={ 'trans-detail-copy-btn' }
            style={ styles.borderedBtnStyle }
            titleStyle={ COMMON_STYLE.textStyle(9, COLORS.YELLOWFB, 'BOLD') }
            isBordered
            title={ localize('COPY_TRANS_ID_UPPERCASE') }
            onPress={ () => {
              Clipboard.setString(transactionDetails.txnExploreUrl);
              Toast.show({
                text1: localize('COPIED_SUCCESS'),
                text2: localize('TRANSACTION_HASH_COPIED_SUCCESS'),
              });
            } }
          />
        </View>
      </View>
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <View style={ styles.transactionTypeViewStyle }>
        <CachedImage source={ getTransImage() } style={ COMMON_STYLE.imageStyle(10) } />
        <View style={ styles.transactionDateViewStyle }>
          {transaction.type === 'dapp' ? (
            <Text numberOfLines={ 1 } style={ styles.typeStyle }>
              {getTransType() + ' ' + peerMeta.name}
            </Text>
          ) : transaction.type === 'referral_claim' ? (
            <Text style={ styles.typeStyle }>{getTransType() + ' IM'}</Text>
          ) : transaction.type !== 'claim' ? (
            <Text style={ styles.typeStyle }>
              {getTransType() + ' ' + transactionDetail.fromToken.symbol}
            </Text>
          ) : transaction.type !== 'receive' ? (
            <Text style={ styles.typeStyle }>
              {getTransType() + ' ' + transactionDetail.fromToken.symbol}
            </Text>
          ) : (
            <Text style={ styles.typeStyle }>{getTransType()}</Text>
          )}
          <Text style={ styles.dateStyle }>
            {transactionDetails.date
              ? moment(transactionDetails.date).format('MMMM DD, YYYY | hh:mm A')
              : ''}
          </Text>
        </View>
      </View>
      {transactionStatus === 'loading' && renderLoading()}
      {transactionStatus === 'pending' && renderPending()}
      {transactionStatus === 'failed' && renderFailed()}
      {transactionStatus === 'success' && renderTransaction()}
    </SafeAreaWrapper>
  );
};

TransactionDetailScreen.propTypes = { route: PropTypes.any };
export default TransactionDetailScreen;
