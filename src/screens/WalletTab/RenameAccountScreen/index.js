import React, { useState } from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';

import { AccountsAction } from '@redux';

// import components
import {
  SafeAreaWrapper,
  KeyboardAvoidScrollView,
  RoundedButton,
  TitleTextInput,
} from '@components';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { IMAGES, COLORS, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import { Validation } from '@utils';

const RenameAccountScreen = props => {
  const { navigation } = props;
  const params = props.route.params;
  const dispatch = useDispatch();

  const accounts = useSelector(state => state.accounts);
  const prevAccountName = params.accountDetail.name;

  const [accountName, onChangeAccountName] = useState({
    id: 'renameaccount',
    value: params.accountDetail.name,
    title: 'RENAME_ACCOUNT',
    extraProps: {
      isError: false,
      errorText: localize('ENTER_VALID_ACCOUNT_NAME_MSG'),
    },
  });

  const reveal = {
    id: 'reveal',
    value: localize('REVEAL_PRIVATE_KEY_UPPERCASE'),
    extraProps: {
      onPressInput: () =>
        navigation.navigate('REVEAL_KEY_VERIFY_SCREEN', {
          accountDetail: params.accountDetail,
        }),
      inputStyle: COMMON_STYLE.textStyle(14, COLORS.YELLOWFB, 'BOLD'),
      iconRight: IMAGES.RIGHT_ARROW,
      rightIconStyle: COMMON_STYLE.imageStyle(4),
    },
  };

  // Action Methods
  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.value = text;
    tempDict.extraProps = { ...state.extraProps, isError: false };
    setFunction(tempDict);
  };

  const changeObj = (obj, state, setFunction) => {
    const tmpObj = { ...state, ...obj };
    setFunction(tmpObj);
  };

  // check all validation
  const checkValidation = () => {
    const isEmptyAccountName = Validation.isEmpty(accountName.value);
    const isNameExist = accounts.accountList.find(
      accObj => accObj.name === accountName.value.trim(),
    );
    const isSameName = prevAccountName === accountName.value.trim();

    if (isSameName && !isEmptyAccountName) {
      navigation.goBack();
      return false;
    } else if (!isEmptyAccountName && !isNameExist) {
      return true;
    } else {
      let msg = '';
      /* istanbul ignore else */
      if (isEmptyAccountName) {
        msg = 'ENTER_VALID_ACCOUNT_NAME_MSG';
      } else if (isNameExist) {
        msg = 'ACCOUNT_NAME_EXIST_MSG';
      }
      changeObj(
        {
          extraProps: {
            ...accountName.extraProps,
            isError: true,
            errorText: localize(msg),
          },
        },
        accountName,
        onChangeAccountName,
      );
      return false;
    }
  };

  const _onPressContinue = async () => {
    if (checkValidation()) {
      const accountList = [...accounts.accountList];
      const index = accountList.findIndex(
        x =>
          x.publicAddress.toLowerCase() ===
          params.accountDetail.publicAddress.toLowerCase(),
      );

      const tempAccObj = { ...accountList[index] };
      tempAccObj.name = accountName.value.trim();
      accountList[index] = tempAccObj;

      dispatch(AccountsAction.updateAccountObject(accountList));
      await StorageOperation.setMultiData([
        [ASYNC_KEYS.ACCOUNT_LIST, JSON.stringify(accountList)],
      ]);

      navigation.navigate('DASHBOARD_TAB_NAVIGATOR');
    }
  };

  // Custom Render Components
  const renderInputComponent = (state, setStateFunction) => {
    return (
      <TitleTextInput
        testID={ `pass-setup-screen-${state.id}` }
        inputContainerStyle={ styles.inputContainerStyle }
        value={ state.value }
        title={ localize(state.title) }
        onChangeText={ text => changeData(text, state, setStateFunction) }
        { ...state.extraProps }
      />
    );
  };

  return (
    <SafeAreaWrapper>
      <KeyboardAvoidScrollView>
        <Text style={ COMMON_STYLE.infoDescStyle(COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('RENAME_ACCOUNT_MSG')}
        </Text>
        {renderInputComponent(accountName, onChangeAccountName)}
        {renderInputComponent(reveal)}
      </KeyboardAvoidScrollView>
      <RoundedButton
        testID="rename_account-save-button"
        style={ COMMON_STYLE.roundedBtnMargin() }
        title={ localize('SAVE_CHANGES_UPPERCASE') }
        onPress={ () => _onPressContinue() }
      />
    </SafeAreaWrapper>
  );
};

RenameAccountScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default RenameAccountScreen;
