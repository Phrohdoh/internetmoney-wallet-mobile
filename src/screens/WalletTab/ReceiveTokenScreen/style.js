import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,

  qrContainerViewStyle: {
    backgroundColor: COLORS.GRAY32,
    width: Responsive.getWidth(80),
    height: Responsive.getWidth(80),
    alignSelf: 'center',
    marginVertical: Responsive.getWidth(12),
    borderRadius: Responsive.getWidth(5),
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  addressStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.YELLOWFB, 'BOLD', 'center'),
    width: '80%',
  },
  scanMsgStyle: {
    ...COMMON_STYLE.textStyle(16, COLORS.LIGHT_OPACITY(0.6), 'BOLD', 'center'),
    width: '90%',
  },
  btnGroupContainerStyle: {
    flexDirection: 'row',
    marginTop: Responsive.getHeight(5),
    width: Responsive.getWidth(80),
    justifyContent: 'space-around',
  },
  btnStyle: {
    width: '40%',
  },
});
