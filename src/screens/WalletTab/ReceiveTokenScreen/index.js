import React from 'react';
import { View, Text, Share } from 'react-native';
import Toast from 'react-native-toast-message';

import PropTypes from 'prop-types';

import QRCode from 'react-native-qrcode-svg';
import Clipboard from '@react-native-clipboard/clipboard';

import { useDispatch } from 'react-redux';

import { GlobalAction } from '@redux';

// import components
import { SafeAreaWrapper, RoundedButton } from '@components';

import { Responsive } from '@helpers';

// import themes
import { IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

const ReceiveTokenScreen = props => {
  const params = props.route.params;

  const dispatch = useDispatch();

  // Action methods
  const onCopy = () => {
    Clipboard.setString(params.accountDetail.publicAddress);
    Toast.show({
      text1: localize('COPIED_SUCCESS'),
      text2: localize('ADDRESS_COPIED_SUCCESS'),
    });
  };

  const onShare = async () => {
    try {
      await Share.share({
        message: params.accountDetail.publicAddress,
      });
    } catch (error) {
      dispatch(GlobalAction.showAlert(localize('ERROR'), error.message));
    }
  };

  // render custom component
  return (
    <SafeAreaWrapper containerStyle={ { alignItems: 'center' } }>
      <View style={ styles.qrContainerViewStyle }>
        <QRCode
          value={ params.accountDetail.publicAddress }
          size={ Responsive.getWidth(55) }
        />
        <Text style={ styles.addressStyle }>
          {params.accountDetail.publicAddress}
        </Text>
      </View>
      <Text style={ styles.scanMsgStyle }>
        {localize('RECEIVE_TOKENS_MSG_UPPERCASE')}
      </Text>
      <View style={ styles.btnGroupContainerStyle }>
        <RoundedButton
          testID="receive-token-copy-button"
          style={ styles.btnStyle }
          title={ localize('COPY_UPPERCASE') }
          leftImage={ IMAGES.COPY_ICON }
          onPress={ () => onCopy() }
        />
        <RoundedButton
          testID="receive-token-share-button"
          style={ styles.btnStyle }
          title={ localize('SHARE_UPPERCASE') }
          leftImage={ IMAGES.SHARE_ICON }
          onPress={ () => onShare() }
        />
      </View>
    </SafeAreaWrapper>
  );
};

ReceiveTokenScreen.propTypes = { route: PropTypes.any };
export default ReceiveTokenScreen;
