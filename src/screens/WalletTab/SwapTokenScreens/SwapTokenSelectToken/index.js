import React, { useState } from 'react';

import {
  View,
  Text,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from 'react-native';

import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';
import {
  NetworksAction,
  WalletAction,
  GlobalAction,
  getTokensByAddress,
  getAccountList,
} from '@redux';

// import components
import {
  SafeAreaWrapper,
  RoundedButton,
  ModalContainerView,
  SegmentController,
  CurrencyInputField,
  TitleTextInput,
  SelectTokenListCell,
  StepsListView,
  NetworkSelectionModalView,
  SendTokenAccountListCell,
  CachedImage,
} from '@components';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { IMAGES, COLORS, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import {
  Validation,
  useTokenIcons,
  parseObject,
  removeCommas,
  IntegerUnits,
  sanitizeFloatingPointString,
} from '@utils';

import * as Web3Layer from '@web3';
import PercentageInput from '../../../../components/inputComponents/PercentageInput';
import { getNetwork } from '../../../../redux';

const SwapTokenSelectToken = props => {
  const { navigation } = props;
  const params = props.route.params;

  const getTokenIcon = useTokenIcons();
  const networks = useSelector(state => state.networks);

  const selectedNetwork = useSelector(getNetwork);

  console.log('params', params);

  const accountDetail = params?.accountDetail;

  const accountList = useSelector(getAccountList);

  const [selectedAccount, setSelectedAccount] = useState(accountDetail ?? accountList[0]);
  const [showSelectAccount, setShowSelectAccount] = useState(false);

  const [isSelectToToken, setIsSelectToToken] = useState(false);
  const [isShowTokenList, setIsShowTokenList] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [isShowNetworkList, setIsShowNetworkList] = useState(false);
  const [maxAmountErrorMessage, setMaxAmountErrorMessage] = useState('');
  const [slippage, setSlippage] = useState(1);
  const tokensByAddress = useSelector(getTokensByAddress);
  const tokens = tokensByAddress[selectedAccount.publicAddress.toLowerCase()];
  const [selectedFromTokenId, setSelectedFromTokenId] = useState(tokens?.[0].address ?? '');
  const selectedFromToken = tokens.find(token => token.address.toLowerCase() === selectedFromTokenId.toLowerCase()) ?? {};
  const [selectedToTokenId, setSelectedToTokenId] = useState(tokens?.[0].address ?? {});
  const selectedToToken = tokens.find(token => token.address.toLowerCase() === selectedToTokenId.toLowerCase()) ?? {};

  const usdValue = selectedFromToken?.usdPrice?.value;
  const isSupportUSD = usdValue !== undefined;

  const tokenBalance = selectedFromToken.balance?.value;
  const toTokenBalance = selectedToToken.balance?.value;
  const toUsdValue = selectedToToken?.usdPrice?.value;

  const nativeToken = tokens?.[0];

  const dispatch = useDispatch();

  const [amount, onChangeAmount] = useState({
    id: 'amount',
    value: '',
    displayValue: '',
    placeholder: 'AMOUNT',
    extraProps: {
      textAlign: 'right',
      keyboardType: 'numeric',
    },
  });

  const [tokenAddress, setTokenAddress] = useState('');
  const [importError, setImportError] = useState(false);

  const isUSD = currentIndex !== 0;

  const selectAccount = (item) => {
    setShowSelectAccount(false);
    setSelectedAccount(item);
    setSelectedFromTokenId(tokensByAddress[item.publicAddress.toLowerCase()]?.[0].address);
    setSelectedToTokenId(tokensByAddress[item.publicAddress.toLowerCase()]?.[0].address);
  };

  const importToken = async (tokenDetails, tokenAddress) => {
    let tempTokens = [];
    if (networks.tokens) {
      tempTokens = [...networks.tokens];
    }

    const tokenObj = tempTokens.find(
      token => token.id.toLowerCase() === selectedAccount.publicAddress.toLowerCase(),
    );

    if (tokenObj) {
      const tokenIndex = tempTokens.indexOf(tokenObj);
      const tokenList = [...tokenObj.tokens];
      tokenList.push({ ...tokenDetails, address: tokenAddress.trim() });
      const tempTokenObj = { ...tokenObj };
      tempTokenObj.tokens = tokenList;
      tempTokens[tokenIndex] = tempTokenObj;
    } else {
      const tempTokenObj = {
        id: selectedAccount.publicAddress,
        tokens: [{ ...tokenDetails, address: tokenAddress.trim() }],
      };
      tempTokens.push(tempTokenObj);
    }

    const { data } = await StorageOperation.getData(ASYNC_KEYS.CHAIN_TOKENS);

    const parseObj = parseObject(data);

    let tempChainTokens = [];
    if (parseObj.success) {
      tempChainTokens = parseObj.data;
    }
    const chainObj = tempChainTokens.find(
      chain => chain.chainId === selectedNetwork.chainId,
    );

    if (chainObj) {
      const chainIndex = tempChainTokens.indexOf(chainObj);
      chainObj.accountTokens = tempTokens;
      tempChainTokens[chainIndex] = chainObj;
    } else {
      tempChainTokens.push({
        chainId: selectedNetwork.chainId,
        accountTokens: tempTokens,
      });
    }
    dispatch(NetworksAction.saveTokens(tempTokens));
    await StorageOperation.setMultiData([
      [ASYNC_KEYS.CHAIN_TOKENS, JSON.stringify(tempChainTokens)],
    ]);

    const token = tempTokens
      .find(tokens => tokens.id.toLowerCase() === selectedAccount.publicAddress.toLowerCase())
      .tokens
      .find(token => token.address.toLowerCase() === tokenAddress.toLowerCase());
    selectToken({
      ...token,
      isDefault: false
    });
  };

  const onSubmitTokenAddress = async (tokenAddress) => {
    if (tokenAddress === '') {
      return;
    }
    const tokenAlreadyImported = tokens.some(token => token.address.toLowerCase() === tokenAddress.toLowerCase());
    if (tokenAlreadyImported) {
      setImportError(false);
      await selectTokenAddress(tokenAddress);
      setTokenAddress('');
    } else {
      const checkIsValidTokenAddress = await Web3Layer.isTokenAddressValid(
        tokenAddress.trim(),
        selectedNetwork,
      );
      setImportError(!checkIsValidTokenAddress.success);
      if (checkIsValidTokenAddress.success) {
        const { tokenDetails } = checkIsValidTokenAddress;
        await importToken(tokenDetails, tokenAddress);
        setTokenAddress('');
      }
    }
  };

  const onChangeTokenAddress = async (tokenAddress) => {
    setTokenAddress(tokenAddress);
    await onSubmitTokenAddress(tokenAddress);
  };

  const handlePercentageChange = (value) => {
    const inputValue = value;
    setSlippage(parseFloat(inputValue, 10));
  };

  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.displayValue = text;
    // remove commas and potential '$'
    tempDict.value = removeCommas(text)?.replace('$', '') 
    tempDict.extraProps = { ...state.extraProps, isError: false };
    setFunction(tempDict);
  };

  const getConvertedValue = () => {
    try {
      const value = IntegerUnits.fromFloatingPoint(amount.value);
      if (!isUSD) {
        return `${localize('USD_UPPERCASE')} ${localize('VALUE')}: $${value.multiply(usdValue).toString(2)}`;
      }
      return `${selectedFromToken.symbol} ${localize('VALUE')}: ${value.divide(usdValue).toString(6)}`;
    } catch (e) {
      // Error
    }
  };

  const selectToken = item => {
    setIsShowTokenList(false);
    if (isSelectToToken) {
      setSelectedToTokenId(item.address);
    } else {
      setSelectedFromTokenId(item.address);
    }
  };

  const selectTokenAddress = async (tokenAddress) => {
    const token = tokens.find(token => (
      token.address.toLowerCase() === tokenAddress.toLowerCase()
    ));
    selectToken({
      ...token,
      isDefault: false
    });
  };

  const getMaxAmount = async () => {
    const maxAmount = await Web3Layer.getMaxAmount(
      2,
      selectedFromToken.address,
      selectedFromToken.decimals,
      tokenBalance,
      selectedNetwork,
    );

    if (maxAmount.success) {
      const baseMax = maxAmount.maxBalance;

      if (!isUSD) {
        changeData(
          baseMax.toString(6, true),
          amount,
          onChangeAmount,
        );
      } else {
        changeData(
          baseMax.multiply(usdValue).toString(6, true),
          amount,
          onChangeAmount,
        );
      }

      setMaxAmountErrorMessage('');
    } else {
      setMaxAmountErrorMessage(maxAmount.error);
    }
  };

  const goNext = async () => {
    if (!Validation.isEmpty(amount.value) && parseFloat(amount.value) > 0) {
      if (
        selectedFromToken.address.toLowerCase() ===
        selectedToToken.address.toLowerCase()
      ) {
        dispatch(
          GlobalAction.showAlert(
            localize('ERROR'),
            localize('CHOOSE_DIFFER_TOKEN_MSG'),
          ),
        );
        return;
      }

      if (slippage === '' || slippage < 0 || slippage > 100) {
        dispatch(
          GlobalAction.showAlert(
            localize('ERROR'),
            localize('SLIPPAGE_ERROR_MSG'),
          ),
        );
        return;
      }

      let swapAmount = IntegerUnits.fromFloatingPoint(amount.value);
      if (isUSD) {
        swapAmount = swapAmount.divide(usdValue, true);
      }
      swapAmount = swapAmount.toPrecision(selectedFromToken.decimals, true);

      if (swapAmount.gt(tokenBalance)) {
        dispatch(
          GlobalAction.showAlert(
            localize('ERROR'),
            localize('SWAP_AMOUNT_ERROR_MSG'),
          ),
        );
      } else {
        const isWrapperSwap = Web3Layer.checkIfWrapperSwap(
          selectedFromToken.address,
          selectedToToken.address,
          swapAmount,
          selectedNetwork,
        );

        if (isWrapperSwap.success) {
          if (isWrapperSwap.isWrapperSwap) {
            navigation.navigate('SWAP_TOKEN_CONFIRMATION_SCREEN', {
              accountDetail: selectedAccount,
              quoteObj: isWrapperSwap.quoteObj,
              fromToken: selectedFromToken,
              toToken: selectedToToken,
              transferAmount: swapAmount,
              isSupportUSD: isSupportUSD,
              usdValue: isSupportUSD ? usdValue : '',
              slippage: slippage,
            });
          } else {
            navigation.navigate('SWAP_TOKEN_GET_QUOTE_SCREEN', {
              accountDetail: selectedAccount,
              selectedFromToken: selectedFromToken,
              selectedToToken: selectedToToken,
              transferAmount: swapAmount,
              isSupportUSD: isSupportUSD,
              usdValue: isSupportUSD ? usdValue : '',
              slippage: slippage,
            });
          }
        } else {
          dispatch(
            GlobalAction.showAlert(
              localize('ERROR'),
              localize(isWrapperSwap.error),
            ),
          );
        }
      }
    } else {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('ENTER_VALID_AMOUNT_MSG'),
        ),
      );
    }
  };

  const changeNetwork = async choosedNetwork => {
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData());
    await StorageOperation.setData(
      ASYNC_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          localize('NETWORK_SWITCH_TITLE'),
          localize('NETWORK_CHANGE_SUCCESS_MSG') + choosedNetwork.networkName,
        ),
      );
    }, 200);
  };

  const renderAccountSelectionView = () => {
    return (
      <TouchableOpacity
        testID={'swap-token-choose-account-button'}
        style={styles.fromAccountViewStyle}
        onPress={() => {
          setShowSelectAccount(true);
        }}>
        <View style={styles.accountDetailStyle}>
          <Text style={styles.accountNameStyle}>
            {selectedAccount.name}
          </Text>
          <Text style={styles.balenceStyle}>
            {localize('BALANCE:')}
            <Text style={styles.balanceValueStyle}>{`${nativeToken?.balance?.value?.toString(6, true) ?? ''} ${nativeToken?.symbol}`}</Text>
          </Text>
        </View>
        <CachedImage source={IMAGES.DOWN_ARROW} style={COMMON_STYLE.imageStyle(6)} />
      </TouchableOpacity>
    );
  };

  const renderFromTokenSelectionView = () => {
    return (
      <TouchableOpacity
        testID={'swap-token-choose-send-from-token-button'}
        style={styles.fromAccountViewStyle}
        onPress={() => {
          setIsShowTokenList(true);
          setIsSelectToToken(false);
        }}>
        <CachedImage
          source={getTokenIcon(
            selectedFromToken.address,
            selectedNetwork.chainId,
          )}
          style={styles.tokenIconStyle}
        />
        <View style={styles.accountDetailStyle}>
          <Text style={styles.accountNameStyle}>
          {tokenBalance?.toString(6) ?? ''} {selectedFromToken.symbol}
          </Text>
          {
            isSupportUSD
              ? (
                <Text style={styles.balenceStyle}>
                  <Text style={styles.orangeUsdValue}>${tokenBalance?.multiply?.(usdValue)?.toString(2) ?? ''}</Text>
                </Text>
              )
              : null
          }
        </View>
        <CachedImage source={IMAGES.DOWN_ARROW} style={COMMON_STYLE.imageStyle(6)} />
      </TouchableOpacity>
    );
  };

  const renderToTokenSelectionView = () => {
    return (
      <TouchableOpacity
        testID={'swap-token-choose-send-to-token-button'}
        style={styles.fromAccountViewStyle}
        onPress={() => {
          setIsShowTokenList(true);
          setIsSelectToToken(true);
        }}>
        <CachedImage
          source={getTokenIcon(
            selectedToToken.address,
            selectedNetwork.chainId,
          )}
          style={styles.tokenIconStyle}
        />
        <View style={styles.accountDetailStyle}>
          <Text style={styles.accountNameStyle}>
            {toTokenBalance?.toString(6) ?? ''} {selectedToToken.symbol}
          </Text>
          {
            toUsdValue === undefined
              ? null
              : (
                <Text style={styles.balenceStyle}>
                  <Text style={styles.orangeUsdValue}>${toTokenBalance?.multiply?.(toUsdValue)?.toString(2) ?? ''}</Text>
                </Text>
              )
          }
        </View>
        <CachedImage source={IMAGES.DOWN_ARROW} style={COMMON_STYLE.imageStyle(6)} />
      </TouchableOpacity>
    );
  };

  const renderAmountView = () => {
    const segmentList = [{ title: selectedFromToken.symbol }];
    if (isSupportUSD) {
      segmentList.push({ title: localize('USD_UPPERCASE') });
    }

    return (
      <View style={styles.amountViewStyle}>
        <SegmentController
          style={styles.segmentContainerStyle(segmentList.length)}
          segments={segmentList}
          currentIndex={currentIndex}
          onChangeIndex={index => {
            changeData('', amount, onChangeAmount);
            setCurrentIndex(index);
          }}
        />
        {renderInputComponent(amount, onChangeAmount)}
      </View>
    );
  };

  const renderInputComponent = (state, setStateFunction) => {
    return (
      <CurrencyInputField
        inputContainerStyle={styles.inputContainerStyle}
        inputViewStyle={styles.inputViewStyle}
        testID={`swap-token-enter-amount-screen-${state.id}`}
        value={state.displayValue}
        title={localize(state.title)}
        isUSD={ isUSD }
        placeholder={localize(state.placeholder)}
        onChangeText={(text) => {
          if (!text) {
            changeData('', state, setStateFunction);
          } else {
            const sanitized = sanitizeFloatingPointString(text, true, isUSD ? 6 : selectedFromToken.decimals);
            changeData(sanitized, state, setStateFunction);
          }
        }}
        decimalsLimit={ isUSD ? 6 : selectedFromToken.decimals }
        {...state.extraProps}
      />
    );
  };

  const renderPercentInputComponent = (state, setStateFunction) => {
    return (
      <TitleTextInput
        inputContainerStyle={styles.inputContainerStyle}
        inputViewStyle={styles.inputViewStyle}
        testID={`swap-token-enter-amount-screen-${state.id}`}
        value={state.value}
        title={localize(state.title)}
        placeholder={localize(state.placeholder)}
        onChangeValue={(text) => {
          if (!text) {
            changeData('0%', state, setStateFunction);
          } else {
            changeData(sanitizeFloatingPointString(text, true), state, setStateFunction);
          }
        }}
        {...state.extraProps}
      />
    );
  };

  // Custom Render Components
  return (
    <SafeAreaWrapper containerStyle={[COMMON_STYLE.tabContainerStyle]}>
      <TouchableOpacity onPress={() => setIsShowNetworkList(true)}>
        <Text style={styles.selectedNetworkStyle}>
          {localize('CURRENT_NETWORK')}
        </Text>
        <Text style={styles.networkNameStyle}>
          {selectedNetwork.networkName}
        </Text>
      </TouchableOpacity>

      <StepsListView selectedSteps={1} />

      <View
        style={[
          COMMON_STYLE.borderedContainerViewStyle(),
          styles.containerStyle,
        ]}>
        <ScrollView
          // ensures that percentageInput is visible when keyboard is open
          automaticallyAdjustKeyboardInsets={true}
        >
          <View style={styles.tokenFromContainerStyle}>
            <Text style={styles.titleStyle}>
              {localize('CHOOSE_SWAP_ACCOUNT')}
            </Text>
            {renderAccountSelectionView()}
            <Text style={styles.titleStyle}>
              {localize('CHOOSE_FROM_TOKEN_TO_SWAP')}
            </Text>
            {renderFromTokenSelectionView()}
            <Text style={styles.titleStyle}>
              {localize('CHOOSE_TO_TOKEN_TO_SWAP')}
            </Text>

            {renderToTokenSelectionView()}
            <View style={styles.labelAndMax}>
              <Text style={styles.amountTitleStyle}>
                {localize('ENTER_AMOUNT_TO_SWAP')}
              </Text>
              <RoundedButton
                testID="max-button"
                style={styles.maxBtnStyle}
                title={localize('MAX_UPPERCASE')}
                onPress={() => getMaxAmount()}
              />
            </View>
            {renderAmountView()}
            {isSupportUSD && (
              <Text
                style={COMMON_STYLE.textStyle(
                  10,
                  COLORS.LIGHT_OPACITY(0.6),
                  'BASE',
                  'right',
                )}>
                {getConvertedValue()}
              </Text>
            )}
            <Text style={styles.maxErrorMessageStyle}>
              {maxAmountErrorMessage}
            </Text>
            {/* {renderPercentInputComponent(slippage, onChangeSlippage)} */}
            <PercentageInput
              onChange={handlePercentageChange}
              onEndEditing={handlePercentageChange}
              onChangeText={handlePercentageChange} />
          </View>

          <CachedImage style={styles.bgStyle} source={IMAGES.SWAP_TOKEN_BORDER} />
          <View style={styles.tokenContainerStyle}>
          </View>
        </ScrollView>

        <RoundedButton
          testID="enter-amount-next-button"
          style={COMMON_STYLE.roundedBtnMargin()}
          title={localize('NEXT_UPPERCASE')}
          onPress={() => goNext()}
        />
      </View>
      <ModalContainerView
        testID={ 'modal-swap-token-account' }
        isVisible={ showSelectAccount }
        onPressClose={ () => setShowSelectAccount(false) }>
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('CHOOSE_ACCOUNTS_UPPERCASE')}
          </Text>
          <ScrollView
            horizontal
            contentContainerStyle={ { width: '100%' } }
            scrollEnabled={ false }>
            <FlatList
              data={ accountList }
              bounces={ false }
              renderItem={ ({ item, index }) => (
                <SendTokenAccountListCell
                  item={ item }
                  index={ index }
                  onSelectAccount={ item => selectAccount(item) }
                />
              ) }
              ItemSeparatorComponent={ () => (
                <View style={ styles.listSepratorStyle } />
              ) }
              keyExtractor={ (item, index) => index + item }
            />
          </ScrollView>
        </React.Fragment>
      </ModalContainerView>
      <ModalContainerView
        testID={'modal-swap-token'}
        isVisible={isShowTokenList}
        onPressClose={() => setIsShowTokenList(false)}>
        <React.Fragment>
          <Text style={COMMON_STYLE.modalTitleStyle}>
            {localize('CHOOSE_TOKEN_UPPERCASE')}
          </Text>
          <TitleTextInput
            testID="modal-swap-token-address"
            inputContainerStyle={styles.addressInputStyle}
            value={tokenAddress}
            title={localize('TOKEN_ADDRESS')}
            placeholder={localize('ENTER_SWAP_TOKEN_ADDRES')}
            onChangeText={onChangeTokenAddress}
            onEndEditing={event => onSubmitTokenAddress(event.nativeEvent.text.trim())}
            isError={importError}
            errorText={localize('ENTER_VALID_TOKEN_ADDRESS')}
          />
          <ScrollView
            horizontal
            contentContainerStyle={{ width: '100%', height: '80%' }}
            scrollEnabled={false}>
            <FlatList
              data={[...tokens]}
              bounces={false}
              renderItem={({ item, index }) => (
                <React.Fragment>
                  {item.balance?.fetching === false && 
                    item.usdPrice?.fetching === false && 
                    <SelectTokenListCell
                      item={item}
                      index={index}
                      networkObj={selectedNetwork}
                      accountAddress={selectedAccount}
                      onSelectToken={selectToken}
                    />
                  }
                </React.Fragment>
              )}
              ItemSeparatorComponent={() => (
                <View style={styles.listSepratorStyle} />
              )}
              keyExtractor={(item, index) => index + item}
            />
          </ScrollView>
        </React.Fragment>
      </ModalContainerView>
      <NetworkSelectionModalView
        isVisible={isShowNetworkList}
        isShowSwapSupported={true}
        onPressClose={() => setIsShowNetworkList(false)}
        onSelectNetwork={item => {
          setIsShowNetworkList(false);
          if (item.chainId !== selectedNetwork.chainId) {
            changeNetwork(item);
          }
        }}
      />
    </SafeAreaWrapper>
  );
};

SwapTokenSelectToken.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};

export default SwapTokenSelectToken;
