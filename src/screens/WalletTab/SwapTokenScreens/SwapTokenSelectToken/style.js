import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  containerStyle: {
    backgroundColor: COLORS.BLACK,
    paddingHorizontal: 0,
    overflow: 'hidden',
  },
  selectedNetworkStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  networkNameStyle: {
    ...COMMON_STYLE.textStyle(13, COLORS.WHITE, 'BOLD', 'center'),
  },
  titleStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)),
    marginTop: Responsive.getHeight(2),
    marginBottom: Responsive.getHeight(1),
  },
  bgStyle: {
    width: '100%',
    height: Responsive.getHeight(4),
    resizeMode: 'cover',
  },
  tokenFromContainerStyle: {
    paddingHorizontal: Responsive.getWidth(5),
    backgroundColor: COLORS.GRAY11,
    overflow: 'hidden',
    borderTopLeftRadius: Responsive.getWidth(4),
    borderTopRightRadius: Responsive.getWidth(4),
    paddingBottom: Responsive.getHeight(2),
  },

  tokenContainerStyle: {
    paddingHorizontal: Responsive.getWidth(5),
  },
  fromAccountViewStyle: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    borderRadius: 25,
    alignItems: 'center',
    borderWidth: 2,
    borderColor: COLORS.GRAY1F,
    paddingHorizontal: Responsive.getWidth(4),
  },

  accountDetailStyle: { flex: 1 },
  accountNameStyle: { ...COMMON_STYLE.textStyle(11, COLORS.GRAYDF, 'BOLD') },
  balenceStyle: { ...COMMON_STYLE.textStyle(10, COLORS.GRAY94) },
  balanceValueStyle: { ...COMMON_STYLE.textStyle(10, COLORS.WHITE) },
  orangeUsdValue: { ...COMMON_STYLE.textStyle(10, COLORS.YELLOWFB) },
  flatlistStyle: {
    borderRadius: Responsive.getWidth(5),
    borderColor: COLORS.GRAY41,
    borderWidth: 2,
    marginBottom: Responsive.getHeight(2),
  },
  sepratorStyle: { height: 1, backgroundColor: COLORS.LIGHT_OPACITY(0.1) },
  listSepratorStyle: { height: Responsive.getHeight(1) },
  tokenIconStyle: {
    ...COMMON_STYLE.imageStyle(5),
    marginRight: Responsive.getWidth(2),
  },
  segmentContainerStyle: segmentCount => {
    return {
      width: `${segmentCount * 20}%`,
    };
  },
  amountViewStyle: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    borderRadius: 25,
    alignItems: 'center',
    borderWidth: 2,
    borderColor: COLORS.GRAY1F,
  },
  amountTitleStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)),
  },
  inputContainerStyle: {
    flex: 1,
    borderWidth: 0,
  },
  inputViewStyle: {
    borderWidth: 0,
    height: '100%',
  },
  addressInputStyle: {
    marginBottom: Responsive.getHeight(3),
  },
  labelAndMax: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    marginBottom: Responsive.getHeight(1),
    marginTop: Responsive.getHeight(2),
  },
  maxBtnStyle: {
    width: '20%',
    height: 30,
    alignSelf: 'flex-end',
  },
  maxErrorMessageStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.ERROR),
  },
});
