import React, { useEffect, useState } from 'react';

import { View, Text, FlatList, TouchableOpacity } from 'react-native';

import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';

import { GlobalAction } from '@redux';

// import components
import {
  SafeAreaWrapper,
  StepsListView,
  QuoteCell,
  AlertActionModel,
  CachedImage,
} from '@components';

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

import * as Web3Layer from '@web3';

import { log } from '@utils';
import { getNetwork } from '../../../../redux';

const SwapTokenGetQuote = props => {
  const { navigation } = props;
  const params = props.route.params;

  const networks = useSelector(state => state.networks);
  const selectedNetwork = useSelector(getNetwork);
  const wdSupportObject = networks.wdSupportObject;
  const accountDetail = params.accountDetail;
  const [quoteList, setQuoteList] = useState([]);
  const [checkAllowanceObj, setCheckAllowanceObj] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [isShowApproveRequestPopup, setIsShowApproveRequestPopup] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      allowanceCheck();
    });

    return unsubscribe;
  }, []);

  const allowanceCheck = async () => {
    setIsLoading(true);
    const allowanceObj = await Web3Layer.checkAllowance(
      params.transferAmount,
      params.selectedFromToken.decimals,
      accountDetail.publicAddress,
      wdSupportObject.swapRouterV3,
      params.selectedFromToken.address,
      selectedNetwork,
    );
    setCheckAllowanceObj(allowanceObj);

    if (allowanceObj.success) {
      if (allowanceObj.status) {
        getQuotes();
      } else {
        setIsShowApproveRequestPopup(true);
        setIsLoading(false);
      }
    } else {
      setIsLoading(false);
      setError(allowanceObj.error);
    }
  };

  const getQuotes = async () => {
    try {
      const trsAmount = params.transferAmount.toPrecision(params.selectedFromToken.decimals);

      const parm = [
        wdSupportObject.swapRouterV3,
        params.selectedFromToken.address,
        params.selectedFromToken.decimals,
        trsAmount,
        params.selectedToToken.address,
        params.selectedToToken.decimals,
        selectedNetwork,
        params.slippage,
        accountDetail.publicAddress,
      ];
      const quoteObj = await Web3Layer.getQuotes(...parm);

      setIsLoading(false);
      if (quoteObj.success) {
        setQuoteList(quoteObj.quotes);
      } else {
        dispatch(GlobalAction.showAlert(localize('ERROR'), quoteObj.error));
      }
    } catch (e) {
      log('swap err >>', e);
    }
  };

  // render custom components

  const quoteListComponent = () => {
    return (
      !isLoading && checkAllowanceObj.status && (
        <React.Fragment>
          <Text
            style={ COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BASE', 'center') }>
            {localize('RECEIVE_TOKEN_MSG')}
          </Text>
          <Text style={ styles.swapAmountStyle }>
            {`${params.transferAmount.toString(6)} ${params.selectedFromToken.symbol} -> ${params.selectedToToken.symbol}`}
          </Text>
          <FlatList
            data={ quoteList }
            bounces={ false }
            renderItem={ ({ item, index }) => (
              <QuoteCell
                item={ item }
                index={ index }
                selectedFromToken={ params.selectedFromToken }
                selectedToToken={ params.selectedToToken }
                selectedNetwork={ selectedNetwork }
                onSelectQuote={ item => {
                  navigation.navigate('SWAP_TOKEN_CONFIRMATION_SCREEN', {
                    accountDetail: params.accountDetail,
                    quoteObj: item,
                    fromToken: params.selectedFromToken,
                    toToken: params.selectedToToken,
                    transferAmount: params.transferAmount,
                    isSupportUSD: params.isSupportUSD,
                    usdValue: params.usdValue,
                    slippage: params.slippage,
                    walletFee: item.walletFee,
                  });
                } }
              />
            ) }
            ItemSeparatorComponent={ () => <View style={ styles.sepratorStyle } /> }
            keyExtractor={ (_item, index) => index + '' }
          />
          <TouchableOpacity
            style={styles.feeFootnoteStyle}
            onPress={() => {
              dispatch(
                GlobalAction.showAlert(
                  localize('FEE_DETAILS_TITLE'),
                  localize('FEE_DETAILS').replace('{NATIVE_TOKEN}', selectedNetwork.sym),
                ),
              );
            }}
          >
            <Text style={ COMMON_STYLE.textStyle(10, COLORS.WHITE, 'BASE', 'center') }>
              {localize('FEE_FOOTNOTE')}
            </Text>
            <CachedImage
              source={ IMAGES.SUPPORT_ICON }
              style={ [COMMON_STYLE.imageStyle(5), { alignSelf: 'center', marginLeft: 10, marginTop: 0 }] }
            />
          </TouchableOpacity>
        </React.Fragment>
      )
    );
  };

  const renderLoadingView = () => {
    return (
      isLoading && (
        <View style={ styles.loadingContainerStyle }>
          <CachedImage source={ IMAGES.ROUNDED_LOADING } style={ styles.loadingStyle } />
        </View>
      )
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <StepsListView selectedSteps={ 2 } />
      {renderLoadingView()}
      {quoteListComponent()}
      <AlertActionModel
        alertTitle={ localize('APPROVE_REQUEST') }
        alertMsg={ localize('GIVE_ALLOWANCES_MSG', [
          params.selectedFromToken.symbol,
        ]) }
        isShowAlert={ isShowApproveRequestPopup }
        successBtnTitle={ localize('APPROVE') }
        cancelBtnTitle={ localize('REJECT') }
        onPressSuccess={ () => {
          setIsShowApproveRequestPopup(false);
          navigation.navigate('SWAP_APPROVE_CONFIRMATION_SCREEN', {
            accountDetail: params.accountDetail,
            selectedFromToken: params.selectedFromToken,
            selectedToToken: params.selectedToToken,
            transferAmount: params.transferAmount,
            isSupportUSD: params.isSupportUSD,
            usdValue: params.isSupportUSD ? params.usdValue : '',
            slippage: params.slippage,
          });
        } }
        onPressCancel={ () => {
          setIsShowApproveRequestPopup(false);
          navigation.goBack();
        } }>
      </AlertActionModel>
    </SafeAreaWrapper>
  );
};

SwapTokenGetQuote.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};

export default SwapTokenGetQuote;
