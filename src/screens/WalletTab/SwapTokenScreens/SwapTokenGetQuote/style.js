import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  swapAmountStyle: {
    ...COMMON_STYLE.textStyle(13, COLORS.YELLOWFB, 'BOLD', 'center'),
    marginBottom: Responsive.getHeight(1),
  },
  listheaderStyle: {
    width: '100%',
    backgroundColor: COLORS.GRAY27,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: Responsive.getHeight(1),
    paddingHorizontal: Responsive.getWidth(2),
  },
  headerReceiveStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.GRAYCA, 'BOLD', 'center'),
    width: '23%',
  },
  headerFeeStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.GRAYCA, 'BOLD', 'center'),
    width: '23%',
  },
  headerSourceStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.GRAYCA, 'BOLD', 'center'),
    width: '23%',
  },
  nextButtonStyle: { marginVertical: Responsive.getHeight(3) },
  loadingContainerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingStyle: { ...COMMON_STYLE.imageStyle(10) },
  sepratorStyle: { height: Responsive.getHeight(1.5) },
  feeFootnoteStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: Responsive.getHeight(2),
  }
});
