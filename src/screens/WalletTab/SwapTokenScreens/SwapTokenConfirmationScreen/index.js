import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import {
  GlobalAction,
  TransactionsAction,
  getNativeToken,
} from '@redux';

// import components
import {
  AlertActionModel,
  GasFeeRangeView,
  ModalContainerView,
  SafeAreaWrapper,
  StepsListView,
  CachedImage,
} from '@components';
import SwipeVerifyController from '@components/SwipeVerifyController';

import { IntegerUnits } from '@utils';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import { useTokenIcons } from '@utils';

import {
  WalletTransactionTypes,
  calculateEstimateFee,
  calculateMaxFee,
  createAndSendTxn,
  getSingleAccount,
  getSwapTxnObject,
  updateTxnLists,
} from '@web3';
import { getUserFriendlyErrorMessage } from '../../../../utils/errors';
import { getNetwork } from '../../../../redux';

const SwapTokenConfirmationScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const getTokenIcon = useTokenIcons();
  const networks = useSelector(state => state.networks);
  const wallet = useSelector(state => state.wallet);
  const transactions = useSelector(state => state.transactions);
  const selectedNetwork = useSelector(getNetwork);
  const wdSupportObject = networks.wdSupportObject;
  const [selectedRange, setSelectedRange] = useState(1);
  const [txnObject, setTxnObject] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [isShowGasFeeView, setIsShowGasFeeView] = useState(false);
  const [gasRange, setGasRange] = useState({});
  const [accountBalance, setAccountBalance] = useState(0);
  const [isGetTxnObject, setIsGetTxnObject] = useState(false);

  const [isShowError, setIsShowError] = useState(false);
  const [error, setError] = useState('');

  const accountDetail = params.accountDetail;
  const { toToken, fromToken, quoteObj, walletFee } = params;
  const nativeToken = useSelector(getNativeToken);
  const nativeUsdValue = nativeToken.usdPrice?.value;
  const walletFeeUsd = (nativeUsdValue === undefined || walletFee === undefined)
    ? undefined
    : `$${walletFee.multiply(nativeUsdValue).toString(2)}`;

  const dispatch = useDispatch();

  useEffect(() => {
    getTxnObject();
    getAccountBalance();
  }, []);

  // Action Methods
  const getAccountBalance = async () => {
    const balance = await getSingleAccount(
      accountDetail.publicAddress,
      selectedNetwork,
    );

    setAccountBalance(balance.account.value);
  };

  const switchToTokenApproval = () => {
    navigation.navigate('SWAP_APPROVE_CONFIRMATION_SCREEN', {
      accountDetail: params.accountDetail,
      selectedFromToken: params.fromToken,
      selectedToToken: params.toToken,
      transferAmount: params.transferAmount,
      isSupportUSD: params.isSupportUSD,
      usdValue: params.isSupportUSD
        ? params.usdValue
        : '',
    });
  }

  const getTxnObject = async () => {
    setIsLoading(true);

    const parm = [
      wdSupportObject.swapRouterV3,
      quoteObj,
      params.slippage,
      accountDetail.publicAddress,
      selectedNetwork,
    ];

    const txObject = await getSwapTxnObject(...parm);

    if (txObject.success) {
      setGasRange(txObject.range);
      setIsLoading(false);
      setTxnObject(txObject.txnObject);
      setIsGetTxnObject(true);
    } else if (txObject.error.includes('allowance')) {
      switchToTokenApproval();
    } else {
      navigation.navigate('ACCOUNT_DETAIL_SCREEN', { accountDetail });
      dispatch(GlobalAction.showAlert(localize('ERROR'), localize('VOLATILITY_ERROR')));
    }
  };

  const updateTxnObjectTypeTwo = (gas, maxPrioFee, maxFeePerGas) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.maxPriorityFeePerGas = maxPrioFee;
    tempTxObj.maxFeePerGas = maxFeePerGas;
    setTxnObject(tempTxObj);
  };

  const updateTxnObjectTypeOne = (gas, gasPrice) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.gasPrice = gasPrice;
    setTxnObject(tempTxObj);
  };

  const getEstimateFee = () => {
    if (!txnObject) return '';
    const estimateFee = calculateEstimateFee(txnObject, selectedNetwork.txnType);
    return `${estimateFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getMaxFee = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getTotal = () => {
    return `${params.transferAmount.toString(6)} ${fromToken.symbol} + ${getEstimateFee()}`;
  };

  const getMaxTotal = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${params.transferAmount.toString(6)} ${fromToken.symbol} + ${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const swapTokens = async () => {
    try {
      const txnResult = await createAndSendTxn(
        WalletTransactionTypes.SWAP,
        txnObject,
        wallet,
        accountDetail,
        accountBalance,
        selectedNetwork,
        {
          address: fromToken.address,
          transferAmount: params.transferAmount
        }
      );

      if (txnResult.success) {
        const { tempTxns, tempTxnsList } = await updateTxnLists(
          IntegerUnits.serialize({
            type: WalletTransactionTypes.SWAP,
            transactionDetail: {
              accountName: accountDetail.name,
              isSupportUSD: params.isSupportUSD,
              displayAmount: params.transferAmount,
              displayDecimal: fromToken.decimals,
              transferedAmount: params.transferAmount,
              usdValue: params.usdValue,
              fromAddress: accountDetail.publicAddress,
              networkDetail: selectedNetwork,
              fromToken: fromToken,
              toToken: toToken,
              walletFee: walletFee,
              utcTime: moment.utc().format(),
              quoteObj: quoteObj,
            },
            receipt: txnResult.receipt,
            blockNumber: txnResult.blockNumber,
          }),
          transactions,
          selectedNetwork
        );

        dispatch(TransactionsAction.saveTransactions(tempTxnsList));

        await StorageOperation.setMultiData([
          [ASYNC_KEYS.TRANSACTIONS, JSON.stringify(tempTxns)],
        ]);

        setIsLoading(false);
        dispatch(
          GlobalAction.showAlert(
            localize('SUCCESS'),
            localize('SUBMIT_SUCCESS'),
          ),
        );
        navigation.navigate('ACCOUNT_DETAIL_SCREEN', { accountDetail });
      } else {
        setIsLoading(false);
        if (txnResult.error.toLowerCase().includes('insufficient funds')) {
          setError(localize('INSUFFICIENT_FUNDS', [fromToken.symbol]));
        } else {
          setError(txnResult.error);
        }
        setIsShowError(true);
      }
    } catch (err) {
      console.error(err);
      setIsLoading(false);
    }
  }

  const slideToSwapTokens = () => {
    setIsLoading(true);
    setTimeout(swapTokens, 0);
  };

  // render custom component
  const renderFromTokenView = () => {
    return (
      <View style={ styles.fromViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('FROM:')}
        </Text>
        <View style={ styles.tokenViewStyle }>
          <CachedImage
            source={ getTokenIcon(fromToken.address, selectedNetwork.chainId) }
            style={ styles.tokenIconStyle }
          />
          <Text
            style={ [
              COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD'),
              { maxWidth: '30%' },
            ] }>
            {fromToken.symbol}
          </Text>

          <View style={ { flex: 1 } }>
            <Text
              style={ COMMON_STYLE.textStyle(
                13,
                COLORS.GRAYDF,
                'BOLD',
                'right',
              ) }>
              {fromToken.symbol + ' '}
            </Text>
            <Text
              numberOfLines={ 1 }
              style={ COMMON_STYLE.textStyle(
                11,
                COLORS.GRAY94,
                'BASE',
                'right',
              ) }>
              {localize('AMOUNT') + ' '}
              <Text
                style={ COMMON_STYLE.textStyle(
                  11,
                  COLORS.WHITE,
                  'BASE',
                  'right',
                ) }>
                {params.transferAmount.toString(6)}
              </Text>
            </Text>
          </View>
          <CachedImage
            source={ getTokenIcon(fromToken.address, selectedNetwork.chainId) }
            style={ styles.tokenIconStyle }
          />
        </View>
      </View>
    );
  };

  const renderToTokenView = () => {
    return (
      <View style={ styles.fromViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('TO:')}
        </Text>
        <View style={ styles.tokenViewStyle }>
          <CachedImage
            source={ getTokenIcon(toToken.address, selectedNetwork.chainId) }
            style={ styles.tokenIconStyle }
          />
          <Text
            style={ [
              COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD'),
              { maxWidth: '30%' },
            ] }>
            {toToken.symbol}
          </Text>

          <View style={ { flex: 1 } }>
            <Text
              style={ COMMON_STYLE.textStyle(
                13,
                COLORS.GRAYDF,
                'BOLD',
                'right',
              ) }>
              {toToken.symbol + ' '}
            </Text>
            <Text
              numberOfLines={ 1 }
              style={ COMMON_STYLE.textStyle(
                11,
                COLORS.GRAY94,
                'BASE',
                'right',
              ) }>
              {localize('EXPECTED') + ' '}
              <Text
                style={ COMMON_STYLE.textStyle(
                  11,
                  COLORS.WHITE,
                  'BASE',
                  'right',
                ) }>
                {quoteObj.amountOut.toString(6)}
              </Text>
            </Text>
          </View>
          <CachedImage
            source={ getTokenIcon(toToken.address, selectedNetwork.chainId) }
            style={ styles.tokenIconStyle }
          />
        </View>
      </View>
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <StepsListView selectedSteps={ 3 } />
      <View style={ { flex: 1 } }>
        <CachedImage source={ IMAGES.CONFIRMATION_BG } style={ styles.bgStyle } />

        <View style={ styles.detailContainerStyle }>
          <ScrollView bounces={ false }>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <Text style={ styles.confirmStyle }>
              {localize('CONFIRM_TRANSACTION_UPPERCASE')}
            </Text>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />

            <View style={ styles.accountDetailViewStyle }>
              <View style={ styles.lineViewStyle }>
                <CachedImage
                  source={ IMAGES.VERTICAL_DOTTET_LINE }
                  style={ styles.verticalLineStyle }
                />
                <View style={ styles.dotContainStyle }>
                  <View style={ styles.dotViewStyle } />
                  <CachedImage
                    style={ COMMON_STYLE.imageStyle(4) }
                    source={ IMAGES.DOWN_ARROW }
                  />
                  <View style={ styles.dotViewStyle } />
                </View>
              </View>

              <View style={ { flex: 1 } }>
                {renderFromTokenView()}
                {renderToTokenView()}
              </View>
            </View>
            <Text style={ styles.amountTitleStyle }>{localize('AMOUNT')}</Text>

            <View style={ styles.borderedViewStyle }>
              <View style={ styles.amountContainerViewStyle }>
                <Text style={ styles.amountStyle }>
                  {`${params.transferAmount.toString(6)} ${fromToken.symbol}`}
                </Text>
                <CachedImage
                  source={ getTokenIcon(
                    fromToken.address,
                    selectedNetwork.chainId,
                  ) }
                  style={ COMMON_STYLE.imageStyle(7) }
                />
              </View>

              {params.isSupportUSD && (
                <Text
                  style={ COMMON_STYLE.textStyle(
                    12,
                    COLORS.LIGHT_OPACITY(0.6),
                    'BASE',
                    'center',
                  ) }>
                  {`$${params.transferAmount.multiply(params.usdValue).toString(2)}`}
                </Text>
              )}
            </View>

            <View style={ styles.bottomBorderStyle }>
              <View style={ styles.rowViewStyle }>
                <Text
                  style={ styles.estimateStyle }
                  adjustsFontSizeToFit={ true }
                  numberOfLines={ 1 }>
                  {localize('ESTIMATE_GAS_FEE')}
                </Text>
                <TouchableOpacity
                  testID={ 'gas-fee-manage-btn' }
                  style={ [styles.estimateFeeBtnStyle] }
                  onPress={ () => {
                    if (isGetTxnObject) {
                      setIsShowGasFeeView(true);
                    }
                  } }>
                  <Text
                    style={ styles.estimateFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {getEstimateFee()}
                  </Text>
                  <CachedImage
                    style={ COMMON_STYLE.imageStyle(4, COLORS.YELLOWFB) }
                    source={ IMAGES.DOWN_ARROW }
                  />
                </TouchableOpacity>
              </View>
              <View style={ styles.rowViewStyle }>
                <Text style={ styles.secondTextStyle }>
                  {localize('likely_second')}
                </Text>
                {selectedNetwork.txnType == 2 && (
                  <Text
                    style={ styles.maxFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {localize('MAX_FEE:')}
                    <Text style={ styles.feeStyle }>{getMaxFee()}</Text>
                  </Text>
                )}
              </View>
            </View>
            <View style={ styles.bottomBorderStyle }>
              <View style={ styles.walletFeeViewStyle }>
                <Text style={ styles.estimateStyle }>
                  {localize('WALLET_FEE:')}
                </Text>
                <View style={styles.walletFeeRightStyle}>
                  {walletFeeUsd !== undefined && (
                    <Text
                      style={ styles.walletFeeUsdStyle }
                      adjustsFontSizeToFit={ true }
                      numberOfLines={ 1 }
                    >
                      {localize('VALUE')}:
                      {' '}
                      <Text
                        style={ styles.feeStyle }
                        adjustsFontSizeToFit={ true }
                        numberOfLines={ 1 }
                      >
                        {walletFeeUsd}
                      </Text>
                    </Text>
                  )}
                  <Text
                    style={ styles.totalValueStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {`${walletFee?.toString(6) ?? 0} ${selectedNetwork.sym}`}
                  </Text>
                </View>
              </View>
            </View>
            <View>
              <View style={ styles.rowViewStyle }>
                <Text style={ styles.estimateStyle }>{localize('TOTAL')}</Text>
                <Text
                  style={ styles.totalValueStyle }
                  adjustsFontSizeToFit={ true }
                  numberOfLines={ 1 }>
                  {getTotal()}
                </Text>
              </View>
              {selectedNetwork.txnType == 2 && (
                <View style={ styles.rowViewStyle }>
                  <Text
                    style={ styles.maxFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {localize('MAX_FEE:')}
                    <Text
                      style={ styles.feeStyle }
                      adjustsFontSizeToFit={ true }
                      numberOfLines={ 1 }>
                      {getMaxTotal()}
                    </Text>
                  </Text>
                </View>
              )}
            </View>
          </ScrollView>
        </View>
      </View>
      {!isLoading ? (
        <SwipeVerifyController
          style={ styles.swiperStyle }
          isSetReset={ !txnObject }
          onVerified={ () => slideToSwapTokens() }>
          <Text
            style={ [
              COMMON_STYLE.textStyle(14, COLORS.LIGHT_OPACITY(0.3), 'BOLD'),
            ] }>
            {localize('SLIDE_TO_SWAP_TOKEN_UPPERCASE')}
          </Text>
        </SwipeVerifyController>
      ) : (
        <CachedImage source={ IMAGES.ROUNDED_LOADING } style={ styles.loadingStyle } />
      )}
      <ModalContainerView
        testID={ 'modal-gas-fee-manage' }
        isVisible={ isShowGasFeeView }
        onPressClose={ () => setIsShowGasFeeView(false) }>
        <GasFeeRangeView
          selectedRange={ selectedRange }
          gasRange={ gasRange }
          txnObject={ txnObject }
          network={ selectedNetwork }
          onPressSave={ updatedObject => {
            setIsShowGasFeeView(false);

            if (selectedNetwork.txnType != 0) {
              updateTxnObjectTypeTwo(
                updatedObject.gas,
                updatedObject.maxPrioFee,
                updatedObject.maxFeePerGas,
              );
              setSelectedRange(selectedRange);
            } else {
              updateTxnObjectTypeOne(updatedObject.gas, updatedObject.gasPrice);
            }
          } }
        />
      </ModalContainerView>

      {/* Error Message Modal */}
      <AlertActionModel
        alertTitle={ localize('ERROR') }
        isShowAlert={ isShowError }
        successBtnTitle={ localize('OK') }
        onPressSuccess={ () => {
          setIsShowError(false);
          navigation.navigate('ACCOUNT_DETAIL_SCREEN', { accountDetail });
        } }>
        <Text
          style={ COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BASE', 'center') }>
          {getUserFriendlyErrorMessage(error)}
        </Text>
      </AlertActionModel>
    </SafeAreaWrapper>
  );
};

SwapTokenConfirmationScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default SwapTokenConfirmationScreen;
