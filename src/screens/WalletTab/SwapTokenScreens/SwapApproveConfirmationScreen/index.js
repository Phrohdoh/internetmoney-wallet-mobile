import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

// import components
import {
  AlertActionModel,
  CheckBox,
  CurrencyInputField,
  GasFeeRangeView,
  ModalContainerView,
  RoundedButton,
  SafeAreaWrapper,
  StepsListView,
  CachedImage,
} from '@components';

import SwipeVerifyController from '@components/SwipeVerifyController';

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

const MAX_256 = (2n ** 256n) - 1n;

// import Utils
import {
  IntegerUnits,
  displayAddress,
  useTokenIcons,
  sanitizeFloatingPointString,
} from '@utils';

import {
  WalletTransactionTypes,
  calculateEstimateFee,
  calculateMaxFee,
  createAndSendTxn,
  getSingleAccount,
  getTokenApproveTxnObj
} from '@web3';
import { getUserFriendlyErrorMessage } from '../../../../utils/errors';
import { getNetwork } from '../../../../redux';

const SwapApproveConfirmationScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const networks = useSelector(state => state.networks);
  const wallet = useSelector(state => state.wallet);
  const selectedNetwork = useSelector(getNetwork);
  const wdSupportObject = networks.wdSupportObject;
  const [selectedRange, setSelectedRange] = useState(1);
  const [txnObject, setTxnObject] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [isShowGasFeeView, setIsShowGasFeeView] = useState(false);
  const [isShowEditPopup, setIsShowEditPopup] = useState(false);
  const [gasRange, setGasRange] = useState({});
  const [accountBalance, setAccountBalance] = useState(new IntegerUnits(0));
  const [isSelectCustom, setIsSelectCustom] = useState(false);
  const [isShowError, setIsShowError] = useState(false);
  const [error, setError] = useState('');
  const [customAmountError, setCustomAmountError] = useState('');

  const [customAmount, setCustomAmount] = useState(params.transferAmount);
  const [customAmountText, setCustomAmountText] = useState(params.transferAmount.toString(6, false, true));
  // const [customAmountText, setCustomAmountText] = useState({
  //   id: 'custom-amount',
  //   value: params.transferAmount.toString(6),
  //   placeholder: '',
  //   extraProps: {
  //     keyboardType: 'numeric',
  //     isError: false,
  //     errorText: localize('CUSTOM_LIMIT_NOT_VALID'),
  //   },
  // });

  const accountDetail = params.accountDetail;
  const { selectedFromToken } = params;
  const getTokenIcon = useTokenIcons();

  const approvalAmount = isSelectCustom
    ? customAmount
    : new IntegerUnits(MAX_256, selectedFromToken.decimals);

  // Action Methods
  const getAccountBalance = async () => {
    const balance = await getSingleAccount(
      accountDetail.publicAddress,
      selectedNetwork,
    );

    if (balance.success) {
      setAccountBalance(balance.account.value);
    }
  };

  const getTxnObject = useCallback(async () => {
    setIsLoading(true);
    const tokenObj = await getTokenApproveTxnObj(
      wdSupportObject.swapRouterV3,
      accountDetail.publicAddress,
      selectedFromToken.address,
      selectedNetwork,
      approvalAmount,
    );

    setIsLoading(false);

    if (tokenObj.success) {
      setGasRange(tokenObj.range);
      setTxnObject(tokenObj.txnObject);
    }
  }, [approvalAmount.value]);

  useEffect(() => {
    getTxnObject();
    getAccountBalance();
  }, []);

  const updateTxnObjectTypeTwo = (gas, maxPrioFee, maxFeePerGas) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.maxPriorityFeePerGas = maxPrioFee;
    tempTxObj.maxFeePerGas = maxFeePerGas;
    setTxnObject(tempTxObj);
  };

  const updateTxnObjectTypeOne = (gas, gasPrice) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.gasPrice = gasPrice;
    setTxnObject(tempTxObj);
  };

  const getEstimateFee = () => {
    if (!txnObject) return '';
    const estimateFee = calculateEstimateFee(txnObject, selectedNetwork.txnType);
    return `${estimateFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getMaxFee = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const handleTextChange = useCallback(text => {
    text = sanitizeFloatingPointString(text);
    setCustomAmount(IntegerUnits.fromFloatingPoint(text).toPrecision(selectedFromToken.decimals));
    setCustomAmountText(text);
    setCustomAmountError('');
    return text;
  }, [setCustomAmount, setCustomAmountText]);

  const approveTokens = async () => {
    try {
      const txnResult = await createAndSendTxn(
        WalletTransactionTypes.SWAP,
        txnObject,
        wallet,
        accountDetail,
        accountBalance,
        selectedNetwork,
        {
          isApprove: true,
          address: selectedFromToken.address,
          transferAmount: params.transferAmount
        }
      );

      if (txnResult.success) {
        navigation.goBack();
      } else {
        setIsLoading(false);
        setError(txnResult.error);
        setIsShowError(true);
      }
    } catch (err) {
      console.error(err);
      setIsLoading(false);
    }
  };

  const slideToApproveTokens = () => {
    setIsLoading(true);
    setTimeout(approveTokens, 0);
  };

  const onPressSubmit = () => {
    if (isSelectCustom) {
      if (params.transferAmount.gt(customAmount)) {
        setCustomAmountError(localize('CUSTOM_LIMIT_NOT_VALID'));
        return;
      } else {
        getTxnObject();
      }
    } else {
      getTxnObject();
    }
    setIsShowEditPopup(false);
  };

  // render custom component
  const seprator = () => {
    return <View style={ styles.sepratorLineStyle } />;
  };

  const renderDetailComponent = (title, subTitle) => {
    return (
      <View>
        <Text style={ styles.detailTitleStyle }>{localize(title)}</Text>
        <Text style={ styles.detailSubtitleStyle }>{subTitle}</Text>
      </View>
    );
  };

  const renderEditPermissoinView = () => {
    return (
      <View>
        <Text style={ styles.spendLimitStyle }>
          {localize('SPEND_LIMIT_PERMISSON')}
        </Text>
        <Text style={ styles.allowAmountStyle }>{localize('ALLOW_AMOUNT')}</Text>
        {seprator()}
        <View style={ styles.limitContainerStyle }>
          <CheckBox
            checkboxStyle={ styles.checkBoxStyle }
            isSelected={ !isSelectCustom }
            onPress={ () => setIsSelectCustom(false) }
          />
          <View style={ { flex: 1 } }>
            <Text style={ styles.limitTitleStyle(!isSelectCustom) }>
              {localize('PROPOSED_LIMIT')}
            </Text>
            <Text style={ styles.limitSubtitleStyle }>
              {localize('SPEND_LIMIT_REQUEST')}
            </Text>
            <Text style={ styles.limitStyle }>
              {'∞ ' + selectedFromToken.symbol}
            </Text>
          </View>
        </View>
        {seprator()}
        <View style={ styles.limitContainerStyle }>
          <CheckBox
            checkboxStyle={ styles.checkBoxStyle }
            isSelected={ isSelectCustom }
            onPress={ () => setIsSelectCustom(true) }
          />
          <View style={ { flex: 1 } }>
            <Text style={ styles.limitTitleStyle(isSelectCustom) }>
              {localize('CUSTOM_SPEND')}
            </Text>
            <Text style={ styles.limitSubtitleStyle }>
              {localize('ENTER_MAX_SPEND')}
            </Text>
            <CurrencyInputField
              testID={ 'swap-approve-screen-custom-amount' }
              inputContainerStyle={ styles.inputContainerStyle }
              value={ customAmountText }
              // title={ localize(state.title) }
              isDisable={ !isSelectCustom }
              placeholder=""
              onChangeText={ handleTextChange }
              editable={ isSelectCustom }
              keyboardType="numeric"
              isError={ customAmountError !== '' }
              errorText={customAmountError}
              decimalsLimit={ selectedFromToken.decimals }
            />
          </View>
        </View>
        <View style={ styles.btnContainerStyle }>
          <RoundedButton
            style={ styles.btnStyle }
            title={ localize('SUBMIT_UPPERCASE') }
            onPress={ () => onPressSubmit() }
          />
        </View>
      </View>
    );
  };
  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <StepsListView selectedSteps={ 2 } />
      <View style={ { flex: 1 } }>
        <CachedImage source={ IMAGES.CONFIRMATION_BG } style={ styles.bgStyle } />

        <View style={ styles.detailContainerStyle }>
          <ScrollView bounces={ false }>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <Text style={ styles.confirmStyle }>
              {localize('APPROVE_TRANSACTION_UPPERCASE')}
            </Text>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />

            <Text style={ styles.permissionMsgStyle }>
              {localize('GIVE_PERMISSION_MSG', [selectedFromToken.symbol])}
            </Text>
            <Text style={ styles.grantMsgStyle }>{localize('GRANT_MSG')}</Text>
            <View style={ styles.approveTokenViewStyle }>
              <Text style={ styles.accountNameStyle }>
                {localize('CONTRACT')}
              </Text>
              <Text numberOfLines={ 2 } style={ styles.addressStyle }>
                {`${localize('IM_SWAP_ROUTER')} ${displayAddress(wdSupportObject.swapRouterV3)}`}
              </Text>
            </View>

            <RoundedButton
              testID="max-button"
              style={ styles.editBtnStyle }
              isBordered={ true }
              title={ localize('EIDT_PERMISSON') }
              onPress={ () => setIsShowEditPopup(true) }
            />

            {seprator()}
            <Text style={ styles.amountTitleStyle }>
              {localize('APPROVAL_AMOUNT')}
            </Text>

            <View style={ styles.borderedViewStyle }>
              <View style={ styles.amountContainerViewStyle }>
                <Text style={ styles.amountStyle }>
                  {!isSelectCustom
                    ? '∞' + ` ${selectedFromToken.symbol}`
                    : `${approvalAmount.toString(6)} ${selectedFromToken.symbol}`}
                </Text>
                <CachedImage
                  source={ getTokenIcon(
                    selectedFromToken.address,
                    selectedNetwork.chainId,
                  ) }
                  style={ COMMON_STYLE.imageStyle(7) }
                />
              </View>
            </View>

            <View style={ styles.bottomBorderStyle }>
              <View style={ styles.rowViewStyle }>
                <Text
                  style={ styles.estimateStyle }
                  adjustsFontSizeToFit={ true }
                  numberOfLines={ 1 }>
                  {localize('ESTIMATE_GAS_FEE')}
                </Text>
                <TouchableOpacity
                  testID={ 'gas-fee-manage-btn' }
                  style={ [styles.estimateFeeBtnStyle] }
                  onPress={ () => setIsShowGasFeeView(true) }>
                  <Text
                    style={ styles.estimateFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {getEstimateFee()}
                  </Text>
                  <CachedImage
                    style={ COMMON_STYLE.imageStyle(4, COLORS.YELLOWFB) }
                    source={ IMAGES.DOWN_ARROW }
                  />
                </TouchableOpacity>
              </View>

              <View style={ styles.rowViewStyle }>
                <Text style={ styles.secondTextStyle }>
                  {localize('likely_second')}
                </Text>
                {selectedNetwork.txnType == 2 && (
                  <Text
                    style={ styles.maxFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {localize('MAX_FEE:')}
                    <Text style={ styles.feeStyle }>{getMaxFee()}</Text>
                  </Text>
                )}
              </View>
            </View>
            <View style={ styles.rowViewStyle }>
              <Text
                style={ styles.estimateStyle }
                adjustsFontSizeToFit={ true }
                numberOfLines={ 1 }>
                {localize('PERMISSON_REQUEST')}
              </Text>
            </View>
            <Text style={ styles.detailTitleStyle }>
              {localize('SWAP_PERMISSION_INFO')}
            </Text>
            {renderDetailComponent(
              'APPROVED_AMOUNT',
              !isSelectCustom
                ? '∞' + ` ${selectedFromToken.symbol}`
                : `${approvalAmount.toString(6)} ${selectedFromToken.symbol}`,
            )}
            {renderDetailComponent(
              'GRANTED_TO',
              `${localize('CONTRACT')} (${wdSupportObject.swapRouterV3})`,
            )}
            {seprator()}
            <Text style={ styles.dataTitleStyle }>{localize('DATA')}</Text>
            {renderDetailComponent('FUNCTION', localize('APPROVE'))}
            {renderDetailComponent('', txnObject?.data)}
            {seprator()}
          </ScrollView>
        </View>
      </View>
      {!isLoading ? (
        <SwipeVerifyController
          style={ styles.swiperStyle }
          isSetReset={ !txnObject }
          onVerified={ () => slideToApproveTokens() }>
          <Text
            style={ [
              COMMON_STYLE.textStyle(14, COLORS.LIGHT_OPACITY(0.3), 'BOLD'),
            ] }>
            {localize('SLIDE_TO_APPROVE_UPPERCASE')}
          </Text>
        </SwipeVerifyController>
      ) : (
        <CachedImage source={ IMAGES.ROUNDED_LOADING } style={ styles.loadingStyle } />
      )}
      <ModalContainerView
        testID={ 'modal-gas-fee-manage' }
        isVisible={ isShowGasFeeView }
        onPressClose={ () => setIsShowGasFeeView(false) }>
        <GasFeeRangeView
          selectedRange={ selectedRange }
          gasRange={ gasRange }
          txnObject={ txnObject }
          network={ selectedNetwork }
          onPressSave={ updatedObject => {
            setIsShowGasFeeView(false);

            if (selectedNetwork.txnType != 0) {
              updateTxnObjectTypeTwo(
                updatedObject.gas,
                updatedObject.maxPrioFee,
                updatedObject.maxFeePerGas,
              );
              setSelectedRange(selectedRange);
            } else {
              updateTxnObjectTypeOne(updatedObject.gas, updatedObject.gasPrice);
            }
          } }
        />
      </ModalContainerView>
      <ModalContainerView
        testID={ 'edit-permission-popup' }
        isVisible={ isShowEditPopup }>
        {renderEditPermissoinView()}
      </ModalContainerView>

      {/* Error Message Modal */}
      <AlertActionModel
        alertTitle={ localize('ERROR') }
        isShowAlert={ isShowError }
        successBtnTitle={ localize('OK') }
        onPressSuccess={ () => {
          setIsShowError(false);
          navigation.navigate('ACCOUNT_DETAIL_SCREEN', { accountDetail });
        } }>
        <Text
          style={ COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BASE', 'center') }>
          {getUserFriendlyErrorMessage(error)}
        </Text>
      </AlertActionModel>
    </SafeAreaWrapper>
  );
};

SwapApproveConfirmationScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default SwapApproveConfirmationScreen;
