import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,

  selectedNetworkStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  networkNameStyle: {
    ...COMMON_STYLE.textStyle(13, COLORS.WHITE, 'BOLD', 'center'),
  },

  bgStyle: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
  },
  swiperStyle: {
    marginVertical: Responsive.getHeight(2),
    marginHorizontal: Responsive.getWidth(5),
  },
  confirmStyle: {
    ...COMMON_STYLE.textStyle(16, COLORS.WHITE, 'BOLD', 'center'),
    marginVertical: Responsive.getHeight(0.5),
  },

  detailContainerStyle: {
    left: '12%',
    right: '12%',
    top: '6%',
    bottom: '5%',
    position: 'absolute',
    alignSelf: 'center',
    paddingTop: Responsive.getHeight(1.5),
  },
  dottedLineStyle: {
    width: '100%',
    height: 1,
    marginVertical: Responsive.getHeight(0.5),
  },
  permissionMsgStyle: {
    ...COMMON_STYLE.textStyle(18, COLORS.WHITE, 'BOLD', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  grantMsgStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.WHITE, 'BASE', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  editBtnStyle: { width: '80%', height: 30 },
  approveTokenViewStyle: {
    height: Responsive.getHeight(10),
    borderRadius: Responsive.getHeight(3),
    borderColor: COLORS.GRAY27,
    borderWidth: 1,
    marginVertical: Responsive.getHeight(2),
    justifyContent: 'center',
    paddingHorizontal: Responsive.getWidth(5),
  },
  accountNameStyle: { ...COMMON_STYLE.textStyle(13, COLORS.WHITE, 'BOLD') },
  addressStyle: { ...COMMON_STYLE.textStyle(10, COLORS.YELLOWFB, 'BOLD') },
  amountTitleStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  sepratorLineStyle: {
    height: 1,
    backgroundColor: COLORS.GRAY27,
    marginVertical: Responsive.getHeight(1),
  },

  borderedViewStyle: {
    borderBottomWidth: 1,
    borderColor: COLORS.GRAY27,
    paddingBottom: Responsive.getHeight(1.3),
  },
  amountContainerViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    flexDirection: 'row',
  },

  amountStyle: {
    ...COMMON_STYLE.textStyle(24, COLORS.GRAYDF, 'BOLD', 'center'),
    marginRight: Responsive.getWidth(2),
    maxWidth: '90%',
  },
  rowViewStyle: {
    flexDirection: 'row',
    flex: 1,
    marginTop: Responsive.getHeight(1),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  estimateStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.WHITE, 'BOLD'),
  },
  estimateFeeStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.YELLOWFB, 'BOLD', 'right'),
    flex: 1,
  },
  secondTextStyle: { ...COMMON_STYLE.textStyle(10, COLORS.GREEN6D) },
  maxFeeStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'right'),
    flex: 1,
    marginLeft: Responsive.getWidth(2),
  },
  feeStyle: { ...COMMON_STYLE.textStyle(10, COLORS.GRAYDF, 'BOLD') },
  totalValueStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BOLD'),
    marginLeft: Responsive.getWidth(2),
  },
  bottomBorderStyle: {
    borderBottomWidth: 1,
    borderColor: COLORS.GRAY27,
    paddingBottom: Responsive.getHeight(2),
    flex: 1,
  },
  loadingStyle: {
    ...COMMON_STYLE.imageStyle(10),
    alignSelf: 'center',
    marginVertical: Responsive.getHeight(2),
  },
  estimateFeeBtnStyle: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginLeft: Responsive.getWidth(2),
  },
  walletFeeViewStyle: {
    flexDirection: 'row',
    flex: 1,
    marginTop: Responsive.getHeight(1),
    paddingVertical: Responsive.getHeight(1),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  accountBalanceViewStyle: {
    flex: 1,
    alignItems: 'flex-end',
  },

  dataTitleStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.WHITE, 'BOLD'),
    marginTop: Responsive.getHeight(1.5),
  },
  detailTitleStyle: {
    ...COMMON_STYLE.textStyle(9, COLORS.LIGHT_OPACITY(0.6)),
    marginTop: Responsive.getHeight(1.5),
  },
  detailSubtitleStyle: {
    ...COMMON_STYLE.textStyle(9, COLORS.WHITE, 'BOLD'),
  },
  spendLimitStyle: {
    ...COMMON_STYLE.textStyle(18, COLORS.WHITE, 'BOLD', 'center'),
    marginBottom: Responsive.getHeight(2),
  },
  allowAmountStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BASE', 'center'),
    marginBottom: Responsive.getHeight(2),
  },
  limitContainerStyle: {
    flexDirection: 'row',
  },
  checkBoxStyle: { marginRight: Responsive.getWidth(3) },

  limitTitleStyle: isSelected => {
    return {
      ...COMMON_STYLE.textStyle(
        16,
        isSelected ? COLORS.YELLOWFB : COLORS.LIGHT_OPACITY(0.6),
        'BOLD',
      ),
      marginBottom: Responsive.getHeight(0.5),
    };
  },
  limitSubtitleStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.LIGHT_OPACITY(0.6), 'BASE'),
    marginBottom: Responsive.getHeight(0.5),
  },
  limitStyle: {
    ...COMMON_STYLE.textStyle(15, COLORS.WHITE, 'BOLD'),
    marginBottom: Responsive.getHeight(0.5),
  },
  inputContainerStyle: { marginTop: Responsive.getHeight(2) },

  btnContainerStyle: {
    flexDirection: 'row',
    marginVertical: Responsive.getHeight(3),
    justifyContent: 'space-between',
  },
  btnStyle: { width: '100%' },
});
