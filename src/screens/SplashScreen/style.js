import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  containerStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: COLORS.BLACK,
    marginHorizontal: 0,
  },
  welcomeTitleStyle: {
    ...COMMON_STYLE.textStyle(24, COLORS.WHITE, 'BASE', 'center'),
    marginBottom: Responsive.getHeight(2),
  },
  internetMoneyStyle: {
    ...COMMON_STYLE.textStyle(56, COLORS.YELLOWFB, 'PIX_BOY', 'center'),
    marginBottom: Responsive.getHeight(1),
  },
  splashMsgStyle: {
    ...COMMON_STYLE.textStyle(15, COLORS.LIGHT_OPACITY(0.6), 'BOLD', 'center'),
    marginTop: Responsive.getHeight(2),
    marginBottom: Responsive.getHeight(8),
    textTransform: 'uppercase',
  },
  onboardLogoStyle: {
    width: '100%',
    height: Responsive.getHeight(45),
    resizeMode: 'cover',
  },
});
