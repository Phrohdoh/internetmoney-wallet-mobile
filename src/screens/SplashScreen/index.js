import React, { useEffect } from 'react';

import { Text } from 'react-native';

import PropTypes from 'prop-types';

import { useDispatch } from 'react-redux';
import { NetworksAction } from '@redux';

// import components
import { SafeAreaWrapper, CachedImage } from '@components';

// import constants
import { ASYNC_KEYS, CONFIG } from '@constants';

// import themes
import { IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

// import storage functions
import { StorageOperation } from '@storage';

// import style
import { styles } from './style';

import { parseObject } from '@utils';
import JailMonkey from 'jail-monkey';
import Logger from '../../utils/logger';

const SplashScreen = props => {
  const { navigation } = props;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(NetworksAction.initNetwork());
    checkLoginAccountInfo();
  });

  const checkLoginAccountInfo = async () => {

    if (CONFIG.ENABLE_JAILBREAK_CHECK && JailMonkey.isJailBroken()) {

      navigation.reset({
        index: 0,
        routes: [
          {
            name: 'JAILBREAK_ERROR_SCREEN',
          },
        ],
      });
      return;
    }

    const { data, error } = await StorageOperation.getMultiData([
      ASYNC_KEYS.IS_LOGIN,
      ASYNC_KEYS.WALLET_DETAIL,
      ASYNC_KEYS.IS_SET_PASSCODE,
    ]);

    Logger.debug('Obtained wallet detail data from storage on the Splash Screen.', {
      error,
      typeOfData: typeof data,
    });

    /* istanbul ignore else */
    if (!error) {
      if (data[0][1] === 'true') {
        const parseObj = parseObject(data[1][1]);
        if (parseObj.success) {
          Logger.debug('Parsed wallet detail from storage data on the Splash Screen.', {
            typeOfWalletDetail: typeof parseObj.data,
            typeOfWalletObject: typeof parseObj.data.walletObject,
          });
          navigation.reset({
            index: 0,
            routes: [
              {
                name: 'LOGIN_PASSWORD_VERIFICATION_SCREEN',
                params: {
                  walletDetail: parseObj.data,
                  isSetPasscode: data[2][1] === 'true' ? true : false,
                },
              },
            ],
          });
        }
        return;
      }
    }
    navigation.navigate('START_SCREEN');
  };

  return (
    <SafeAreaWrapper containerStyle={ styles.containerStyle }>
      <Text style={ styles.welcomeTitleStyle }>{localize('WELCOME_TITLE')}</Text>
      <Text style={ styles.internetMoneyStyle }>{localize('INTERNET')}</Text>
      <Text style={ styles.internetMoneyStyle }>{localize('MONEY')}</Text>
      <Text style={ styles.splashMsgStyle }>{localize('SPLASH_MSG')}</Text>
      <CachedImage source={ IMAGES.onboardLogo } style={ styles.onboardLogoStyle } />
    </SafeAreaWrapper>
  );
};

SplashScreen.propTypes = { navigation: PropTypes.any };
export default SplashScreen;
