import React, { useState } from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import PropTypes from 'prop-types';

// import components
import { SafeAreaWrapper, ModalContainerView, CachedImage } from '@components';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

import { COMMON_STYLE, IMAGES } from '@themes';

const ChangeLanguageScreen = props => {
  const { navigation } = props;
  const [isShowLanguageList, setIsShowLanguageList] = useState(false);
  const languages = localize('LANGUAGE_LIST');
  const [selectedLanguage, setSelectedLanguage] = useState(languages[0]);

  // render custom component
  const renderLanguageSelectionView = () => {
    return (
      <TouchableOpacity
        testID={ 'general-setting-choose-language-button' }
        style={ styles.languageVieStyle }
        onPress={ () => setIsShowLanguageList(true) }>
        <Text numberOfLines={ 1 } style={ styles.selectedLanguageStyle }>
          {selectedLanguage.title}
        </Text>
        <CachedImage source={ IMAGES.DOWN_ARROW } style={ COMMON_STYLE.imageStyle(6) } />
      </TouchableOpacity>
    );
  };

  const renderLanguageListCell = (item, index) => {
    return (
      <TouchableOpacity
        testID={ `general-setting-language-list-cell-button-${index}` }
        style={ styles.languageVieStyle }
        onPress={ () => {
          setIsShowLanguageList(false);
          setSelectedLanguage(item);
          navigation.goBack();
        } }>
        <Text numberOfLines={ 1 } style={ styles.selectedLanguageStyle }>
          {item.title}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderTitleView = (title, subtitle) => {
    return (
      <View>
        <Text style={ styles.titleStyle }>{title}</Text>
        <Text style={ styles.subtitleStyle }>{subtitle}</Text>
      </View>
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <View style={ COMMON_STYLE.borderedContainerViewStyle() }>
        {renderTitleView(
          localize('CURRENT_LANGUAGE_UPPERCASE'),
          localize('CHOOSE_LANGUAGE_MSG'),
        )}
        {renderLanguageSelectionView()}
      </View>
      <ModalContainerView
        testID={ 'modal-send-token' }
        isVisible={ isShowLanguageList }
        onPressClose={ () => setIsShowLanguageList(false) }>
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('CHANGE_LANGUAGE_UPPERCASE')}
          </Text>
          <ScrollView
            horizontal
            contentContainerStyle={ { width: '100%' } }
            scrollEnabled={ false }>
            <FlatList
              data={ languages }
              bounces={ false }
              renderItem={ ({ item, index }) =>
                renderLanguageListCell(item, index)
              }
              ItemSeparatorComponent={ () => (
                <View style={ styles.listSepratorStyle } />
              ) }
              keyExtractor={ (item, index) => index + item }
            />
          </ScrollView>
        </React.Fragment>
      </ModalContainerView>
    </SafeAreaWrapper>
  );
};

ChangeLanguageScreen.propTypes = { navigation: PropTypes.any };
export default ChangeLanguageScreen;
