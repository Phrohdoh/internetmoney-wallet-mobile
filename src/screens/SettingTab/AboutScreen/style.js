import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,

  titleStyle: {
    ...COMMON_STYLE.textStyle(16, COLORS.YELLOWFB, 'BOLD'),
    marginTop: Responsive.getHeight(2),
  },
  subtitleStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6)),
    marginVertical: Responsive.getHeight(1),
  },

  aboutCellViewStyle: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    borderRadius: 25,
    alignItems: 'center',
    borderWidth: 2,
    borderColor: COLORS.GRAY1F,
    paddingHorizontal: Responsive.getWidth(4),
    marginTop: Responsive.getHeight(2),
  },
  selectedLanguageStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.WHITE),
    flex: 1,
  },
  listSepratorStyle: { height: Responsive.getHeight(1) },
  themeContainerStyle: {
    flexDirection: 'row',
    marginTop: Responsive.getHeight(1),
  },
  themeViewStyle: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  themeTitleStyle: isSelected => {
    return {
      ...COMMON_STYLE.textStyle(
        12,
        isSelected ? COLORS.WHITE : COLORS.LIGHT_OPACITY(0.6),
      ),
      marginLeft: Responsive.getWidth(2),
    };
  },
});
