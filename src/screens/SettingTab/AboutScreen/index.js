import React from 'react';
import { View, Text, FlatList } from 'react-native';
import PropTypes from 'prop-types';

// import components
import { SafeAreaWrapper } from '@components';

// import languages
import { localize } from '@languages';

// import DeviceInfo from 'react-native-device-info';

// import style
import { COMMON_STYLE } from '@themes';
import { styles } from './style';

const AboutScreen = () => {
  const aboutData = [{ title: localize('APP_VERSION'), subtitle: APP_VERSION }];

  const renderAboutListCell = (item, index) => (
    <View testID={`general-setting-about-list-cell-button-${index}`}>
      <Text style={styles.titleStyle}>{item.title}</Text>
      <Text style={styles.subtitleStyle}>{item.subtitle}</Text>
    </View>
  );
  return (
    <SafeAreaWrapper containerStyle={[COMMON_STYLE.tabContainerStyle]}>
      <View style={COMMON_STYLE.borderedContainerViewStyle()}>
        <FlatList
          data={aboutData}
          renderItem={({ item, index }) => renderAboutListCell(item, index)}
        />
      </View>
    </SafeAreaWrapper>
  );
};

AboutScreen.propTypes = { navigation: PropTypes.any };
export default AboutScreen;
