import React from 'react';
import { Text, ScrollView } from 'react-native';
import PropTypes from 'prop-types';

// import components
import { SafeAreaWrapper, RoundedButton } from '@components';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';
import { styles } from './style';

const SecretRecoveryPhraseScreen = (props) => {
  const { navigation } = props;

  return (
    <SafeAreaWrapper>
      <ScrollView bounces={false}>
        <Text style={COMMON_STYLE.infoTitleStyle()}>
          {localize('SEED_PHRASE')}
        </Text>
        <Text style={COMMON_STYLE.infoDescStyle(COLORS.LIGHT_OPACITY(0.6))}>
          {localize('RECOVERY_PHRASE_MSG')}
        </Text>
        <Text style={COMMON_STYLE.scamAlertMsgStyle}>
          {localize('SEED_PHRASE_SCAM_MSG_UPPERCASE')}
        </Text>
      </ScrollView>

      <RoundedButton
        testID="secret-recovery-reveal-button"
        style={COMMON_STYLE.roundedBtnMargin()}
        title={localize('REVEAL_SEED_PHRASE_UPPERCASE')}
        onPress={() => navigation.navigate('SETTING_RECOVERY_PASSWORD_VERIFICATION_SCREEN')}
      />
    </SafeAreaWrapper>
  );
};

SecretRecoveryPhraseScreen.propTypes = { navigation: PropTypes.any };
export default SecretRecoveryPhraseScreen;
