/* istanbul ignore file */
import React, { useState } from 'react';
import {
  TouchableOpacity,
  FlatList,
  View,
  Text,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import PropTypes from 'prop-types';

import { useSelector, useDispatch } from 'react-redux';
import {
  WalletConnectAction,
  getAccountList,
  getSelectedWalletConnectAccount,
} from '@redux';

// import components
import {
  SafeAreaWrapper,
  TitleTextInput,
  RoundedButton,
  QRScannerModal,
  ReedemAccountListCell,
  ModalContainerView,
  CachedImage,
} from '@components';

import { ASYNC_KEYS } from '@constants';

// import languages
import { localize } from '@languages';

// import storage functions
import { StorageOperation } from '@storage';

import { useWalletConnect } from '@contexts/WalletConnectContext';
import { COMMON_STYLE, IMAGES, COLORS } from '@themes';
import { styles } from './style';
import { pair, unpair } from '../../../utils/walletConnect';

const WalletConnectScreen = () => {
  const dispatch = useDispatch();

  const accountList = useSelector(getAccountList);
  const selectedAccount = useSelector(getSelectedWalletConnectAccount);
  const { sessions } = useWalletConnect();
  const [isValidatingAddress, setIsValidatingAddress] = useState(false);
  const [isShowQrSacnner, setIsShowQrSacnner] = useState(false);
  const [isShowAccountList, setIsShowAccountList] = useState(false);

  const [walletAddress, onChangeWalletAddress] = useState({
    id: 'wallet-address',
    value: '',
    placeholder: 'ENTER_WALLET_URL',
  });

  // Action Methods
  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.value = text;
    setFunction(tempDict);
  };

  const _selecteAccount = async item => {
    setIsShowAccountList(false);
    await StorageOperation.setData(
      ASYNC_KEYS.WALLET_CONNECTED_ACCOUNT,
      JSON.stringify(item.publicAddress),
    );
    dispatch(WalletConnectAction.updateSelectedAccount(item.publicAddress));
  };

  const onPaste = async uri => {
    setIsValidatingAddress(true);
    await pair(uri);
    setIsValidatingAddress(false);
    changeData('', walletAddress, onChangeWalletAddress);
  };

  // Custom Render Components
  const renderSelectedAccountCell = () => {
    return (
      <TouchableOpacity
        testID={ 'wallet-connect-choose-account-button' }
        style={ styles.accountListCellStyle }
        onPress={ () => setIsShowAccountList(true) }>
        <View style={ styles.accountDetailStyle }>
          <Text numberOfLines={ 1 } style={ styles.accountNameStyle }>
            {selectedAccount?.name}
          </Text>
          <Text numberOfLines={ 1 } style={ styles.accountAddressStyle }>
            {selectedAccount?.publicAddress}
          </Text>
        </View>
        <CachedImage source={ IMAGES.DOWN_ARROW } style={ COMMON_STYLE.imageStyle(5) } />
      </TouchableOpacity>
    );
  };
  const renderQRButton = () => {
    return (
      <TouchableOpacity
        testID="import-wallet-add-button"
        onPress={ () => setIsShowQrSacnner(true) }>
        <CachedImage source={ IMAGES.QR_ICON } style={ COMMON_STYLE.imageStyle(7) } />
      </TouchableOpacity>
    );
  };

  const renderLoading = () => {
    return <ActivityIndicator />;
  };
  const renderInputComponent = (state, setStateFunction) => {
    return (
      <TitleTextInput
        testID={ `send-token-screen-${state.id}` }
        inputViewStyle={ styles.inputViewStyle }
        value={ state.value }
        title={ localize(state.title) }
        placeholder={ localize(state.placeholder) }
        onChangeText={ text => changeData(text, state, setStateFunction) }
        rightComponent={
          isValidatingAddress ? renderLoading() : renderQRButton()
        }
        onEndEditing={ text => onPaste(text.nativeEvent.text) }
        { ...state.extraProps }
      />
    );
  };

  const renderConnectionCell = item => {
    return (
      <View style={ styles.connectionListCellStyle }>
        <CachedImage
          style={ styles.originIconStyle }
          source={ { uri: item.peerMeta.icons?.[0] } }
        />
        <Text
          style={ [COMMON_STYLE.textStyle(12, COLORS.WHITE), { flex: 1 }] }
          numberOfLines={ 1 }>
          {item.peerMeta.name}
        </Text>
        <RoundedButton
          style={ styles.connectBtnStyle }
          title={ localize('DISCONNECT') }
          onPress={ () => {
            unpair(item.topic)
          } }
        />
      </View>
    );
  };

  const renderSessionCell = item => {
    return (
      <View style={ styles.connectionListCellStyle }>
        <CachedImage
          style={ styles.originIconStyle }
          source={ { uri: item.peer.metadata.icons?.[0] } }
        />
        <Text
          style={ [COMMON_STYLE.textStyle(12, COLORS.WHITE), { flex: 1 }] }
          numberOfLines={ 1 }>
          {item.peer.metadata.name}
        </Text>
        <RoundedButton
          style={ styles.connectBtnStyle }
          title={ localize('DISCONNECT') }
          onPress={ () => {
            unpair(item.topic)
          } }
        />
      </View>
    );
  };

  return (
    <SafeAreaWrapper>
      {renderSelectedAccountCell()}
      {renderInputComponent(walletAddress, onChangeWalletAddress)}
      <FlatList
        keyExtractor={ (item) => item.topic }
        style={ styles.flatlistStyle }
        data={ sessions.filter(session => !session.disconnected) }
        bounces={ false }
        renderItem={ ({ item }) => renderSessionCell(item) }
        ItemSeparatorComponent={ () => <View style={ styles.listSepratorStyle } /> }
      />
      <QRScannerModal
        isVisible={ isShowQrSacnner }
        scanedValue={ value => {
          setIsShowQrSacnner(false);
          changeData(value, walletAddress, onChangeWalletAddress);
          onPaste(value);
        } }
        onPressClose={ () => setIsShowQrSacnner(false) }
      />
      <ModalContainerView
        testID={ 'modal-send-token' }
        isVisible={ isShowAccountList }
        onPressClose={ () => setIsShowAccountList(false) }>
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('CHOOSE_ACCOUNTS_UPPERCASE')}
          </Text>
          <ScrollView
            horizontal
            contentContainerStyle={ { width: '100%' } }
            scrollEnabled={ false }>
            <FlatList
              key={ 'account_list' }
              data={ accountList }
              bounces={ false }
              renderItem={ ({ item, index }) => (
                <ReedemAccountListCell
                  item={ item }
                  index={ index }
                  onSelectAccount={ (item, index) => {
                    _selecteAccount(item, index);
                  } }
                />
              ) }
              ItemSeparatorComponent={ () => (
                <View style={ styles.accountListSepratorStyle } />
              ) }
              keyExtractor={ (item, index) => index + item }
            />
          </ScrollView>
        </React.Fragment>
      </ModalContainerView>
    </SafeAreaWrapper>
  );
};
WalletConnectScreen.propTypes = { navigation: PropTypes.any };
export default WalletConnectScreen;
