import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  accountListCellStyle: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    borderRadius: 25,
    alignItems: 'center',
    borderWidth: 2,
    borderColor: COLORS.GRAY1F,
    paddingHorizontal: Responsive.getWidth(4),
    marginVertical: Responsive.getHeight(2),
  },
  accountDetailStyle: { flex: 1, marginRight: Responsive.getWidth(2) },
  accountNameStyle: { ...COMMON_STYLE.textStyle(10, COLORS.WHITE, 'BOLD') },
  accountAddressStyle: {
    ...COMMON_STYLE.textStyle(8, COLORS.YELLOWFB, 'BOLD'),
    width: '70%',
  },
  accountListSepratorStyle: { height: Responsive.getHeight(1) },

  inputViewStyle: {
    borderColor: COLORS.GRAY1F,
    marginBottom: Responsive.getHeight(3),
  },
  connectionListCellStyle: {
    flexDirection: 'row',
    height: Responsive.getHeight(6),
    width: '100%',
    alignItems: 'center',
    paddingHorizontal: Responsive.getWidth(3),
  },
  connectBtnStyle: {
    height: '60%',
    width: '35%',
    marginLeft: Responsive.getWidth(4),
  },
  transListCellStyle: {
    padding: Responsive.getWidth(3),
    borderRadius: Responsive.getHeight(3),
    borderWidth: 1,
    borderColor: COLORS.GRAY1F,
  },
  flatlistStyle: {
    borderRadius: Responsive.getWidth(5),
    backgroundColor: COLORS.GRAY15,
    borderColor: COLORS.GRAY1F,
    borderWidth: 2,
    marginBottom: Responsive.getHeight(2),
    flexGrow: 0,
  },
  originIconStyle: {
    ...COMMON_STYLE.imageStyle(8),
    borderRadius: Responsive.getWidth(4),
    marginRight: Responsive.getWidth(2),
  },
  listSepratorStyle: {
    height: 1,
    backgroundColor: COLORS.GRAY1F,
  },
});
