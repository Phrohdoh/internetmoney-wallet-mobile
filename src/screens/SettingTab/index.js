import DataCollectionScreen from "./DataCollectionScreen";

export SettingScreen from './SettingScreen';
export ChangePasswordInfoScreen from './ChangePasswordInfoScreen';
export ChangePasswordScreen from './ChangePasswordScreen';
export SettingRecoveryPasswordVerificationScreen from './RecoveryPasswordVerificationScreen';
export SettingSecretRecoveryPhraseInfoScreen from './SecretRecoveryPhraseInfoScreen';
export SettingSecretRecoveryPhraseScreen from './SecretRecoveryPhraseScreen';
export WalletConnectScreen from './WalletConnectScreen';
export ChangeLanguageScreen from './ChangeLanguageScreen';
export DataCollectionScreen from './DataCollectionScreen';
export SupportScreen from './SupportScreen';
export SignTransactionConfirmationScreen from './SignTransactionConfirmationScreen';
export SendTransactionConfirmationScreen from './SendTransactionConfirmationScreen';
export AboutScreen from './AboutScreen';
