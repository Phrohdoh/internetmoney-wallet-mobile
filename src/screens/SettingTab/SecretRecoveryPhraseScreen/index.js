import React, { useRef, useEffect, useState } from 'react';
import { AppState, Platform, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import Toast from 'react-native-toast-message';

import Clipboard from '@react-native-clipboard/clipboard';

// import components
import {
  AlertActionModel,
  SafeAreaWrapper,
  RoundedButton,
  Tags,
} from '@components';

// import themes
import { COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

import * as Web3Layer from '@web3';
import RNScreenshotPrevent, {
  addListener,
} from 'react-native-screenshot-prevent';
import { BlurView } from '@react-native-community/blur';

// local constants
const SEED_PHRASE_PLACEHOLDER = '******';
const ROUNDED_BUTTON_MARGIN = 12;

const YourSecretRecoveryPhraseScreen = props => {
  const params = props.route.params;

  const [secretPhrase, setSecretPhrase] = useState([]);
  const [isHidden, setIsHidden] = useState(true);

  const [showScreenShotAlert, setShowScreenShotAlert] = useState(false);
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  useEffect(() => {
    getSecretPhrase();

    if (Platform.OS === 'ios') {
      RNScreenshotPrevent && RNScreenshotPrevent.enabled(true);
    }

    const screenshotSubscription = addListener(() => {
      setShowScreenShotAlert(true);
    });
    const appStateSubscription = AppState.addEventListener(
      'change',
      nextAppState => {
        appState.current = nextAppState;
        setAppStateVisible(appState.current);
      },
    );
    return () => {
      RNScreenshotPrevent && RNScreenshotPrevent.enabled(false);
      screenshotSubscription.remove();
      appStateSubscription.remove();
    };
  }, []);

  // Action Methods

  const getSecretPhrase = async () => {
    const mnemonicObject = await Web3Layer.decryptMnemonicSecurely(
      params.walletDetail.mnemonic,
      params.password,
    );
    const phraseArray = mnemonicObject.mnemonic.split(' ');
    setSecretPhrase(phraseArray);
  };

  const renderScreenShotTakenMessage = () => (
    <AlertActionModel
      isShowAlert={showScreenShotAlert}
      alertTitle={localize('SCREENSHOT_WARNING')}
      successBtnTitle={localize('UNDERSTAND')}
      bigSuccess={true}
      onPressSuccess={() => {
        setShowScreenShotAlert(false);
      }}
    />
  );

  return (
    <SafeAreaWrapper>
      <Text style={COMMON_STYLE.infoTitleStyle()}>
        {localize('YOUR_SEED_PHRASE_UPPERCASE')}
      </Text>
      <Text style={styles.scamAlertMsgStyle}>
        {localize('SEED_PHRASE_24_WORDS_MSG')}
      </Text>

      <View style={{ flex: 1 }}>
        <Tags
          style={styles.seedPhraseTagsStyle}
          tags={secretPhrase}
          isEditable={false}
          isHidden={isHidden}
          hiddenTagPlaceholder={SEED_PHRASE_PLACEHOLDER}
        />
      </View>

      {renderScreenShotTakenMessage()}

      <RoundedButton
        testID={'secret-recovery-copy-button'}
        style={COMMON_STYLE.roundedBtnMargin(ROUNDED_BUTTON_MARGIN)}
        title={localize('COPY_CLIPBOARD_UPPERCASE')}
        onPress={() => {
          Clipboard.setString(secretPhrase.join(' '));
          Toast.show({
            text1: localize('COPIED_SUCCESS'),
            text2: localize('SEED_PHRASE_COPIED_SUCCESS'),
          });
        }}
      />
      <RoundedButton
        isBordered
        testID="reveal-key-hide-button"
        style={ COMMON_STYLE.roundedBtnMargin(ROUNDED_BUTTON_MARGIN) }
        title={ localize(isHidden ? 'SHOW_SEED_PHRASE_UPPERCASE' : 'HIDE_SEED_PHRASE_UPPERCASE') }
        onPress={ () => setIsHidden(!isHidden) }
      />
      <RoundedButton
        isBordered
        testID="reveal-key-hide-button"
        style={ COMMON_STYLE.roundedBtnMargin(ROUNDED_BUTTON_MARGIN) }
        title={ localize('GO_BACK_BUTTON_UPPERCASE') }
        onPress={ () => props.navigation.navigate('SETTING_DASHBOARD_TAB') }
      />
      {
        (appStateVisible !== 'active' && Platform.OS === 'ios') && (
          <BlurView
            style={ styles.absolute }
            blurType="dark"
            blurAmount={ 10 }
          />
        )
      }
    </SafeAreaWrapper>
  );
};

YourSecretRecoveryPhraseScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default YourSecretRecoveryPhraseScreen;
