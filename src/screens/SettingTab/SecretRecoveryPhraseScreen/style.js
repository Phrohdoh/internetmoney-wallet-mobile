import {StyleSheet} from 'react-native';

import {COMMON_STYLE, COLORS} from '@themes';
import {Responsive} from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  infoMsgstyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6)),
    marginTop: Responsive.getHeight(2),
  },
  scrollStyle: {
    marginVertical: Responsive.getHeight(2),
  },
  confirmationViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Responsive.getHeight(2),
    width: '100%',
  },
  confirmStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6)),
    marginLeft: Responsive.getWidth(2),
  },
  swiperStyle: {
    marginBottom: Responsive.getHeight(2),
  },
  scamAlertMsgStyle: {
    ...COMMON_STYLE.scamAlertMsgStyle,
    marginTop: Responsive.getHeight(1),
    marginBottom: Responsive.getHeight(1),
  },
  seedPhraseTagsStyle: {
    marginBottom: Responsive.getHeight(2),
  },
});
