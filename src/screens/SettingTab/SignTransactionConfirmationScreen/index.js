import React, { useEffect, useState } from 'react';

import { View, Text, ScrollView } from 'react-native';
import PropTypes from 'prop-types';

import { useSelector } from 'react-redux';

// import components
import { SafeAreaWrapper, RoundedButton, CachedImage } from '@components';

// import themes
import { IMAGES, COLORS, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils

import {
  useTokenIcons,
  rejectTransaction,
  approveTransactionRequest,
} from '@utils';

import { useWalletConnect } from '@contexts/WalletConnectContext';
import * as Web3Layer from '@web3';
import { getNetwork, getSelectedWalletConnectAccount } from '../../../redux';

const getMeta = (transaction, sessions) => {
  return sessions.find(s => s.topic === transaction.topic)?.peer.metadata;
}

const SignTransactionConfirmationScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const getTokenIcon = useTokenIcons();

  const wallet = useSelector(state => state.wallet);
  const selectedAccount = useSelector(getSelectedWalletConnectAccount);
  const { sessions } = useWalletConnect();
  const selectedNetwork = useSelector(getNetwork);

  const { transaction } = params;
  const peerMeta = getMeta(transaction, sessions);

  const [signMsg, setSignMsg] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [accountBalance, setAccountBalance] = useState(0);

  useEffect(() => {
    getSignMsg();
    getAccountBalance();
  }, []);

  const getAccountBalance = async () => {
    const balance = await Web3Layer.getSingleAccount(
      selectedAccount.publicAddress,
      selectedNetwork,
    );

    setAccountBalance(balance.account.value);
  };

  const getSignMsg = async () => {
    if (typeof transaction.message === 'string') {
      setSignMsg(transaction.message);
      return;
    }
    setIsLoading(true);
    const signObject = await Web3Layer.getSignMsg(
      transaction.params.request,
      selectedNetwork,
    );

    if (signObject.success) {
      setSignMsg(signObject.signMsg);
    } else {
      setSignMsg(signObject.error);
    }
    setIsLoading(false);
  };

  const signTransaction = async () => {
    const dappObject = await Web3Layer.handleDappTxnObject(
      transaction,
      selectedAccount.publicAddress,
      wallet.walletDetail.walletObject,
      wallet.password,
      selectedNetwork,
    );

    if (dappObject.success) {
      await approveTransactionRequest(
        transaction,
        dappObject.signResult,
      );
    } else {
      rejectTransaction(transaction);
    }

    setIsLoading(false);
    navigation.goBack();
  };

  const clickToSignTransaction = async () => {
    setIsLoading(true);
    setTimeout(signTransaction, 0);
  };

  // render custom component
  const renderFromAccountView = () => {
    return (
      <View style={ styles.fromViewStyle }>
        <View style={ { flex: 1 } }>
          <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
            {localize('FROM:')}
          </Text>
          <Text style={ COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD') }>
            {selectedAccount.name}
          </Text>
        </View>
        <View style={ { flex: 1 } }>
          <Text
            style={ COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD', 'right') }>
            {selectedNetwork.sym}
          </Text>
          <Text
            numberOfLines={ 1 }
            style={ COMMON_STYLE.textStyle(11, COLORS.GRAY94, 'BASE', 'right') }>
            {localize('BALANCE:')}
            <Text
              style={ COMMON_STYLE.textStyle(11, COLORS.WHITE, 'BASE', 'right') }>
              {accountBalance?.toString(6)}
            </Text>
          </Text>
        </View>
        <CachedImage
          source={ getTokenIcon('', selectedNetwork.chainId) }
          style={ styles.tokenIconStyle }
        />
      </View>
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <Text style={ styles.selectedNetworkStyle }>
        {localize('CURRENT_NETWORK')}
      </Text>
      <Text style={ styles.networkNameStyle }>{selectedNetwork.networkName}</Text>

      <View style={ styles.bgImageContainerStyle }>
        <CachedImage source={ IMAGES.CONFIRMATION_BG } style={ styles.bgStyle } />

        <View style={ styles.detailContainerStyle }>
          <ScrollView bounces={ false }>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <Text style={ styles.confirmStyle }>
              {localize('CONFIRM_SIGNATURE_UPPERCASE')}
            </Text>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />

            <View style={ styles.accountDetailViewStyle }>
              <View style={ { flex: 1 } }>{renderFromAccountView()}</View>
            </View>
            <View style={ styles.originViewStyle }>
              <Text style={ styles.originTitleStyle }>{localize('ORIGIN')}</Text>
              <CachedImage
                style={ styles.originIconStyle }
                source={ { uri: peerMeta.icons?.[0] } }
              />
              <Text
                numberOfLines={ 1 }
                style={ styles.originTextStyle }
                ellipsizeMode="head">
                {peerMeta.url}
              </Text>
            </View>

            <Text style={ styles.signingTitleStyle }>
              {localize('YOU_ARE_SIGNING')}
            </Text>
            <Text style={ styles.messageTitleStyle }>{localize('MESSAGE')}</Text>
            <Text style={ styles.messageStyle }>{signMsg}</Text>
          </ScrollView>
        </View>
      </View>
      {!isLoading ? (
        <View style={ styles.actionBtnViewStyle }>
          <RoundedButton
            testID="sign-transaction-cancel-btn"
            style={ styles.actionBtnStyle }
            isBordered
            title={ localize('CANCEL') }
            onPress={ async () => {
              await rejectTransaction(transaction);
              navigation.goBack();
            } }
          />
          <RoundedButton
            testID="sign-transaction-sign-btn"
            style={ styles.actionBtnStyle }
            title={ localize('SIGN') }
            onPress={ () => clickToSignTransaction() }
          />
        </View>
      ) : (
        <CachedImage source={ IMAGES.ROUNDED_LOADING } style={ styles.loadingStyle } />
      )}
    </SafeAreaWrapper>
  );
};

SignTransactionConfirmationScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default SignTransactionConfirmationScreen;
