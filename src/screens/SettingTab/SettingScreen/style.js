import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  headerTopMarginStyle: {
    backgroundColor: COLORS.BLACK,
  },

  sectionListStyle: {
    marginVertical: Responsive.getHeight(2),
  },
  headerStyle: dataLength => {
    const borderObj = {};
    if (dataLength) {
      borderObj.borderTopLeftRadius = Responsive.getHeight(3);
      borderObj.borderTopRightRadius = Responsive.getHeight(3);
    } else {
      borderObj.borderRadius = Responsive.getHeight(3);
    }
    return {
      height: Responsive.getHeight(6),
      borderColor: COLORS.GRAY1F,
      backgroundColor: COLORS.GRAY15,
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: Responsive.getWidth(4),
      ...borderObj,
      marginTop: Responsive.getHeight(1.5),
      borderBottomWidth: 1,
    };
  },
  settingCellStyle: isLastIndex => {
    const borderObj = {};
    if (isLastIndex) {
      borderObj.borderBottomLeftRadius = Responsive.getHeight(3);
      borderObj.borderBottomRightRadius = Responsive.getHeight(3);
    }
    return {
      flexDirection: 'row',
      alignItems: 'center',
      height: Responsive.getHeight(6),
      backgroundColor: COLORS.GRAY15,
      paddingHorizontal: Responsive.getWidth(3),
      ...borderObj,
    };
  },
  sepratorStyle: { height: 1, backgroundColor: COLORS.GRAY1F },
  headerTitleStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.YELLOWFB, 'BOLD'),
    flex: 1,
  },
  footerTitleStyle: {
    ...COMMON_STYLE.textStyle(8, COLORS.LIGHT_OPACITY(0.6)),
    marginVertical: Responsive.getHeight(2),
  },
  settingTitleStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.WHITE),
    flex: 1,
  },
  sectionSepratorStyle: {
    height: Responsive.getHeight(4),
  },
  fromAccountViewStyle: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    borderRadius: 25,
    alignItems: 'center',
    borderWidth: 2,
    borderColor: COLORS.GRAY1F,
    paddingHorizontal: Responsive.getWidth(4),
  },

  versionNameStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6), 'BOLD', 'center'),
    marginBottom: Responsive.getHeight(0.5),
  },
});
