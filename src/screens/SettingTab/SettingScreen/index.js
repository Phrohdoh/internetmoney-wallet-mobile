import React, { useState } from 'react';
import {
  View,
  Text,
  SectionList,
  TouchableOpacity,
  Linking,
  SafeAreaView,
} from 'react-native';

import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';
import Toast from 'react-native-toast-message';

import { GlobalAction } from '@redux';
// import components
import { QRScannerModal, CachedImage } from '@components';

import { COMMON_DATA, ASYNC_KEYS, CONFIG } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { COMMON_STYLE, IMAGES, bottomTabPadding } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

import { pair } from '../../../utils/walletConnect';

const SettingScreen = props => {
  const wallet = useSelector(state => state.wallet);

  const sectionData = [
    {
      title: 'GENERAL',
      data: [
        {
          title: 'ABOUT',
          navigateTo: 'ABOUT_SCREEN',
          // onPress: () => Linking.openURL(COMMON_DATA.ABOUNT_LINK),
        },
        { title: 'CHOOSE_LANGUAGE', navigateTo: 'CHANGE_LANGUAGE_SCREEN' },
        { title: 'LOGOUT', navigateTo: 'SPLASH_SCREEN' },
      ],
    },
    {
      title: 'SECURITY_PRIVACY',
      data: [
        {
          title: wallet.isPasscode ? 'CHANGE_PASSCODE' : 'CHANGE_PASSWORD',
          navigateTo: 'CHANGE_PASSWORD_INFO_SCREEN',
        },
        {
          title: 'SEED_PHRASE',
          navigateTo: 'SETTING_SECRET_REVOVERY_PHRASE_INFO_SCREEN',
        },
        {
          title: 'DATA_COLLECTION',
          navigateTo: 'DATA_COLLECTION_SCREEN',
        },
      ],
    },
    {
      title: 'WALLET_CONNECT',
      data: [
        {
          title: 'SCAN_QR_CODE',
          onPress: () => {
            setIsShowQrSacnner(true);
          },
        },
        { title: 'VIEW_CONNECTED_APPS', navigateTo: 'WALLET_CONNECT_SCREEN' },
      ],
    },
    {
      title: 'SUPPORT',
      data: [
        {
          title: 'FEATURE_REQUEST',
          navigateTo: 'SUPPORT_SCREEN',
          params: { supportType: 'sendfeedback' },
        },
        {
          title: 'SUBMIT_BUG',
          navigateTo: 'SUPPORT_SCREEN',
          params: { supportType: 'submitbug' },
        },
        {
          title: 'CONTACT_US',
          navigateTo: 'SUPPORT_SCREEN',
          params: { supportType: 'contactus' },
        },
      ],
    },
    {
      title: 'LEGAL',
      data: [
        {
          title: 'TERMS_AND_CONDITIONS',
          onPress: () => Linking.openURL(COMMON_DATA.TERMS_OF_SERVICE_LINK),
        },
        {
          title: 'PRIVACY_POLICY',
          onPress: () => Linking.openURL(COMMON_DATA.PRIVACY_POLICY_LINK),
        },
      ],
    },
  ];

  const { navigation } = props;

  const dispatch = useDispatch();

  const [isShowQrSacnner, setIsShowQrSacnner] = useState(false);

  // Action methods
  const establishConnection = async (uri) => {
    await pair(uri);
  };

  // Render custom components
  const renderSectionHeader = section => {
    return (
      <TouchableOpacity
        style={ styles.headerStyle(section.data.length) }
        testID={ section.title }
        onPress={ () => {
          if (section.navigateTo) {
            navigation.navigate(section.navigateTo);
          }
        } }>
        <Text style={ styles.headerTitleStyle }>
          {section.params
            ? localize(section.title, section.params)
            : localize(section.title)}
        </Text>
        {section.navigateTo && (
          <CachedImage
            style={ COMMON_STYLE.imageStyle(4) }
            source={ IMAGES.RIGHT_ARROW }
          />
        )}
      </TouchableOpacity>
    );
  };

  const renderSettingCell = section => {
    const {
      item,
      section: { data },
      index,
    } = section;

    return (
      <TouchableOpacity
        style={ styles.settingCellStyle(index === data.length - 1) }
        testID={ item.title }
        onPress={ () => {
          if (item.onPress) {
            item.onPress();
          }
          if (item.navigateTo) {
            if (item.params) navigation.navigate(item.navigateTo, item.params);
            else navigation.navigate(item.navigateTo);
          }
        } }>
        <Text style={ styles.settingTitleStyle }>{localize(item.title)}</Text>
        <CachedImage style={ COMMON_STYLE.imageStyle(4) } source={ IMAGES.RIGHT_ARROW } />
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={ COMMON_STYLE.safeAreaViewStyle }>
      <View
        style={ [
          COMMON_STYLE.borderedContainerViewStyle(bottomTabPadding),
          styles.headerTopMarginStyle,
        ] }>
        <SectionList
          style={ styles.sectionListStyle }
          testID="tab-section"
          sections={ sectionData }
          renderItem={ section => renderSettingCell(section) }
          renderSectionHeader={ ({ section }) => renderSectionHeader(section) }
          stickySectionHeadersEnabled={ false }
          ItemSeparatorComponent={ () => <View style={ styles.sepratorStyle } /> }
          keyExtractor={ (item, index) => index }
        />
      </View>

      <QRScannerModal
        isVisible={ isShowQrSacnner }
        scanedValue={ value => {
          setIsShowQrSacnner(false);
          establishConnection(value);
          navigation.navigate('WALLET_CONNECT_SCREEN');
        } }
        onPressClose={ () => setIsShowQrSacnner(false) }
      />
    </SafeAreaView>
  );
};

SettingScreen.propTypes = { navigation: PropTypes.any };
export default SettingScreen;
