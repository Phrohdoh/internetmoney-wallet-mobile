import React, { useState } from 'react';

import { View, Text } from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch } from 'react-redux';

import axios from 'axios';

import { GlobalAction } from '@redux';

import { BACKEND_SERVER } from '@constants';
// import components
import {
  SafeAreaWrapper,
  TitleTextInput,
  KeyboardAvoidScrollView,
  RoundedButton,
} from '@components';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

import { COMMON_STYLE } from '@themes';

import { Validation } from '@utils';

const SupportScreen = props => {
  const { navigation } = props;
  const params = props.route.params;
  const dispatch = useDispatch();

  const supportTypes = localize('SUPPORT_TYPES');

  const selectedSupportType = supportTypes.find(
    type => type.type === params.supportType,
  );

  const [isLoading, setIsLoading] = useState(false);

  const [name, onChangeName] = useState({
    id: 'name',
    value: '',
    placeholder: 'NAME_PLACEHOLDER',
    title: 'NAME',
  });

  const [email, onChangeEmail] = useState({
    id: 'how_do_reach',
    value: '',
    placeholder: 'EMAIL_OPT',
    title: 'HOW_DO_REACH',
    extraProps: { keyboardType: 'email-address' },
  });
  const [subject, onChangeSubject] = useState({
    id: 'subject',
    value: '',
    placeholder: 'ENTER_SUBJECT',
    title: 'SUBJECT',
  });

  const [message, onChangeMessage] = useState({
    id: 'message',
    value: '',
    title:
      params.supportType === 'submitbug'
        ? 'TELL_ISSUE'
        : params.supportType === 'contactus'
          ? 'DESCRIBE_ISSUE'
          : 'WHAT_YOU_LIKE',
    placeholder:
      params.supportType === 'submitbug' ? 'PROVIDE_DETAIL' : 'TELL_ABOUT_IT',
    extraProps: { inputViewStyle: styles.messageStyle, multiline: true },
  });

  // Action Methods
  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.value = text;
    setFunction(tempDict);
  };

  const checkValidation = () => {
    const isValidEmail =
      Validation.isEmpty(email.value) || Validation.isValidEmail(email.value);

    const isEmptySubject = Validation.isEmpty(subject.value);
    const isEmptyMessage = Validation.isEmpty(message.value);

    if (isValidEmail) {
      if (isEmptySubject && params.supportType !== 'contactus') {
        dispatch(
          GlobalAction.showAlert(
            localize('ERROR'),
            localize('ENTER_SUBJECT_MSG'),
          ),
        );
        return false;
      } else if (isEmptyMessage) {
        dispatch(
          GlobalAction.showAlert(localize('ERROR'), localize('ENTER_MESSAGE')),
        );

        return false;
      }
      return true;
    } else {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('ENTER_VALID_EMAIL'),
        ),
      );

      return false;
    }
  };

  const sendSupportMail = async () => {
    const baseUrl = BACKEND_SERVER.BASE_URL + BACKEND_SERVER.SEND_SUPPORT_MAIL;

    const body = {
      subject:
        params.supportType === 'contactus'
          ? selectedSupportType.subTitle
          : subject.value,
    };

    if (!Validation.isEmpty(message.value)) {
      body['message'] = message.value;
    }
    if (!Validation.isEmpty(name.value)) {
      body['name'] = name.value;
    }
    if (!Validation.isEmpty(email.value)) {
      body['email'] = email.value;
    }

    setIsLoading(true);

    try {
      const { data } = await axios.post(baseUrl, body, {
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      });
    
      setIsLoading(false);
    
      if (data.success) {
        dispatch(GlobalAction.showAlert(localize('SUCCESS'), data.message));
        navigation.goBack();
      } else {
        dispatch(GlobalAction.showAlert(localize('ERROR'), data.message));
      }
    
    } catch (error) {
      setIsLoading(false);
      dispatch(GlobalAction.showAlert(localize('ERROR'), localize('SOMETHING_WENT_WRONG')));
    }
  };

  const _onPressSubmit = () => {
    if (checkValidation()) {
      sendSupportMail();
    }
  };

  // render custom component
  const renderInputComponent = (state, setStateFunction) => {
    return (
      <TitleTextInput
        testID={ `support-input-${state.id}` }
        inputContainerStyle={ styles.inputContainerStyle }
        value={ state.value }
        title={ localize(state.title) }
        placeholder={ localize(state.placeholder) }
        onChangeText={ text => changeData(text, state, setStateFunction) }
        { ...state.extraProps }
      />
    );
  };

  const renderContactUsComponent = () => {
    return (
      params.supportType === 'contactus' && (
        <React.Fragment>
          {renderInputComponent(name, onChangeName)}
        </React.Fragment>
      )
    );
  };
  const renderSubjectConponent = () => {
    return (
      params.supportType !== 'contactus' && (
        <React.Fragment>
          {renderInputComponent(subject, onChangeSubject)}
        </React.Fragment>
      )
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      
      
      <KeyboardAvoidScrollView>
        <View style={ styles.titleMarginStyle }>
          <Text style={ styles.titleStyle }>{selectedSupportType.detailTitle}</Text>
          <Text style={ styles.descStyle }>{selectedSupportType.detailMsg}</Text>
        </View>
        <View style={ [COMMON_STYLE.borderedContainerViewStyle()] }>
          <Text style={ styles.subTitleStyle }>
            {selectedSupportType.subTitle}
          </Text>
          {renderContactUsComponent()}
          {renderSubjectConponent()}
          {renderInputComponent(message, onChangeMessage)}
          {renderInputComponent(email, onChangeEmail)}
          <RoundedButton
            isLoading={ isLoading }
            testID="submit-button"
            style={ styles.submitBtnStyle }
            title={ localize('SUBMIT_UPPERCASE') }
            onPress={ () => _onPressSubmit() }
          />
        </View>
      </KeyboardAvoidScrollView>
        
    </SafeAreaWrapper>
  );
};

SupportScreen.propTypes = { navigation: PropTypes.any, route: PropTypes.any };
export default SupportScreen;
