import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  titleMarginStyle: {
    paddingHorizontal: Responsive.getWidth(5),
    marginBottom: Responsive.getHeight(2),
  },
  titleStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.YELLOWFB, 'BOLD'),
    marginTop: Responsive.getHeight(2),
  },
  descStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6)),
    marginVertical: Responsive.getHeight(1),
  },
  subTitleStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.YELLOWFB, 'BOLD', 'center'),
    marginTop: Responsive.getHeight(2),
  },
  selectedTypeViewStyle: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    borderRadius: 25,
    alignItems: 'center',
    borderWidth: 2,
    borderColor: COLORS.GRAY1F,
    paddingHorizontal: Responsive.getWidth(4),
    marginTop: Responsive.getHeight(2),
  },
  selectedTypeStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.WHITE),
    flex: 1,
  },
  listSepratorStyle: { height: Responsive.getHeight(1) },
  inputContainerStyle: {
    marginTop: Responsive.getHeight(1.5),
  },
  messageStyle: {
    height: Responsive.getHeight(12),
  },
  submitBtnStyle: {
    ...COMMON_STYLE.roundedBtnMargin(),
    marginTop: Responsive.getHeight(2),
  },
});
