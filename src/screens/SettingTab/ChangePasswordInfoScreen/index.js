import React, { useEffect } from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

import { useSelector } from 'react-redux';

// import components
import { SafeAreaWrapper, RoundedButton } from '@components';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

const ChangePasswordInfoScreen = props => {
  const { navigation } = props;

  const wallet = useSelector(state => state.wallet);

  useEffect(() => {
    navigation.setOptions({
      headerTitle: localize(
        wallet.isPasscode
          ? 'PASSCODE_SETTING_UPPERCASE'
          : 'PASSCODE_SETTING_UPPERCASE',
      ),
    });
  }, []);

  return (
    <SafeAreaWrapper>
      <Text style={ styles.titleStyle }>
        {localize(
          wallet.isPasscode
            ? 'CHANGE_PASSCODE_INFO_MSG'
            : 'CHANGE_PASSWORD_INFO_MSG',
        )}
      </Text>
      <RoundedButton
        testID="change-password-button"
        style={ styles.buttonMargin }
        title={ localize(
          wallet.isPasscode
            ? 'CHANGE_PASSCODE_UPPERCASE'
            : 'CHANGE_PASSWORD_UPPERCASE',
        ) }
        onPress={ () => navigation.navigate('CHANGE_PASSWORD_SCREEN') }
      />
    </SafeAreaWrapper>
  );
};

ChangePasswordInfoScreen.propTypes = { navigation: PropTypes.any };
export default ChangePasswordInfoScreen;
