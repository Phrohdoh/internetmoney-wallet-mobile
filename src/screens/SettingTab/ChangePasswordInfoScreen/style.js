import {StyleSheet} from 'react-native';

import {COMMON_STYLE, COLORS} from '@themes';
import {Responsive} from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  titleStyle: {
    ...COMMON_STYLE.textStyle(16, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginTop: Responsive.getHeight(2),
  },
  buttonMargin: {
    marginTop: Responsive.getHeight(5),
  },
});
