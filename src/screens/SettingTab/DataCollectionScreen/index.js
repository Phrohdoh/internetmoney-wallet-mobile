import React, { useState } from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import PropTypes from 'prop-types';

// import components
import { SafeAreaWrapper, ModalContainerView, CachedImage } from '@components';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

import { COMMON_STYLE, IMAGES } from '@themes';
import { getLogsEnabled, setLogsEnabled } from '../../../utils/logger';

const DataCollectionScreen = props => {
  const [showList, setShowList] = useState(false);
  const options = localize('DATA_COLLECTION_LIST');
  const [selectedOption, setSelectedOption] = useState(getLogsEnabled() ? options[1] : options[0]);

  // render custom component
  const renderSelectionView = () => {
    return (
      <TouchableOpacity
        testID={ 'general-setting-choose-data-collection-button' }
        style={ styles.languageVieStyle }
        onPress={ () => setShowList(true) }>
        <Text numberOfLines={ 1 } style={ styles.selectedLanguageStyle }>
          {selectedOption.title}
        </Text>
        <CachedImage source={ IMAGES.DOWN_ARROW } style={ COMMON_STYLE.imageStyle(6) } />
      </TouchableOpacity>
    );
  };

  const renderListCell = (item, index) => {
    return (
      <TouchableOpacity
        testID={ `general-setting-data-collection-list-cell-button-${index}` }
        style={ styles.languageVieStyle }
        onPress={ () => {
          setShowList(false);
          setSelectedOption(item);
          setLogsEnabled(item.key === 'enable')
        } }>
        <Text numberOfLines={ 1 } style={ styles.selectedLanguageStyle }>
          {item.title}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderTitleView = (title, subtitle) => {
    return (
      <View>
        <Text style={ styles.titleStyle }>{title}</Text>
        <Text style={ styles.subtitleStyle }>{subtitle}</Text>
      </View>
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <View style={ COMMON_STYLE.borderedContainerViewStyle() }>
        {renderTitleView(
          localize('LOGS_AND_ERROR_REPORTING'),
          localize('DATA_COLLECTION_MSG'),
        )}
        {renderSelectionView()}
      </View>
      <ModalContainerView
        testID={ 'modal-data-collection' }
        isVisible={ showList }
        onPressClose={ () => setShowList(false) }>
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('LOGS_AND_ERROR_REPORTING')}
          </Text>
          <ScrollView
            horizontal
            contentContainerStyle={ { width: '100%' } }
            scrollEnabled={ false }>
            <FlatList
              data={ options }
              bounces={ false }
              renderItem={ ({ item, index }) =>
                renderListCell(item, index)
              }
              ItemSeparatorComponent={ () => (
                <View style={ styles.listSepratorStyle } />
              ) }
              keyExtractor={ (item, index) => index + item }
            />
          </ScrollView>
        </React.Fragment>
      </ModalContainerView>
    </SafeAreaWrapper>
  );
};

DataCollectionScreen.propTypes = {};
export default DataCollectionScreen;
