import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';

import { GlobalAction, WalletAction } from '@redux';

// import components
import {
  SafeAreaWrapper,
  KeyboardAvoidScrollView,
  RoundedButton,
  TitleTextInput,
  PasscodeComponent,
} from '@components';

// import constants
import { ASYNC_KEYS } from '@constants';

// import themes
import { COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import storage functions
import { StorageOperation } from '@storage';

// import Utils
import { Validation } from '@utils';

import * as Web3Layer from '@web3';

const ChangePasswordScreen = props => {
  const { navigation } = props;

  const dispatch = useDispatch();
  const wallet = useSelector(state => state.wallet);
  const [isLoading, setIsLoading] = useState(false);

  const { isPasscode } = wallet;

  const [oldPasscode, setOldPasscode] = useState('');
  const [newPasscode, setNewPasscode] = useState('');
  const [confPasscode, setConfPasscode] = useState('');

  const [selectedType, setSelectedType] = useState('old');

  const [oldPassword, onChangeOldPassword] = useState({
    id: 'old-password',
    value: '',
    title: 'OLD_PASSWORD',
    placeholder: 'ENTER_PASSWORD',
    extraProps: {
      isPassword: true,
    },
  });
  const [newPassword, onChangeNewPassword] = useState({
    id: 'new-password',
    value: '',
    title: 'NEW_PASSWORD',
    placeholder: 'ENTER_PASSWORD',
    extraProps: {
      isPassword: true,
    },
  });

  const [confPassword, onChangeConfPassword] = useState({
    id: 'conf-password',
    value: '',
    title: 'CONF_PASSWORD',
    placeholder: 'ENTER_CONF_PASSWORD',
    extraProps: {
      isPassword: true,
    },
  });

  useEffect(() => {
    navigation.setOptions({
      headerTitle: localize(
        isPasscode
          ? 'PASSCODE_SETTING_UPPERCASE'
          : 'PASSCODE_SETTING_UPPERCASE',
      ),
    });
  }, []);

  // Action Methods
  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.value = text;
    setFunction(tempDict);
  };

  const handlePasscode = (item, code, setCode) => {
    const tempCode = code.split('');
    if (item.value === 'remove') {
      if (tempCode.length) {
        tempCode.pop();
      }
    } else {
      if (tempCode.length < 6) {
        tempCode.push(item.value);
      }
    }
    const strPasscode = tempCode.join('');
    setCode(strPasscode);
  };

  // check all validation
  const checkPasswordValidation = () => {
    const isEmptyOldPassword = Validation.isEmpty(oldPassword.value);
    const isValidNewPassword = Validation.isValidPassword(newPassword.value);
    const matchPassword = newPassword.value === confPassword.value;

    if (!isEmptyOldPassword && isValidNewPassword && matchPassword) {
      return true;
    } else {
      let msg = '';

      /* istanbul ignore else */
      if (isEmptyOldPassword) {
        msg = 'ENTER_OLD_PASSWORD_MSG';
      } else if (Validation.isEmpty(newPassword.value)) {
        msg = 'ENTER_NEW_PASSWORD_MSG';
      } else if (!isValidNewPassword) {
        msg = 'ENTER_VALID_NEW_PASS_MSG';
      } else if (Validation.isEmpty(confPassword.value)) {
        msg = 'ENTER_CONF_PASS_MSG';
      } else if (!matchPassword) {
        msg = 'MATCH_NEW_PASS_MSG';
      }
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize(msg),
          localize('TRY_AGAIN'),
        ),
      );
      return false;
    }
  };

  const getBtnText = () => {
    let btnString = 'SAVE_CHANGES_UPPERCASE';
    if (isPasscode) {
      if (selectedType === 'old' || selectedType === 'new') {
        btnString = 'NEXT_UPPERCASE';
      }
    }

    return localize(btnString);
  };
  const checkPasscodeValidation = () => {
    const isValidOldPasscode = oldPasscode.length === 6;
    const isValidNewPasscode = newPasscode.length === 6;
    const matchPasscode = newPasscode === confPasscode;

    if (selectedType === 'old') {
      if (isValidOldPasscode) {
        setSelectedType('new');
      } else {
        dispatch(
          GlobalAction.showAlert(
            localize('ERROR'),
            localize('ENTER_OLD_PASSCODE_MSG'),
            localize('TRY_AGAIN'),
          ),
        );
      }
    } else if (selectedType === 'new') {
      if (isValidNewPasscode) {
        setSelectedType('conf');
      } else {
        dispatch(
          GlobalAction.showAlert(
            localize('ERROR'),
            localize('ENTER_NEW_PASSCODE_MSG'),
            localize('TRY_AGAIN'),
          ),
        );
      }
    } else {
      if (matchPasscode) {
        return true;
      } else {
        dispatch(
          GlobalAction.showAlert(
            localize('ERROR'),
            localize('PASSCODE_NOT_MATCH_MSG'),
            localize('TRY_AGAIN'),
          ),
        );
      }
    }

    return false;
  };

  const checkOldPasswordIsValid = async (walletObj, password) => {
    const checkPass = await Web3Layer.checkPasswordSecurely(
      password,
      walletObj.passwordHash,
    );
    return checkPass;
  };

  const _onPressContinue = async () => {
    if (isPasscode ? checkPasscodeValidation() : checkPasswordValidation()) {
      setIsLoading(true);

      if (
        checkOldPasswordIsValid(
          wallet.walletDetail,
          isPasscode ? oldPasscode : oldPassword.value,
        )
      ) {
        const changePassObj = await Web3Layer.changePassword(
          wallet.walletDetail.walletObject,
          wallet.walletDetail.mnemonic,
          isPasscode ? oldPasscode : oldPassword.value,
          isPasscode ? newPasscode : newPassword.value,
        );

        setIsLoading(false);

        if (changePassObj.success) {
          const tempWalletDetail = { ...wallet.walletDetail };
          tempWalletDetail.walletObject = changePassObj.walletObject;
          tempWalletDetail.mnemonic = changePassObj.mnemonic;
          tempWalletDetail.passwordHash = changePassObj.passwordHash;

          const { error } = await StorageOperation.setMultiData([
            [ASYNC_KEYS.WALLET_DETAIL, JSON.stringify(tempWalletDetail)],
          ]);

          if (!error) {
            dispatch(
              WalletAction.initApp(
                tempWalletDetail,
                isPasscode ? newPasscode : newPassword.value,
                isPasscode,
              ),
            );
            navigation.navigate('SETTING_DASHBOARD_TAB');
          } else {
            dispatch(
              GlobalAction.showAlert(
                localize('ERROR'),
                localize('CHANGE_PASS_ERROR_MSG'),
                localize('TRY_AGAIN'),
              ),
            );
          }
        } else {
          setIsLoading(false);
          dispatch(
            GlobalAction.showAlert(
              localize('ERROR'),
              changePassObj.error,
              localize('TRY_AGAIN'),
            ),
          );
        }
      } else {
        dispatch(
          GlobalAction.showAlert(
            localize('ERROR'),
            localize('INCORRECT_OLD_PASSWORD'),
            localize('TRY_AGAIN'),
          ),
        );
      }
    }
  };

  // Custom Render Components
  const renderInputComponent = (state, setStateFunction) => {
    return (
      <TitleTextInput
        testID={ `pass-setup-screen-${state.id}` }
        inputContainerStyle={ styles.inputContainerStyle }
        value={ state.value }
        title={ localize(state.title) }
        placeholder={ localize(state.placeholder) }
        onChangeText={ text => changeData(text, state, setStateFunction) }
        { ...state.extraProps }
      />
    );
  };

  const renderPasscodeComponent = () => {
    switch (selectedType) {
    case 'old':
      return (
        <PasscodeComponent
          title={ localize('OLD_PASSCODE') }
          passcode={ oldPasscode }
          onPressKey={ item =>
            handlePasscode(item, oldPasscode, setOldPasscode)
          }
        />
      );
    case 'new':
      return (
        <PasscodeComponent
          title={ localize('NEW_PASSCODE') }
          passcode={ newPasscode }
          onPressKey={ item =>
            handlePasscode(item, newPasscode, setNewPasscode)
          }
        />
      );
    case 'conf':
      return (
        <PasscodeComponent
          title={ localize('CONF_PASSCODE') }
          passcode={ confPasscode }
          onPressKey={ item =>
            handlePasscode(item, confPasscode, setConfPasscode)
          }
        />
      );
    default:
    }
  };
  return (
    <SafeAreaWrapper>
      {isPasscode ? (
        renderPasscodeComponent()
      ) : (
        <KeyboardAvoidScrollView>
          {renderInputComponent(oldPassword, onChangeOldPassword)}
          {renderInputComponent(newPassword, onChangeNewPassword)}
          {renderInputComponent(confPassword, onChangeConfPassword)}
        </KeyboardAvoidScrollView>
      )}

      <RoundedButton
        isLoading={ isLoading }
        testID="password-setup-getstart-button"
        style={ COMMON_STYLE.roundedBtnMargin() }
        title={ getBtnText() }
        onPress={ () => _onPressContinue() }
      />
    </SafeAreaWrapper>
  );
};

ChangePasswordScreen.propTypes = { navigation: PropTypes.any };
export default ChangePasswordScreen;
