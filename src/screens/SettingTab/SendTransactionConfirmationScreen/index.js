import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { TransactionsAction } from '@redux';

// import components
import {
  AlertActionModel,
  GasFeeRangeView,
  ModalContainerView,
  SafeAreaWrapper,
  CachedImage,
} from '@components';

import SwipeVerifyController from '@components/SwipeVerifyController';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

import { useWalletConnect } from '@contexts/WalletConnectContext';
import { IntegerUnits } from '@utils';

// import style
import { styles } from './style';

// import Utils

import {
  approveTransactionRequest,
  displayAddress,
  rejectTransaction,
  useTokenIcons,
} from '@utils';

import {
  WalletTransactionTypes,
  calculateEstimateFee,
  calculateMaxFee,
  createAndSendTxn,
  getSingleAccount,
  handleDappTxnObject,
  updateTxnLists
} from '@web3';
import { getNetwork, getSelectedWalletConnectAccount } from '../../../redux';
import { getUserFriendlyErrorMessage } from '../../../utils/errors';

const SendTransactionConfirmationScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const getTokenIcon = useTokenIcons();

  const wallet = useSelector(state => state.wallet);
  const transactions = useSelector(state => state.transactions);
  const selectedAccount = useSelector(getSelectedWalletConnectAccount);
  const { sessions } = useWalletConnect();

  const { transaction } = params;
  const isV2 = typeof transaction.topic === 'string';
  const peerMeta = isV2
    ? sessions.find(s => s.topic === transaction.topic).peer.metadata
    : transaction.connection.peerMeta;
  const selectedNetwork = useSelector(getNetwork);

  const [selectedRange, setSelectedRange] = useState(1);
  const [txnObject, setTxnObject] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [isShowGasFeeView, setIsShowGasFeeView] = useState(false);
  const [gasRange, setGasRange] = useState({});
  const [accountBalance, setAccountBalance] = useState(0);
  const [isShowError, setIsShowError] = useState(false);
  const [error, setError] = useState('');

  const dispatch = useDispatch();

  useEffect(() => {
    getTxnObject();
    getAccountBalance();
  }, []);

  const getAccountBalance = async () => {
    const balance = await getSingleAccount(
      selectedAccount.publicAddress,
      selectedNetwork,
    );

    setAccountBalance(balance.account.value);
  };

  const getTxnObject = async () => {
    setIsLoading(true);

    const dappObject = await handleDappTxnObject(
      transaction,
      selectedAccount.publicAddress,
      wallet.walletDetail.walletObject,
      wallet.password,
      selectedNetwork,
    );

    if (dappObject.success) {
      setGasRange(dappObject.range);
      setIsLoading(false);
      setTxnObject(dappObject.txnObject);
    } else {
      rejectTransaction(transaction);
      navigation.goBack();
    }
  };

  const updateTxnObjectTypeTwo = (gas, maxPrioFee, maxFeePerGas) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.maxPriorityFeePerGas = maxPrioFee;
    tempTxObj.maxFeePerGas = maxFeePerGas;
    setTxnObject(tempTxObj);
  };

  const updateTxnObjectTypeOne = (gas, gasPrice) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.gasPrice = gasPrice;
    setTxnObject(tempTxObj);
  };

  const getEstimateFee = () => {
    if (!txnObject) return '';
    const estimateFee = calculateEstimateFee(txnObject, selectedNetwork.txnType);
    return `${estimateFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getMaxFee = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getTotal = () => {
    if (!txnObject) return '';
    return `${txnObject.value.toString(6)} ${selectedNetwork.sym} + ${getEstimateFee()}`;
  };

  const getMaxTotal = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${txnObject.value.toString(6)} ${selectedNetwork.symbol} + ${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const sendTransaction = async () => {
    try {
      const txnResult = await createAndSendTxn(
        WalletTransactionTypes.DAPP,
        txnObject,
        wallet,
        selectedAccount,
        accountBalance,
        selectedNetwork
      );

      if (txnResult.success) {
        const { tempTxns, tempTxnsList } = await updateTxnLists(
          IntegerUnits.serialize({
            type: WalletTransactionTypes.DAPP,
            transactionDetail: {
              accountName: selectedAccount.name,
              displayAmount: txnObject.value,
              displayDecimal: 18,
              transferedAmount: txnObject.value,
              toAddress: txnObject.to,
              fromAddress: txnObject.from,
              isSupportUSD: false,
              networkDetail: selectedNetwork,
              dappObject: txnObject,
              fromToken: { symbol: selectedNetwork.sym },
              dappTransactionDetail: transaction,
              utcTime: moment.utc().format(),
            },
            receipt: txnResult.receipt,
            blockNumber: txnResult.blockNumber,
          }),
          transactions,
          selectedNetwork
        );

        dispatch(TransactionsAction.saveTransactions(tempTxnsList));

        await StorageOperation.setMultiData([
          [ASYNC_KEYS.TRANSACTIONS, JSON.stringify(tempTxns)],
        ]);

        await approveTransactionRequest(
          transaction,
          txnResult.receipt.transactionHash,
        );

        navigation.goBack();
      } else {
        setIsLoading(false);
        rejectTransaction(transaction);
        setError(txnResult.error);
        setIsShowError(true);
      }
    } catch (err) {
      console.error(err);
      setIsLoading(false);
    }
  }

  const slideToSendTransaction = async () => {
    setIsLoading(true);
    setTimeout(sendTransaction, 0);
  };

  // render custom component
  const renderFromAccountView = () => {
    return (
      <View style={ styles.fromViewStyle }>
        <View style={ { flex: 1 } }>
          <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
            {localize('FROM:')}
          </Text>
          <Text style={ COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD') }>
            {selectedAccount.name}
          </Text>
        </View>
        <View style={ { flex: 1 } }>
          <Text
            style={ COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD', 'right') }>
            {selectedNetwork.sym}
          </Text>
          <Text
            numberOfLines={ 1 }
            style={ COMMON_STYLE.textStyle(11, COLORS.GRAY94, 'BASE', 'right') }>
            {localize('BALANCE:')}
            <Text
              style={ COMMON_STYLE.textStyle(11, COLORS.WHITE, 'BASE', 'right') }>
              {accountBalance.toString(6)}
            </Text>
          </Text>
        </View>
        <CachedImage
          source={ getTokenIcon('', selectedNetwork.chainId) }
          style={ styles.tokenIconStyle }
        />
      </View>
    );
  };

  const renderToAccountView = () => {
    return (
      <View style={ styles.toViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('TO:')}
        </Text>
        <Text numberOfLines={ 1 } style={ styles.toAddressTextStyle }>
          {displayAddress(txnObject?.to)}
        </Text>
      </View>
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <Text style={ styles.selectedNetworkStyle }>
        {localize('CURRENT_NETWORK')}
      </Text>
      <Text style={ styles.networkNameStyle }>{selectedNetwork.networkName}</Text>

      <View style={ styles.bgImageContainerStyle }>
        <CachedImage source={ IMAGES.CONFIRMATION_BG } style={ styles.bgStyle } />

        <View style={ styles.detailContainerStyle }>
          <ScrollView bounces={ false }>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <Text style={ styles.confirmStyle }>
              {localize('CONFIRM_TRANSACTION_UPPERCASE')}
            </Text>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />

            <View style={ styles.dappDetailViewStyle }>
              <Text style={ styles.dappTitleStyle }>{localize('DAPP')}</Text>
              <Text style={ styles.peetNameStyle }>{peerMeta.name}</Text>
            </View>
            <View style={ styles.accountDetailViewStyle }>
              <View style={ styles.lineViewStyle }>
                <CachedImage
                  source={ IMAGES.VERTICAL_DOTTET_LINE }
                  style={ styles.verticalLineStyle }
                />
                <View style={ styles.dotContainStyle }>
                  <View style={ styles.dotViewStyle } />
                  <CachedImage
                    style={ COMMON_STYLE.imageStyle(4) }
                    source={ IMAGES.DOWN_ARROW }
                  />
                  <View style={ styles.dotViewStyle } />
                </View>
              </View>
              <View style={ { flex: 1 } }>
                {renderFromAccountView()}
                {renderToAccountView()}
              </View>
            </View>
            <Text style={ styles.amountTitleStyle }>{localize('VALUE')}</Text>
            <View style={ styles.amountContainerViewStyle }>
              <Text style={ styles.amountStyle }>
                {`${txnObject?.value?.toString(6)} ${selectedNetwork.sym}`}
              </Text>
              <CachedImage
                source={ getTokenIcon('', selectedNetwork.chainId) }
                style={ COMMON_STYLE.imageStyle(7) }
              />
            </View>
            <View style={ styles.bottomBorderStyle }>
              <View style={ styles.rowViewStyle }>
                <Text
                  style={ styles.estimateStyle }
                  adjustsFontSizeToFit={ true }
                  numberOfLines={ 1 }>
                  {localize('ESTIMATE_GAS_FEE')}
                </Text>
                <TouchableOpacity
                  testID={ 'gas-fee-manage-btn' }
                  style={ styles.estimateFeeBtnStyle }
                  onPress={ () => setIsShowGasFeeView(true) }>
                  <Text
                    style={ styles.estimateFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {getEstimateFee()}
                  </Text>
                  <CachedImage
                    style={ COMMON_STYLE.imageStyle(4, COLORS.YELLOWFB) }
                    source={ IMAGES.DOWN_ARROW }
                  />
                </TouchableOpacity>
              </View>
              <View style={ styles.rowViewStyle }>
                <Text style={ styles.secondTextStyle }>
                  {localize('likely_second')}
                </Text>
                {selectedNetwork.txnType == 2 && (
                  <Text
                    style={ styles.maxFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {localize('MAX_FEE:')}
                    <Text style={ styles.feeStyle }>{getMaxFee()}</Text>
                  </Text>
                )}
              </View>
            </View>
            <View>
              <View style={ styles.rowViewStyle }>
                <Text style={ styles.estimateStyle }>{localize('TOTAL')}</Text>
                <Text
                  style={ styles.totalValueStyle }
                  adjustsFontSizeToFit={ true }
                  numberOfLines={ 1 }>
                  {getTotal()}
                </Text>
              </View>
              {selectedNetwork.txnType == 2 && (
                <View style={ styles.rowViewStyle }>
                  <Text
                    style={ styles.maxFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {localize('MAX_FEE:')}
                    <Text
                      adjustsFontSizeToFit={ true }
                      numberOfLines={ 1 }
                      style={ styles.feeStyle }>
                      {getMaxTotal()}
                    </Text>
                  </Text>
                </View>
              )}
            </View>
          </ScrollView>
        </View>
      </View>
      {!isLoading ? (
        <SwipeVerifyController
          style={ styles.swiperStyle }
          isSetReset={ !txnObject }
          onVerified={ () => slideToSendTransaction() }>
          <Text
            style={ [
              COMMON_STYLE.textStyle(14, COLORS.LIGHT_OPACITY(0.3), 'BOLD'),
            ] }>
            {localize('SLIDE_TO_SEND_TRANSACTION_UPPERCASE')}
          </Text>
        </SwipeVerifyController>
      ) : (
        <CachedImage source={ IMAGES.ROUNDED_LOADING } style={ styles.loadingStyle } />
      )}
      <ModalContainerView
        testID={ 'modal-gas-fee-manage' }
        isVisible={ isShowGasFeeView }
        onPressClose={ () => setIsShowGasFeeView(false) }>
        <GasFeeRangeView
          selectedRange={ selectedRange }
          gasRange={ gasRange }
          txnObject={ txnObject }
          network={ selectedNetwork }
          onPressSave={ updatedObject => {
            setIsShowGasFeeView(false);

            if (selectedNetwork.txnType != 0) {
              updateTxnObjectTypeTwo(
                updatedObject.gas,
                updatedObject.maxPrioFee,
                updatedObject.maxFeePerGas,
              );
              setSelectedRange(selectedRange);
            } else {
              updateTxnObjectTypeOne(updatedObject.gas, updatedObject.gasPrice);
            }
          } }
        />
      </ModalContainerView>

      {/* Error Message Modal */}
      <AlertActionModel
        alertTitle={ localize('ERROR') }
        isShowAlert={ isShowError }
        successBtnTitle={ localize('OK') }
        onPressSuccess={ () => {
          setIsShowError(false);
          navigation.goBack();
        } }>
        <Text
          style={ COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BASE', 'center') }>
          {getUserFriendlyErrorMessage(error)}
        </Text>
      </AlertActionModel>
    </SafeAreaWrapper>
  );
};

SendTransactionConfirmationScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default SendTransactionConfirmationScreen;
