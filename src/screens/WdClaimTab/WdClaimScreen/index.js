import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { GlobalAction, TransactionsAction } from '@redux';

// import components
import {
  AlertActionModel,
  GasFeeRangeView,
  ModalContainerView,
  SwipeVerifyController,
  CachedImage,
} from '@components';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import {
  IntegerUnits,
  displayAddress,
  useTokenIcons,
} from '@utils';

import {
  abi as ABI,
  WalletTransactionTypes,
  calculateEstimateFee,
  calculateMaxFee,
  claimWDTxnObj,
  createAndSendTxn,
  getContractInstance,
  getSingleAccount,
  updateTxnLists
} from '@web3';
import { getUserFriendlyErrorMessage } from '../../../utils/errors';
import { getNetwork } from '../../../redux';


const WdClaimScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const getTokenIcon = useTokenIcons();
  const networks = useSelector(state => state.networks);
  const wallet = useSelector(state => state.wallet);
  const transactions = useSelector(state => state.transactions);
  const selectedNetwork = useSelector(getNetwork);
  const [selectedRange, setSelectedRange] = useState(1);
  const [txnObject, setTxnObject] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [isShowGasFeeView, setIsShowGasFeeView] = useState(false);
  const [gasRange, setGasRange] = useState({});
  const [accountBalance, setAccountBalance] = useState(new IntegerUnits(0));
  const [transferAmount, setTransferAmount] = useState(new IntegerUnits(0));
  const [isShowError, setIsShowError] = useState(false);
  const [error, setError] = useState('');

  const networkDetail = networks.wdSupportObject;
  const [dividendsDetail] = useState(params.dividendsDetail);
  const [wdStateDetail] = useState(params.wdStateDetail);

  const isSupportUSD = true;
  const [usdValue, setUsdValue] = useState(0);

  const dispatch = useDispatch();

  useEffect(() => {
    getTxnObject();
    getAccountBalance();
  }, []);

  // Action Methods
  const getAccountBalance = async () => {
    const balance = await getSingleAccount(
      params.fromAccount.publicAddress,
      selectedNetwork,
    );

    if (params.isSweep === true) {
      setTransferAmount(
        new IntegerUnits(dividendsDetail.pendingDistribution.value, dividendsDetail.pendingDistribution.decimals)
          .multiply(dividendsDetail.balance)
          .divide(wdStateDetail.totalSupply)
      );
    } else {
      setTransferAmount(dividendsDetail.claimableDiv);
    }

    setUsdValue(balance.tokenValues.usdPrice);
    setAccountBalance(balance.account.value);
  };

  const getTxnObject = async () => {
    setIsLoading(true);

    const txObject = await claimWDTxnObj(
      networkDetail.wdAddress,
      params.fromAccount.publicAddress,
      selectedNetwork,
    );

    setGasRange(txObject.range);
    setIsLoading(false);
    setTxnObject(txObject.txnObject);
  };

  const updateTxnObjectTypeTwo = (gas, maxPrioFee, maxFeePerGas) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.maxPriorityFeePerGas = maxPrioFee;
    tempTxObj.maxFeePerGas = maxFeePerGas;
    setTxnObject(tempTxObj);
  };

  const updateTxnObjectTypeOne = (gas, gasPrice) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.gasPrice = gasPrice;
    setTxnObject(tempTxObj);
  };

  const getEstimateFee = () => {
    if (!txnObject) return '';
    const estimateFee = calculateEstimateFee(txnObject, selectedNetwork.txnType);
    return `${estimateFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getMaxFee = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getMaxTotal = () => {
    if (!txnObject) return '';
    const maxFee = txnObject.maxFeePerGas.multiply(txnObject.gas);
    return `${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const claimTokens = async () => {
    try {
      const swapContract = await getContractInstance(
        networkDetail.swapRouterV3,
        ABI['quotes'],
        networkDetail
      );

      const wdContract = await getContractInstance(
        networkDetail.wdAddress,
        ABI['wdtoken'],
        networkDetail
      );

      let tx = null;
      if (params.isSweep) {
        tx = await swapContract.contractInstance.methods.distributeAll('0');
        txnObject.to = networkDetail.swapRouterV3;
      } else {
        tx = await wdContract.contractInstance.methods.claimDividend(params.fromAccount.publicAddress, '0');
        txnObject.to = networkDetail.wdAddress;
      }

      txnObject.data = tx.encodeABI();

      const txnResult = await createAndSendTxn(
        WalletTransactionTypes.CLAIM,
        txnObject,
        wallet,
        params.fromAccount,
        accountBalance,
        selectedNetwork
      );

      if (txnResult.success) {
        const amount = params.isSweep
          ? dividendsDetail.pendingDistribution
            .multiply(dividendsDetail.balance)
            .divide(wdStateDetail.totalSupply)
          : dividendsDetail.claimableDiv;

        const { tempTxns, tempTxnsList } = await updateTxnLists(
          IntegerUnits.serialize({
            type: WalletTransactionTypes.CLAIM,
            transactionDetail: {
              accountName: params.fromAccount.name,
              displayAmount: amount,
              displayDecimal: '18',
              transferedAmount: amount,
              claimableDiv: amount,
              toAddress: networkDetail.wdAddress,
              wdAddress: networkDetail.wdAddress,
              fromAddress: params.fromAccount.publicAddress,
              wdTokenDetail: wdStateDetail,
              fromToken: { symbol: selectedNetwork.sym },
              networkDetail: selectedNetwork,
              isSupportUSD: isSupportUSD,
              usdValue: usdValue,
              utcTime: moment.utc().format(),
            },
            receipt: txnResult.receipt,
            blockNumber: txnResult.blockNumber,
          }),
          transactions,
          selectedNetwork
        );

        dispatch(TransactionsAction.saveTransactions(tempTxnsList));

        await StorageOperation.setMultiData([
          [ASYNC_KEYS.TRANSACTIONS, JSON.stringify(tempTxns)],
        ]);

        setIsLoading(false);
        dispatch(
          GlobalAction.showAlert(
            localize('SUCCESS'),
            localize('SUBMIT_SUCCESS'),
          ),
        );
        navigation.navigate('CLAIM_DASHBOARD_TAB', {
          dividendsDetail: dividendsDetail,
          wdStateDetail: wdStateDetail,
          fromAccount: params.fromAccount,
        });
      } else {
        setIsLoading(false);
        if (txnResult.error.toLowerCase().includes('insufficient funds')) {
          setError(localize('INSUFFICIENT_FUNDS', [selectedNetwork.sym]));
        } else {
          setError(txnResult.error);
        }
        setIsShowError(true);
      }
    } catch (err) {
      setIsLoading(false);
      console.error(err);
    }
  };

  const slideToClaimTokens = () => {
    setIsLoading(true);
    setTimeout(claimTokens, 0);
  };

  // render custom component
  const renderFromAccountView = () => {
    return (
      <View style={ styles.fromViewStyle }>
        <View style={ { flex: 1 } }>
          <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
            {localize('FROM:')}
          </Text>
          <Text style={ COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD') }>
            {params.fromAccount.name}
          </Text>
        </View>
        <View style={ { flex: 1 } }>
          <Text
            style={ COMMON_STYLE.textStyle(13, COLORS.GRAYDF, 'BOLD', 'right') }>
            {selectedNetwork.sym + ' '}
          </Text>
          <Text
            numberOfLines={ 1 }
            style={ COMMON_STYLE.textStyle(11, COLORS.GRAY94, 'BASE', 'right') }>
            {localize('BALANCE:')}
            <Text
              style={ COMMON_STYLE.textStyle(11, COLORS.WHITE, 'BASE', 'right') }>
              {accountBalance.toString(6)}
            </Text>
          </Text>
        </View>
        <CachedImage
          source={ getTokenIcon('', selectedNetwork.chainId) }
          style={ styles.tokenIconStyle }
        />
      </View>
    );
  };

  const renderToAccountView = () => {
    return (
      <View style={ styles.toViewStyle }>
        <Text style={ COMMON_STYLE.textStyle(11, COLORS.LIGHT_OPACITY(0.6)) }>
          {`${localize('TO')} (${wdStateDetail.sym})`}
        </Text>
        <Text numberOfLines={ 1 } style={ [styles.toAddressTextStyle] }>
          {displayAddress(networkDetail.wdAddress)}
          <CachedImage
            source={ IMAGES.WD_TOKEN_ICON }
            style={ styles.wdTokenIconStyle }
          />
        </Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={ COMMON_STYLE.safeAreaViewStyle }>
      <Text style={ styles.selectedNetworkStyle }>
        {localize('CURRENT_NETWORK')}
      </Text>
      <Text style={ styles.networkNameStyle }>{selectedNetwork.networkName}</Text>

      <View style={ styles.confirmationContainreStyle }>
        <CachedImage source={ IMAGES.CONFIRMATION_BG } style={ styles.bgStyle } />

        <View style={ styles.detailContainerStyle }>
          <ScrollView bounces={ false }>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <Text style={ styles.confirmStyle }>
              {localize('CONFIRM_TRANSACTION_UPPERCASE')}
            </Text>
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />
            <CachedImage source={ IMAGES.DOTTET_LINE } style={ styles.dottedLineStyle } />

            <View style={ styles.accountDetailViewStyle }>
              <View style={ styles.lineViewStyle }>
                <CachedImage
                  source={ IMAGES.VERTICAL_DOTTET_LINE }
                  style={ styles.verticalLineStyle }
                />
                <View style={ styles.dotContainStyle }>
                  <View style={ styles.dotViewStyle } />
                  <CachedImage
                    style={ COMMON_STYLE.imageStyle(4) }
                    source={ IMAGES.DOWN_ARROW }
                  />
                  <View style={ styles.dotViewStyle } />
                </View>
              </View>
              <View style={ { flex: 1 } }>
                {renderFromAccountView()}
                {renderToAccountView()}
              </View>
            </View>
            <Text style={ styles.amountTitleStyle }>
              {localize('DIVIDENDS_AVAILABLE_TO_CLAIM')}
            </Text>

            <View style={ styles.borderedViewStyle }>
              <View style={ styles.amountContainerViewStyle }>
                <Text
                  numberOfLines={ 1 }
                  adjustsFontSizeToFit={ true }
                  style={ styles.amountStyle }>
                  {`${transferAmount.toString(9)} ${selectedNetwork.sym}`}
                </Text>
                <CachedImage
                  source={ getTokenIcon('', selectedNetwork.chainId) }
                  style={ COMMON_STYLE.imageStyle(7) }
                />
              </View>

              {params.isSupportUSD && (
                <Text
                  style={ COMMON_STYLE.textStyle(
                    12,
                    COLORS.LIGHT_OPACITY(0.6),
                    'BASE',
                    'center',
                  ) }>
                  {`$${transferAmount.multiply(params.usdValue).toString(2)}`}
                </Text>
              )}
            </View>

            <View style={ styles.bottomBorderStyle }>
              <View style={ styles.rowViewStyle }>
                <Text
                  style={ styles.estimateStyle }
                  adjustsFontSizeToFit={ true }
                  numberOfLines={ 1 }>
                  {localize('ESTIMATE_GAS_FEE')}
                </Text>
                <TouchableOpacity
                  testID={ 'gas-fee-manage-btn' }
                  style={ styles.estimateFeeBtnStyle }
                  onPress={ () => setIsShowGasFeeView(true) }>
                  <Text
                    style={ styles.estimateFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {getEstimateFee()}
                  </Text>
                  <CachedImage
                    style={ COMMON_STYLE.imageStyle(4, COLORS.YELLOWFB) }
                    source={ IMAGES.DOWN_ARROW }
                  />
                </TouchableOpacity>
              </View>
              <View style={ styles.rowViewStyle }>
                <Text style={ styles.secondTextStyle }>
                  {localize('likely_second')}
                </Text>
                {selectedNetwork.txnType == 2 && (
                  <Text
                    style={ styles.maxFeeStyle }
                    adjustsFontSizeToFit={ true }
                    numberOfLines={ 1 }>
                    {localize('MAX_FEE:')}
                    <Text style={ styles.feeStyle }>{getMaxFee()}</Text>
                  </Text>
                )}
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
      {!isLoading ? (
        <SwipeVerifyController
          style={ styles.swiperStyle }
          isSetReset={ !txnObject }
          onVerified={ () => slideToClaimTokens() }>
          <Text
            style={ [
              COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.3), 'BOLD'),
            ] }>
            {localize('SLIDE_TO_CLAIM_DIVIDENDS_UPPERCASE')}
          </Text>
        </SwipeVerifyController>
      ) : (
        <CachedImage source={ IMAGES.ROUNDED_LOADING } style={ styles.loadingStyle } />
      )}
      <ModalContainerView
        testID={ 'modal-gas-fee-manage' }
        isVisible={ isShowGasFeeView }
        onPressClose={ () => setIsShowGasFeeView(false) }>
        <GasFeeRangeView
          selectedRange={ selectedRange }
          gasRange={ gasRange }
          txnObject={ txnObject }
          network={ selectedNetwork }
          onPressSave={ updatedObject => {
            setIsShowGasFeeView(false);

            if (selectedNetwork.txnType != 0) {
              updateTxnObjectTypeTwo(
                updatedObject.gas,
                updatedObject.maxPrioFee,
                updatedObject.maxFeePerGas,
              );
              setSelectedRange(selectedRange);
            } else {
              updateTxnObjectTypeOne(updatedObject.gas, updatedObject.gasPrice);
            }
          } }
        />
      </ModalContainerView>

      {/* Error Message Modal */}
      <AlertActionModel
        alertTitle={ localize('ERROR') }
        isShowAlert={ isShowError }
        successBtnTitle={ localize('OK') }
        onPressSuccess={ () => {
          setIsShowError(false);
          navigation.navigate('CLAIM_DASHBOARD_TAB', {
            dividendsDetail: dividendsDetail,
            wdStateDetail: wdStateDetail,
            fromAccount: params.fromAccount,
          });
        } }>
        <Text
          style={ COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BASE', 'center') }>
          {getUserFriendlyErrorMessage(error)}
        </Text>
      </AlertActionModel>
    </SafeAreaView>
  );
};

WdClaimScreen.propTypes = { navigation: PropTypes.any, route: PropTypes.any };
export default WdClaimScreen;
