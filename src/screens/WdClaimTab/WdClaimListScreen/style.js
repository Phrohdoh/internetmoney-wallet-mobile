import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,

  headerViewStyle: {
    flexDirection: 'row',
    paddingHorizontal: Responsive.getWidth(5),
  },
  headerTitleViewStyle: {
    marginHorizontal: Responsive.getWidth(3),
    flex: 1,
    justifyContent: 'center',
  },

  headerBtnStyle: { marginLeft: Responsive.getWidth(3) },

  addressViewStyle: {
    backgroundColor: COLORS.GRAY15,
    borderColor: COLORS.GRAY1F,
    borderWidth: 1,
    height: Responsive.getHeight(6),
    borderRadius: Responsive.getHeight(3.5),
    marginVertical: Responsive.getHeight(2),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: Responsive.getWidth(3),
  },
  accountDetailViewStyle: {
    justifyContent: 'flex-end',
    flex: 1,
  },
  accountAddressStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.YELLOWFB, 'BOLD'),
    width: '40%',
  },
  tokenBalanceStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginTop: Responsive.getHeight(2),
  },
  balanceStyle: {
    ...COMMON_STYLE.textStyle(24, COLORS.WHITE, 'BOLD', 'center'),
    marginRight: Responsive.getWidth(1),
  },
  balanceViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: Responsive.getHeight(2),
  },
  detailViewStyle: {
    height: Responsive.getHeight(6),
    borderTopWidth: 1,
    borderColor: COLORS.GRAY1F,
    flexDirection: 'row',
    alignItems: 'center',
  },
  lineSepratorStyle: {
    height: 1,
    backgroundColor: COLORS.GRAY1F,
    marginBottom: Responsive.getHeight(2),
  },
  detailTitleStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.GRAYB5, 'BOLD'),
    flex: 1,
  },
  detailValueStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.YELLOWFB, 'BOLD', 'right'),
    marginLeft: Responsive.getWidth(2),
  },
  accountListCellStyle: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    borderRadius: 25,
    alignItems: 'center',
    borderWidth: 2,
    borderColor: COLORS.GRAY1F,
    paddingHorizontal: Responsive.getWidth(4),
    marginTop: Responsive.getHeight(2),
  },
  infoButtonStyle: {
    marginLeft: Responsive.getHeight(2),
  },
  accountNameStyle: { ...COMMON_STYLE.textStyle(14, COLORS.WHITE), flex: 1 },
  networkDetailContainerStyle: {
    paddingHorizontal: Responsive.getWidth(5),
  },
  loadingConteinarStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: Responsive.getWidth(1),
  },
  dateStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
  },
  neverClaimedStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.YELLOWFB, 'BASE', 'center'),
  },
  claimBtnStyle: { marginTop: Responsive.getHeight(2), width: '60%' },
  whatTimeStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6), 'BOLD', 'center'),
    marginVertical: Responsive.getHeight(2),
  },
  learnMoreStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.YELLOWFB, 'BOLD'),
    textDecorationLine: 'underline',
  },
  loadingStyle: {
    ...COMMON_STYLE.imageStyle(8),
    alignSelf: 'center',
  },
});
