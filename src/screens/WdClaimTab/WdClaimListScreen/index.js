import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  Linking,
  TouchableOpacity,
} from 'react-native';

import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import {
  GlobalAction,
  AccountsAction,
  getAccountList,
} from '@redux';
import { COMMON_DATA } from '@constants';

// import components
import {
  SafeAreaWrapper,
  RoundedButton,
  ModalContainerView,
  CachedImage,
} from '@components';

// import themes
import { COMMON_STYLE, COLORS, IMAGES, bottomTabPadding } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import { useTokenIcons, displayAddress, IntegerUnits } from '@utils';

import * as Web3Layer from '@web3';
import { getNetwork } from '../../../redux';

const WdClaimListScreen = props => {
  const { navigation } = props;
  const getTokenIcon = useTokenIcons();
  const dispatch = useDispatch();
  const accountList = useSelector(getAccountList);
  const networks = useSelector(state => state.networks);
  const selectedNetwork = useSelector(getNetwork);
  const [selectedAccount, setSelectedAccount] = useState(undefined);
  const [isShowAccountList, setIsShowAccountList] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [dividendsDetail, setDividendsDetail] = useState({});
  const [wdStateDetail, setWdStateDetail] = useState({});
  const [refreshingScreen, setRefreshingScreen] = useState(false)

  const IS_SELECTED_DIVIDEND_ACCOUNT = 'isSelectedDividendAccount';

  const refreshScreen = () => {
    setRefreshingScreen(true)
    navigation.navigate('CLAIM_DASHBOARD_TAB', {
      dividendsDetail: dividendsDetail,
      wdStateDetail: wdStateDetail,
    });
  }

  // refreshScreen helper
  useEffect(() => {
    if (refreshingScreen) {
      const timer = setTimeout(() => {
        getDividendsDetail().then(() => {});
        setRefreshingScreen(false)
      }, 3000)
      return () => {
        clearTimeout(timer);
      };
    }
  }, [navigation, refreshingScreen])

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      if (accountList && !selectedAccount) {
        setSelectedAccount(getSelectedDividendAccount() ||  accountList[0]);
      } else {
        getWDStateDetail().then(() => {});
      }
    });

    return unsubscribe;
  }, [navigation, accountList]);

  // Our usage of useEffect and its dependencies are a bit of an issue and leads to stale data. We
  // should refactor our hooks to make use of useCallback, and making those callbacks themselves
  // into dependencies when applicable. This hook is only one of many such situations in this repo,
  // so I'll reserve that refactoring for a larger future effort. Adding stronger lint rules, such
  // as with eslint-config-principled, will help us to identify issues with hook dependencies.
  useEffect(() => {
    if (selectedAccount) {
      getWDStateDetail().then(() => {});
    }
  }, [selectedAccount, networks.wdSupportObject]);

  const getSelectedDividendAccount = () => {
    let selectedDividendAccount = false;

    if (accountList) {
      accountList.forEach((account) => {
        if (IS_SELECTED_DIVIDEND_ACCOUNT in account && account[IS_SELECTED_DIVIDEND_ACCOUNT]) {
          selectedDividendAccount = account;
        }
      });
    }

    return selectedDividendAccount;
  };

  const setSelectedDividendAccount = (selectedAccount) => {
    const accountsToSave = [];

    accountList.forEach((account) => {
      account[IS_SELECTED_DIVIDEND_ACCOUNT] = account.publicAddress === selectedAccount.publicAddress;
      accountsToSave.push(account);
    });

    setSelectedAccount(selectedAccount);
    setIsShowAccountList(false);

    dispatch(AccountsAction.saveAccountList(accountsToSave));
  };

  const getWDStateDetail = async () => {
    if (
      networks?.wdSupportObject?.success &&
      networks?.wdSupportObject?.wdAddress
    ) {
      setIsLoading(true);

      const wdDetail = await Web3Layer.getWDStats(
        networks.wdSupportObject.wdAddress,
        selectedNetwork,
      );

      if (wdDetail.success) {
        setWdStateDetail(wdDetail.stats);
      }
      getDividendsDetail().then(() => {});
    }
  };

  const getDividendsDetail = async () => {
    setIsLoading(true);

    const dividends = await Web3Layer.getUserDividendsInfo(
      selectedAccount.publicAddress,
      networks.wdSupportObject.wdAddress,
      networks.wdSupportObject.swapRouterV3,
      selectedNetwork,
    );

    setIsLoading(false);

    if (dividends.success) {
      setDividendsDetail(dividends.stats);
    }
  };

  const setTokenValue = value => {
    return `${value.toString(9)} ${selectedNetwork.sym}`;
  };

  // Render custom component
  const renderHeaderButtons = (image, onPress) => {
    return (
      <TouchableOpacity
        testID={ 'wd-claim-list-' + image + '-tabheader-button' }
        style={ styles.headerBtnStyle }
        onPress={ onPress }>
        <CachedImage source={ image } style={ COMMON_STYLE.imageStyle(6) } />
      </TouchableOpacity>
    );
  };
  const renderHeaderComponent = () => {
    return (
      <View style={ styles.headerViewStyle }>
        <CachedImage
          source={ IMAGES.TIME }
          style={ [COMMON_STYLE.imageStyle(8), { alignSelf: 'center' }] }
        />
        <View style={ styles.headerTitleViewStyle }>
          <Text style={ COMMON_STYLE.textStyle(18, COLORS.YELLOWFB, 'BOLD') }>
            {localize('WALLET_DIVIDENDS_UPPERCASE')}
          </Text>
        </View>
          { !refreshingScreen && renderHeaderButtons(IMAGES.REFRESH_ICON, () => {
            refreshScreen()
          })}
          { refreshingScreen &&
            <CachedImage source={IMAGES.ROUNDED_LOADING} style={styles.loadingStyle} />
          }
        {renderHeaderButtons(IMAGES.SUPPORT_ICON, () => {
          navigation.navigate('SUPPORT_SCREEN', {
            supportType: 'contactus',
          });
        })}
      </View>
    );
  };
  const renderDetailComponent = (title, value) => {
    return (
      <View style={ styles.detailViewStyle }>
        <Text numberOfLines={ 1 } style={ styles.detailTitleStyle }>
          {localize(title)}
        </Text>
        <Text
          numberOfLines={ 1 }
          adjustsFontSizeToFit={ true }
          style={ styles.detailValueStyle }>
          {`${value}`}
        </Text>
      </View>
    );
  };

  const renderAccountListCell = (account) => {
    return (
      <TouchableOpacity
        testID={ 'wd-account-list-cell-button' }
        style={ styles.accountListCellStyle }
        onPress={ () => {
          setSelectedDividendAccount(account);
        } }>
        <Text numberOfLines={ 1 } style={ styles.accountNameStyle }>
          {account.name}
        </Text>
      </TouchableOpacity>
    );
  };

  //  TODO: main Dividends detail view
  const renderDividendsDetail = () => {
    return (
      <React.Fragment>
        <View style={ styles.networkDetailContainerStyle }>
          {selectedAccount && (
            <TouchableOpacity
              style={ styles.addressViewStyle }
              onPress={ () => setIsShowAccountList(true) }>

              {/* } TODO: This is the top input that displays the account name and the percentage {*/}
              {/* } TODO: it is using the selectedAccount property that is set in the useEffect {*/}
              <View style={ styles.accountDetailViewStyle }>
                <Text style={ COMMON_STYLE.textStyle(11, COLORS.WHITE, 'BOLD') }>
                  {selectedAccount.name}
                </Text>
                <Text style={ styles.accountAddressStyle } numberOfLines={ 1 }>
                  {displayAddress(selectedAccount.publicAddress)}
                </Text>
              </View>
              {/* } TODO: This is the top input that displays the account name and the percentage {*/}

              <CachedImage
                source={ IMAGES.RIGHT_ARROW }
                style={ COMMON_STYLE.imageStyle(4) }
              />
            </TouchableOpacity>
          )}

          {/* } TODO: This is the next section down displaying the Available to Claim text and the info bubble {*/}
          <View style={ styles.balanceViewStyle }>
            <Text style={ styles.tokenBalanceStyle } >
              {localize('AVAILABLE_CLAIM')}
            </Text>
            <TouchableOpacity onPress={ () => {
              dispatch(
                GlobalAction.showAlert(
                  '',
                  localize('AVAILABLE_TO_CLAIM_INFO'),
                ),
              );
            } }>
                <CachedImage
                source={ IMAGES.SUPPORT_ICON }
                style={ [COMMON_STYLE.imageStyle(5), { alignSelf: 'center', marginLeft: 10, marginTop: 10 }] }
              />
            </TouchableOpacity>

            
          </View>
          {/* } TODO: This is the next section down displaying the Available to Claim text and the info bubble {*/}

          {/* } TODO: This is the next section down displaying the selected network token and the amount available {*/}
          <View style={ styles.balanceViewStyle }>
            <Text style={ styles.balanceStyle }>
              {dividendsDetail.claimableDiv
                .add(
                  dividendsDetail.pendingDistribution
                    .multiply(dividendsDetail.balance)
                    .divide(wdStateDetail.totalSupply)
                )
                .toString(9)}
              {' '}
              {selectedNetwork.sym}
            </Text>
            <CachedImage
              style={ COMMON_STYLE.imageStyle(6) }
              source={ getTokenIcon('', selectedNetwork.chainId) }
            />
          </View>
          {/* } TODO: This is the next section down displaying the selected network token and the amount available {*/}

        </View>


        <View style={ COMMON_STYLE.borderedContainerViewStyle(bottomTabPadding) }>

          {/*} TODO: the main TIME balance section {*/}
          <ScrollView>
            <Text style={ styles.tokenBalanceStyle }>
              {localize('TIME_BALANCE')}
            </Text>
            <View style={ styles.balanceViewStyle }>
              <Text style={ styles.balanceStyle }>
                {`${dividendsDetail.balance.toString(2)} ${wdStateDetail.sym}`}
              </Text>
              <CachedImage
                style={ COMMON_STYLE.imageStyle(6) }
                source={ IMAGES.TIME }
              />
            </View>
            {renderDetailComponent(
              'TOTAL_DIVIDENDS_CLAIMED',
              setTokenValue(
                dividendsDetail.totalEarnedDiv.subtract(dividendsDetail.claimableDiv).toString(9)
              ),
            )}
            {renderDetailComponent(
              'CLAIMABLE_DIVIDENDS',
              setTokenValue(
                dividendsDetail.claimableDiv,
              ),
            )}
            {renderDetailComponent(
              'SWEEPABLE_DIVIDENDS',
              setTokenValue(
                dividendsDetail.pendingDistribution.multiply(dividendsDetail.balance).divide(wdStateDetail.totalSupply).toString(9),
              ),
            )}
            {/*} TODO: the 3 sections of info {*/}

            {/*} TODO: SWEEP yellow button {*/}
            <View style={ styles.balanceViewStyle }>
                <RoundedButton
                  style={ styles.claimBtnStyle }
                  title={ localize('SWEEP_DIVIDENDS_UPPERCASE') }
                  onPress={ () => {
                    if (dividendsDetail.pendingDistribution.multiply(dividendsDetail.balance).divide(wdStateDetail.totalSupply).gt(new IntegerUnits(0))) {
                      navigation.navigate('WD_CLAIM_SCREEN', {
                        dividendsDetail: dividendsDetail,
                        wdStateDetail: wdStateDetail,
                        fromAccount: selectedAccount,
                        isSweep: true,
                      });
                    } else {
                      dispatch(
                        GlobalAction.showAlert(
                          '',
                          localize('SWEEP_ZERO_BALANCE_ERROR_MSG'),
                        ),
                      );
                    }
                  } }
                />

                <TouchableOpacity onPress={ () => {
                  dispatch(
                    GlobalAction.showAlert(
                      '',
                      localize('SWEEP_DIVIDENDS_INFO'),
                    ),
                  );
                } }>
                <CachedImage
                  source={ IMAGES.SUPPORT_ICON }
                  style={ [COMMON_STYLE.imageStyle(5), { alignSelf: 'center', marginLeft: 10, marginTop: 10 }] }
                />
              </TouchableOpacity>
            </View>
            {/*} TODO: SWEEP yellow button {*/}

            {/*} TODO: CLAIM yellow button {*/}
            <View style={ styles.balanceViewStyle }>
              <RoundedButton
                style={ styles.claimBtnStyle }
                title={ localize('CLAIM_DIVIDENDS_UPPERCASE') }
                onPress={ () => {
                  if (dividendsDetail.claimableDiv.gt(new IntegerUnits(0))) {
                    navigation.navigate('WD_CLAIM_SCREEN', {
                      dividendsDetail: dividendsDetail,
                      wdStateDetail: wdStateDetail,
                      fromAccount: selectedAccount,
                      isSweep: false,
                    });
                  } else {
                    dispatch(
                      GlobalAction.showAlert(
                        '',
                        localize('CLAIM_ZERO_BALANCE_ERROR_MSG'),
                      ),
                    );
                  }
                } }
              />

              <TouchableOpacity onPress={ () => {
                  dispatch(
                    GlobalAction.showAlert(
                      '',
                      localize('CLAIM_DIVIDENDS_INFO'),
                    ),
                  );
                } }>
                  <CachedImage
                    source={ IMAGES.SUPPORT_ICON }
                    style={ [COMMON_STYLE.imageStyle(5), { alignSelf: 'center', marginLeft: 10, marginTop: 10 }] }
                  />
              </TouchableOpacity>
            </View>
            
            {/*} TODO: CLAIM yellow button {*/}

            {/*} TODO: bottom learn more links {*/}
            <Text style={ styles.whatTimeStyle }>
              {localize('WHAT_IS_TIME')}
              <Text
                style={ styles.learnMoreStyle }
                onPress={ () => Linking.openURL(COMMON_DATA.TIME_LINK) }>
                {localize('LEARN_MORE_HERE')}
              </Text>
            </Text>
            {/*} TODO: bottom learn more links {*/}

          </ScrollView>
        </View>
      </React.Fragment>
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      {renderHeaderComponent()}
      {!isLoading ? (
        networks?.wdSupportObject?.success &&
        networks?.wdSupportObject?.wdAddress ? (
            Object.keys(dividendsDetail).length > 0 && renderDividendsDetail()
          ) : (
            <View style={ styles.loadingConteinarStyle }>
              <Text
                style={ COMMON_STYLE.textStyle(
                  12,
                  COLORS.LIGHT_OPACITY(0.6),
                  'BOLD',
                  'center',
                ) }>
                {localize('TIME_NOT_SUPPORTED')}
              </Text>
              <Text style={ styles.whatTimeStyle }>
                {localize('WHAT_IS_TIME')}
                <Text
                  style={ styles.learnMoreStyle }
                  onPress={ () => Linking.openURL(COMMON_DATA.TIME_LINK) }>
                  {localize('LEARN_MORE_HERE')}
                </Text>
              </Text>
            </View>
          )
      ) : (
        <View style={ styles.loadingConteinarStyle }>
          <CachedImage
            source={ IMAGES.ROUNDED_LOADING }
            style={ COMMON_STYLE.imageStyle(15) }
          />
        </View>
      )}

      {/*} TODO: this is the bottom modal that allows a user to select an account {*/}
      <ModalContainerView
        testID={ 'modal-send-token' }
        isVisible={ isShowAccountList }
        onPressClose={ () => setIsShowAccountList(false) }>
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('CHOOSE_ACCOUNTS_UPPERCASE')}
          </Text>
          <ScrollView
            horizontal
            contentContainerStyle={ { width: '100%' } }
            scrollEnabled={ false }>
            <FlatList
              data={ accountList }
              bounces={ false }
              renderItem={ ({ item }) => renderAccountListCell(item) }
              ItemSeparatorComponent={ () => (
                <View style={ styles.listSepratorStyle } />
              ) }
              keyExtractor={ (item, index) => index + item }
            />
          </ScrollView>
        </React.Fragment>
      </ModalContainerView>
      {/*} TODO: this is the bottom modal that allows a user to select an account {*/}

    </SafeAreaWrapper>
  );
};

WdClaimListScreen.propTypes = { navigation: PropTypes.any };
export default WdClaimListScreen;
