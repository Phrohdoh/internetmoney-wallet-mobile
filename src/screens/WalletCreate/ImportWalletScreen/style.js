import {StyleSheet} from 'react-native';

import {COMMON_STYLE, COLORS} from '@themes';
import {Responsive} from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  infoMsgstyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6)),
    marginTop: Responsive.getHeight(3),
  },
  inputContainerStyle: {
    marginTop: Responsive.getHeight(1),
  },
  scrollStyle: {
    marginVertical: Responsive.getHeight(1),
  },
  inputViewStyle: {
    height: '',
  },
  inputStyle: {
    height: '',
    minHeight: Responsive.getHeight(16),
  },
  addBtnStyle: {
    width: '30%',
    height: '80%',
    marginHorizontal: Responsive.getWidth(2),
  },
});
