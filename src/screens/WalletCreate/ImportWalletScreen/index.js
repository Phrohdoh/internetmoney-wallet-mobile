import React, { useState } from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { GlobalAction, WalletAction } from '@redux';
const bip39 = require('bip39');

// import components
import {
  SafeAreaWrapper,
  RoundedButton,
  Tags,
  SelectCountComponent,
  TitleTextInput,
} from '@components';

import { ASYNC_KEYS } from '@constants';
import { COMMON_STYLE } from '@themes';
import { localize } from '@languages';
import { StorageOperation } from '@storage';
import { styles } from './style';
import * as Web3Layer from '@web3';
import { getNetwork } from '../../../redux';
import KeyboardAvoidScrollView from '../../../components/KeyboardAvoidScrollView';

const sanitizePhrase = (phrase) => (
  phrase.replace(/[^a-zA-Z ]/g, '') // filter for characters and spaces
);

const countList = [12, 15, 18, 21, 24];

const ImportWalletScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const dispatch = useDispatch();
  const selectedNetwork = useSelector(getNetwork);

  const [phrase, setPhrase] = useState('');
  const [selectedCount, setSelectedCount] = useState(24);
  const words = phrase
    .split(/\s+/)
    .filter(word => word.length > 0);

  const onChangeText = (text) => {
    setPhrase(sanitizePhrase(text));
  }

  const checkPhraseValidation = () => words.length === selectedCount;

  // Import wallet
  const getStarted = async () => {

    if (checkPhraseValidation()) {
      const intialDetails = {
        mnemonic: words.join(' '),
        password: params.password,
        numberOfWords: selectedCount,
      };

      dispatch(GlobalAction.isShowLoading(true));
      const wallet = await Web3Layer.createNewWallet(
        intialDetails,
        selectedNetwork,
      );

      dispatch(GlobalAction.isShowLoading(false));
      /* istanbul ignore else */
      if (!wallet.success) {
        // show error alert
        dispatch(
          GlobalAction.showAlert(
            localize('ERROR'),
            localize('IMPORT_WALLET_ERROR'),
          ),
        );
        return;
      }

      const { error } = await StorageOperation.setMultiData([
        [ASYNC_KEYS.IS_LOGIN, 'true'],
        [ASYNC_KEYS.WALLET_DETAIL, JSON.stringify(wallet)],
        [
          ASYNC_KEYS.ACCOUNT_LIST,
          JSON.stringify([
            {
              publicAddress: wallet.publicAddress,
              isHardware: false,
              isImported: false, // false because we imported the entire seed phrase, not just this one account
              isArchived: false,
              isPopularTokens: true,
              name: 'Account 1',
            },
          ]),
        ],
        [ASYNC_KEYS.IS_SET_PASSCODE, params.isSetPasscode ? 'true' : 'false'],
        [ASYNC_KEYS.IS_UPDATED_HASH, 'true'],
      ]);

      if (!error) {
        dispatch(
          WalletAction.initApp(wallet, params.password, params.isSetPasscode),
        );
        navigation.navigate('GET_START_INFO_SCREEN');
      } else {
        GlobalAction.showAlert(localize('ERROR'), 'Something went wrong!');
      }
    } else {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('PHRASE_VALIDATION_MSG'),
        ),
      );
    }
  };

  return (
    <SafeAreaWrapper>
      <KeyboardAvoidScrollView>
        <Text style={ COMMON_STYLE.infoTitleStyle() }>
          {localize('IMPORT_WALLET_UPPERCASE')}
        </Text>
        <Text style={ styles.infoSubTitleStyle() }>
          {localize('IMPORT_WALLET_MSG')}
        </Text>
        <SelectCountComponent
          wordCounts={ countList }
          selectedCount={ selectedCount }
          onPressCount={ count => {
            setSelectedCount(count);
          } }
        />
        <TitleTextInput
          autoFocus={false}
          placeholder={localize('ENTER_PHRASE_MSG')}
          testID={ `import-wallet-screen-add-tag` }
          inputContainerStyle={ styles.inputContainerStyle }
          inputViewStyle={ styles.inputViewStyle }
          inputStyle={ styles.inputStyle }
          value={ phrase }
          onChangeText={ onChangeText }
          autoCapitalize="none"
          autoCorrect={false}
          multiline
        />
      </KeyboardAvoidScrollView>
      <RoundedButton
        testID="import-wallet-getstart-button"
        style={ COMMON_STYLE.roundedBtnMargin(1) }
        title={ localize('GET_START_UPPERCASE') }
        onPress={ () => getStarted() }
      />
    </SafeAreaWrapper>
  );
};

ImportWalletScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default ImportWalletScreen;
