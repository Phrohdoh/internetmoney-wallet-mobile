import React, { useEffect, useState, useRef } from 'react';

import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  ImageBackground,
  Animated,
  FlatList,
} from 'react-native';
import PropTypes from 'prop-types';

import { useSelector } from 'react-redux';

// import components
import { RoundedButton, CachedImage } from '@components';

// import themes
import { IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

import { Responsive } from '@helpers';

// import style
import { styles } from './style';

// import Utils
import { log } from '@utils';

import * as Web3Layer from '@web3';
import { getNetwork } from '../../../redux';

const pageWidth = Responsive.getWidth(88);
const walletImagesList = [
  'WALLET_IMAGE_ONE',
  'WALLET_IMAGE_TWO',
  'WALLET_IMAGE_THREE',
  'WALLET_IMAGE_FOUR',
  'WALLET_IMAGE_FOUR',
];

const walletCreateStrings = localize('WALLET_CREATE_STRINGS');

const WalletCreateScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  let currentIndex = 0;
  const selectedNetwork = useSelector(getNetwork);

  const scrollViewRef = useRef(null);
  const listViewRef = useRef(null);

  const [progress, setProgress] = useState(new Animated.Value(0));
  const [isProcressComplete, setIsProcressComplete] = useState(false);
  const [isWalletCreated, setIsWalletCreated] = useState(false);
  const [walletDetail, setWalletDetail] = useState({});

  useEffect(() => {
    setTimeout(() => {
      _toNextPage(currentIndex);
    }, 1000);
    progressAnimation();

    setTimeout(() => {
      createWallet(params.password);
    }, 50);
  }, []);

  // Create wallet
  const createWallet = async password => {
    const intialDetails = {
      mnemonic: null,
      password: password,
      numberOfWords: 24,
    };

    const wallet = await Web3Layer.createNewWallet(
      intialDetails,
      selectedNetwork,
    );

    /* istanbul ignore if */
    if (!wallet.success) {
      // show error alert
      return;
    }

    setWalletDetail(wallet);
    setIsWalletCreated(true);
  };

  const progressAnim = progress.interpolate({
    inputRange: [0, 1],
    outputRange: ['0%', '100%'],
  });

  const barWidth = {
    width: progressAnim,
  };

  const progressAnimation = () => {
    Animated.timing(progress, {
      toValue: 1,
      duration: 6000,
      useNativeDriver: false,
      isInteraction: false,
    }).start(() => {
      setIsProcressComplete(true);
    });
  };

  /* istanbul ignore next */
  const _onMomentumScrollEnd = ({ nativeEvent }) => {
    const index = Math.round(nativeEvent.contentOffset.x / pageWidth);

    if (index !== currentIndex) {
      currentIndex = index;

      setTimeout(() => {
        _toNextPage(index);
      }, 1000);
    }
  };

  const _toNextPage = index => {
    const nextIndex = index + 1;

    /* istanbul ignore else */
    if (nextIndex < walletImagesList.length) {
      try {
        {
          /* listViewRef.current.scrollToIndex({
          animated: true,
          index: nextIndex,
        });*/
        }
        scrollViewRef.current?.scrollTo({
          x: pageWidth * nextIndex,
          animated: true,
        });
      } catch (e) {
        log('scroll error >>>', e);
      }
    }
  };

  // render Coustom component
  const RenderWalletImages = ({ item, index }) => {
    return (
      <View
        key={ index }
        style={ {
          width: `${100 / walletImagesList.length}%`,
          height: '100%',
        } }>
        <CachedImage
          style={ styles.walletImgStyle }
          source={ IMAGES[item] }
          key={ index }
        />
      </View>
    );
  };

  const CreateWalletSuccessView = () => {
    return (
      <View
        style={ { justifyContent: 'space-between', flex: 1 } }
        key="create-wallet-view">
        <SafeAreaView
          style={ styles.walletContainerStyle }
          key="create-wallet-safearea">
          {/* <View style={styles.walletContainerStyle}>*/}
          <ScrollView
            ref={ scrollViewRef }
            contentContainerStyle={ {
              width: `${walletImagesList.length * 100}%`,
            } }
            snapToInterval={ pageWidth }
            horizontal
            scrollEnabled={ false }
            pagingEnabled
            key="create-wallet-scrollview"
            scrollEventThrottle={ 16 }
            onMomentumScrollEnd={ event => _onMomentumScrollEnd(event) }>
            {walletImagesList.map((item, index) => {
              return (
                <RenderWalletImages item={ item } key={ index } index={ index } />
              );
            })}
          </ScrollView>
          {/* </View>*/}
        </SafeAreaView>
        <View style={ styles.progressViewStyle }>
          <Animated.View
            style={ [styles.progressBarStyle, barWidth] }
            isInteraction={ false }
          />
        </View>
        <View style={ { height: Responsive.getHeight(5) } }>
          <Text style={ styles.descTextStyle }>{walletCreateStrings}</Text>
          {/* <FlatList
            ref={listViewRef}
            inverted
            onScrollToIndexFailed={() => {}}
            data={walletCreateStrings}
            renderItem={({ item }) => (
              <Text style={styles.descTextStyle}>{item}</Text>
            )}
          /> */}
        </View>

        <View style={ styles.roundCurveStyle }>
          <SafeAreaView>
            <Text style={ styles.createWalletStyle }>
              {localize('CREATE_WALLET')}
            </Text>
            <Text style={ styles.walletStyle }>
              {localize('WALLET_UPPERCASE')}
            </Text>
            <CachedImage source={ IMAGES.loading } style={ styles.loadingStyle } />
          </SafeAreaView>
        </View>
      </View>
    );
  };

  const RenderWalletSuccessView = () => {
    return (
      <View style={ { flex: 1 } }>
        <SafeAreaView style={ { alignItems: 'center', flex: 1 } }>
          <CachedImage source={ IMAGES.IM_TOKEN_ICON } style={ styles.walletImageStyle } />
          <Text style={ styles.congratesStyle }>
            {localize('CONGRATULATIONS_UPPERCASE')}
          </Text>
          <Text style={ styles.successMsgStyle }>
            {localize('WALLET_CREATE_SUCCESS_MSG')}
          </Text>
        </SafeAreaView>

        <View style={ [styles.roundCurveStyle] }>
          <SafeAreaView>
            <ScrollView style={ styles.scrollStyle }>
              <Text style={ styles.readyStartMsg }>
                {localize('ALMOST_THERE')}
              </Text>
              <Text style={ styles.contninueMsgStyle }>
                {localize('CONTINUE_INFO_MSG')}
              </Text>
            </ScrollView>
            <RoundedButton
              testID={ 'wallet-create-continue-button' }
              style={ styles.btnStyle }
              title={ localize('CONTINUE_UPPERCASE') }
              onPress={ () => {
                navigation.navigate('SECRET_REVOVERY_PHRASE_SCREEN', {
                  walletDetail: walletDetail,
                  password: params.password,
                  isSetPasscode: params.isSetPasscode,
                });
              } }
            />
          </SafeAreaView>
        </View>
      </View>
    );
  };

  return (
    <View>
      <ImageBackground
        source={ IMAGES.BG_GRID_IMG }
        style={ styles.backgroundStyle }>
        {isProcressComplete && isWalletCreated ? (
          <RenderWalletSuccessView />
        ) : (
          <CreateWalletSuccessView />
        )}
      </ImageBackground>
    </View>
  );
};

WalletCreateScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default WalletCreateScreen;
