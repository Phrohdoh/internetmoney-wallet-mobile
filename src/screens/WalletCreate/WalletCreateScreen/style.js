import { Platform, StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  backgroundStyle: {
    height: '100%',
    width: '100%',
    backgroundColor: COLORS.BLACK,
  },
  walletContainerStyle: {
    marginHorizontal: Responsive.getWidth(6),
    alignItems: 'center',
    marginBottom: Responsive.getHeight(8),
    marginTop: Responsive.getHeight(3),
  },
  walletImgStyle: {
    ...COMMON_STYLE.imageStyle(40),
    alignSelf: 'center',
    marginTop: Responsive.getHeight(3),
  },
  progressViewStyle: {
    width: '80%',
    backgroundColor: COLORS.GRAY2E,
    height: 6,
    borderRadius: 3,
    alignSelf: 'center',
  },
  progressBarStyle: {
    height: '100%',
    borderRadius: 3,
    backgroundColor: COLORS.YELLOWFB,
  },
  descTextStyle: {
    ...COMMON_STYLE.textStyle(
      18,
      COLORS.LIGHT_OPACITY(0.6),
      undefined,
      'center',
    ),
    height: Responsive.getHeight(5),
  },
  createWalletStyle: {
    ...COMMON_STYLE.textStyle(
      26,
      COLORS.LIGHT_OPACITY(0.6),
      undefined,
      'center',
    ),
    marginTop: Responsive.getHeight(10),
  },
  walletStyle: {
    ...COMMON_STYLE.textStyle(50, COLORS.YELLOWFB, 'BOLD', 'center'),
    marginTop: Responsive.getHeight(3),
    marginBottom: Responsive.getHeight(3),
  },
  roundCurveStyle: {
    justifyContent: 'flex-end',
    backgroundColor: COLORS.GRAY18,
    width: '100%',

    felx: 1,
    paddingHorizontal: Responsive.getWidth(6),
    marginTop: Responsive.getHeight(4),
    borderTopLeftRadius: Responsive.getWidth(5),
    borderTopRightRadius: Responsive.getWidth(5),
  },
  scrollStyle: { marginVertical: Responsive.getHeight(3) },
  loadingStyle: {
    ...COMMON_STYLE.imageStyle(25),
    alignSelf: 'center',
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  fadingContainer: {
    padding: 20,
    backgroundColor: 'powderblue',
  },
  fadingText: {
    fontSize: 28,
  },
  buttonRow: {
    flexBasis: 100,
    justifyContent: 'space-evenly',
    marginVertical: 16,
  },

  containerStyle: {
    marginHorizontal: 0,
  },
  walletImageStyle: {
    ...COMMON_STYLE.imageStyle(35),
    marginTop: Platform.OS === 'android' ? Responsive.getHeight(10) : 0,
  },
  congratesStyle: {
    ...COMMON_STYLE.textStyle(23, COLORS.YELLOWFB, 'BOLD', 'center'),
    marginTop: Responsive.getHeight(5),
  },
  successMsgStyle: {
    ...COMMON_STYLE.textStyle(20, COLORS.WHITE, undefined, 'center'),
    lineHeight: Responsive.getFontSize(24) + 10,
    marginTop: Responsive.getHeight(2),
  },
  successViewStyle: { flex: 1, justifyContent: 'flex-end' },
  readyStartMsg: {
    ...COMMON_STYLE.textStyle(16, COLORS.LIGHT_OPACITY(0.6), 'BOLD', 'center'),
    marginBottom: Responsive.getHeight(2),
    lineHeight: Responsive.getFontSize(17) + 10,
  },
  contninueMsgStyle: {
    ...COMMON_STYLE.textStyle(15, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    lineHeight: Responsive.getFontSize(16) + 10,
  },
  btnStyle: {
    ...COMMON_STYLE.roundedBtnMargin(),
  },
});
