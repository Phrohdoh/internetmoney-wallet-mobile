import React from 'react';

import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  ImageBackground,
  Linking,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';

// import components
import { RoundedButton, CachedImage } from '@components';

import { COMMON_DATA } from '@constants';
// import themes
import { IMAGES, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

const GetStartInfoScreen = props => {
  const { navigation } = props;

  const renderMediaImage = (image, link) => {
    return (
      <TouchableOpacity onPress={ () => Linking.openURL(link) }>
        <CachedImage style={ COMMON_STYLE.imageStyle(14) } source={ image } />
      </TouchableOpacity>
    );
  };
  const RenderInfoView = () => {
    return (
      <View style={ { flex: 1 } }>
        <SafeAreaView style={ { alignItems: 'center' } }>
          <CachedImage source={ IMAGES.IM_TOKEN_ICON } style={ styles.walletImageStyle } />
          <Text style={ styles.welcomeTitleStyle }>
            {localize('WELCOME_TITLE')}
          </Text>
          <Text style={ styles.internetMoneyStyle }>
            {localize('INTERNET_MONEY_WALLET')}
          </Text>
        </SafeAreaView>

        <View style={ [styles.roundCurveStyle] }>
          <SafeAreaView style={ { flex: 1 } }>
            <ScrollView style={ styles.scrollStyle }>
              <Text style={ styles.contninueMsgStyle }>
                {localize('GET_START_INFO_MSG')}
              </Text>
              <View style={ styles.mediaViewStyle }>
                {renderMediaImage(IMAGES.GLOB_ICON, COMMON_DATA.SITE_LINK)}
                {renderMediaImage(
                  IMAGES.TWITTER_ICON,
                  COMMON_DATA.TWITTER_LINK,
                )}
                {renderMediaImage(
                  IMAGES.TELEGRAM_ICON,
                  COMMON_DATA.TELEGRAM_LINK,
                )}
                {renderMediaImage(IMAGES.REDDIT_ICON, COMMON_DATA.REDDIT_LINK)}
              </View>
              <Text style={ styles.contninueMsgStyle }>
                {localize('GET_START_THANKYOU_MSG')}
              </Text>
            </ScrollView>
            <RoundedButton
              testID={ 'wallet-create-continue-button' }
              style={ styles.btnStyle }
              title={ localize('GET_START_UPPERCASE') }
              onPress={ () => {
                navigation.reset({
                  index: 0,
                  routes: [
                    {
                      name: 'DASHBOARD_TAB_NAVIGATOR',
                    },
                  ],
                });
              } }
            />
          </SafeAreaView>
        </View>
      </View>
    );
  };

  return (
    <View>
      <ImageBackground
        source={ IMAGES.BG_GRID_IMG }
        style={ styles.backgroundStyle }>
        <RenderInfoView />
      </ImageBackground>
    </View>
  );
};

GetStartInfoScreen.propTypes = {
  navigation: PropTypes.any,
};
export default GetStartInfoScreen;
