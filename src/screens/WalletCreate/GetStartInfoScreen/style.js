import { StyleSheet, Platform } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  welcomeTitleStyle: {
    ...COMMON_STYLE.textStyle(20, COLORS.WHITE, 'BASE', 'center'),
    marginTop: Responsive.getHeight(2),
  },
  internetMoneyStyle: {
    ...COMMON_STYLE.textStyle(30, COLORS.YELLOWFB, 'PIX_BOY', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  backgroundStyle: {
    height: '100%',
    width: '100%',
    backgroundColor: COLORS.BLACK,
  },

  roundCurveStyle: {
    justifyContent: 'flex-end',
    backgroundColor: COLORS.GRAY18,
    width: '100%',
    flex: 1,
    paddingHorizontal: Responsive.getWidth(6),
    marginTop: Responsive.getHeight(3),
    borderTopLeftRadius: Responsive.getWidth(5),
    borderTopRightRadius: Responsive.getWidth(5),
  },
  scrollStyle: {
    marginVertical: Responsive.getHeight(3),
  },
  walletImageStyle: {
    ...COMMON_STYLE.imageStyle(30),
    marginTop:
      Platform.OS === 'android'
        ? Responsive.getHeight(5)
        : Responsive.getHeight(3),
  },

  mediaViewStyle: {
    flexDirection: 'row',
    marginVertical: Responsive.getHeight(2),
    justifyContent: 'space-between',
    alignSelf: 'center',
    width: '80%',
  },
  contninueMsgStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.LIGHT_OPACITY(0.6), 'BASE'),
  },
  btnStyle: {
    ...COMMON_STYLE.roundedBtnMargin(),
  },
});
