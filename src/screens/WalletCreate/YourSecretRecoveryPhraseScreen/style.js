import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  infoMsgstyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6)),
    marginTop: Responsive.getHeight(2),
  },
  tagViewStyle: {
    marginVertical: Responsive.getHeight(2),
    flex: 1,
  },
  confirmationViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Responsive.getHeight(2),
    width: '100%',
  },
  confirmStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6)),
    marginLeft: Responsive.getWidth(2),
    flex: 1,
  },
  swiperStyle: {
    marginBottom: Responsive.getHeight(2),
  },
});
