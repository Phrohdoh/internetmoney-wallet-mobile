import React, { useRef, useEffect, useState } from 'react';
import { AppState, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import Toast from 'react-native-toast-message';
import Clipboard from '@react-native-clipboard/clipboard';

import { useDispatch } from 'react-redux';

import { GlobalAction, WalletAction } from '@redux';

// import components
import { AlertActionModel, SafeAreaWrapper, CheckBox, Tags } from '@components';

import SwipeVerifyController from '@components/SwipeVerifyController';

// import constants
import { ASYNC_KEYS } from '@constants';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import storage functions
import { StorageOperation } from '@storage';

// import style
import { styles } from './style';

import * as Web3Layer from '@web3';
import RNScreenshotPrevent, { addListener } from 'react-native-screenshot-prevent';
import { BlurView } from '@react-native-community/blur';

const YourSecretRecoveryPhraseScreen = props => {
  
  const { navigation } = props;
  const params = props.route.params;

  const dispatch = useDispatch();

  const [secretPhrase, setSecretPhrase] = useState([]);
  const [secretPhraseString, setSecretPhraseString] = useState('');
  const [isConfirm, setIsConfirm] = useState(false);

  const [showScreenShotAlert, setShowScreenShotAlert] = useState(false);
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  useEffect(() => {
    getSecretPhrase();

    if (Platform.OS === 'ios') {
      RNScreenshotPrevent && RNScreenshotPrevent.enabled(true);
    }

    const screenshotSubscription = addListener(() => {
      setShowScreenShotAlert(true);
    });
    const appStateSubscription = AppState.addEventListener('change', nextAppState => {
      appState.current = nextAppState;
      setAppStateVisible(appState.current);
    });
    return () => {
      RNScreenshotPrevent && RNScreenshotPrevent.enabled(false);
      screenshotSubscription.remove();
      appStateSubscription.remove();
    };
  }, []);

  // Create wallet
  const getSecretPhrase = async () => {
    const mnemonicObject = await Web3Layer.decryptMnemonicSecurely(
      params.walletDetail.mnemonic,
      params.password,
    );

    setSecretPhraseString(mnemonicObject.mnemonic);

    const phraseArray = mnemonicObject.mnemonic.split(' ');
    setSecretPhrase(phraseArray);
  };

  const storeWalletAccountData = async () => {
    // Move on another screen
    const { error } = await StorageOperation.setMultiData([
      [ASYNC_KEYS.IS_LOGIN, 'true'],
      [ASYNC_KEYS.WALLET_DETAIL, JSON.stringify(params.walletDetail)],
      [
        ASYNC_KEYS.ACCOUNT_LIST,
        JSON.stringify([
          {
            publicAddress: params.walletDetail.publicAddress,
            isHardware: false,
            isImported: false,
            isArchived: false,
            isPopularTokens: true,
            name: 'Account 1',
          },
        ]),
      ],
      [ASYNC_KEYS.IS_SET_PASSCODE, params.isSetPasscode ? 'true' : 'false'],
      [ASYNC_KEYS.IS_UPDATED_HASH, 'true'],
    ]);

    /* istanbul ignore else */
    if (!error) {
      dispatch(
        WalletAction.initApp(
          params.walletDetail,
          params.password,
          params.isSetPasscode,
        ),
      );
      navigation.navigate('GET_START_INFO_SCREEN');
    }
  };

  const checkVerifySwipe = () => {
    if (!isConfirm) {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('CHECK_CONFIRM_MSG'),
        ),
      );
    } else {
      storeWalletAccountData();
    }
  };

  // Custom render components
  const RenderAgreeView = () => {
    return (
      <View style={ styles.confirmationViewStyle }>
        <CheckBox
          isSelected={ isConfirm }
          onPress={ () => setIsConfirm(!isConfirm) }
        />
        <Text style={ styles.confirmStyle }>{localize('CONFIRM_MSG')}</Text>
      </View>
    );
  };

  const renderScreenShotTakenMessage = () => (
    <AlertActionModel
      isShowAlert={showScreenShotAlert}
      alertTitle={localize('SCREENSHOT_WARNING')}
      successBtnTitle={localize('UNDERSTAND')}
      bigSuccess={true}
      onPressSuccess={() => {
        setShowScreenShotAlert(false);
      }}
    />
  );

  return (
    <SafeAreaWrapper>
      <Text style={ COMMON_STYLE.infoTitleStyle() }>
        {localize('YOUR_SEED_PHRASE_UPPERCASE')}
      </Text>
      <Text style={ styles.infoMsgstyle }>
        {localize('NEVER_SHARE_WITH_ANYONE')}
      </Text>
      <View style={ styles.tagViewStyle }>
        <Tags tags={ secretPhrase } isEditable={ false } />
      </View>
      {/* <RoundedButton
        testID={'secret-recovery-copy-button'}
        style={COMMON_STYLE.roundedBtnMargin(1)}
        title={localize('COPY_CLIPBOARD_UPPERCASE')}
        onPress={() => {
          Clipboard.setString(secretPhraseString);
          Toast.show({
            text1: localize('COPIED_SUCCESS'),
            text2: localize('SEED_PHRASE_COPIED_SUCCESS'),
          });
        }}
      />*/}
      <RenderAgreeView />
      {renderScreenShotTakenMessage()}

      <SwipeVerifyController
        style={ styles.swiperStyle }
        onVerified={ () => checkVerifySwipe() }
        isSetReset={ !isConfirm }>
        <Text
          style={ [
            COMMON_STYLE.textStyle(14, COLORS.LIGHT_OPACITY(0.3), 'BOLD'),
          ] }>
          {localize('SLIDE_TO_CONFIRM_UPPERCASE') }
        </Text>
      </SwipeVerifyController>
      {
        (appStateVisible !== 'active' && Platform.OS === 'ios') && (
          <BlurView
            style={ styles.absolute }
            blurType="dark"
            blurAmount={ 10 }
          />
        )
      }
    </SafeAreaWrapper>
  );
};

YourSecretRecoveryPhraseScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default YourSecretRecoveryPhraseScreen;
