import React, { useState } from 'react';
import { View, Text, Linking, Alert } from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';
import { GlobalAction, WalletAction } from '@redux';

// import components
import { SafeAreaWrapper, RoundedButton, CheckBox, CachedImage } from '@components';

// import constants
import { COMMON_DATA } from '@constants';

// import themes
import { IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

const StartScreen = props => {
  const { navigation } = props;

  const dispatch = useDispatch();

  const [isAcknowledge, setIsAcknowledge] = useState(false);

  // Action and functions
  const checkIsAcknowledge = () => {
    if (!isAcknowledge) {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('CHECK_ACKNOWLEDGE_MSG'),
        ),
      );
    }
    return isAcknowledge;
  };

  // render custom compnents
  const renderAgreeView = () => {
    return (
      <View style={ styles.acknowledgeViewStyle }>
        <CheckBox
          isSelected={ isAcknowledge }
          onPress={ () => setIsAcknowledge(!isAcknowledge) }
        />
        <Text style={ styles.acknowledgeStyle }>
          {localize('ACKNOWLEDGE_MSG', [
            <Text
              style={ styles.linkStyle }
              onPress={ () => Linking.openURL(COMMON_DATA.PRIVACY_POLICY_LINK) }>
              {localize('PRIVACY_POLICY')}
            </Text>,
            <Text
              style={ styles.linkStyle }
              onPress={ () => Linking.openURL(COMMON_DATA.TERMS_OF_SERVICE_LINK) }>
              {localize('TERMS_AND_CONDITIONS')}
            </Text>
          ])}
        </Text>
      </View>
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ styles.containerStyle }>
      <View style={ styles.titleContainerStyle }>
        <Text style={ styles.welcomeTitleStyle }>
          {localize('WELCOME_TITLE')}
        </Text>
        <Text style={ styles.internetMoneyStyle }>
          {localize('INTERNET_MONEY')}
        </Text>
        <Text style={ styles.welcomeMsgStyle }>{localize('WELCOME_MSG')}</Text>
        <View style={ styles.btnContainerStyle }>
          <RoundedButton
            testID={ 'start-newcrypto-button' }
            title={ localize('CREATE_NEW_WALLET_UPPERCASE') }
            onPress={ () => {
              if (checkIsAcknowledge()) {
                navigation.navigate('LEARN_ONE_SCREEN');
              }
            } }
          />
          <RoundedButton
            testID={ 'start-experiencecrypto-button' }
            title={ localize('IMPORT_WALLET_UPPERCASE') }
            isBordered={ true }
            onPress={ () => {
              if (checkIsAcknowledge()) {
                navigation.navigate('LEARN_FIVE_SCREEN', {
                  isFromWalletImport: true,
                })
              }
            } }
          />
          {renderAgreeView()}
        </View>
      </View>
      <CachedImage source={ IMAGES.onboardLogo } style={ styles.onboardLogoStyle } />
    </SafeAreaWrapper>
  );
};

StartScreen.propTypes = { navigation: PropTypes.any };
export default StartScreen;
