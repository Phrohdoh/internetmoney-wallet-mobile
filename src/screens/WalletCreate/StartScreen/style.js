import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  containerStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: COLORS.BLACK,
    marginHorizontal: 0,
  },
  titleContainerStyle: { flex: 1 },
  onboardLogoStyle: {
    width: '100%',
    height: Responsive.getHeight(42),
  },
  welcomeTitleStyle: {
    ...COMMON_STYLE.textStyle(20, COLORS.WHITE, 'BASE', 'center'),
    marginTop: Responsive.getHeight(3),
  },
  internetMoneyStyle: {
    ...COMMON_STYLE.textStyle(38, COLORS.YELLOWFB, 'PIX_BOY', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  welcomeMsgStyle: {
    ...COMMON_STYLE.textStyle(15, COLORS.LIGHT_OPACITY(0.6), 'BOLD', 'center'),
    marginTop: Responsive.getHeight(2),
    textTransform: 'uppercase',
  },

  acknowledgeViewStyle: {
    flexDirection: 'row',
    width: '80%',
    marginVertical: Responsive.getHeight(2),
    alignItems: 'center',
  },
  acknowledgeStyle: {
    ...COMMON_STYLE.textStyle(9, COLORS.LIGHT_OPACITY(0.6)),
    flex: 1,
    marginLeft: Responsive.getWidth(2),
  },
  linkStyle: {
    ...COMMON_STYLE.textStyle(9, COLORS.YELLOWFB),
    textDecorationLine: 'underline',
  },
  btnContainerStyle: {
    width: '100%',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flex: 1,
  },
});
