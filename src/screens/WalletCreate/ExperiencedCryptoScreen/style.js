import {StyleSheet} from 'react-native';

import {COMMON_STYLE} from '@themes';
import {Responsive} from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  buttonMargin: {
    marginTop: Responsive.getHeight(3),
  },
});
