import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

// import components
import {
  SafeAreaWrapper,
  KeyboardAvoidScrollView,
  RoundedButton,
} from '@components';

// import themes
import { COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

const ExperiencedCryptoScreen = (props) => {
  const { navigation } = props;

  return (
    <SafeAreaWrapper>
      <Text style={COMMON_STYLE.infoTitleStyle()}>
        {localize('THANK_YOU_FOR_CHOOSE')}
      </Text>
      <KeyboardAvoidScrollView>
        <Text style={COMMON_STYLE.infoDescStyle()}>
          {localize('EXPERIENCE_CRYPTO_MSG')}
        </Text>
      </KeyboardAvoidScrollView>
      <RoundedButton
        testID="exp-crypto-create-wallet-button"
        style={styles.buttonMargin}
        title={localize('CREATE_NEW_WALLET_UPPERCASE')}
        onPress={() => navigation.navigate('LEARN_FIVE_SCREEN', {
          isFromWalletImport: false,
        })}
      />

      <RoundedButton
        testID="exp-crypto-import-wallet-button"
        style={styles.buttonMargin}
        isBordered
        title={localize('IMPORT_WALLET_UPPERCASE')}
        onPress={() => navigation.navigate('LEARN_FIVE_SCREEN', {
          isFromWalletImport: true,
        })}
      />
    </SafeAreaWrapper>
  );
};

ExperiencedCryptoScreen.propTypes = { navigation: PropTypes.any };
export default ExperiencedCryptoScreen;
