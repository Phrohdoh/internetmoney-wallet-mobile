import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';
export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  alertContainerStyle: {
    width: '100%',
    backgroundColor: COLORS.GRAY15,
    marginTop: Responsive.getHeight(4),
    borderRadius: Responsive.getWidth(5),
    borderWidth: 2,
    borderColor: COLORS.GRAY1F,
    padding: Responsive.getWidth(4),
    alignItems: 'center',
  },
  alertMsgStyle: {
    ...COMMON_STYLE.textStyle(15, COLORS.REDC1, 'BOLD', 'center'),
    marginTop: Responsive.getHeight(2),
  },
});
