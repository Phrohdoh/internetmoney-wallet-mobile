import React from 'react';
import { Text, ScrollView, View } from 'react-native';
import PropTypes from 'prop-types';

// import components
import { SafeAreaWrapper, RoundedButton } from '@components';

// import themes
import { COMMON_STYLE, IMAGES } from '@themes';

// import languages
import { localize } from '@languages';
import { styles } from './style';

const SecretRecoveryPhraseScreen = (props) => {
  const { navigation } = props;
  const { params } = props.route;

  return (
    <SafeAreaWrapper>
      <ScrollView bounces={false}>
        <Text style={COMMON_STYLE.infoTitleStyle()}>
          {localize('SEED_PHRASE')}
        </Text>
        <Text style={COMMON_STYLE.infoDescStyle()}>
          {localize('RECOVERY_PHRASE_MSG')}
        </Text>
        <View style={styles.alertContainerStyle}>
          <Text style={styles.alertMsgStyle}>{localize('ALERT_MSG')}</Text>
        </View>
      </ScrollView>

      <RoundedButton
        testID="secret-recovery-reveal-button"
        style={COMMON_STYLE.roundedBtnMargin()}
        title={localize('REVEAL_SEED_PHRASE_UPPERCASE')}
        onPress={() => navigation.navigate('YOUR_SECRET_REVOVERY_PHRASE_SCREEN', {
          walletDetail: params.walletDetail,
          password: params.password,
          isSetPasscode: params.isSetPasscode,
        })}
      />
    </SafeAreaWrapper>
  );
};

SecretRecoveryPhraseScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default SecretRecoveryPhraseScreen;
