import { StyleSheet } from 'react-native';

import { COMMON_STYLE } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  containerStyle: {
    marginTop: Responsive.getHeight(3),
    flex: 1,
  },
  titleStyle: {
    textAlign: 'center',
    width: '100%',
    paddingHorizontal: Responsive.getWidth(2),
  }
});
