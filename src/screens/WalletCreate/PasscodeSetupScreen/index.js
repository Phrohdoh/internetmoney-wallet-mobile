import React, { useState } from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch } from 'react-redux';

import { GlobalAction } from '@redux';

// import components
import { SafeAreaWrapper, RoundedButton, PasscodeComponent } from '@components';

// import themes
import { COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

const PasscodeSetupScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const [passcode, setPasscode] = useState('');
  const [confPasscode, setConfPasscode] = useState('');
  const [isConfPass, setIsConfPass] = useState(false);
  const dispatch = useDispatch();

  // Action Methods
  const handlePasscode = (item, code, setCode) => {
    const tempCode = code.split('');
    if (item.value === 'remove') {
      if (tempCode.length) {
        tempCode.pop();
      }
    } else {
      if (tempCode.length < 6) {
        tempCode.push(item.value);
      }
    }

    const strPasscode = tempCode.join('');
    setCode(strPasscode);
  };

  // check all validation
  const onPressContinuePasscode = () => {
    if (passcode.length < 6) {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('ENTER_VALID_PASSCODE_MSG'),
          localize('TRY_AGAIN'),
        ),
      );
    } else {
      setIsConfPass(true);
    }
  };

  const onPressContinueConfPasscode = () => {
    const tempPasscode = passcode;
    const tempConfPasscode = confPasscode;

    if (confPasscode.length < 6) {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('ENTER_VALID_PASSCODE_MSG'),
          localize('TRY_AGAIN'),
        ),
      );
    } else if (tempPasscode !== tempConfPasscode) {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('PASSCODE_NOT_MATCH_MSG'),
          localize('TRY_AGAIN'),
        ),
      );
    } else {
      if (params.isFromWalletImport) {
        navigation.navigate('WALLET_IMPORT_SCREEN', {
          password: passcode,
          isSetPasscode: true,
        });
      } else {
        navigation.navigate('WALLET_CREATE_SCREEN', {
          password: passcode,
          isSetPasscode: true,
        });
      }
    }
  };

  return (
    <SafeAreaWrapper>
      <Text style={ [COMMON_STYLE.infoTitleStyle(), styles.titleStyle] }>
        {localize('PASSCODE_SETUP')}
      </Text>

      <View style={ styles.containerStyle }>
        <PasscodeComponent
          title={ isConfPass ? localize('CONF_PASSCODE') : localize('PASSCODE') }
          passcode={ isConfPass ? confPasscode : passcode }
          onPressKey={ item => {
            if (isConfPass) {
              handlePasscode(item, confPasscode, setConfPasscode);
            } else {
              handlePasscode(item, passcode, setPasscode);
            }
          } }
        />
        <RoundedButton
          testID="password-setup-getstart-button"
          style={ COMMON_STYLE.roundedBtnMargin() }
          title={ localize('CONTINUE_UPPERCASE') }
          onPress={ () =>
            isConfPass
              ? onPressContinueConfPasscode()
              : onPressContinuePasscode()
          }
        />
      </View>
    </SafeAreaWrapper>
  );
};

PasscodeSetupScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default PasscodeSetupScreen;
