import React, { useState } from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch } from 'react-redux';

import { GlobalAction } from '@redux';

// import components
import {
  SafeAreaWrapper,
  KeyboardAvoidScrollView,
  RoundedButton,
  TitleTextInput,
} from '@components';

// import themes
import { COMMON_STYLE, COLORS } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import { Validation } from '@utils';

const PasswordSetupScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const dispatch = useDispatch();

  const [password, onChangePassword] = useState({
    id: 'password',
    value: '',
    title: 'PASSWORD',
    placeholder: 'ENTER_PASSWORD',
    extraProps: {
      isPassword: true,
    },
  });

  const [confPassword, onChangeConfPassword] = useState({
    id: 'conf-password',
    value: '',
    title: 'CONF_PASSWORD',
    placeholder: 'ENTER_CONF_PASSWORD',
    extraProps: {
      isPassword: true,
    },
  });

  // Action Methods
  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.value = text;
    setFunction(tempDict);
  };

  // check all validation
  const checkValidation = () => {
    const isValidPassword = Validation.isValidPassword(password.value);
    const matchPassword = password.value === confPassword.value;

    if (isValidPassword && matchPassword) {
      return true;
    } else {
      let msg = '';

      /* istanbul ignore else */
      if (Validation.isEmpty(password.value)) {
        msg = 'ENTER_PASS_MSG';
      } else if (!isValidPassword) {
        msg = 'ENTER_VALID_PASS_MSG';
      } else if (Validation.isEmpty(confPassword.value)) {
        msg = 'ENTER_CONF_PASS_MSG';
      } else if (!matchPassword) {
        msg = 'MATCH_PASS_MSG';
      }
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize(msg),
          localize('TRY_AGAIN'),
        ),
      );
      return false;
    }
  };

  const _onPressContinue = () => {
    if (checkValidation()) {
      if (params.isFromWalletImport) {
        navigation.navigate('WALLET_IMPORT_SCREEN', {
          password: password.value,
          isSetPasscode: false,
        });
      } else {
        navigation.navigate('WALLET_CREATE_SCREEN', {
          password: password.value,
          isSetPasscode: false,
        });
      }
    }
  };

  // Custom Render Components
  const renderInputComponent = (state, setStateFunction) => {
    return (
      <TitleTextInput
        testID={ `pass-setup-screen-${state.id}` }
        inputContainerStyle={ styles.inputContainerStyle }
        value={ state.value }
        title={ localize(state.title) }
        placeholder={ localize(state.placeholder) }
        onChangeText={ text => changeData(text, state, setStateFunction) }
        { ...state.extraProps }
      />
    );
  };

  return (
    <SafeAreaWrapper>
      <KeyboardAvoidScrollView>
        <Text style={ [COMMON_STYLE.infoTitleStyle(), styles.titleStyle] }>
          {localize('PASS_SETUP')}
        </Text>
        <Text style={ COMMON_STYLE.textStyle(14, COLORS.WHITE) }>
          {localize('PASS_SETUP_MSG')}
        </Text>

        {renderInputComponent(password, onChangePassword)}
        {renderInputComponent(confPassword, onChangeConfPassword)}
      </KeyboardAvoidScrollView>
      <RoundedButton
        testID="password-setup-getstart-button"
        style={ COMMON_STYLE.roundedBtnMargin() }
        title={ localize('CONTINUE_UPPERCASE') }
        onPress={ () => _onPressContinue() }
      />
    </SafeAreaWrapper>
  );
};

PasswordSetupScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default PasswordSetupScreen;
