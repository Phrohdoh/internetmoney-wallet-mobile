import {StyleSheet} from 'react-native';

import {COMMON_STYLE} from '@themes';
import {Responsive} from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  inputContainerStyle: {
    marginTop: Responsive.getHeight(3),
  },
  titleStyle: {
    textAlign: 'center',
    width: '100%',
    paddingHorizontal: Responsive.getWidth(2),
  }
});
