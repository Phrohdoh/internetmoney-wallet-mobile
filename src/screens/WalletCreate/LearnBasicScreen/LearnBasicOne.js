import React from 'react';
import { View, SafeAreaView } from 'react-native';
import PropTypes from 'prop-types';

// import components
import { RoundedButton, LearnBasisInfo } from '@components';

// import languages
import { localize } from '@languages';
import { styles } from './style';

const LearnBasicScreen = (props) => {
  const { navigation } = props;

  const learnBasicsInfo = localize('LEARN_BASIC_INFO_LIST')[0];

  const _onPressContinue = () => {
    navigation.navigate('LEARN_TWO_SCREEN');
  };

  return (
    <View style={styles.containerStyle}>
      <LearnBasisInfo item={learnBasicsInfo} />

      <SafeAreaView>
        <RoundedButton
          testID="learn-basics-continue-button"
          style={styles.roundButtonStyle}
          title={localize('CONTINUE_UPPERCASE')}
          onPress={() => _onPressContinue()}
        />
      </SafeAreaView>
    </View>
  );
};

LearnBasicScreen.propTypes = { navigation: PropTypes.any };
export default LearnBasicScreen;
