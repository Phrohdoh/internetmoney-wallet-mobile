import React, { useState } from 'react';

import { View, Text } from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch } from 'react-redux';

import { GlobalAction } from '@redux';

// import components
import {
  SafeAreaWrapper,
  RoundedButton,
  CheckBox,
  LearnBasisInfo,
} from '@components';

// import themes
import { COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

const LearnBasicScreen = props => {
  const { navigation } = props;

  const dispatch = useDispatch();

  const learnBasicsInfo = localize('LEARN_BASIC_INFO_LIST')[3];
  const [isUnderstand, setIsUnderstand] = useState(false);

  const _onPressContinue = () => {
    if (!isUnderstand) {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('CHECK_UNDERSTAND_MSG'),
        ),
      );
    } else {
      navigation.navigate('LEARN_FIVE_SCREEN', { isFromWalletImport: false });
    }
  };

  const RenderAgreeView = () => {
    return (
      <View style={ styles.agreementViewStyle }>
        <CheckBox
          isSelected={ isUnderstand }
          onPress={ () => setIsUnderstand(!isUnderstand) }
        />
        <Text style={ styles.understandStyle }>
          {localize('UNDERSTAND_BASICS')}
        </Text>
      </View>
    );
  };

  return (
    <SafeAreaWrapper containerStyle={ [COMMON_STYLE.tabContainerStyle] }>
      <LearnBasisInfo item={ learnBasicsInfo } />

      {RenderAgreeView()}
      <RoundedButton
        testID="learn-basics-continue-button"
        style={ styles.roundButtonStyle }
        title={ localize('CONTINUE_UPPERCASE') }
        onPress={ () => _onPressContinue() }
      />
    </SafeAreaWrapper>
  );
};

LearnBasicScreen.propTypes = { navigation: PropTypes.any };
export default LearnBasicScreen;
