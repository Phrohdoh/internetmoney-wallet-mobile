import React, { useState } from 'react';

import { View, Text, SafeAreaView } from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch } from 'react-redux';

import { GlobalAction } from '@redux';

// import components
import { RoundedButton, CheckBox, LearnBasisInfo } from '@components';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

const LearnBasicScreen = props => {
  const { navigation } = props;
  const params = props.route.params;

  const dispatch = useDispatch();

  const learnBasicsInfo = localize('LEARN_BASIC_INFO_LIST')[4];
  const [isUnderstand, setIsUnderstand] = useState(false);

  const _onPressContinue = isPasscode => {
    if (!isUnderstand) {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('CHECK_UNDERSTAND_MSG_2'),
        ),
      );
    } else {
      if (!isPasscode) {
        navigation.navigate('PASS_SETUP_SCREEN', {
          isFromWalletImport: params.isFromWalletImport,
        });
      } else {
        navigation.navigate('PASSCODE_SETUP_SCREEN', {
          isFromWalletImport: params.isFromWalletImport,
        });
      }
    }
  };

  const RenderAgreeView = () => {
    return (
      <View style={ [styles.agreementViewStyle, { justifyContent: 'center' }] }>
        <CheckBox
          isSelected={ isUnderstand }
          onPress={ () => setIsUnderstand(!isUnderstand) }
        />
        <Text style={ styles.understandStyle }>{localize('UNDERSTAND')}</Text>
      </View>
    );
  };

  return (
    <View style={ styles.containerStyle }>
      <LearnBasisInfo
        item={ learnBasicsInfo }
        imageStyle={ styles.passCodeImageStyle }
      />

      <SafeAreaView>
        {RenderAgreeView()}
        <RoundedButton
          testID="learn-basics-password-button"
          style={ styles.roundButtonStyle }
          title={ localize('SETUP_PASSWORD') }
          onPress={ () => _onPressContinue(false) }
        />
        <RoundedButton
          isBordered
          testID="learn-basics-passcode-button"
          style={ styles.roundButtonStyle }
          title={ localize('SETUP_PASSCODE') }
          onPress={ () => _onPressContinue(true) }
        />
      </SafeAreaView>
    </View>
  );
};

LearnBasicScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
};
export default LearnBasicScreen;
