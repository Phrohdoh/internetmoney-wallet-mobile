import React from 'react';
import PropTypes from 'prop-types';

// import components
import { SafeAreaWrapper, RoundedButton, LearnBasisInfo } from '@components';

// import themes
import { COMMON_STYLE } from '@themes';

// import languages
import { localize } from '@languages';
// import style
import { styles } from './style';

const LearnBasicScreen = (props) => {
  const { navigation } = props;

  const learnBasicsInfo = localize('LEARN_BASIC_INFO_LIST')[2];

  const _onPressContinue = () => {
    navigation.navigate('LEARN_FOUR_SCREEN');
  };

  return (
    <SafeAreaWrapper containerStyle={[COMMON_STYLE.tabContainerStyle]}>
      <LearnBasisInfo item={learnBasicsInfo} />
      <RoundedButton
        testID="learn-basics-continue-button"
        style={styles.roundButtonStyle}
        title={localize('CONTINUE_UPPERCASE')}
        onPress={() => _onPressContinue()}
      />
    </SafeAreaWrapper>
  );
};

LearnBasicScreen.propTypes = { navigation: PropTypes.any };
export default LearnBasicScreen;
