import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  containerStyle: { backgroundColor: COLORS.BLACK, flex: 1 },
  agreementViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Responsive.getHeight(2),
    marginHorizontal: Responsive.getWidth(5),
  },
  understandStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6)),
    marginLeft: Responsive.getWidth(2),
  },
  roundButtonStyle: {
    marginTop: Responsive.getHeight(2),
  },
  passCodeImageStyle: {
    height: Responsive.getWidth(100)/(1920/1080),
    resizeMode: 'contain',
    marginTop: Responsive.getHeight(10),
  },
});
