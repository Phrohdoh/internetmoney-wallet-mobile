import {StyleSheet} from 'react-native';

import {COMMON_STYLE, COLORS} from '@themes';
import {Responsive} from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  inputContainerStyle: {
    marginTop: Responsive.getHeight(3),
  },
  internetMoneyStyle: {
    ...COMMON_STYLE.textStyle(56, COLORS.YELLOWFB, 'PIX_BOY', 'center'),
    marginBottom: Responsive.getHeight(1),
  },
  rowInternetMoneyStyle: {
    ...COMMON_STYLE.textStyle(40, COLORS.YELLOWFB, 'PIX_BOY', 'center'),
  },
  headerIconStyle: {
    height: Responsive.getHeight(20),
    width: Responsive.getHeight(20),
    marginBottom: Responsive.getHeight(5),
  },
  rowHeaderIconStyle: {
    height: Responsive.getHeight(10),
    width: Responsive.getHeight(10),
  },
  headerContainerStyle: {
    alignItems: 'center', 
    flexDirection: 'row', 
    justifyContent: 'space-around',
  },
  titleStyle: {
    textAlign: 'center',
    marginTop: Responsive.getHeight(5),
  },
  scrollViewStyle: {
    marginBottom: Responsive.getHeight(3),
  }
});
