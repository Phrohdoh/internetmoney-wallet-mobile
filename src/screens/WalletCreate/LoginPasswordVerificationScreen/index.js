import React, { useRef, useState, useEffect } from 'react';
import {
  Text,
  View,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard,
} from 'react-native';
import PropTypes from 'prop-types';

import * as Web3Layer from '@web3';

import { useDispatch, useSelector } from 'react-redux';

import { WalletAction, AccountsAction, GlobalAction } from '@redux';

// import components
import {
  SafeAreaWrapper,
  PasscodeComponent,
  RoundedButton,
  TitleTextInput,
  CachedImage,
} from '@components';

import { COMMON_DATA, ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import themes
import { COMMON_STYLE, IMAGES } from '@themes';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import Utils
import { Validation, log, parseObject } from '@utils';

import { SafeAreaView } from 'react-native-safe-area-context';
import Logger from '../../../utils/logger';
import { getNetwork } from '../../../redux';

const LoginPasswordVerificationScreen = props => {
  const dispatch = useDispatch();

  const { navigation } = props;
  const params = props.route.params;
  const selectedNetwork = useSelector(getNetwork);

  let walletDetail = params.walletDetail;

  const [password, onChangePassword] = useState({
    id: 'password',
    value: '',
    title: 'PASSWORD',
    placeholder: 'ENTER_PASSWORD',
    extraProps: {
      isPassword: true,
    },
  });
  const [passcode, setPasscode] = useState('');
  const scrollRef = useRef(null);
  // const [isKeyboardVisible, setKeyboardVisible] = useState(false);

  useEffect(() => {
    if (props.isSetPasscode || !scrollRef) return;
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        scrollRef.current?.scrollToEnd({ animated: true });
        // scrollRef.current?.scrollTo({
        //   y: '100%',
        //   animated: true,
        // });        // setKeyboardVisible(true); // or some other action
      },
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        // setKeyboardVisible(false); // or some other action
      },
    );

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, [props.isSetPasscode]);

  // Action Methods
  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.value = text;
    setFunction(tempDict);
  };

  const handlePasscode = item => {
    const tempCode = passcode.split('');
    if (item.value === 'remove') {
      if (tempCode.length) {
        tempCode.pop();
      }
    } else {
      if (tempCode.length < 6) {
        tempCode.push(item.value);
      }
    }
    const strPasscode = tempCode.join('');
    setPasscode(strPasscode);
  };

  const checkPassword = async (walletObj, password) => {
    try {
      const { data } = await StorageOperation.getData(
        ASYNC_KEYS.IS_UPDATED_HASH,
      );

      if (data) {
        const checkPass = await Web3Layer.checkPasswordSecurely(
          password,
          walletObj.passwordHash,
        );

        return checkPass;
      } else {
        const checkPass = await Web3Layer.checkPassword(
          password,
          walletObj.passwordHash,
        );

        if (checkPass) {
          try {
            const updatedHash = await Web3Layer.getUpdatedHashes(
              password,
              walletObj.mnemonic,
            );

            if(updatedHash.success){
              const tempWalletDetail = { ...walletDetail };
              tempWalletDetail.mnemonic = updatedHash.mnemonic;
              tempWalletDetail.passwordHash = updatedHash.passwordHash;
              walletDetail = tempWalletDetail;

              await StorageOperation.setMultiData([
                [ASYNC_KEYS.WALLET_DETAIL, JSON.stringify(tempWalletDetail)],
                [ASYNC_KEYS.IS_UPDATED_HASH, 'true'],
              ]);
              return checkPass;
            }
            else
              return false;
          } catch (e) {
            log('er >>>', e);
          }
        } else {
          return checkPass;
        }
      }
    } catch (e) {
      log('login error >>>', e);
    }
  };

  // check all validation
  const checkPasswordValidation = () => {
    const isValidPassword = Validation.isValidPassword(password.value);

    if (isValidPassword) {
      return true;
    } else {
      let msg = '';

      /* istanbul ignore else */
      if (Validation.isEmpty(password.value)) {
        msg = 'ENTER_PASS_MSG';
      } else if (!isValidPassword) {
        msg = 'ENTER_VALID_PASS_MSG';
      }

      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize(msg),
          localize('TRY_AGAIN'),
        ),
      );
      setPasscode('');
      return false;
    }
  };

  const checkPasscodeValidation = () => {
    if (passcode.length < 6) {
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize('ENTER_VALID_PASSCODE_MSG'),
          localize('TRY_AGAIN'),
        ),
      );
      setPasscode('');
      return false;
    } else {
      return true;
    }
  };

  const _onPressContinue = () => {
    if (
      params.isSetPasscode
        ? checkPasscodeValidation()
        : checkPasswordValidation()
    ) {
      dispatch(GlobalAction.isShowLoading(true));

      setTimeout(() => {
        callWeb3();
      }, 300);
    }
  };

  const regenerateWallet = async (walletDetail, password) => {
    const mnemonicObject = await Web3Layer.decryptMnemonicSecurely(
      walletDetail.mnemonic,
      password,
    );
    Logger.debug('Mnemonic object was able to be decrypted successfully.', {
      typeOfMnemonic: typeof mnemonicObject.mnemonic,
    });

    const initialDetails = {
      mnemonic: mnemonicObject.mnemonic,
      password,
      numberOfWords: mnemonicObject.mnemonic.split(' ').length,
    };
    
    const newWalletDetail = await Web3Layer.createNewWallet(
      initialDetails,
      selectedNetwork,
    );

    const accountListData = await StorageOperation.getData(ASYNC_KEYS.ACCOUNT_LIST)
    if (!accountListData) {
      throw new Error('Unable to retrieve account list from storage');
    }

    const accountListParsed = parseObject(accountListData.data);
    if (!accountListParsed.success) {
      throw new Error('Unable to parse account list from storage');
    }

    const accountList = accountListParsed.data;

    Logger.debug('Successfully retrieved the account list from storage', {
      accountListLength: accountList.length,
    });

    if (accountList[0].publicAddress.toLowerCase() !== `0x${newWalletDetail.walletObject[0].address}`) {
      throw new Error('First generated address does not match the first account list address');
    }

    const accountsToAdd = [];
    const newAccountList = [accountList[0]];

    for (let i = 1; i < accountList.length; i += 1) {
      const { address, privateKey } = await Web3Layer.generateAccountFromMnemonicOf(
        i,
        mnemonicObject.mnemonic
      );
      const accountFromList = accountList.find(account => account.publicAddress.toLowerCase() === address.toLowerCase());
      if (accountFromList) {
        accountsToAdd.push({address, privateKey});
        newAccountList.push(accountFromList);
      }
    }

    const encryptedAccounts = await Promise.all(
      accountsToAdd.map(account => (
        Web3Layer.createAccountFromPrivateKey(account.privateKey, password)
      ))
    );

    newWalletDetail.walletObject = [
      ...newWalletDetail.walletObject,
      ...encryptedAccounts,
    ];

    Logger.debug('Successfully created a new wallet object.', {
      typeOfNewWalletObject: typeof newWalletDetail.walletObject,
      lengthOfNewWalletObject: newWalletDetail.walletObject.length,
      lengthOfAccountList: accountList.length,
      lengthOfNewAccountList: newAccountList.length,
    });

    await StorageOperation.setMultiData([
      [ASYNC_KEYS.WALLET_DETAIL, JSON.stringify(newWalletDetail)],
      [ASYNC_KEYS.ACCOUNT_LIST, JSON.stringify(newAccountList)],
    ]);
    dispatch(WalletAction.updateWalletObject(newWalletDetail));
    dispatch(AccountsAction.updateAccountObject(newAccountList));
    return newWalletDetail;
  }

  const callWeb3 = async () => {
    const checkPass = await checkPassword(
      walletDetail,
      params.isSetPasscode ? passcode : password.value,
    );

    if (checkPass) {
      Logger.debug('Password checked successfully. Current state.', {
        typeOfWalletObject: typeof walletDetail.walletObject,
        typeOfMnemonic: typeof walletDetail.mnemonic,
      });

      // Regenerate the walletObject from the mnemonic, if it is missing
      if (walletDetail.walletObject === undefined) {
        try {
          Logger.debug('Attempting to recreate the wallet object');
          walletDetail = await regenerateWallet(
            walletDetail,
            params.isSetPasscode ? passcode : password.value,
          );
          Logger.debug('Successfully recreated the wallet object');
        } catch (e) {
          Logger.error('Error recreating the wallet object', { errorMessage: e.message });
        }
      }

      dispatch(
        WalletAction.initApp(
          walletDetail,
          params.isSetPasscode ? passcode : password.value,
          params.isSetPasscode,
        ),
      );

      dispatch(GlobalAction.isShowLoading(false));

      navigation.reset({
        index: 0,
        routes: [
          {
            name: 'DASHBOARD_TAB_NAVIGATOR',
          },
        ],
      });
    } else {
      dispatch(GlobalAction.isShowLoading(false));
      dispatch(
        GlobalAction.showAlert(
          localize('ERROR'),
          localize(
            params.isSetPasscode
              ? 'LOGIN_VERIFY_PASSCODE_NOT_MATCH_MSG'
              : 'LOGIN_VERIFY_PASSWORD_NOT_MATCH_MSG',
          ),
          localize('TRY_AGAIN'),
        ),
      );
      setPasscode('');
    }
  };

  // Custom Render Components
  const renderInputComponent = (state, setStateFunction) => {
    return (
      <TitleTextInput
        testID={ `login-password-verification-${state.id}` }
        inputContainerStyle={ styles.inputContainerStyle }
        // inputStyle={{backgroundColor: 'gray', paddingTop: 30}}
        // inputViewStyle={{backgroundColor: 'blue', marginBottom: 20}}
        value={ state.value }
        title={ localize(state.title) }
        placeholder={ localize(state.placeholder) }
        onChangeText={ text => changeData(text, state, setStateFunction) }
        { ...state.extraProps }
      />
    );
  };

  const renderPasscodeView = () => {
    return (
      <SafeAreaView style={ { width: '100%', flex: 1 } }>
        <PasscodeComponent
          title={ localize('PASSCODE') }
          passcode={ passcode }
          onPressKey={ item => handlePasscode(item) }
          headerComp={
            <View>
              <View style={ styles.headerContainerStyle }>
                <CachedImage
                  source={ IMAGES.IM_TOKEN_ICON }
                  style={ styles.rowHeaderIconStyle }
                />
                <View>
                  <Text style={ styles.rowInternetMoneyStyle }>
                    {localize('INTERNET')}
                  </Text>
                  <Text style={ styles.rowInternetMoneyStyle }>
                    {localize('MONEY')}
                  </Text>
                </View>
              </View>
              <Text style={ [COMMON_STYLE.infoTitleStyle(), styles.titleStyle] }>
                {localize(
                  params.isSetPasscode
                    ? 'LOGIN_PASSCODE_VERIFY_UPPERCASE'
                    : 'LOGIN_PASSWORD_VERIFY_UPPERCASE',
                )}
              </Text>
            </View>
          }
        />
      </SafeAreaView>
    );
  };

  const renderPasswordView = () => {
    return (
      <KeyboardAvoidingView
        keyboardVerticalOffset={ 20 }
        behavior={ COMMON_DATA.DEVICE_TYPE === 'ANDROID' ? null : 'padding' }
        style={ { flex: 1 } }>
        <ScrollView
          contentContainerStyle={ { flexGrow: 1, justifyContent: 'center' } }
          ref={ scrollRef }
          bounces={ false }
          keyboardShouldPersistTaps={ 'handled' }
          showsVerticalScrollIndicator={ false }
          showsHorizontalScrollIndicator={ false }
          nestedScrollEnabled={ true }
          style={ styles.scrollViewStyle }>
          <View style={ { alignItems: 'center' } }>
            <CachedImage source={ IMAGES.IM_TOKEN_ICON } style={ styles.headerIconStyle } />
            <Text style={ styles.internetMoneyStyle }>
              {localize('INTERNET')}
            </Text>
            <Text style={ styles.internetMoneyStyle }>{localize('MONEY')}</Text>
          </View>

          <Text style={ [COMMON_STYLE.infoTitleStyle(), { marginBottom: 20 }] }>
            {localize('LOGIN_PASSWORD_VERIFY_UPPERCASE')}
          </Text>
          {renderInputComponent(password, onChangePassword)}
        </ScrollView>
        <RoundedButton
          testID="login-pass-verification-continue-button"
          style={ COMMON_STYLE.roundedBtnMargin() }
          title={ localize('CONTINUE_UPPERCASE') }
          onPress={ () => _onPressContinue() }
        />
      </KeyboardAvoidingView>
    );
  };

  return (
    <SafeAreaWrapper style={ { flex: 1 } }>
      {params.isSetPasscode ? renderPasscodeView() : renderPasswordView()}
      {params.isSetPasscode ? (
        <RoundedButton
          testID="login-pass-verification-continue-button"
          style={ COMMON_STYLE.roundedBtnMargin() }
          title={ localize('CONTINUE_UPPERCASE') }
          onPress={ () => _onPressContinue() }
        />
      ) : null}
    </SafeAreaWrapper>
  );
};

LoginPasswordVerificationScreen.propTypes = {
  navigation: PropTypes.any,
  route: PropTypes.any,
  isSetPasscode: PropTypes.bool,
};
export default LoginPasswordVerificationScreen;
