import React, { useCallback, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  LogBox,
  TouchableOpacity,
  Linking,
} from 'react-native';
import Toast from 'react-native-toast-message';

// import third-party packages
import { Provider } from 'react-redux';
import { store, WalletAction } from '@redux';

// import components
import {
  AlertModel,
  LoadingModel,
  UpgradeModel,
  WalletConnectSessionRequestModal,
  WalletConnectTransactionRequestModal,
  WalletConnectChainRequestModal,
  ErrorBoundary,
} from '@components';

// import languages
import { changeLanguage } from '@languages';

// import navigator
import { AppContainer } from '@navigator';
import UserInactivity from 'react-native-user-inactivity';
import { resetAppNavigation } from '@utils/navigation';
import { WalletConnectProvider } from '@contexts/WalletConnectContext';
import globals from './globals';
import { pair } from './utils/walletConnect';
import { openedByDapp } from './utils/walletConnect';

const LOGOUT_TIMER = 10 * 60 * 1000; // 10 minutes

const App = () => {
  LogBox.ignoreLogs(['Remote debugger']);

  useEffect(() => {
    if (!Text.defaultProps) {
      Text.defaultProps = {};
    }
    if (!TouchableOpacity.defaultProps) {
      TouchableOpacity.defaultProps = {};
    }
    Text.defaultProps.allowFontScaling = false;
    TouchableOpacity.defaultProps.activeOpacity = 0.2;

    if (!TextInput.defaultProps) {
      TextInput.defaultProps = {};
    }
    TextInput.defaultProps.allowFontScaling = false;
    console.disableYellowBox = true;
    changeLanguage('en');
  });

  const handleOpenURL = useCallback(async (event) => {
    openedByDapp();
    await pair(event.url);
  }, []);

  useEffect(() => {
    Linking.getInitialURL().then(url => {
      if (url) {
        handleOpenURL({ url })
      }
    });
  }, []);

  useEffect(() => {
    const urlListener = Linking.addEventListener('url', handleOpenURL);
    return () => {
      urlListener.remove();
    };
  }, [handleOpenURL]);

  const userInactive = () => {
    const state = store.getState();
    const { password } = state.wallet;

    if (password !== '') {
      resetApp();
    }
  };

  const resetApp = () => {
    store.dispatch(WalletAction.savePassword(''));
    resetAppNavigation();
  };

  return (
    <ErrorBoundary>
      <UserInactivity
        timeForInactivity={LOGOUT_TIMER}
        onAction={(isActive) => {
          if (!isActive) {
            userInactive();
          }
        }}
        style={styles.container}
      >
        <Provider store={store}>
          <WalletConnectProvider>
            <AppContainer />
            <AlertModel />
            <LoadingModel />
            <Toast />
            <UpgradeModel />
            <WalletConnectSessionRequestModal />
            <WalletConnectTransactionRequestModal />
            <WalletConnectChainRequestModal />
          </WalletConnectProvider>
        </Provider>
      </UserInactivity>
    </ErrorBoundary>
  );
};

export default App;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
