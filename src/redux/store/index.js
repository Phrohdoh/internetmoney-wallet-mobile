import { combineReducers } from 'redux';
import { configureStore, } from '@reduxjs/toolkit';

import {
  GlobalReducer,
  WalletReducer,
  AccountsReducer,
  NetworksReducer,
  TransactionsReducer,
  WalletConnectReducer,
} from '@slices';
import { fetchTokenPricesMiddleware } from '../middleware/fetchTokenPricesMiddleware';
import { fetchTokenBalancesMiddleware } from '../middleware/fetchTokenBalancesMiddleware';
import { checkForImportedAccountsMiddleware } from '../middleware/checkForImportedAccountsMiddleware';
import { checkForDefaultTokenAccountsMiddleware } from '../middleware/checkForDefaultTokenAccountsMiddleware';

const rootReducer = combineReducers({
  global: GlobalReducer,
  wallet: WalletReducer,
  accounts: AccountsReducer,
  networks: NetworksReducer,
  transactions: TransactionsReducer,
  walletConnect: WalletConnectReducer,
});

export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware => ([
    ...getDefaultMiddleware(),
    fetchTokenPricesMiddleware,
    fetchTokenBalancesMiddleware,
    checkForImportedAccountsMiddleware,
    checkForDefaultTokenAccountsMiddleware
    // Disabling the logger (for now) because it's so noisy
    // logger,
  ]),
});
