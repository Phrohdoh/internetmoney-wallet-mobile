import { createSelector } from '@reduxjs/toolkit';
import { IntegerUnits } from '@utils';

// Accounts

export const getFullAccountList = state => state.accounts.accountList;

export const getAccountList = createSelector(
  getFullAccountList,
  (accountList) => (
    accountList.filter(account => account.isArchived === false)
  ),
);

export const getArchivedAccountList = createSelector(
  getFullAccountList,
  (accountList) => (
    accountList.filter(account => account.isArchived === true)
  ),
);

// Networks

export const getSwapSupportedChainIds = (state) => state.networks.swapSupportedNetworks;
export const getNetworkTokens = (state) => state.networks.tokens;
export const getSelectedNetworkObj = (state) => state.networks.selectedNetworkObj;
export const getAllNetworks = (state) => state.networks.networks;
export const getRemovedDefaultNetworks = (state) => state.networks.removedDefaultNetworks;
export const getRemovedDefaultTokens = (state) => state.networks.removedDefaultTokens;
export const getBalances = (state) => state.networks.tokenBalances;
export const getUsdPrices = (state) => state.networks.usdPrices;

export const getNetworks = createSelector(
  getAllNetworks,
  getRemovedDefaultNetworks,
  getSwapSupportedChainIds,
  (networks, removedDefaultNetworks, swapSupportedChainIds) => {
    const allNetworks = networks.filter(network => {
      const isRemovedDefault = network.isDefault && removedDefaultNetworks[network.chainId];
      return !isRemovedDefault;
    });
    const swapNetworks = allNetworks.filter(network => swapSupportedChainIds.includes(network.chainId));
    const nonSwapNetworks = allNetworks.filter(network => !swapSupportedChainIds.includes(network.chainId));
    return [
      ...swapNetworks,
      ...nonSwapNetworks,
    ];
  },
)

export const getNetwork = createSelector(
  getNetworks,
  getSelectedNetworkObj,
  (networks, selectedNetwork) => (
    networks.find(network => parseInt(network.chainId, 10) === parseInt(selectedNetwork.chainId, 10)) ?? {}
  ),
);

export const getTokensByAddress = createSelector(
  getNetwork,
  getNetworkTokens,
  getAccountList,
  getRemovedDefaultTokens,
  getBalances,
  getUsdPrices,
  (network, networkTokens, accountList, removedDefaultTokens, balances, usdPrices) => {
    if (network === undefined) {
      return {};
    }

    const { chainId } = network;
    const tokensByAddress = accountList.reduce((tokensByAddress, account) => {
      const accountAddress = account.publicAddress.toLowerCase();
      const tokenObj = (networkTokens ?? []).find(tokens => tokens.id.toLowerCase() === accountAddress) ?? {};
      const configuredTokens = tokenObj.tokens ?? [];
      const nativeToken = {
        decimals: 18,
        symbol: network.sym,
        address: '',
        isDefault: true,
        isNative: true,
      };
      const tokens = [
        nativeToken,
        ...configuredTokens,
      ].map(token => {
        const address = token.address.toLowerCase();
        const balance = balances[chainId]?.[accountAddress]?.[address];
        const usdPrice = usdPrices[chainId]?.[address];
        return {
          ...token,
          decimals: Number(token.decimals),
          balance: balance?.value === undefined ? balance : {
            ...balance,
            value: IntegerUnits.fromJSON(balance.value)
          },
          usdPrice: usdPrice?.value === undefined ? usdPrice : {
            ...usdPrice,
            value: IntegerUnits.fromJSON(usdPrice.value)
          },
        }
      });
      return {
        [accountAddress]: tokens,
        ...tokensByAddress,
      }
    }, {});
    return tokensByAddress;
  },
);

export const getTokens = createSelector(
  getTokensByAddress,
  (tokensByAddress) => {
    const tokens = [];
    Object.values(tokensByAddress).forEach(tokenList => {
      tokenList.forEach(token => {
        if (!tokens.includes(token.address)) {
          tokens.push(token);
        }
      });
    });
    return tokens;
  },
);

export const getPopularTokens = createSelector(
  getTokensByAddress,
  getNetwork,
  (tokensByAddress, network) => {
    const popularTokens = {};
    const chainTokens = network.tokens ?? [];
    
    Object.keys(tokensByAddress).forEach((accountAddress) => {
      popularTokens[accountAddress] = [];

      const accountTokens = tokensByAddress[accountAddress];
      chainTokens.forEach((chainToken) => {
        // filter out tokens already added to the account
        if (!accountTokens.find(({ symbol }) => symbol === chainToken.symbol))
          popularTokens[accountAddress].push(chainToken);
      });
    });

    return popularTokens;
  }
);

export const getNativeToken = createSelector(
  getTokens,
  (tokens) => {
    return tokens.find(token => token.isNative);
  },
);

export const getTotalValueByAddress = createSelector(
  getTokensByAddress,
  (tokensByAddress) => {
    return Object.entries(tokensByAddress)
      .reduce((totalValueByAddress, [address, tokenList]) => {
        let totalValue = new IntegerUnits(0, 2);
        for (const token of tokenList) {
          const balance = token.balance?.value;
          const usdPrice = token.usdPrice?.value;
          if (balance !== undefined && usdPrice !== undefined) {
            const tokenValue = balance.multiply(usdPrice).toPrecision(2);
            totalValue = totalValue.add(tokenValue);
          }
          if (token.balance?.fetching || token.usdPrice?.fetching) {
            totalValue = undefined;
            break;
          }
        }
        return {
          ...totalValueByAddress,
          [address]: totalValue,
        };
      }, {});
  }
);

export const getTotalValue = createSelector(
  getTotalValueByAddress,
  (totalValueByAddress) => {
    return Object.values(totalValueByAddress)
      .reduce((totalValue, value) => {
        if (totalValue === undefined || value === undefined) {
          return undefined;
        }
        return totalValue.add(value);
      }, new IntegerUnits(0, 2));
  }
);

// Transactions

const getTransactions = (state) => state.transactions;

export const getTransactionsList = createSelector(
  getTransactions,
  (transactions) => {
    const transactionsList = IntegerUnits.deserialize(transactions.transactionsList);
    return transactionsList.filter(transaction => transaction.transactionDetail.displayAmount.isIntegerUnits);
  }
);

// Wallet Connect

export const getSelectedWalletConnectAddress = (state) => (
  state.walletConnect.selectedAccount
);

export const getSelectedWalletConnectAccount = createSelector(
  getAccountList,
  getSelectedWalletConnectAddress,
  (accountList, selectedAddress) => {
    return accountList.find(account => (
      account.publicAddress?.toLowerCase() === selectedAddress?.toLowerCase()
    )) ?? accountList[0];
  }
);

export const getCompletedAuthRequestIds = (state) => (
  state.walletConnect.completedAuthRequestIds
);

export const getCompletedSessionProposalIds = (state) => (
  state.walletConnect.completedSessionProposalIds
);

export const getCompletedSessionRequestIds = (state) => (
  state.walletConnect.completedSessionRequestIds
);

export const getRequestVerifications = (state) => (
  state.walletConnect.requestVerifications
);

// Wallet

export const getWallet = (state) => state.wallet;
export const getPassword = (state) => state.wallet.password;
export const getWalletDetail = (state) => state.wallet.walletDetail;
export const getMnemonic = (state) => state.wallet.walletDetail.mnemonic;
export const getWalletObject = (state) => state.wallet.walletDetail.walletObject;
