import {
  NetworksAction,
  getNetwork,
  getTokensByAddress,
} from '@redux';
import { getBalanceOf } from '@web3';

const CACHE_DURATION = 60 * 1000; // 60 seconds

const ACTION_TYPES = [
  'networks/refreshBalances',
  'networks/saveTokens',
  'accounts/saveAccountList',
  'accounts/updateAccountObject',
  'accounts/restoreAccount',
];

export const fetchTokenBalancesMiddleware = store => next => async (action) => {  
  next(action);

  if (!ACTION_TYPES.includes(action.type)) {
    return;
  }

  const state = store.getState();
  const network = getNetwork(state);

  if (network === undefined) {
    return;
  }

  const tokensByAddress = getTokensByAddress(state);
  const { chainId } = network;

  const resultPayloadPromises = [];
  const fetchingPayloads = [];

  const isManualRefresh = action.type === 'networks/refreshBalances';

  Object.entries(tokensByAddress)
    .forEach(([accountAddress, tokens]) => {
      tokens.forEach((token) => {
        const durationSinceLastFetch = Date.now() - (token.balance?.lastFetched ?? 0);
        const isCacheExpired = durationSinceLastFetch > CACHE_DURATION;
        if ((isManualRefresh || isCacheExpired) && !token.balance?.fetching) {
          const tokenAddress = token.address;
          const { decimals } = token;
          fetchingPayloads.push({
            chainId,
            accountAddress,
            tokenAddress,
          });
          const resultPayloadPromise = getBalanceOf(accountAddress, tokenAddress, decimals, network)
            .then(result => {
              if (!result.success) {
                throw new Error('Failed to fetch balance');
              }
              const balance = result.balance.toJSON();
              return {
                success: true,
                chainId,
                accountAddress,
                tokenAddress,
                balance,
              };
            })
            .catch((e) => {
              return {
                success: false,
                chainId,
                accountAddress,
                tokenAddress,
              };
            });
          resultPayloadPromises.push(resultPayloadPromise);
        }
      });
    });

    store.dispatch(NetworksAction.fetchingTokenBalances(fetchingPayloads));
    const resultPayloads = await Promise.all(resultPayloadPromises);
    store.dispatch(NetworksAction.fetchedTokenBalances(resultPayloads));
};