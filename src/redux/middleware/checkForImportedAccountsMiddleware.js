import { StorageOperation } from '@storage';
import * as Web3Layer from '@web3';
import { ASYNC_KEYS } from "../../constants";
import {
  AccountsAction,
  getFullAccountList,
  getMnemonic,
  getPassword,
  getWalletObject,
} from '@redux';

const ACTION_TYPES = [
  'accounts/saveAccountList',
  'accounts/updateAccountObject',
];

// {
//   [accountAddress]: true
// }
const checkedAddresses = {};

// This middleware checks for accounts which were created/imported prior to adding
// the `isImported` flag, determines whether or not the account was imported, and
// sets the flag accordingly.
export const checkForImportedAccountsMiddleware = store => next => async (action) => {  
  next(action);

  if (!ACTION_TYPES.includes(action.type)) {
    return;
  }

  const state = store.getState();
  const accountList = getFullAccountList(state);
  const password = getPassword(state);
  const mnemonic = getMnemonic(state);
  const walletObject = getWalletObject(state);

  if (accountList === undefined || password === '' || mnemonic === undefined || walletObject === undefined) {
    return;
  }

  const accountsToCheck = accountList.filter(account => {
    const publicAddress = account.publicAddress.toLowerCase();
    if (checkedAddresses[publicAddress]) {
      return;
    }
    if (account.isImported !== undefined) {
      return;
    }
    checkedAddresses[publicAddress] = true;
    return publicAddress;
  });

  if (accountsToCheck.length === 0) {
    return;
  }

  const possibleGeneratedAddressesPromises = [];
  for (let i = 0; i < accountList.length; i += 1) {
    possibleGeneratedAddressesPromises.push(Web3Layer.getPublicAddressOfIndex(
      mnemonic,
      password,
      i,
    ));
  }

  const possibleGeneratedAddresses = await Promise.all(possibleGeneratedAddressesPromises);

  const isImportedUpdates = accountsToCheck.map(account => ({
    publicAddress: account.publicAddress.toLowerCase(),
    isImported: !account.isHardware && possibleGeneratedAddresses.every(generatedAddress => (
      generatedAddress.toLowerCase() !== account.publicAddress.toLowerCase()
    )),
  }));
  store.dispatch(AccountsAction.isImportedUpdates(isImportedUpdates));
  await StorageOperation.setData(
    ASYNC_KEYS.ACCOUNT_LIST,
    JSON.stringify(getFullAccountList(store.getState()))
  );
};