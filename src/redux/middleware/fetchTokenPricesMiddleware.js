import {
  NetworksAction,
  getNetwork,
  getTokens,
} from '@redux';
import { getUSDPrice } from '@web3';

const CACHE_DURATION = 60 * 1000; // 60 seconds

const ACTION_TYPES = [
  'networks/refreshBalances',
  'networks/saveTokens',
  'accounts/saveAccountList',
  'accounts/updateAccountObject',
  'accounts/restoreAccount',
];

export const fetchTokenPricesMiddleware = store => next => async (action) => {
  next(action);

  if (!ACTION_TYPES.includes(action.type)) {
    return;
  }

  const state = store.getState();
  const tokens = getTokens(state);
  const network = getNetwork(state);

  if (network === undefined) {
    return;
  }

  const { chainId } = network;

  const resultPayloadPromises = [];
  const fetchingPayloads = [];

  const isManualRefresh = action.type === 'networks/refreshBalances';
  
  tokens.forEach((token) => {
    const durationSinceLastFetch = Date.now() - (token.usdPrice?.lastFetched ?? 0);
    const isCacheExpired = durationSinceLastFetch > CACHE_DURATION;
    if ((isManualRefresh || isCacheExpired) && !token.usdPrice?.fetching) {
      fetchingPayloads.push({
        chainId,
        tokenAddress: token.address,
      });
      const resultPayloadPromise = getUSDPrice(token.address, network)
        .then(result => {
          if (!result.success) {
            throw new Error('Failed to fetch USD price');
          }
          const usdPrice = result.usdPrice.toJSON();
          return {
            success: true,
            chainId,
            tokenAddress: token.address,
            usdPrice,
          };
        })
        .catch((e) => {
          return {
            success: false,
            chainId,
            tokenAddress: token.address,
          };
        });
        resultPayloadPromises.push(resultPayloadPromise);
    }
  });

  store.dispatch(NetworksAction.fetchingTokenPrices(fetchingPayloads));
  const resultPayloads = await Promise.all(resultPayloadPromises);
  store.dispatch(NetworksAction.fetchedTokenPrices(resultPayloads));
};