import {
  AccountsAction,
  NetworksAction,
  getFullAccountList,
  getNetworkTokens,
  getNetwork,
  getRemovedDefaultTokens,
} from '@redux';
import { StorageOperation } from '@storage';
import { parseObject } from '@utils';
import { ASYNC_KEYS } from '../../constants';

const ACTION_TYPES = [
  'accounts/saveAccountList',
  'accounts/updateAccountObject',
];

export const checkForDefaultTokenAccountsMiddleware = store => next => async (action) => {  
  next(action);

  if (!ACTION_TYPES.includes(action.type)) {
    return;
  }

  const state = store.getState();
  const accountList = getFullAccountList(state);
  const networksTokens = getNetworkTokens(state);
  const selectedNetwork = getNetwork(state);
  const removedDefaultTokens = getRemovedDefaultTokens(state);

  if (
    accountList === undefined ||
    networksTokens === undefined ||
    selectedNetwork === undefined ||
    removedDefaultTokens === undefined
  ) {
    return;
  }

  const tokenDetails = selectedNetwork.tokens ?? [];
  if (tokenDetails === undefined) return;

  const networkTokens = networksTokens ? [...networksTokens] : [];

  const { data } = await StorageOperation.getData(ASYNC_KEYS.CHAIN_TOKENS);
  const parsedData = parseObject(data);

  const chainTokens = parsedData.success ? [...parsedData.data] : [];
  const chain = chainTokens.find((chain) => chain.chainId === selectedNetwork.chainId);

  const upgradedAccounts = accountList.map((account) => {
    const upgradedAccount = { ...account };
    const accountAddress = upgradedAccount.publicAddress.toLowerCase();
    
    if (!upgradedAccount.isPopularTokens) {
      // Get legacy default tokens list
      const defaultTokens = [];
      const removedTokens =
        removedDefaultTokens[selectedNetwork.chainId]?.[accountAddress] ?? [];
      tokenDetails.forEach(token => {
        if (!removedTokens.includes(token.address.toLowerCase())) {
          defaultTokens.push({
            ...token,
            isDefault: true,
          });
        }
      });
      
      // Convert them to popular tokens (same process as importing a token)
      defaultTokens.forEach((defaultToken) => {
        const accountTokens = networkTokens
          .find(({ id }) => id.toLowerCase() === accountAddress);
      
        if (accountTokens) {
          const tokenIndex = networkTokens.indexOf(accountTokens);
          const tokenList = [...accountTokens.tokens];
          tokenList.push({ ...defaultToken });
          const newAccountTokens = { ...accountTokens };
          newAccountTokens.tokens = tokenList;
          networkTokens[tokenIndex] = newAccountTokens;
        } else {
          const newAccountTokens = {
            id: accountAddress,
            tokens: [{ ...defaultToken }],
          };
          networkTokens.push(newAccountTokens);
        }

        if (chain) {
          const chainIndex = chainTokens.indexOf(chain);        
          chain.accountTokens = networkTokens;
          chainTokens[chainIndex] = chain;
        } else {
          const newChainTokens = {
            chainId: selectedNetwork.chainId,
            accountTokens: networkTokens,
          };
          chainTokens.push(newChainTokens);
        }
      });
    }
    
    return {
      ...upgradedAccount,
      publicAddress: accountAddress,
      isPopularTokens: true,
    };
  });

  store.dispatch(NetworksAction.saveTokens(networkTokens));
  await StorageOperation.setData(
    ASYNC_KEYS.CHAIN_TOKENS,
    JSON.stringify(chainTokens),
  );

  store.dispatch(AccountsAction.isPopularTokensUpdate(upgradedAccounts));
  await StorageOperation.setData(
    ASYNC_KEYS.ACCOUNT_LIST,
    JSON.stringify(getFullAccountList(store.getState())),
  );
};