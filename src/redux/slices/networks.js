import axios from 'axios';
import { createSlice, createSelector } from '@reduxjs/toolkit';

// import constants
import { ASYNC_KEYS, BACKEND_SERVER } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

import { parseObject, log } from '@utils';
import { getAccountList } from './accounts';
import { handleNetworkTokenIcons } from '../../themes/images';
import fallbackNetworks from '../../constants/fallbackNetworks';

const networkSlice = createSlice({
  name: 'networks',
  initialState: {
    tokens: [],
    networks: [],
    removedDefaultNetworks: {},
    selectedNetworkObj: {},
    wdSupportObject: {},
    swapSupportedNetworks: [],
    /*
    *  {
    *    [chainId]: {
    *      [accountAddress]: [address]
    *    }
    *  }
    */
    removedDefaultTokens: {},
    /*
    *  {
    *    [chainId]: {
    *      [tokenAddress]: {
    *        value: number,
    *        fetching: boolean,
    *        lastFetched: number (ms timestamp),
    *      }
    *    }
    *  }
    */
    usdPrices: {},
    /*
    *  {
    *    [chainId]: {
    *      [accountAddress]: {
    *        [tokenAddress]: {
    *          value: number,
    *          fetching: boolean,
    *          lastFetched: number (ms timestamp),
    *        }
    *      }
    *    }
    *  }
    */
    tokenBalances: {},
  },
  reducers: {
    resetSliceData: state => {
      state.tokens = [];
      state.usdPrices = {};
      state.tokenBalances = {};
    },
    saveTokens: (state, action) => {
      state.tokens = action.payload;
    },
    saveNetworks: (state, action) => {
      state.networks = action.payload;
    },
    initializeRemovedDefaultNetworks: (state, action) => {
      state.removedDefaultNetworks = action.payload;
    },
    removeDefaultNetwork: (state, action) => {
      const chainId = parseInt(action.payload);
      state.removedDefaultNetworks[chainId] = true;
    },
    saveSelectedNetworkObject: (state, action) => {
      state.selectedNetworkObj = action.payload;
    },
    saveWDAddressObject: (state, action) => {
      state.wdSupportObject = action.payload;
    },
    saveSwapSupportedNetworks: (state, action) => {
      state.swapSupportedNetworks = action.payload;
    },
    initializeRemovedDefaultTokens: (state, action) => {
      state.removedDefaultTokens = action.payload;
    },
    removeDefaultToken: (state, action) => {
      const address = action.payload.address.toLowerCase();
      const accountAddress = action.payload.accountAddress.toLowerCase();
      const { chainId } = state.selectedNetworkObj;
      state.removedDefaultTokens[chainId] = state.removedDefaultTokens[chainId] ?? {};
      state.removedDefaultTokens[chainId][accountAddress] = state.removedDefaultTokens[chainId][accountAddress] ?? [];
      const removedTokens = state.removedDefaultTokens[chainId][accountAddress];
      if (!removedTokens.includes(address)) {
        removedTokens.push(address);
      }
    },
    fetchingTokenPrices: (state, action) => {
      action.payload.forEach(payload => {
        const { chainId } = payload;
        const tokenAddress = payload.tokenAddress.toLowerCase();
        state.usdPrices[chainId] = state.usdPrices[chainId] ?? {};
        state.usdPrices[chainId][tokenAddress] = state.usdPrices[chainId][tokenAddress] ?? {};
        state.usdPrices[chainId][tokenAddress].fetching = true;
      });
    },
    fetchedTokenPrices: (state, action) => {
      action.payload.forEach(payload => {
        const { success, chainId, usdPrice } = payload;
        const tokenAddress = payload.tokenAddress.toLowerCase();
        if (success) {
          state.usdPrices[chainId][tokenAddress].value = usdPrice;
          state.usdPrices[chainId][tokenAddress].fetching = false;
          state.usdPrices[chainId][tokenAddress].lastFetched = Date.now();
        } else {
          state.usdPrices[chainId][tokenAddress].fetching = false;
        }
      });
    },
    fetchingTokenBalances: (state, action) => {
      action.payload.forEach(payload => {
        const { chainId } = payload;
        const accountAddress = payload.accountAddress.toLowerCase();
        const tokenAddress = payload.tokenAddress.toLowerCase();
        state.tokenBalances[chainId] = state.tokenBalances[chainId] ?? {};
        state.tokenBalances[chainId][accountAddress] = state.tokenBalances[chainId][accountAddress] ?? {};
        state.tokenBalances[chainId][accountAddress][tokenAddress] = state.tokenBalances[chainId][accountAddress][tokenAddress] ?? {};
        state.tokenBalances[chainId][accountAddress][tokenAddress].fetching = true;
      });
    },
    fetchedTokenBalances: (state, action) => {
      action.payload.forEach(payload => {
        const { success, chainId, balance } = payload;
        const accountAddress = payload.accountAddress.toLowerCase();
        const tokenAddress = payload.tokenAddress.toLowerCase();
        if (success) {
          state.tokenBalances[chainId][accountAddress][tokenAddress].value = balance;
          state.tokenBalances[chainId][accountAddress][tokenAddress].fetching = false;
          state.tokenBalances[chainId][accountAddress][tokenAddress].lastFetched = Date.now();
        } else {
          state.tokenBalances[chainId][accountAddress][tokenAddress].fetching = false;
        }
      });
    },
    refreshBalances: () => {
      // noop (handled by fetchTokenBalancesMiddleware and fetchTokenPricesMiddleware)
      // Maybe not ideal, but it's better than the mess we had before
    },
  },
});

const initNetwork = () => async dispatch => {
  try {
    const { data } = await StorageOperation.getMultiData([
      ASYNC_KEYS.NETWORKS,
      ASYNC_KEYS.SELECTED_NETWORK,
      ASYNC_KEYS.REMOVED_DEFAULT_NETWORKS,
      ASYNC_KEYS.REMOVED_DEFAULT_TOKENS,
    ]);
    const selectedNetwork = data[1][1];
    const parseSelectedObj = parseObject(selectedNetwork);

    const networks = data[0][1];
    console.log('networks: ', networks);

    const parsedRemovedDefaultNetworks = parseObject(data[2][1]);
    if (parsedRemovedDefaultNetworks.success) {
      dispatch(networkSlice.actions.initializeRemovedDefaultNetworks(parsedRemovedDefaultNetworks.data));
    }

    const parsedRemovedDefaultTokens = parseObject(data[3][1]);
    if (parsedRemovedDefaultTokens.success) {
      dispatch(networkSlice.actions.initializeRemovedDefaultTokens(parsedRemovedDefaultTokens.data));
    }

    let NetworkDetails = [];
    try {
      const networksUrl = BACKEND_SERVER.BASE_URL + BACKEND_SERVER.GET_NETWORKS;
      const networksResponse = await axios.get(networksUrl);
      NetworkDetails = networksResponse.data
        .filter(network => !network.disabled)
        .map(network => ({
          ...network,
          isDefault: true,
        }));
    } catch (e) {
      NetworkDetails = fallbackNetworks;
    }

    if (networks) {
      const parseNetworks = parseObject(networks);

      if (parseNetworks.success) {
        // Fix to convert any string chainId to number
        parseNetworks.data.forEach(network => {
          network.chainId = parseInt(network.chainId);
        });

        let mergedNetworks = [...NetworkDetails];
        parseNetworks.data.forEach(network => {
          if (!network.isDefault) {
            const defaultNetwork = mergedNetworks.find(mergedNetwork => mergedNetwork.chainId === network.chainId && mergedNetwork.isDefault);
            if (defaultNetwork) {
              network.icon ??= defaultNetwork.icon;
              network.tokens ??= defaultNetwork.tokens;
              network.wNativeAddress ??= defaultNetwork.wNativeAddress;
              network.tokens.forEach(token => {
                try {
                  const defaultToken = defaultNetwork.tokens.find(t => {
                    const defaultTokenAddress = t.address?.toLowerCase() ?? t.tokenId?.toLowerCase();
                    const tokenAddress = token.address?.toLowerCase() ?? token.tokenId?.toLowerCase();
                    return defaultTokenAddress === tokenAddress;
                  });
                  if (defaultToken) {
                    token.symbol = defaultToken.symbol;
                    token.decimals = defaultToken.decimals;
                  }
                } catch (e) {
                  // Failed to assign default token values
                }
              });
            }
            mergedNetworks = mergedNetworks.filter(mergedNetwork => mergedNetwork.chainId !== network.chainId);
          }
          const alreadyIncluded = mergedNetworks.some(mergedNetwork => mergedNetwork.chainId === network.chainId);
          if (!alreadyIncluded) {
            mergedNetworks.push(network);
          }
        })
        handleNetworkTokenIcons(mergedNetworks);

        console.log('MERGED: ', mergedNetworks);
        dispatch(networkSlice.actions.saveNetworks(mergedNetworks));
      }
    } else {
      await StorageOperation.setData(
        ASYNC_KEYS.NETWORKS,
        JSON.stringify(NetworkDetails),
      );
      handleNetworkTokenIcons(NetworkDetails);
      dispatch(networkSlice.actions.saveNetworks(NetworkDetails));
    }

    if (selectedNetwork && parseSelectedObj.success) {
      dispatch(
        networkSlice.actions.saveSelectedNetworkObject(parseSelectedObj.data),
      );
    } else {
      dispatch(
        networkSlice.actions.saveSelectedNetworkObject(NetworkDetails[0]),
      );
      await StorageOperation.setData(
        ASYNC_KEYS.SELECTED_NETWORK,
        JSON.stringify(NetworkDetails[0]),
      );
    }

    dispatch(getSwapSupportNetworkList());
  } catch (e) {
    log('er >>>', e);
  }
};

const checkIsNetworkSupport = networkDetail => async dispatch => {
  try {
    const body = {
      chainId: networkDetail.chainId,
      networkName: networkDetail.networkName,
    };

    const baseUrl = BACKEND_SERVER.BASE_URL + BACKEND_SERVER.GET_NETWORK_SUPPORT_DETAIL;

    const { data } = await axios.post(baseUrl, body, {
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    });

    dispatch(networkSlice.actions.saveWDAddressObject(data));

  } catch (error) {
    console.log('ERROR: ', error);
    const { data } = error.response;
    dispatch(networkSlice.actions.saveWDAddressObject(data));
  }
};

const getSwapSupportNetworkList = () => async dispatch => {
  try {
    const baseUrl = BACKEND_SERVER.BASE_URL + BACKEND_SERVER.GET_SWAP_SUPPORT_NETWORK_LIST;

    const { data, status } = await axios.post(baseUrl, {}, {
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    });

    if (status === 200) {
      if (data.success) {
        dispatch(networkSlice.actions.saveSwapSupportedNetworks(data.data));
      }
    }

  } catch (error) {
    console.log('ERROR: ', error);
  }
};

export const NetworksAction = {
  ...networkSlice.actions,
  initNetwork,
  checkIsNetworkSupport,
};
export const NetworksReducer = networkSlice.reducer;
