import { createSlice } from '@reduxjs/toolkit';

// import languages
import { localize } from '@languages';
import { getUserFriendlyErrorMessage } from '../../utils/errors';

const globalSlice = createSlice({
  name: 'global',
  initialState: {
    alertData: {
      isShowAlert: false,
      alertTitle: '',
      alertMsg: '',
      successBtnTitle: localize('OK'),
    },
    isLoading: false,
  },
  reducers: {
    showAlertMsg: (state, action) => {
      state.alertData = {
        ...state.alertData,
        isShowAlert: true,
        ...action.payload,
      };
    },
    hideAlertMsg: (state, action) => {
      state.alertData = {
        ...state.alertData,
        ...action.payload,
        isShowAlert: false,
      };
    },
    isShowLoading: (state, action) => {
      state.isLoading = action.payload;
    },
  },
});

const showAlert =
  (title = '', message = '', successBtnTitle = localize('OK')) =>
    dispatch => {
      message = getUserFriendlyErrorMessage(message);
      setTimeout(() => {
        dispatch(
          globalSlice.actions.showAlertMsg({
            alertTitle: title,
            alertMsg: message,
            successBtnTitle: successBtnTitle,
          }),
        );
      }, 200);
    };
const hideAlert = () => dispatch => {
  dispatch(
    globalSlice.actions.hideAlertMsg({
      alertTitle: '',
      alertMsg: '',
      successBtnTitle: localize('OK'),
    }),
  );
};

export const GlobalAction = { ...globalSlice.actions, showAlert, hideAlert };
export const GlobalReducer = globalSlice.reducer;
