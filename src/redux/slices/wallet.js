import { createSlice } from '@reduxjs/toolkit';

import {
  AccountsAction,
  NetworksAction,
  TransactionsAction,
  WalletConnectAction,
  store,
} from '@redux';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

import { log, parseObject } from '@utils';

const walletSlice = createSlice({
  name: 'wallet',
  initialState: {
    walletDetail: {},
    password: '',
    isPasscode: false,
  },
  reducers: {
    savePassword: (state, action) => {
      state.password = action.payload;
    },
    setIsPasscode: (state, action) => {
      state.isPasscode = action.payload;
    },
    saveWalletDetail: (state, action) => {
      state.walletDetail = action.payload;
    },
    updateWalletObject: (state, action) => {
      state.walletDetail = action.payload;
    },
    removeAccount: (state, action) => {
      const { isImported, isHardware } = action.payload;
      const publicAddress = action.payload.publicAddress.toLowerCase();
      if (isImported || isHardware) {
        // Completely remove imported/hardware accounts from wallet object
        state.walletDetail.walletObject = state.walletDetail.walletObject.filter((
          account => `0x${account.address.toLowerCase()}` !== publicAddress
        ));
      } else {
        // Archive generated accounts (keep in wallet object)
      }
    },
  },
});

const initApp = (wallet, password, isPasscode) => async dispatch => {
  try {
    // save wallet object into redux
    dispatch(walletSlice.actions.saveWalletDetail(wallet));

    // save password object into redux
    dispatch(walletSlice.actions.savePassword(password));
    dispatch(walletSlice.actions.setIsPasscode(isPasscode));

    // reset All Data
    dispatch(resetData());
  } catch (e) {
    return log(e.message);
  }
};

const getAccountList = () => async dispatch => {
  const { data } = await StorageOperation.getData(ASYNC_KEYS.ACCOUNT_LIST);

  if (data) {
    const parseObj = parseObject(data);
    if (parseObj.success) {
      dispatch(AccountsAction.saveAccountList(parseObj.data));

      dispatch(
        WalletConnectAction.initSelectedAccounts(
          parseObj.data[0].publicAddress,
        ),
      );
    }
  }
};

const getNetworkTokenList = () => async (dispatch) => {
  const networks = store.getState().networks;
  const chainData = await StorageOperation.getData(ASYNC_KEYS.CHAIN_TOKENS);
  if (chainData.data) {
    
    let chainTokens = undefined;

    const parseObj = parseObject(chainData.data);
    if (parseObj.success) {
      chainTokens = parseObj.data;
    }
    if (chainTokens) {
      let upgraded = false;
      chainTokens.forEach(chain => {
        chain.accountTokens?.forEach(account => {
          account.tokens?.forEach(token => {
            if (token.tokenId !== undefined) {
              token.address = token.tokenId;
              delete token.tokenId;
              upgraded = true;
            }
            if (token.decimal !== undefined) {
              token.decimals = token.decimal;
              delete token.decimal;
              upgraded = true;
            }
          });
        });
      });
      if (upgraded) {
        await StorageOperation.setMultiData([
          [ASYNC_KEYS.CHAIN_TOKENS, JSON.stringify(chainTokens)],
        ]);
      }

      const chainObj = chainTokens.find(
        chain => chain.chainId === networks.selectedNetworkObj.chainId,
      );
      if (chainObj) {
        dispatch(NetworksAction.saveTokens(chainObj.accountTokens));
      } else {
        dispatch(NetworksAction.saveTokens([]));
      }
    }
  }
};

const saveBlockNumber = async wallet => {
  try {
    const { data } = await StorageOperation.getData(ASYNC_KEYS.BLOCK_NO);

    const parseObj = parseObject(data);
    if (!parseObj.success) {
      const tempObj = wallet.blockNumbers.reduce(
        (r, c) => Object.assign(r, c),
        {},
      );
      await StorageOperation.setData(
        ASYNC_KEYS.BLOCK_NO,
        JSON.stringify(tempObj),
      );
    }
  } catch (e) {
    log('e >>', e);
  }
};

const getTransactionsList = () => async dispatch => {
  const networks = store.getState().networks;

  const { data } = await StorageOperation.getMultiData([
    ASYNC_KEYS.TRANSACTIONS,
    ASYNC_KEYS.RECENT_TRANSACTIONS,
  ]);
  if (data) {
    let transactions;

    const parseObj = parseObject(data[0][1]);
    if (parseObj.success) {
      transactions = parseObj.data;
    }

    if (transactions) {
      const transactionObj = transactions.find(
        trans => trans.chainId === networks.selectedNetworkObj.chainId,
      );
      if (transactionObj) {
        dispatch(
          TransactionsAction.saveTransactions(transactionObj.transactions),
        );
      } else {
        dispatch(TransactionsAction.saveTransactions([]));
      }
    }

    let recentAddresses;

    const parseObj2 = parseObject(data[1][1]);
    if (parseObj2.success) {
      recentAddresses = parseObj2.data;
    }

    if (recentAddresses) {
      dispatch(TransactionsAction.saveRecentAddresses(recentAddresses));
    } else {
      dispatch(TransactionsAction.saveRecentAddresses([]));
    }
  }
};

const resetData = () => async dispatch => {
  try {
    // reset all slice data
    dispatch(AccountsAction.resetSliceData());
    dispatch(NetworksAction.resetSliceData());
    dispatch(TransactionsAction.resetSliceData());
    dispatch(WalletConnectAction.initCompletedAuthRequests());
    dispatch(WalletConnectAction.initCompletedSessionProposals());
    dispatch(WalletConnectAction.initCompletedSessionRequests());
    dispatch(WalletConnectAction.initRequestVerifications());
    const networks = store.getState().networks;

    // get Network & tokenList
    dispatch(getNetworkTokenList());
    // getAccounts
    dispatch(getAccountList());
    // get Transactions List
    dispatch(getTransactionsList());
    // check Selected network support WD
    dispatch(NetworksAction.checkIsNetworkSupport(networks.selectedNetworkObj));
  } catch (e) {
    return log(e.message);
  }
};

export const WalletAction = {
  ...walletSlice.actions,
  initApp,
  getAccountList,
  resetData,
};
export const WalletReducer = walletSlice.reducer;
