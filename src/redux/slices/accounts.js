import { createSelector, createSlice } from '@reduxjs/toolkit';

const assignDefaultIsArchived = (accounts) => (
  accounts.map(account => ({
    ...account,
    isArchived: account.isArchived ?? false,
  }))
);

const accountSlice = createSlice({
  name: 'accounts',
  initialState: {
    accountList: [],
  },
  reducers: {
    resetSliceData: state => {
      state.accountList = [];
    },
    saveAccountList: (state, action) => {
      state.accountList = assignDefaultIsArchived(action.payload);
    },
    updateAccountObject: (state, action) => {
      state.accountList = assignDefaultIsArchived(action.payload);
    },
    isImportedUpdates: (state, action) => {
      action.payload.forEach(({ publicAddress, isImported }) => {
        const account = state.accountList.find(account => (
          account.publicAddress.toLowerCase() === publicAddress.toLowerCase()
        ));
        account.isImported = isImported;
      });
    },
    isPopularTokensUpdate: (state, action) => {
      action.payload.forEach(({ publicAddress, isPopularTokens }) => {
        const account = state.accountList.find(account => (
          account.publicAddress.toLowerCase() === publicAddress.toLowerCase()
        ));
        account.isPopularTokens = isPopularTokens;
      });
    },
    removeAccount: (state, action) => {
      const { isImported, isHardware } = action.payload;
      const publicAddress = action.payload.publicAddress.toLowerCase();
      if (isImported || isHardware) {
        // Completely remove imported/hardware accounts
        state.accountList = state.accountList.filter((
          account => account.publicAddress.toLowerCase() !== publicAddress
        ));
      } else {
        // Archive generated accounts
        state.accountList.find((
          account => account.publicAddress.toLowerCase() === publicAddress
        )).isArchived = true;
      }
    },
    restoreAccount: (state, action) => {
      const publicAddress = action.payload.publicAddress.toLowerCase();
      state.accountList.find((
        account => account.publicAddress.toLowerCase() === publicAddress
      )).isArchived = false;
    },
  },
});

export const AccountsAction = {
  ...accountSlice.actions,
};
export const AccountsReducer = accountSlice.reducer;
