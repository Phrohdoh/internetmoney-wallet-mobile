import {createSlice} from '@reduxjs/toolkit';

const transactionsSlice = createSlice({
  name: 'transactions',
  initialState: {
    recentAddresses: [],
    transactionsList: [],
  },
  reducers: {
    resetSliceData: state => {
      state.recentAddresses = [];
      state.transactionsList = [];
    },
    saveTransactions: (state, action) => {
      state.transactionsList = action.payload;
    },
    saveRecentAddresses: (state, action) => {
      state.recentAddresses = action.payload;
    },
  },
});

export const TransactionsAction = {
  ...transactionsSlice.actions,
};
export const TransactionsReducer = transactionsSlice.reducer;
