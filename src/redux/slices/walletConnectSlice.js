import { createSlice } from '@reduxjs/toolkit';

// import constants
import { ASYNC_KEYS } from '@constants';

// import storage functions
import { StorageOperation } from '@storage';

// import constants
import { log, parseObject } from '@utils';

export const initialState = {
  selectedAccount: undefined,
  openedByDapp: false,
  completedAuthRequestIds: [],
  completedSessionProposalIds: [],
  completedSessionRequestIds: [],
  requestVerifications: {},
};

const walletConnectSlice = createSlice({
  name: 'walletConnnect',
  initialState: { ...initialState },
  reducers: {
    updateSelectedAccount: (state, action) => {
      state.selectedAccount = action.payload;
    },
    openedByDapp: (state) => {
      state.openedByDapp = true;
    },
    returnToDapp: (state) => {
      state.openedByDapp = false;
    },
    completedAuthRequests: (state, action) => {
      state.completedAuthRequestIds = action.payload;
    },
    completedAuthRequest: (state, action) => {
      const id = action.payload;
      if (!state.completedAuthRequestIds.includes(id)) {
        state.completedAuthRequestIds.push(id);
      }
    },
    completedSessionProposals: (state, action) => {
      state.completedSessionProposalIds = action.payload;
    },
    completedSessionProposal: (state, action) => {
      const id = action.payload;
      if (!state.completedSessionProposalIds.includes(id)) {
        state.completedSessionProposalIds.push(id);
      }
    },
    completedSessionRequests: (state, action) => {
      state.completedSessionRequestIds = action.payload;
    },
    completedSessionRequest: (state, action) => {
      const id = action.payload;
      if (!state.completedSessionRequestIds.includes(id)) {
        state.completedSessionRequestIds.push(id);
      }
    },
    verifyRequest: (state, action) => {
      const { id, verification } = action.payload;
      state.requestVerifications[id] = verification ?? 'UNKNOWN';
    },
    requestVerifications: (state, action) => {
      state.requestVerifications = action.payload;
    },
  },
});

const initSelectedAccounts = account => async (dispatch) => {
  try {
    const { data } = await StorageOperation.getData(
      ASYNC_KEYS.WALLET_CONNECTED_ACCOUNT,
    );

    const parseObj = parseObject(data);
    if (parseObj.success) {
      dispatch(walletConnectSlice.actions.updateSelectedAccount(parseObj.data));
    } else {
      dispatch(walletConnectSlice.actions.updateSelectedAccount(account));
      await StorageOperation.setData(
        ASYNC_KEYS.WALLET_CONNECTED_ACCOUNT,
        JSON.stringify(account),
      );
    }
  } catch (e) {
    return log(e.message);
  }
};

const initCompletedAuthRequests = () => async (dispatch) => {
  try {
    const { data } = await StorageOperation.getData(
      ASYNC_KEYS.WALLET_CONNECT_COMPLETED_AUTH_REQUEST_IDS,
    );
    const parseObj = parseObject(data);
    if (parseObj.success) {
      dispatch(walletConnectSlice.actions.completedAuthRequests(parseObj.data));
    }
  } catch (e) {
    return log(e.message);
  }
}

const initCompletedSessionProposals = () => async (dispatch) => {
  try {
    const { data } = await StorageOperation.getData(
      ASYNC_KEYS.WALLET_CONNECT_COMPLETED_SESSION_PROPOSAL_IDS,
    );
    const parseObj = parseObject(data);
    if (parseObj.success) {
      dispatch(walletConnectSlice.actions.completedSessionProposals(parseObj.data));
    }
  } catch (e) {
    return log(e.message);
  }
}

const initCompletedSessionRequests = () => async (dispatch) => {
  try {
    const { data } = await StorageOperation.getData(
      ASYNC_KEYS.WALLET_CONNECT_COMPLETED_SESSION_REQUEST_IDS,
    );
    const parseObj = parseObject(data);
    if (parseObj.success) {
      dispatch(walletConnectSlice.actions.completedSessionRequests(parseObj.data));
    }
  } catch (e) {
    return log(e.message);
  }
}

const initRequestVerifications = () => async (dispatch) => {
  try {
    const { data } = await StorageOperation.getData(
      ASYNC_KEYS.WALLET_CONNECT_REQUEST_VERIFICATIONS,
    );
    const parseObj = parseObject(data);
    if (parseObj.success) {
      dispatch(walletConnectSlice.actions.requestVerifications(parseObj.data));
    }
  } catch (e) {
    return log(e.message);
  }
}

export const WalletConnectAction = {
  ...walletConnectSlice.actions,
  initSelectedAccounts,
  initCompletedAuthRequests,
  initCompletedSessionProposals,
  initCompletedSessionRequests,
  initRequestVerifications,
};
export const WalletConnectReducer = walletConnectSlice.reducer;
export default walletConnectSlice.reducer;
