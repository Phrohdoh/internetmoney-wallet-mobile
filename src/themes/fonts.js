// Font types list
export const FONTS = {
  BASE: 'Poppins-Regular',
  BOLD: 'Poppins-Bold',
  EXTRA_BOLD: 'Poppins-ExtraBold',
  LIGHT: 'Poppins-Light',
  MEDIUM: 'Poppins-Medium',
  SEMIBOLD: 'Poppins-SemiBold',
  PIX_BOY: 'Pixeboy',
};
