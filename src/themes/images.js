import { Platform } from 'react-native';

export const IMAGES = {
  loading: { uri: Platform.OS === 'ios' ? 'loading.gif' : 'loading' },
  ROUNDED_LOADING: {
    uri: Platform.OS === 'ios' ? 'rounded_loading.gif' : 'rounded_loading',
  },
  BACK_ARROW: { uri: 'back_arrow' },
  onboardLogo: { uri: 'onboarding_logo' },
  GET_READY_BG: { uri: 'get_ready_bg' },
  UNDERSTAND_BASIC_BG: { uri: 'understand_basic_bg' },
  PASS_SETUP_BG: { uri: 'shield_and_lock' },
  check: { uri: 'check' },
  uncheck: { uri: 'uncheck' },
  show: { uri: 'show' },
  hide: { uri: 'hide' },
  BG_GRID_IMG: { uri: 'bg_grid_img' },
  WALLET_IMAGE_ONE: { uri: 'wallet_img_one' },
  WALLET_IMAGE_TWO: { uri: 'wallet_img_two' },
  WALLET_IMAGE_THREE: { uri: 'wallet_img_three' },
  WALLET_IMAGE_FOUR: { uri: 'wallet_img_four' },
  WALLET_SUCCESS_IMG: { uri: 'wallet_success_img' },
  DOLLOR_ICON: { uri: 'dollor_icon' },
  SUPPORT_ICON: { uri: 'support_icon' },
  GIFT_ICON: { uri: 'gift_icon' },
  RIGHT_ARROW: { uri: 'right_arrow' },
  SEND_ICON: { uri: 'send_icon' },
  RECEIVE_ICON: { uri: 'receive_icon' },
  SWAP_ICON: { uri: 'swap_icon' },
  DAPP_ICON: { uri: 'dapp_icon' },
  EARN_REFERRAL_ICON: { uri: 'earn_referral_icon' },
  ADD_ICON: { uri: 'add_icon' },
  MINUS_ICON: { uri: 'minus_icon' },
  DELETE_TEXT_ICON: { uri: 'delete_text_icon' },

  CLOSE_ICON: { uri: 'close_icon' },
  CHART_IMG: { uri: 'chart_img' },
  DOWN_ARROW: { uri: 'down_arrow' },
  QR_ICON: { uri: 'qr_icon' },
  CONFIRMATION_BG: { uri: 'confirmation_bg' },
  DOTTET_LINE: { uri: 'dotted_line' },
  VERTICAL_DOTTET_LINE: { uri: 'vertical_dotted_line' },
  COPY_ICON: { uri: 'copy_icon' },
  SHARE_ICON: { uri: 'share_icon' },
  SWAP_TOKEN_BORDER: { uri: 'swap_token_border' },
  GIFT_BG: { uri: 'gift_bg' },
  GIFT_IMG_TOP: { uri: 'gift_img_top' },
  GIFT_IMG_BOTTOM: { uri: 'gift_img_bottom' },
  REEDEM_BG: { uri: 'reedem_bg' },
  CLAIM_ICON: { uri: 'claim_icon' },
  TIME: { uri: 'time' },
  WD_TOKEN_ICON: { uri: 'time_icon_3d' },
  GRID_ICON: { uri: 'grid_icon' },
  LIST_ICON: { uri: 'list_icon' },
  RIGHT_ARROW_SOLID: { uri: 'right_arrow_solid' },
  SCAM_ALERT_ICON: { uri: 'scam_alert_icon' },
  TWITTER_ICON: { uri: 'twitter_icon' },
  GLOB_ICON: { uri: 'glob_icon' },
  TELEGRAM_ICON: { uri: 'telegram_icon' },
  REDDIT_ICON: { uri: 'reddit_icon' },
  SETTING_BORDERED_ICON: { uri: 'setting_bordered_icon' },
  GIFT_CLAIM_BG: { uri: 'gift_claim_bg' },
  PLS_TOKEN_ICON: { uri: 'wpls_token' },
  PLS_V3_TOKEN_ICON: { uri: 'pls' },
  IM_TOKEN_ICON: { uri: 'im_icon_3d' },
  TIME_TOKEN_ICON: { uri: 'time_icon_3d' },
  PLAY_BUTTON: { uri: 'play_button' },
  ARTICAL_CELL_BG: { uri: 'artical_cell_bg' },
  RIGHT_LONG_ARROW: { uri: 'right_long_arrow' },
  REMOVE_ICON: { uri: 'remove_icon' },
  ALERT_ICON: { uri: 'alert_icon' },
  TOKEN_PLACEHOLDER_ICON: { uri: 'token_placeholder_icon' },
  REFRESH_ICON: { uri: 'refresh_icon' },

  // Tab bar bottom icons
  NETWORK_DASHBOARD_TAB: { uri: 'network_icon' },
  CLAIM_DASHBOARD_TAB: { uri: 'time' },
  CLAIM_DASHBOARD_TAB_GRAY: { uri: 'time_gray' },
  WALLET_DASHBOARD_TAB: { uri: 'wallet_icon' },
  SETTING_DASHBOARD_TAB: { uri: 'setting_icon' },
  SWAP_DASHBOARD_TAB: { uri: 'swap_nav_icon' },
  SWAP_DASHBOARD_TAB_GRAY: { uri: 'swap_nav_icon_gray' },
};

export const handleNetworkTokenIcons = (NetworkDetails) => {
  NetworkDetails.forEach((network) => {
    IMAGES[network.chainId] = {
      NATIVE_ICON: {
        uri: network.icon,
      },
    };
    network.tokens?.forEach((token) => {
      IMAGES[network.chainId][token.address.toLowerCase()] = {
        uri: token.icon,
      };
    });
  });
};
