// Common color list
export const COLORS = {
  // White Shades
  WHITE: '#ffffff',

  // Black Shades
  BLACK: '#000000',

  // Gray Shades
  PLACEHOLDER: '#AEAEAE',
  GRAY43: '#434343',
  GRAY15: '#151515',
  GRAYAE: '#AEAEAE',
  GRAY32: '#323232',
  GRAY1A: '#1A1A1A',
  GRAY11: '#111111',
  GRAY2E: '#2E2E2E',
  GRAY18: '#181818',
  GRAY1F: '#1f1f1f',
  GRAY1C: '#1c1c1c',
  GRAYDF: '#DFDFDF',
  GRAY6D: '#6D6D6D',
  GRAYBF: '#BFBFBF',
  GRAYB5: '#B5B5B5',
  GRAY41: '#414141',
  GRAY1E: '#1E1E1E',
  GRAY94: '#949494',
  GRAY27: '#272727',
  GRAY28: '#282828',
  GRAY50: '#505050',
  GRAYCA: '#CACACA',
  GRAY83: '#838383',

  // Yellow shades
  YELLOWFB: '#FBA81A',
  YELLOW77: '#77551F',

  // Green shades
  GREEN6D: '#6DDE45',

  // Red Shades
  REDC1: '#C1272D',

  // Others
  ERROR: 'red',
  TRANSPARENT: 'transparent',
  DARK_OPACITY: (opacity = 0.5) => `rgba(0, 0, 0, ${opacity})`,
  LIGHT_OPACITY: (opacity = 0.5) => `rgba(255, 255, 255, ${opacity})`,
};
