import { StyleSheet } from 'react-native';

// import constants
import { COLORS } from './colors';
import { FONTS } from './fonts';

// import helpers
import { Responsive } from '@helpers';

export const bottomTabPadding = Responsive.getHeight(14);

export const STYLES = StyleSheet.create({
  textStyle: (size, color = COLORS.BLACK, font = 'BASE', align) => {
    return {
      fontSize: Responsive.getFontSize(size),
      fontFamily: FONTS[font],
      color: color,
      textAlign: align,
      padding: 0,

      // lineHeight: Responsive.getFontSize(size) + 3,
    };
  },
  paddingStyle: (left, right, top, bottom) => {
    const paddingObj = {};
    if (left) {
      paddingObj.paddingLeft = Responsive.getWidth(left);
    }
    if (right) {
      paddingObj.paddingRight = Responsive.getWidth(right);
    }
    if (top) {
      paddingObj.paddingTop = Responsive.getHeight(top);
    }
    if (bottom) {
      paddingObj.paddingBottom = Responsive.getHeight(bottom);
    }

    return { ...paddingObj };
  },
  marginStyle: (left, right = 0, top = 0, bottom = 0) => {
    const marginObj = {};
    if (left) {
      marginObj.marginLeft = Responsive.getWidth(left);
    }
    if (right) {
      marginObj.marginRight = Responsive.getWidth(right);
    }
    if (top) {
      marginObj.marginTop = Responsive.getHeight(top);
    }
    if (bottom) {
      marginObj.marginBottom = Responsive.getHeight(bottom);
    }

    return { ...marginObj };
  },
});

export const COMMON_STYLE = StyleSheet.create({
  ...STYLES,
  textStyle: (size, color = COLORS.BLACK, font = 'BASE', align) => {
    return STYLES.textStyle(size, color, font, align);
  },

  imageStyle: (size, color) => {
    return {
      width: Responsive.getWidth(size),
      height: Responsive.getWidth(size),
      resizeMode: 'contain',
      tintColor: color,
    };
  },

  SafeAreaView: {
    flexGrow: 1,
    backgroundColor: COLORS.WHITE,
  },

  safeAreaViewStyle: {
    flex: 1,
    backgroundColor: COLORS.BLACK,
  },
  KeyboardAvoidingView: {
    flex: 1,
  },

  containerStyle: {
    ...STYLES.marginStyle(6, 6),
    flex: 1,
  },

  IndicatorStyle: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },

  infoTitleStyle: (color = COLORS.YELLOWFB) => {
    return {
      ...STYLES.textStyle(19, color, 'BOLD'),
      lineHeight: Responsive.getFontSize(19) + 15,
    };
  },
  infoSubTitleStyle: (color = COLORS.LIGHT_OPACITY(0.6)) => {
    return {
      ...STYLES.textStyle(13, color),
    };
  },
  infoDescStyle: (color = COLORS.WHITE) => {
    return {
      ...STYLES.textStyle(13, color),
      marginTop: Responsive.getHeight(2),
      lineHeight: Responsive.getFontSize(14) + 8,
    };
  },
  roundedBtnMargin: (bottom = Responsive.getHeight(3)) => {
    return { marginBottom: bottom };
  },
  tabContainerStyle: {
    marginHorizontal: 0,
    backgroundColor: COLORS.BLACK,
  },
  borderedContainerViewStyle: (bottomPadding = 0) => {
    return {
      flex: 1,
      borderColor: COLORS.GRAY18,
      backgroundColor: COLORS.GRAY11,
      borderTopWidth: 3,
      borderLeftWidth: 3,
      borderRightWidth: 3,
      borderTopLeftRadius: Responsive.getWidth(5),
      borderTopRightRadius: Responsive.getWidth(5),
      paddingHorizontal: Responsive.getWidth(5),
      paddingBottom: bottomPadding,
    };
  },
  modalTitleStyle: {
    ...STYLES.textStyle(12, COLORS.GRAYB5, 'BOLD', 'center'),
    marginBottom: Responsive.getHeight(3),
  },
  scamAlertMsgStyle: {
    ...STYLES.textStyle(14, COLORS.LIGHT_OPACITY(0.6), 'BOLD'),
    marginTop: Responsive.getHeight(2),
    lineHeight: Responsive.getFontSize(14) + 8,
  },
});
