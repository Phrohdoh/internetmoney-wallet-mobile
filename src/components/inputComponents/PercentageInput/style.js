import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  slippageInput: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#151515',
    borderWidth: 2,
    borderColor: '#1F1F1F',
    height: 50,
    borderRadius: 50,
    paddingHorizontal: 20,
    width: '100%',
  },
  slippageInputLabel: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  slippageInputValue: {
    backgroundColor: '#151515',
    borderWidth: 2,
    borderColor: '#1F1F1F',
    fontSize: 16,
    height: 50,
    color: '#DFDFDF',
    paddingHorizontal: 20,
    width: 'auto',
    borderRadius: 50,
  },
});

export default styles;
