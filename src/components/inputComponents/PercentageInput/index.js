import React, { useState } from 'react';
import { Text, TextInput, View } from 'react-native';
import PropTypes from 'prop-types';
import { localize } from '@languages';
import styles from './style.js';

const PercentageInput = ({ onChange = () => {} }) => {
  const [value, setValue] = useState(1);

  return (
    <View style={ styles.wrapper }>
      <View style={ styles.slippageInput }>
        <Text style={ styles.slippageInputLabel }>{ localize('SLIPPAGE')}</Text>
        <TextInput
          style={ styles.slippageInputValue }
          keyboardType="numeric"
          defaultValue={ `${value}` }
          placeholder="%"
          onChangeText={ async (text) => { await onChange(text); } }
        />
      </View>
    </View>
  );
};

PercentageInput.propTypes = {
  onChange: PropTypes.func,
};

export default PercentageInput;
