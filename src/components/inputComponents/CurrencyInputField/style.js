import {StyleSheet} from 'react-native';

// import helpers
import {Responsive} from '@helpers';

// import themes
import {COLORS, COMMON_STYLE} from '@themes';

export const styles = StyleSheet.create({
  inputContainerStyle: {},
  titleStyle: {
    ...COMMON_STYLE.textStyle(13, COLORS.LIGHT_OPACITY(0.6)),
    marginBottom: Responsive.getHeight(1),
  },
  inputViewStyle: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    borderRadius: 25,
    overflow: 'hidden',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: COLORS.GRAY50,
    paddingHorizontal: Responsive.getWidth(4),
  },
  inputStyle: {
    height: '100%',
    flex: 1,
    ...COMMON_STYLE.textStyle(13, COLORS.WHITE),
  },
  multilineInputStyle: {
    height: '100%',
    flex: 1,
    ...COMMON_STYLE.textStyle(13, COLORS.WHITE),
    textAlignVertical: 'top',
    paddingTop: 10
  },
  buttonStyle: {
    height: '100%',
    flex: 1,
    justifyContent: 'center',
  },
  btnStyle: {
    ...COMMON_STYLE.marginStyle(3),
    height: '100%',
    justifyContent: 'center',
  },
  errorStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.ERROR),
    marginTop: Responsive.getWidth(0.3),
    marginHorizontal: Responsive.getWidth(5),
  },
});
