import React, { useState } from 'react';
import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import { CachedImage } from '@components';

import PropTypes from 'prop-types';

// import style
import { styles } from './style';

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

// import Utils
import { Validation } from '@utils';


const CurrencyInputField = props => {
  const [isShowPass, setIsShowPass] = useState(false);

  const {
    testID,
    title,
    value,
    placeholder,
    placeholderTextColor,
    inputContainerStyle,
    isPassword,
    passBtnStyle,
    iconRight,
    rightIconStyle,
    rightComponent,
    onPressRight,
    onPressInput,
    isError,
    errorText,
    multiline,
    isUSD,
    decimalsLimit = 6,
  } = props;

  return (
    <View style={ [styles.inputContainerStyle, inputContainerStyle] }>
      {title && (
        <Text style={ [styles.titleStyle, props.titleStyle] }>{title}</Text>
      )}
      <View style={ [styles.inputViewStyle, props.inputViewStyle] }>
        {!onPressInput ? (
          <TextInput
            prefix={ isUSD ? '$' : '' }
            delimiter=','
            separator="."
            precision={ decimalsLimit }
            minValue={ 0 }
            testID={ testID + '-input' }
            style={
              multiline
                ? [styles.multilineInputStyle, props.inputStyle]
                : [styles.inputStyle, props.inputStyle]
            }
            placeholderTextColor={ placeholderTextColor }
            secureTextEntry={ isPassword && !isShowPass }
            autoCorrect={ false }
            autoCapitalize={ 'none' }
            { ...props }
          />
        ) : (
          <TouchableOpacity
            testID={ testID + '-inputpress-button' }
            style={ styles.buttonStyle }
            activeOpacity={ 1 }
            onPress={ onPressInput }>
            <Text
              style={ [
                COMMON_STYLE.textStyle(
                  13,
                  Validation.isEmpty(value)
                    ? COLORS.PLACEHOLDER
                    : COLORS.GRAYAE,
                ),
                props.inputStyle,
              ] }>
              {Validation.isEmpty(value) ? placeholder : value}
            </Text>
          </TouchableOpacity>
        )}
        {isPassword && (
          <TouchableOpacity
            testID={ testID + '-pass-hideshow-button' }
            style={ [styles.btnStyle, passBtnStyle] }
            onPress={ () => setIsShowPass(!isShowPass) }>
            <CachedImage
              source={ !isShowPass ? IMAGES.hide : IMAGES.show }
              style={ COMMON_STYLE.imageStyle(5, COLORS.GRAYAE) }
            />
          </TouchableOpacity>
        )}
        {rightComponent}
        {iconRight && (
          <TouchableOpacity
            testID={ testID + '-rightaction--button' }
            style={ styles.btnStyle }
            onPress={ onPressRight ? onPressRight : onPressInput }>
            <CachedImage
              source={ iconRight }
              style={ [COMMON_STYLE.imageStyle(6), rightIconStyle] }
            />
          </TouchableOpacity>
        )}
      </View>
      {isError && (
        <Text style={ [styles.errorStyle, props.errorStyle] }>{errorText}</Text>
      )}
    </View>
  );
};

CurrencyInputField.defaultProps = {
  placeholderTextColor: COLORS.PLACEHOLDER,
};

CurrencyInputField.propTypes = {
  testID: PropTypes.string,
  title: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  isPassword: PropTypes.bool,
  iconRight: PropTypes.any,
  rightComponent: PropTypes.element,
  onPressRight: PropTypes.func,
  onPressInput: PropTypes.func,
  isError: PropTypes.bool,
  errorText: PropTypes.string,
  inputContainerStyle: PropTypes.any,
  passBtnStyle: PropTypes.any,
  rightIconStyle: PropTypes.any,
  titleStyle: PropTypes.any,
  inputViewStyle: PropTypes.any,
  inputStyle: PropTypes.any,
  errorStyle: PropTypes.any,
  multiline: PropTypes.bool,
  isUSD: PropTypes.bool,
};

export default CurrencyInputField;
