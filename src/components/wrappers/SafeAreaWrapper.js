// COMOPONENT OR LIBRARY IMPORT
import React, { Fragment } from 'react';
import { useHeaderHeight } from '@react-navigation/elements';
import PropTypes from 'prop-types';
import { View, StyleSheet, StatusBar, SafeAreaView } from 'react-native';
import { CachedImage } from '@components';

// import themes
import { COLORS } from '@themes';

// import helpers
import { Responsive } from '@helpers';

const Wrapper = (props) => {
  const {
    isTranslucent,
    backgroundColor,
    statusBarColor,
    containerStyle,
    children,
    bgImage,
    containerViewStyle,
    isRemoveSafearea,
  } = props;
  return (
    <Fragment>
      <StatusBar
        backgroundColor={statusBarColor}
        translucent={isTranslucent}
        barStyle="light-content"
      />

      <View style={styles.safeAreaViewStyle(backgroundColor)}>
        <CachedImage style={styles.backgroundStyle} source={bgImage} />
        <View style={[styles.containerViewStyle, containerViewStyle]}>
          {isRemoveSafearea
            ? (
              <View style={[styles.containerStyle(), containerStyle]}>
                {children}
              </View>
            )
            : (
              <SafeAreaView style={[styles.containerStyle(), containerStyle]}>
                {children}
              </SafeAreaView>
            )}
        </View>
      </View>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  safeAreaViewStyle: color => ({
    flex: 1,
    backgroundColor: color,
  }),
  backgroundStyle: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
  },
  containerViewStyle: {
    position: 'absolute',
    height: '100%',
    width: '100%',
  },
  containerStyle: () => {
    const headerHeight = useHeaderHeight();

    return {
      flex: 1,
      marginHorizontal: Responsive.getWidth(6),
      paddingTop: headerHeight <= 0 ? StatusBar.currentHeight : headerHeight,
    };
  },
});

Wrapper.defaultProps = {
  backgroundColor: COLORS.BLACK,
  statusBarColor: COLORS.TRANSPARENT,
  bgImage: '',
  isTranslucent: true,
  isRemoveSafearea: false,
};

Wrapper.propTypes = {
  isTranslucent: PropTypes.bool,
  backgroundColor: PropTypes.string,
  statusBarColor: PropTypes.string,
  containerStyle: PropTypes.any,
  children: PropTypes.any,
  bgImage: PropTypes.any,
  containerViewStyle: PropTypes.any,
  isRemoveSafearea: PropTypes.bool,
};

export const SafeAreaWrapper = Wrapper;
