import React from 'react';
import { View, FlatList } from 'react-native';
import PropTypes from 'prop-types';
import Tag from '../Tag';
import styles from './style';

const Tags = (props) => {
  const { style, hasError } = props;

  return (
    <View style={[styles.container, style]}>
      <FlatList
        columnWrapperStyle={styles.flatListContainerStyle}
        key="tags_grid_list"
        data={props.tags}
        numColumns={3}
        renderItem={({ item, index }) => (
          <Tag
            key={`${item}-${index}`}
            tag={item}
            index={index}
            isEditable={props.isEditable}
            error={hasError(item)}
            {...props}
          />
        )}
      />
    </View>
  );
};

Tags.defaultProps = {
  tags: [],
  isEditable: false,
  hasError: () => false,
};

Tags.propTypes = {
  isEditable: PropTypes.bool,
  tags: PropTypes.array,
  style: PropTypes.any,
  hasError: PropTypes.func,
};
export default Tags;
