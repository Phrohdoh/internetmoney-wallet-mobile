import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, TextInput } from 'react-native';
import { CachedImage } from '@components';
import PropTypes from 'prop-types';

// import themes
import { IMAGES } from '@themes';
import styles from './style';

const Tag = (props) => {
  const { tag, index, error } = props;
  const [tagTitle, setTagTitle] = useState(tag);

  return (
    <View key={`${tag}-${index}`} style={styles.tagContainerStyle}>
      <Text style={styles.indexStyle}>{`${index + 1}.`}</Text>
      <View style={error ? styles.errorTag : styles.tag}>
        {props.isEditable
          ? (
            <TextInput
              style={styles.textTag}
              value={tagTitle}
              onChangeText={text => setTagTitle(text)}
              onEndEditing={text => props.onEndEditing(text.nativeEvent.text, index)}
              testID={`tag-${index}`}
              autoCapitalize="none"
              autoCorrect={false}
            />
          )
          : (
            <Text
              numberOfLines={1}
              adjustsFontSizeToFit
              style={styles.textTag}
            >
              {!props.isHidden ? tagTitle : props.hiddenTagPlaceholder}
            </Text>
          )}
        {props.isEditable && (
          <TouchableOpacity
            style={styles.closeBtnstyle}
            testID={`tag-delete-${index}`}
            onPress={() => props.onPressDelete(index)}
          >
            <CachedImage source={IMAGES.CLOSE_ICON} style={styles.closeIconStyle} />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

Tag.defaultProps = {
  isEditable: false,
  isHidden: false,
  hiddenTagPlaceholder: '',
};

Tag.propTypes = {
  isEditable: PropTypes.bool,
  isHidden: PropTypes.bool,
  hiddenTagPlaceholder: PropTypes.string,
  onEndEditing: PropTypes.func,
  onPressDelete: PropTypes.func,
  tag: PropTypes.string,
  index: PropTypes.number,
  error: PropTypes.bool,
};
export default Tag;
