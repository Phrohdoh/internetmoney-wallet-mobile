import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export default StyleSheet.create({
  tagContainerStyle: {
    flexDirection: 'row',
    marginVertical: Responsive.getHeight(1),
    alignItems: 'center',
    width: `${100 / 3}%`,
    paddingHorizontal: '1%',
  },
  indexStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.GRAY94, 'BOLD'),
    marginRight: Responsive.getWidth(1),
  },
  flatListContainerStyle: { justifyContent: 'space-between' },

  tag: {
    flex: 1,
    minWidth: Responsive.getHeight(4.5),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.GRAY15,
    borderColor: COLORS.GRAY43,
    borderWidth: 3,
    borderRadius: Responsive.getHeight(3),
    paddingHorizontal: Responsive.getWidth(2),
    flexDirection: 'row',
    height: Responsive.getHeight(4.5),
  },
  errorTag: {
    flex: 1,
    minWidth: Responsive.getHeight(4.5),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.GRAY15,
    borderColor: COLORS.REDC1,
    borderWidth: 3,
    borderRadius: Responsive.getHeight(3),
    paddingHorizontal: Responsive.getWidth(2),
    flexDirection: 'row',
    height: Responsive.getHeight(4.5),
  },
  textTag: {
    ...COMMON_STYLE.textStyle(11, COLORS.WHITE, 'BOLD'),
    flex: 1,
  },
  closeBtnstyle: {
    height: '100%',
    width: Responsive.getWidth(5),
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: Responsive.getWidth(1),
    marginRight: Responsive.getWidth(-2),
  },
  closeIconStyle: {
    ...COMMON_STYLE.imageStyle(3),
  },
});
