import { StyleSheet } from 'react-native';

import { COLORS, COMMON_STYLE } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: COLORS.DARK_OPACITY(),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    height: '100%',
  },

  container: {
    width: Responsive.getWidth(90),
    borderRadius: Responsive.getWidth(3),
    backgroundColor: COLORS.GRAY1A,
    alignItems: 'center',
    padding: Responsive.getWidth(5),
  },

  title: {
    ...COMMON_STYLE.textStyle(16, COLORS.WHITE, 'BOLD', 'center'),
    // marginBottom: Responsive.getHeight(2),
  },

  upgradeBtnView: {
    flexDirection: 'row',
  },
  
  dapp: {
    flexDirection: 'row',
    padding: Responsive.getWidth(3),
    alignItems: 'center',
  },

  icon: {
    ...COMMON_STYLE.imageStyle(15),
    borderRadius: Responsive.getWidth(7.5),
  },

  info: {
    marginLeft: Responsive.getWidth(3),
  },

  name: {
    ...COMMON_STYLE.textStyle(14, COLORS.WHITE),
    textAlign: 'center',
  },

  url: {
    ...COMMON_STYLE.textStyle(8, COLORS.WHITE),
    textAlign: 'center',
  },

  gray: {
    ...COMMON_STYLE.textStyle(8, COLORS.WHITE),
    textAlign: 'center',
  },

  green: {
    ...COMMON_STYLE.textStyle(8, COLORS.GREEN6D),
    textAlign: 'center',
  },

  red: {
    ...COMMON_STYLE.textStyle(8, COLORS.REDC1),
    textAlign: 'center',
  },

  buttons: {
    flexDirection: 'row',
    marginTop: Responsive.getWidth(2),
    justifyContent: 'space-between',
  },

  view: {
    width: '31%',
    height: 35,
  },

  reject: {
    width: '31%',
    height: 35,
    marginLeft: Responsive.getWidth(3),
  },

  dismiss: {
    width: '31%',
    height: 35,
    marginLeft: Responsive.getWidth(3),
  },
});
