import React, { useCallback, useEffect, useState } from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Modal,
} from 'react-native';
import { useSelector } from 'react-redux';

import { RoundedButton, CachedImage } from '@components';
import { localize } from '@languages';
import { navigationRef } from '@utils/navigation';

import { styles } from './style';
import { rejectTransaction } from '@utils/walletConnect';
import { useWalletConnect } from '@contexts/WalletConnectContext';
import useDebouncedBoolean from '@utils/useDebouncedBoolean';
import { getNetwork } from '../../../redux';

const getMeta = (transaction, sessions) => {
  return sessions.find(s => s.topic === transaction.topic)?.peer.metadata;
};

const getChainsFromRequest = (request) => {
  try {
    let chains = [];
    if (request.cacaoPayload !== undefined) {
      chains = [request.cacaoPayload.chainId]
    } else if (request.requiredNamespaces !== undefined) {
      if (request.requiredNamespaces.eip155?.chains !== undefined) {
        chains = [...request.requiredNamespaces.eip155.chains];
      }
      if (request.optionalNamespaces?.eip155?.chains !== undefined) {
        chains = [
          ...chains,
          ...request.optionalNamespaces.eip155.chains,
        ];
      }
    } else if (request.params?.request?.params?.[0]?.chainId !== undefined) {
      chains = [request.params?.request?.params?.[0]?.chainId];
    } else if (request.params?.chainId !== undefined) {
      chains = [request.params.chainId];
    } else if (request.params[0].chainId !== undefined) {
      chains = [request.params[0].chainId];
    }
    const chainIds = chains
      .filter(chain => chain !== null)
      .map(chain => (
        (typeof chain === 'string' && chain.startsWith('0x'))
          ? parseInt(chain, 16)
          : chain
      ))
      .map(chain => (
        (typeof chain === 'string' && chain.startsWith('eip155:'))
          ? chain.split('eip155:')[1]
          : chain
      ))
      .map(chainId => parseInt(chainId, 10));
    return chainIds;
  } catch (e) {
    return [null];
  }
};

const WalletConnectTransactionRequestModal = (props) => {
  const navigation = navigationRef.current;
  const [route, setRoute] = useState();
  const viewingRequest = [
    'SIGN_TRANSACTION_CONFIRMAITON_SCREEN',
    'SEND_TRANSACTION_CONFIRMAITON_SCREEN',
  ].includes(route);
  const loggedIn = useSelector(state => state.wallet.password !== '');
  const selectedNetworkObj = useSelector(getNetwork);
  const selectedChainId = selectedNetworkObj.chainId;
  const { sessions, sessionRequests } = useWalletConnect();
  const transaction = sessionRequests[0];
  const requestedChainIds = getChainsFromRequest(transaction);
  const chainSelected = requestedChainIds.length === 0 || requestedChainIds.includes(selectedChainId);
  const isChainSwitchRequest = transaction?.params?.request?.method === 'wallet_switchEthereumChain';

  const canBeVisible = loggedIn && (transaction !== undefined) && !viewingRequest && chainSelected && !isChainSwitchRequest;

  // Because we rely on the navigationRef to determine whether or not to render, and
  // ref changes do not trigger a rerender, we will use this interval hook to check
  useEffect(() => {
    const interval = setInterval(() => {
      setRoute(navigationRef.current?.getCurrentRoute().name)
    }, 250);
    return () => clearInterval(interval);
  }, []);

  // Wait 750ms before displaying, to ensure that the request to be displayed hasn't
  // already been accepted and just hasn't been removed from the list quite yet.
  // This fixes the issue where we accept a request, and then the request modal pops up again
  // very briefly on the page until the request gets removed from Wallet Connect's state.
  const isVisible = useDebouncedBoolean(canBeVisible, 750);

  const viewTransactionScreen = (transaction) => {
    const isSendTransaction = transaction.params.request.method === 'eth_sendTransaction';
    if (isSendTransaction) {
      navigationRef.current.navigate('SEND_TRANSACTION_CONFIRMAITON_SCREEN', {
        transaction,
      });
    } else {
      navigationRef.current.navigate('SIGN_TRANSACTION_CONFIRMAITON_SCREEN', {
        transaction,
      });
    }
  };

  const onView = useCallback(() => {
    viewTransactionScreen(transaction);
  }, [transaction]);

  const onReject = useCallback(async() => {
    await rejectTransaction(transaction);
  }, [transaction]);

  if (!isVisible) {
    return null;
  }

  const meta = getMeta(transaction, sessions);
  const dappName = meta?.name;
  const dappUrl = meta?.url;
  const dappIcon = meta?.icons?.[0];

  const verificationMessage = {
    'UNKNOWN': 'Unknown domain.',
    'VALID': 'Successfully verified domain.',
    'INVALID': 'Domain verification failed!',
  }[transaction.verification];

  const verificationColor = {
    'UNKNOWN': 'gray',
    'VALID': 'green',
    'INVALID': 'red',
  }[transaction.verification];

  return (
    <Modal animationType="fade" transparent visible={isVisible}>
      <SafeAreaView style={[styles.safeArea]}>
        <View style={styles.container}>
          <Text style={styles.title}>
            {localize('WC_TRANSACTION_REQUEST_TITLE')}
          </Text>
          <View style={styles.dapp}>
            <CachedImage
              style={styles.icon}
              source={{ uri: dappIcon }}
            />
            <View style={styles.info}>
              <Text style={styles.name}>
                {dappName}
              </Text>
              <Text style={styles.url}>
                {dappUrl}
              </Text>
              <Text style={styles[verificationColor]}>
                {verificationMessage}
              </Text>
            </View>
          </View>
            <View style={styles.buttons}>
              <RoundedButton
                style={styles.view}
                title={localize('WC_TRANSACTION_REQUEST_VIEW')}
                onPress={onView}
              />
              <RoundedButton
                isBordered
                style={styles.reject}
                title={localize('WC_TRANSACTION_REQUEST_REJECT')}
                onPress={onReject}
              />
            </View>
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export default WalletConnectTransactionRequestModal;
