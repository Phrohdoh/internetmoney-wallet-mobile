import { StyleSheet } from 'react-native';
import { COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  safeareaStyle: { flex: 1, backgroundColor: COLORS.BLACK },
  backBtnStyle: {
    width: Responsive.getWidth(20),
    height: Responsive.getWidth(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerStyle: {
    width: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  permissionDenied: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
    paddingHorizontal: '10%',
    paddingVertical: '10%',
  },
});
