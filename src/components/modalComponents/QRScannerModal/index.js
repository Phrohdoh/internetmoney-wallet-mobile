import React, { Fragment, useCallback, useEffect, useState } from 'react';
import {
  View,
  TouchableOpacity,
  Modal,
  Text,
  Linking,
  SafeAreaView,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import { check, request, PERMISSIONS, RESULTS } from 'react-native-permissions';
import PropTypes from 'prop-types';

import { CameraScreen } from 'react-native-camera-kit';

// import style
import { styles } from './style';

import { IMAGES, COLORS, COMMON_STYLE } from '@themes';
import { RoundedButton, CachedImage } from '@components';
import { localize } from '@languages';
import Logger from '@utils/logger';

import * as Web3Layer from '@web3';

const checkCameraPermission = async () => {
  let permissionStatus = RESULTS.UNAVAILABLE;
  if (Platform.OS === 'android') {
    permissionStatus = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.CAMERA
    );
  } else if (Platform.OS === 'ios') {
    permissionStatus = await check(PERMISSIONS.IOS.CAMERA);
  }
  return permissionStatus === RESULTS.GRANTED;
};

const requestCameraPermission = async () => {
  let permissionStatus = RESULTS.UNAVAILABLE;
  if (Platform.OS === 'android') {
    permissionStatus = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA
    );
  } else if (Platform.OS === 'ios') {
    permissionStatus = await request(PERMISSIONS.IOS.CAMERA);
  }
  return permissionStatus === RESULTS.GRANTED;
};

const QRScannerModal = props => {
  let isRead = false;
  const { isVisible } = props;
  const [isValidating, setIsValidating] = useState(undefined);

  const [authorized, setAuthorized] = useState();

  const checkAuthorization = useCallback(async() => {
    let authorized = await checkCameraPermission();
    if (!authorized) {
      authorized = await requestCameraPermission();
    }
    setAuthorized(authorized);
  }, []);

  useEffect(() => {
    if (isVisible) {
      checkAuthorization().catch(() => {
        Logger.error('Error checking camera authorization');
      });
    }
  }, [isVisible, checkAuthorization]);

  // Validate to address Or ENS
  const validateAddress = async text => {
    setIsValidating(true);
    const isValidENS = await Web3Layer.checkValidENSOrToAddress(text);
    setIsValidating(false);
    if (isValidENS.success) {
      props.scanedValue(text, isValidENS.address);
    }
  };

  const openSettings = useCallback(async() => {
    await Linking.openSettings();
  }, []);

  const permissionDenied = authorized === false;
  const loading = isValidating || !authorized;

  const renderCameraScreen = () => {
    if (permissionDenied) {
      return (
        <Fragment>
          <Text style={ styles.permissionDenied }>
            {localize('CAMERA_PERMISSION_DENIED')}
          </Text>
          <RoundedButton
            testID="open_permission_settings_button"
            title={localize('OPEN_PERMISSION_SETTINGS')}
            onPress={openSettings}
            isBordered
          />
        </Fragment>
      );
    }
    if (loading) {
      return (
        <CachedImage
          style={ COMMON_STYLE.imageStyle(10) }
          source={ IMAGES.ROUNDED_LOADING }
        />
      );
    }
    return (
      <View style={ { flex: 1, width: '100%' } }>
        <CameraScreen
          scanBarcode={ true }
          onReadCode={ event => {
            if (!isRead) {
              isRead = true;
              if (!props.isValidateAddress) {
                props.scanedValue(event.nativeEvent.codeStringValue);
              } else {
                validateAddress(event.nativeEvent.codeStringValue);
              }
            }
          } }
          showFrame={ true }
          laserColor={ COLORS.YELLOWFB }
          frameColor={ COLORS.YELLOWFB }
        />
      </View>
    );
  }

  return (
    <Modal transparent visible={ props.isVisible }>
      <SafeAreaView style={ styles.safeareaStyle }>
        <TouchableOpacity
          testID={ 'scan-modal-close-btn' }
          style={ styles.backBtnStyle }
          onPress={ () => props.onPressClose() }>
          <CachedImage source={ IMAGES.BACK_ARROW } style={ COMMON_STYLE.imageStyle(5) } />
        </TouchableOpacity>

        <View style={ styles.containerStyle }>
          {renderCameraScreen()}
        </View>
      </SafeAreaView>
    </Modal>
  );
};

QRScannerModal.propTypes = {
  testID: PropTypes.string,
  isVisible: PropTypes.bool.isRequired,
  onPressClose: PropTypes.func,
  scanedValue: PropTypes.func,
  isValidateAddress: PropTypes.bool,
};
export default QRScannerModal;
