import React from 'react';
import {
  Text,
  View,
  Modal,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { GlobalAction } from '@redux';

// import components
import { RoundedButton, CachedImage } from '@components';

// import style
import { styles } from './style';

const AlertModel = () => {
  const dispatch = useDispatch();
  const global = useSelector(state => state.global);

  return (
    <Modal
      animationType="fade"
      transparent
      visible={global.alertData.isShowAlert}
    >
      <SafeAreaView style={styles.safeAreaStyle}>
        <View style={styles.alertBoxStyle}>
          {global.alertData.alertImage && (
            <CachedImage
              style={[styles.alertImageStyle]}
              source={global.alertData.alertImage}
            />
          )}
          {global.alertData.alertTitle && (
            <Text numberOfLines={2} style={styles.titleStyle}>
              {global.alertData.alertTitle}
            </Text>
          )}
          <ScrollView bounces={false}>
            <Text style={styles.descriptionStyle}>
              {global.alertData.alertMsg}
            </Text>
          </ScrollView>
          <RoundedButton
            testID="alert_success_btn"
            style={styles.successBtnStyle}
            title={global.alertData.successBtnTitle}
            onPress={() => {
              dispatch(GlobalAction.hideAlert());
            }}
          />
        </View>
      </SafeAreaView>
    </Modal>
  );
};

AlertModel.propTypes = {};
export default AlertModel;
