import React, { Fragment, useCallback, useState } from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Modal,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import {
  RoundedButton,
  ModalContainerView,
  ReedemAccountListCell,
  CachedImage,
} from '@components';
import { localize } from '@languages';

import { styles } from './style';
import { acceptConnection, approveAuthRequest, getAuthRequest, getWeb3Wallet, rejectAuthRequest, rejectConnection } from '@utils/walletConnect';
import {
  WalletConnectAction,
  getAccountList,
  getSelectedWalletConnectAccount,
} from '@redux';
import { COMMON_STYLE, IMAGES } from '@themes';
import { ASYNC_KEYS } from '@constants';
import { StorageOperation } from '@storage/asyncStorage';

import { useWalletConnect } from '@contexts/WalletConnectContext';
import useDebouncedBoolean from '@utils/useDebouncedBoolean';
import * as Web3Layer from '@web3';
import { getNetwork } from '../../../redux';

const getChainsFromRequest = (request) => {
  try {
    let chains = [];
    if (request.cacaoPayload !== undefined) {
      chains = [request.cacaoPayload.chainId]
    } else if (request.requiredNamespaces !== undefined) {
      if (request.requiredNamespaces.eip155?.chains !== undefined) {
        chains = [...request.requiredNamespaces.eip155.chains];
      }
      if (request.optionalNamespaces?.eip155?.chains !== undefined) {
        chains = [
          ...chains,
          ...request.optionalNamespaces.eip155.chains,
        ];
      }
    } else if (request.params?.request?.params?.[0]?.chainId !== undefined) {
      chains = [request.params?.request?.params?.[0]?.chainId];
    } else if (request.params?.chainId !== undefined) {
      chains = [request.params.chainId];
    } else if (request.params[0].chainId !== undefined) {
      chains = [request.params[0].chainId];
    }
    const chainIds = chains
      .filter(chain => chain !== null)
      .map(chain => (
        (typeof chain === 'string' && chain.startsWith('0x'))
          ? parseInt(chain, 16)
          : chain
      ))
      .map(chain => (
        (typeof chain === 'string' && chain.startsWith('eip155:'))
          ? chain.split('eip155:')[1]
          : chain
      ))
      .map(chainId => parseInt(chainId, 10));
    return chainIds;
  } catch (e) {
    return [null];
  }
};

const STATUSES = {
  DEFAULT: 'DEFAULT',
  CONNECTING: 'CONNECTING',
  REJECTING: 'REJECTING',
  ERROR: 'ERROR',
};

const WalletConnectSessionRequestModal = () => {
  const dispatch = useDispatch();
  const loggedIn = useSelector(state => state.wallet.password !== '');
  const wallet = useSelector((state) => state.wallet);
  const selectedNetworkObj = useSelector(getNetwork);
  const selectedChainId = selectedNetworkObj.chainId;
  const { sessionProposals, authRequests } = useWalletConnect();
  const request = sessionProposals[0] ?? authRequests[0];
  const isAuthRequest = request?.cacaoPayload !== undefined;
  const requestedChainIds = getChainsFromRequest(request);
  const chainSelected = requestedChainIds.length === 0 || requestedChainIds.includes(selectedChainId);
  const canBeVisible = loggedIn && (request !== undefined) && chainSelected;
  const [status, setStatus] = useState(STATUSES.DEFAULT);
  const submitting = [STATUSES.CONNECTING, STATUSES.REJECTING].includes(status);
  const errorMessage = status === STATUSES.ERROR
    ? localize('WC_SESSION_REQUEST_ERROR')
    : undefined;

  // Wait 750ms before displaying, to ensure that the request to be displayed hasn't
  // already been accepted and just hasn't been removed from the list quite yet.
  // This fixes the issue where we accept a request, and then the request modal pops up again
  // very briefly on the page until the request gets removed from Wallet Connect's state.
  const isVisible = useDebouncedBoolean(canBeVisible, 750);

  const id = request?.id;
  const accountList = useSelector(getAccountList);
  const selectedAccount = useSelector(getSelectedWalletConnectAccount);
  const [showAccountList, setShowAccountList] = useState(false);

  const iss = `did:pkh:eip155:${selectedChainId}:${selectedAccount?.publicAddress}`;

  const onSelectAccount = useCallback(async (account) => {
    setShowAccountList(false);
    await StorageOperation.setData(
      ASYNC_KEYS.WALLET_CONNECTED_ACCOUNT,
      JSON.stringify(account.publicAddress),
    );
    dispatch(WalletConnectAction.updateSelectedAccount(account.publicAddress));
    setStatus(STATUSES.DEFAULT);
  }, []);

  const onAcceptAuthRequest = useCallback(async () => {
    const web3Wallet = await getWeb3Wallet();
    const authRequest = await getAuthRequest(id);
    const message = web3Wallet.formatMessage(authRequest.cacaoPayload, iss);
    const payload = {
      address: selectedAccount.publicAddress,
      message,
    };
    const resultObj = await Web3Layer.ethSignAuthRequest(
      payload,
      wallet.walletDetail.walletObject,
      wallet.password,
      selectedNetworkObj,
    );

    const signature = resultObj.signResult;

    approveAuthRequest(id, iss, signature);
  }, [id, iss, selectedAccount, wallet, selectedNetworkObj]);

  const onAccept = useCallback(async() => {
    setStatus(STATUSES.CONNECTING);
    try {
      if (isAuthRequest) {
        await onAcceptAuthRequest();
      } else {
        await acceptConnection(id);
      }
      setStatus(STATUSES.DEFAULT);
    } catch (e) {
      setStatus(STATUSES.ERROR);
    }
  }, [id]);

  const onReject = useCallback(async() => {
    setStatus(STATUSES.REJECTING);
    try {
      if (isAuthRequest) {
        await rejectAuthRequest(id, iss);
      } else {
        await rejectConnection(id);
      }
      setStatus(STATUSES.DEFAULT);
    } catch (e) {
      setStatus(STATUSES.ERROR);
    }
  }, [id, iss]);

  if (!isVisible) {
    return null;
  }

  const meta = isAuthRequest
    ? request.requester.metadata
    : request.proposer.metadata;
  const dappName = meta.name;
  const dappUrl = meta.url;
  const dappIcon = meta.icons?.[0];

  const verificationMessage = {
    'UNKNOWN': 'Unknown domain.',
    'VALID': 'Successfully verified domain.',
    'INVALID': 'Domain verification failed!',
  }[request.verification];

  const verificationColor = {
    'UNKNOWN': 'gray',
    'VALID': 'green',
    'INVALID': 'red',
  }[request.verification];

  const renderAccountList = () => (
    <SafeAreaView style={[styles.safeArea]}>
      <View style={styles.container}>
        <Text style={ COMMON_STYLE.modalTitleStyle }>
          {localize('WC_SESSION_REQUEST_SELECT_ACCOUNT')}
        </Text>
        <ScrollView
          horizontal
          contentContainerStyle={ { width: '100%' } }
          scrollEnabled={ false }>
          <FlatList
            key={ 'account_list' }
            data={ accountList }
            bounces={ false }
            renderItem={ ({ item, index }) => (
              <ReedemAccountListCell
                item={ item }
                index={ index }
                onSelectAccount={ onSelectAccount }
              />
            ) }
            ItemSeparatorComponent={ () => (
              <View style={ styles.accountListSeparatorStyle } />
            ) }
            keyExtractor={ (item, index) => index + item }
          />
        </ScrollView>
      </View>
    </SafeAreaView>
  );

  const renderConnectionRequest = () => (
    <SafeAreaView style={[styles.safeArea]}>
      <View style={styles.container}>
        <Text style={styles.title}>
          {localize('WC_SESSION_REQUEST_TITLE')}
        </Text>
        <View style={styles.dapp}>
          <CachedImage
            style={styles.icon}
            source={{ uri: dappIcon }}
          />
          <View style={styles.info}>
            <Text style={styles.name}>
              {dappName}
            </Text>
            <Text style={styles.url}>
              {dappUrl}
            </Text>
            <Text style={styles[verificationColor]}>
              {verificationMessage}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          testID={ 'wallet-connect-modal-choose-account-button' }
          style={ styles.accountListCellStyle }
          onPress={ () => setShowAccountList(true) }
        >
          <View style={ styles.accountDetailStyle }>
            <Text numberOfLines={ 1 } style={ styles.accountNameStyle }>
              {selectedAccount?.name}
            </Text>
            <Text numberOfLines={ 1 } style={ styles.accountAddressStyle }>
              {selectedAccount?.publicAddress}
            </Text>
          </View>
          <CachedImage source={ IMAGES.DOWN_ARROW } style={ COMMON_STYLE.imageStyle(5) } />
        </TouchableOpacity>
        {errorMessage !== undefined && (
          <Text style={styles.errorMessage}>
            {errorMessage}
          </Text>
        )}
        <View style={styles.buttons}>
          {!submitting && (
            <RoundedButton
              style={styles.accept}
              title={localize('WC_SESSION_REQUEST_ACCEPT')}
              onPress={selectedAccount && onAccept}
            />
          )}
          {!submitting && (
            <RoundedButton
              isBordered
              style={styles.reject}
              title={localize('WC_SESSION_REQUEST_REJECT')}
              onPress={onReject}
            />
          )}
          {submitting && <CachedImage source={ IMAGES.ROUNDED_LOADING } style={ styles.loadingStyle } />}
        </View>
      </View>
    </SafeAreaView>
  );

  return (
    <Fragment>
      <Modal animationType="fade" transparent visible={isVisible}>
        {
          showAccountList
            ? renderAccountList()
            : renderConnectionRequest()
        }
      </Modal>
    </Fragment>
  );
};

export default WalletConnectSessionRequestModal;
