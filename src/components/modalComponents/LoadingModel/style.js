import { StyleSheet } from 'react-native';
import { COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  safeAreaStyle: {
    flex: 1,
    backgroundColor: COLORS.DARK_OPACITY(),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    height: '100%',
  },

  loadingBoxStyle: {
    width: Responsive.getWidth(80),
    height: Responsive.getHeight(20),
    alignItems: 'center',
    justifyContent: 'center',
  },
});
