import React from 'react';
import { SafeAreaView } from 'react-native';
import { useSelector } from 'react-redux';
import { CachedImage } from '@components';

// import style
import { IMAGES, COMMON_STYLE } from '@themes';
import { styles } from './style';

// import constants

const LoadingModel = () => {
  const isLoading = useSelector(state => state.global.isLoading);

  return isLoading
    ? (
      <SafeAreaView style={[styles.safeAreaStyle]}>
        <CachedImage
          style={COMMON_STYLE.imageStyle(15)}
          source={IMAGES.ROUNDED_LOADING}
        />
      </SafeAreaView>
    )
    : null;
};

export default LoadingModel;
