import React, { useCallback, useState } from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Modal,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { WalletAction, NetworksAction } from '@redux';
import { ASYNC_KEYS } from '@constants';
import { RoundedButton, CachedImage } from '@components';
import { localize } from '@languages';
import { StorageOperation } from '@storage';

import { styles } from './style';
import { useWalletConnect } from '@contexts/WalletConnectContext';
import useDebouncedBoolean from '@utils/useDebouncedBoolean';
import {
  rejectConnection,
  rejectTransaction,
} from '@utils/walletConnect';
import { getNetwork } from '../../../redux';

const getMeta = (request, sessions) => {
  if (request?.cacaoPayload) {
    return request.requester.metadata;
  }
  const isSwitchEthereumChain = request.method === 'wallet_switchEthereumChain';
  if (isSwitchEthereumChain) {
    return request.connection.peerMeta;
  }
  const isTransaction = typeof request?.params?.request?.method === 'string';
  if (isTransaction) {
    return sessions.find(s => s.topic === request.topic)?.peer.metadata;
  }
  return request.proposer.metadata;
};

const getChainsFromRequest = (request) => {
  try {
    let chains = [];
    if (request.cacaoPayload !== undefined) {
      chains = [request.cacaoPayload.chainId]
    } else if (request.requiredNamespaces !== undefined) {
      if (request.requiredNamespaces.eip155?.chains !== undefined) {
        chains = [...request.requiredNamespaces.eip155.chains];
      }
      if (request.optionalNamespaces?.eip155?.chains !== undefined) {
        chains = [
          ...chains,
          ...request.optionalNamespaces.eip155.chains,
        ];
      }
    } else if (request.params?.request?.params?.[0]?.chainId !== undefined) {
      chains = [request.params?.request?.params?.[0]?.chainId];
    } else if (request.params?.chainId !== undefined) {
      chains = [request.params.chainId];
    } else if (request.params[0].chainId !== undefined) {
      chains = [request.params[0].chainId];
    }
    const chainIds = chains
      .filter(chain => chain !== null)
      .map(chain => (
        (typeof chain === 'string' && chain.startsWith('0x'))
          ? parseInt(chain, 16)
          : chain
      ))
      .map(chain => (
        (typeof chain === 'string' && chain.startsWith('eip155:'))
          ? chain.split('eip155:')[1]
          : chain
      ))
      .map(chainId => parseInt(chainId, 10));
    return chainIds;
  } catch (e) {
    return [null];
  }
};

const WalletConnectChainRequestModal = () => {
  const dispatch = useDispatch();
  const loggedIn = useSelector(state => state.wallet.password !== '');
  const networks = useSelector(state => state.networks.networks);
  const selectedNetworkObj = useSelector(getNetwork);
  const selectedChainId = selectedNetworkObj.chainId;
  const { sessions, sessionProposals, sessionRequests, authRequests } = useWalletConnect();
  const request = sessionProposals[0] ?? authRequests[0] ?? sessionRequests[0];
  const requestedChainIds = getChainsFromRequest(request);
  const chainSelected = requestedChainIds.length === 0 || requestedChainIds.includes(selectedChainId);
  const supportedChainIds = networks
    .filter(network => !network.disabled)
    .map(network => network.chainId);

  const canBeVisible = loggedIn && (request !== undefined) && !chainSelected;

  // Wait 750ms before displaying, to ensure that the request to be displayed hasn't
  // already been accepted and just hasn't been removed from the list quite yet.
  // This fixes the issue where we accept a request, and then the request modal pops up again
  // very briefly on the page until the request gets removed from Wallet Connect's state.
  const isVisible = useDebouncedBoolean(canBeVisible, 750);

  const id = request?.id;

  const onAccept = useCallback(async() => {
    const requestedNetwork = networks.find(network => network.chainId === requestedChainIds[0]);
    dispatch(NetworksAction.saveSelectedNetworkObject(requestedNetwork));
    dispatch(WalletAction.resetData());
    await StorageOperation.setData(
      ASYNC_KEYS.SELECTED_NETWORK,
      JSON.stringify(requestedNetwork),
    );
  }, [id]);

  const onReject = useCallback(async() => {
    if (typeof request?.params?.request?.method === 'string') {
      await rejectTransaction(sessionRequests[0]);
    } else {
      rejectConnection(id);
    }
  }, [id]);

  if (!isVisible) {
    return null;
  }

  let errorMessage = undefined;

  if (requestedChainIds === null) {
    errorMessage = localize('WC_CHAIN_REQUEST_UNABLE_TO_PARSE');
  } else if (!supportedChainIds.includes(requestedChainIds[0])) {
    errorMessage = localize('WC_CHAIN_REQUEST_UNSUPPORTED_CHAIN');
  }

  const meta = getMeta(request, sessions);
  const dappName = meta?.name;
  const dappUrl = meta?.url;
  const dappIcon = meta?.icons?.[0];

  const networkName = errorMessage === undefined
    ? networks.find(network => network.chainId === requestedChainIds[0]).networkName
    : undefined;
  const networkIcon = errorMessage === undefined
    ? networks.find(network => network.chainId === requestedChainIds[0]).icon
    : undefined;

  const verificationMessage = {
    'UNKNOWN': 'Unknown domain.',
    'VALID': 'Successfully verified domain.',
    'INVALID': 'Domain verification failed!',
  }[request.verification];

  const verificationColor = {
    'UNKNOWN': 'gray',
    'VALID': 'green',
    'INVALID': 'red',
  }[request.verification];

  const buttons = errorMessage === undefined
    ? (
      <View style={styles.buttons}>
        <RoundedButton
          style={styles.accept}
          title={localize('WC_CHAIN_REQUEST_ACCEPT')}
          onPress={onAccept}
        />
        <RoundedButton
          isBordered
          style={styles.reject}
          title={localize('WC_CHAIN_REQUEST_REJECT')}
          onPress={onReject}
        />
      </View>
    )
    : (
      <View style={styles.buttons}>
        <RoundedButton
          style={styles.dismiss}
          title={localize('WC_CHAIN_REQUEST_DISMISS')}
          onPress={onReject}
        />
      </View>
    );
  
  const chainRequest = errorMessage === undefined
    ? (
      <View style={styles.chain}>
        <CachedImage
          style={styles.icon}
          source={{ uri: networkIcon }}
        />
        <View style={styles.info}>
          <Text style={styles.name}>
            {networkName}
          </Text>
          <Text style={styles.chainId}>
            ({localize('WC_CHAIN_REQUEST_CHAIN_ID')}: {requestedChainIds[0]})
          </Text>
        </View>
      </View>
    )
    : (
      <Text style={styles.errorMessage}>
        {errorMessage}
      </Text>
    );

  return (
    <Modal animationType="fade" transparent visible={isVisible}>
      <SafeAreaView style={[styles.safeArea]}>
        <View style={styles.container}>
          <Text style={styles.title}>
            {localize('WC_CHAIN_REQUEST_TITLE')}
          </Text>
          <View style={styles.dapp}>
            <CachedImage
              style={styles.icon}
              source={{ uri: dappIcon }}
            />
            <View style={styles.info}>
              <Text style={styles.name}>
                {dappName}
              </Text>
              <Text style={styles.url}>
                {dappUrl}
              </Text>
              <Text style={styles[verificationColor]}>
                {verificationMessage}
              </Text> 
            </View>
          </View>
          {chainRequest}
          {buttons}
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export default WalletConnectChainRequestModal;
