import React from 'react';
import {
  View,
  TouchableOpacity,
  Modal,
  SafeAreaView,
  FlatList,
  Text,
} from 'react-native';

import PropTypes from 'prop-types';

import { useSelector } from 'react-redux';

// import components
import { ChangeNetworkListCell } from '@components';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import themes
import { COMMON_STYLE, COLORS } from '@themes';
import { getNetwork } from '../../../redux';

// default props value
const defaultProps = {
  onPressClose: () => {},
};

const NetworkSelectionModalView = props => {
  const { testID, onSelectNetwork, isShowSwapSupported } = props;

  const networks = useSelector(state => state.networks);
  const selectedNetwork = useSelector(getNetwork);

  return (
    <Modal animationType="fade" transparent={ true } visible={ props.isVisible }>
      <View style={ styles.scrollContainerStyle }>
        <TouchableOpacity
          testID={ testID + '-bgpress-button' }
          style={ styles.overlayAreaStyle }
          onPress={ () => props.onPressClose() }
        />
        <View style={ styles.modelViewStyle }>
          <SafeAreaView style={ styles.safeAreaStyle }>
            <View style={ styles.containerStyle }>
              <Text
                style={ COMMON_STYLE.textStyle(
                  20,
                  COLORS.YELLOWFB,
                  'BOLD',
                  'center',
                ) }>
                {localize('CHANGE_NETWORK')}
              </Text>
              <FlatList
                style={ styles.flatlistStyle }
                data={ networks.networks }
                renderItem={ ({ item, index }) => (
                  <ChangeNetworkListCell
                    item={ item }
                    index={ index }
                    selectedNetwork={ selectedNetwork }
                    swapSupportedList={ networks.swapSupportedNetworks }
                    isShowSwapSupported={ isShowSwapSupported }
                    onSelectNetwork={ item => onSelectNetwork(item) }
                  />
                ) }
                ItemSeparatorComponent={ () => (
                  <View style={ styles.sepratorStyle } />
                ) }
                keyExtractor={ (item, index) => index + item }
              />
            </View>
          </SafeAreaView>
        </View>
      </View>
    </Modal>
  );
};

NetworkSelectionModalView.defaultProps = defaultProps;

NetworkSelectionModalView.propTypes = {
  testID: PropTypes.string,
  isVisible: PropTypes.bool,
  isShowSwapSupported: PropTypes.bool,
  onSelectNetwork: PropTypes.func,
  onPressClose: PropTypes.func,
};
export default NetworkSelectionModalView;
