import {StyleSheet} from 'react-native';

import {COMMON_STYLE, COLORS} from '@themes';
import {Responsive} from '@helpers';

export const styles = StyleSheet.create({
  safeAreaStyle: {
    flex: 1,
    backgroundColor: COLORS.DARK_OPACITY(),
    justifyContent: 'center',
    alignItems: 'center',
  },

  alertBoxStyle: {
    width: Responsive.getWidth(90),
    maxHeight: Responsive.getHeight(55),
    borderRadius: Responsive.getWidth(3),
    backgroundColor: COLORS.GRAY1A,
    alignItems: 'center',
    padding: Responsive.getWidth(5),
  },

  alertImageStyle: {
    height: Responsive.getWidth(8),
    width: Responsive.getWidth(8),
    borderRadius: Responsive.getWidth(2),
    resizeMode: 'contain',
    marginTop: Responsive.getHeight(1),
    overflow: 'hidden',
  },

  titleStyle: {
    ...COMMON_STYLE.textStyle(16, COLORS.YELLOWFB, 'BOLD', 'center'),
    margin: Responsive.getHeight(1),
    marginBottom: 0,
  },

  descriptionStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.WHITE, undefined, 'center'),
  },
  descScrollStyle: {
    margin: Responsive.getHeight(1),
  },
  actionBtnBigStyle: {
    marginTop: Responsive.getHeight(3),
    width: '90%',
  },
  actionBtnStyle: {
    marginTop: Responsive.getHeight(3),
    width: '30%',
  },
  actionBtnContainerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '100%',
  },
});
