import React from 'react';
import {
  Text,
  View,
  Modal,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import PropTypes from 'prop-types';

// import components
import { RoundedButton, CachedImage } from '@components';

// import Languages
import { localize } from '@languages';

// import style
import { IMAGES } from '@themes';
import { styles } from './style';

const AlertActionModel = props => (
  <Modal animationType="fade" transparent visible={props.isShowAlert}>
    <SafeAreaView style={styles.safeAreaStyle}>
      <View style={styles.alertBoxStyle}>
        {props.alertImage && (
          <CachedImage style={[styles.alertImageStyle]} source={props.alertImage} />
        )}
        {props.alertTitle && (
          <Text numberOfLines={3} style={styles.titleStyle}>
            {props.alertTitle}
          </Text>
        )}
        <ScrollView bounces={false} style={styles.descScrollStyle}>
          {!props.children
            ? (
              <Text style={styles.descriptionStyle}>{props.alertMsg}</Text>
            )
            : (
              props.children
            )}
        </ScrollView>
        <View style={styles.actionBtnContainerStyle}>
          <RoundedButton
            testID="alert_success_btn"
            style={
              props.bigSuccess
                ? styles.actionBtnBigStyle
                : styles.actionBtnStyle
            }
            title={props.successBtnTitle}
            onPress={() => props.onPressSuccess()}
          />
          {props.onPressCancel && (
            <RoundedButton
              isBordered
              testID="alert_cancel_btn"
              style={styles.actionBtnStyle}
              title={props.cancelBtnTitle}
              onPress={() => props.onPressCancel()}
            />
          )}
        </View>
      </View>
    </SafeAreaView>
  </Modal>
);

AlertActionModel.defaultProps = {
  isShowAlert: false,
  alertImage: IMAGES.app_logo,
  alertTitle: undefined,
  alertMsg: undefined,
  successBtnTitle: localize('YES'),
  cancelBtnTitle: localize('NO'),
};

AlertActionModel.propTypes = {
  isShowAlert: PropTypes.bool.isRequired,
  alertImage: PropTypes.any,
  alertTitle: PropTypes.string,
  alertMsg: PropTypes.string,
  children: PropTypes.any,
  successBtnTitle: PropTypes.string.isRequired,
  cancelBtnTitle: PropTypes.string,
  onPressSuccess: PropTypes.func.isRequired,
  onPressCancel: PropTypes.func,
  bigSuccess: PropTypes.bool,
};
export default AlertActionModel;
