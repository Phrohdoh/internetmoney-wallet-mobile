import React from 'react';
import { View, TouchableOpacity, Modal, SafeAreaView } from 'react-native';
import PropTypes from 'prop-types';

// import components
import { KeyboardAvoidScrollView } from '@components';

// import style
import { styles } from './style';

// default props value
const defaultProps = {
  onPressClose: () => {},
};

const ModalContainerView = (props) => {
  const { testID } = props;
  return (
    <Modal animationType="fade" transparent visible={props.isVisible}>
      <KeyboardAvoidScrollView
        contentContainerStyle={styles.scrollContainerStyle}
      >
        <TouchableOpacity
          testID={`${testID}-bgpress-button`}
          style={styles.overlayAreaStyle}
          onPress={() => props.onPressClose()}
        />
        <View style={styles.modelViewStyle}>
          <SafeAreaView style={styles.safeAreaStyle}>
            <View style={styles.containerStyle}>{props.children}</View>
          </SafeAreaView>
        </View>
      </KeyboardAvoidScrollView>
    </Modal>
  );
};

ModalContainerView.defaultProps = defaultProps;

ModalContainerView.propTypes = {
  testID: PropTypes.string,
  onPressClose: PropTypes.func,
  children: PropTypes.any,
  isVisible: PropTypes.bool,
};
export default ModalContainerView;
