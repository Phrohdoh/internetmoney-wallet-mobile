import { StyleSheet } from 'react-native';

import { Responsive } from '@helpers';
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  modelViewStyle: {
    backgroundColor: COLORS.BLACK,
    borderTopLeftRadius: Responsive.getWidth(10),
    borderTopRightRadius: Responsive.getWidth(10),
    borderTopWidth: 3,
    borderLeftWidth: 3,
    borderRightWidth: 3,
    borderColor: COLORS.GRAY1A,
  },
  scrollContainerStyle: {
    flexGrow: 1,
    justifyContent: 'flex-end',
  },
  safeAreaStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: Responsive.getWidth(5),
    maxHeight: Responsive.getHeight(85),
  },

  overlayAreaStyle: {
    position: 'absolute',
    height: '100%',
    width: '100%',
  },
  containerStyle: {
    width: '100%',
    ...COMMON_STYLE.paddingStyle(4, 4, 4, 2),
  },
});
