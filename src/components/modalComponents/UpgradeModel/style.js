import { StyleSheet } from 'react-native';

import { COLORS, COMMON_STYLE } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  safeAreaStyle: {
    flex: 1,
    backgroundColor: COLORS.DARK_OPACITY(),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    height: '100%',
  },

  containerStyle: {
    width: Responsive.getWidth(90),
    borderRadius: Responsive.getWidth(3),
    backgroundColor: COLORS.GRAY1A,
    alignItems: 'center',
    padding: Responsive.getWidth(5),
  },

  upgradeTitleStyle: {
    ...COMMON_STYLE.textStyle(16, COLORS.WHITE, 'BOLD', 'center'),
    marginBottom: Responsive.getHeight(2),
  },
  upgradeMessageStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BASE', 'center'),
    marginBottom: Responsive.getHeight(2),
  },
  upgradeBtnViewStyle: {
    flexDirection: 'row',
  },
  btnStyle: { flex: 1, marginHorizontal: Responsive.getWidth(2) },
});
