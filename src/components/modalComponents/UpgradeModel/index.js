import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Linking,
  Modal,
  Platform,
} from 'react-native';
import VersionCheck from 'react-native-version-check';

// import components
import { RoundedButton } from '@components';

// import Languages
import { localize } from '@languages';

// import style
import { styles } from './style';

import { log } from '@utils';

const UpgradeModel = () => {
  const [isShowUpgrade, setIsShowUpgrade] = useState(false);
  const [isForceUpgrade, setIsForceUpgrade] = useState(false);

  useEffect(() => {
    checkVersionUpdate();
  }, []);

  const checkVersionUpdate = async () => {
    try {
      const currentVersion = VersionCheck.getCurrentVersion();
      const latestVersion = await VersionCheck.getLatestVersion();
      const { isNeeded } = await VersionCheck.needUpdate({ currentVersion, latestVersion });
      if (isNeeded) {
        const currentMajorVersion = parseInt(currentVersion.split('.')[0], 10);
        const latestMajorVersion = parseInt(latestVersion.split('.')[0], 10);
        setIsShowUpgrade(true);
        if (latestMajorVersion > currentMajorVersion) {
          setIsForceUpgrade(true);
        }
      }
    } catch (e) {
      log('version check error >>>', e);
    }
  };

  const onPressUpgrade = async () => {
    let storeURL = await VersionCheck.getStoreUrl({
      appID: '1641771042',
      packageName: 'com.internetmoneywallet.app',
    });
    Linking.openURL(storeURL);
  };

  return (
    <Modal animationType="fade" transparent={ true } visible={ isShowUpgrade }>
      <SafeAreaView style={ [styles.safeAreaStyle] }>
        <View style={ styles.containerStyle }>
          <Text style={ styles.upgradeTitleStyle }>
            {localize(
              isForceUpgrade ? 'MAJOR_UPGRADE_TITLE' : 'MINOR_UPGRADE_TITLE',
            )}
          </Text>
          <Text style={ styles.upgradeMessageStyle }>
            {localize('UPDATE_MSG')}
          </Text>
          <View style={ styles.upgradeBtnViewStyle }>
            <RoundedButton
              style={ styles.btnStyle }
              title={ localize('UPDATE_UPPERCASE') }
              onPress={ () => {
                onPressUpgrade();
              } }
            />
            {!isForceUpgrade && (
              <RoundedButton
                style={ styles.btnStyle }
                title={ localize('SKIP_NOW_UPPERCASE') }
                onPress={ () => setIsShowUpgrade(false) }
              />
            )}
          </View>
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export default UpgradeModel;
