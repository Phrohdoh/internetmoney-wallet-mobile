import React from 'react';
import { Pressable } from 'react-native';
import { CachedImage } from '@components';
import PropTypes from 'prop-types';

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

const CheckBox = (props) => {
  const { isSelected, onPress } = props;
  return (
    <Pressable style={props.checkboxStyle} onPress={onPress} testID="checkbox">
      <CachedImage
        source={isSelected ? IMAGES.check : IMAGES.uncheck}
        style={COMMON_STYLE.imageStyle(7, COLORS.YELLOWFB)}
      />
    </Pressable>
  );
};

CheckBox.propTypes = {
  isSelected: PropTypes.bool.isRequired,
  onPress: PropTypes.func,
  checkboxStyle: PropTypes.any,
};

export default CheckBox;
