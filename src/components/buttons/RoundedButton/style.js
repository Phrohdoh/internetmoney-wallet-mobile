import {StyleSheet} from 'react-native';

// import helpers
import {Responsive} from '@helpers';

// import themes
import {COLORS, COMMON_STYLE} from '@themes';

export const styles = StyleSheet.create({
  roundedBtnStyle: (isBordered, pressed) => {
    return {
      height: 50,
      width: Responsive.getWidth(80),
      backgroundColor: pressed ? (isBordered ? COLORS.YELLOWFB : COLORS.GRAY43) : (isBordered ? COLORS.TRANSPARENT : COLORS.YELLOWFB),
      borderRadius: 25,
      borderWidth: pressed ? (isBordered ? 2 : 1) : (isBordered ? 1 : 0),
      borderColor: pressed ? COLORS.YELLOWFB : COLORS.GRAY43,
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      flexDirection: 'row',
    };
  },

  titleStyle: (isBordered, pressed) => {
    return {
      ...COMMON_STYLE.textStyle(
        12,
        pressed ? (isBordered ? COLORS.GRAY43 : COLORS.YELLOWFB) : (isBordered ? COLORS.YELLOWFB : COLORS.BLACK),
        'BOLD',
        'center',
      ),
    };
  },
  leftImageStyle: {
    ...COMMON_STYLE.imageStyle(6),
    marginRight: Responsive.getWidth(2),
  },
});
