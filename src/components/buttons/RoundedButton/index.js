import React, { useState } from 'react';
import { Text, Pressable, ActivityIndicator, Alert } from 'react-native';
import { CachedImage } from '@components';
import PropTypes from 'prop-types';

// import style
import { COLORS, COMMON_STYLE } from '@themes';
import { styles } from './style';

// import themes

const RoundedButton = (props) => {
  const { isBordered, style, titleStyle, onPress, onLongPress, testID } = props;
  const [isPressed, setIsPressed] = useState(false);
  return (
    <Pressable
      style={({ pressed }) => [
        styles.roundedBtnStyle(isBordered, pressed),
        style,
      ]}
      onPressIn={() => setIsPressed(true)}
      onPressOut={() => setIsPressed(false)}
      onPress={onPress}
      onLongPress={onLongPress}
      testID={testID}
    >
      {props.leftImage && !props.isLoading && (
        <CachedImage
          source={props.leftImage}
          style={[styles.leftImageStyle, props.leftImageStyle]}
        />
      )}
      {!props.isLoading
        ? (
          <Text style={[styles.titleStyle(isBordered, isPressed), titleStyle]}>
            {props.title}
          </Text>
        )
        : (
          <ActivityIndicator
            color={isBordered ? COLORS.YELLOWFB : COLORS.BLACK}
          />
        )}
      {props.image && !props.isLoading && (
        <CachedImage
          source={props.image}
          style={[COMMON_STYLE.imageStyle(3), props.imageStyle]}
        />
      )}
    </Pressable>
  );
};

RoundedButton.propTypes = {
  testID: PropTypes.string,
  leftImage: PropTypes.any,
  isLoading: PropTypes.bool,
  image: PropTypes.any,
  isBordered: PropTypes.bool,
  onPress: PropTypes.func,
  onLongPress: PropTypes.func,
  style: PropTypes.any,
  titleStyle: PropTypes.any,
  leftImageStyle: PropTypes.any,
  title: PropTypes.string,
  imageStyle: PropTypes.any,
};

export default RoundedButton;
