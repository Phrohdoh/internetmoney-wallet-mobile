import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { getTokensByAddress } from '@redux';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

const SendTokenAccountListCell = (props) => {
  const { item, index } = props;

  const tokens = useSelector(getTokensByAddress)[item.publicAddress.toLowerCase()];
  const nativeToken = tokens[0];

  return (
    <TouchableOpacity
      testID={`choose-account-${index}`}
      style={styles.cellStyle}
      key={`${index + item}view1`}
      onPress={() => props.onSelectAccount(item, index)}
    >
      <View style={{ flex: 0.4 }} key={`${index + item}view2`}>
        <Text style={styles.nameStyle} numberOfLines={1}>
          {item.name}
        </Text>
      </View>
      <View style={{ flex: 0.6 }} key={`${index + item}view3`}>
        <Text style={styles.valueStyle}>{localize('BALANCE')}</Text>
        <Text style={styles.balanceStyle} numberOfLines={1}>
          {nativeToken?.balance?.value?.toString(6, true)}
          {' '}
          {nativeToken.symbol}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

SendTokenAccountListCell.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  onSelectAccount: PropTypes.func,
};

export default SendTokenAccountListCell;
