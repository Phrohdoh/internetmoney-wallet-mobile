import {StyleSheet} from 'react-native';

// import helpers
import {Responsive} from '@helpers';

// import themes
import {COLORS, COMMON_STYLE} from '@themes';

export const styles = StyleSheet.create({
  cellStyle: {
    height: Responsive.getHeight(7),
    width: '100%',
    borderRadius: Responsive.getHeight(3.5),
    borderColor: COLORS.GRAY18,
    borderWidth: 2,
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    paddingHorizontal: Responsive.getWidth(5),
    backgroundColor: COLORS.GRAY11,
  },
  rightArrowStyle: {
    ...COMMON_STYLE.imageStyle(4),
    marginLeft: Responsive.getWidth(3),
  },
  nameStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BOLD'),
  },
  statusStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.YELLOWFB, 'BOLD'),
  },
  valueStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.GRAYBF, 'BOLD', 'right'),
  },
  balanceStyle: {
    ...COMMON_STYLE.textStyle(15, COLORS.YELLOWFB, 'BOLD', 'right'),
  },
});
