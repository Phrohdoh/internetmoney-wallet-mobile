import React, { useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { CachedImage } from '@components';
import PropTypes from 'prop-types';

// import languages
import { localize } from '@languages';

// import style
import { COMMON_STYLE, IMAGES } from '@themes';
import { styles } from './style';

// import themes

const TabHeaderView = (props) => {
  const { navigation, testID, onPressRefresh } = props;
  const [refreshBtnPressed, setRefreshBtnPressed] = useState(false);

  const renderHeaderBtns = (image, onPress) => (
    <TouchableOpacity
      testID={`${testID + image}-tabheader-button`}
      style={styles.headerBtnStyle}
      onPress={onPress}
    >
      <CachedImage source={image} style={COMMON_STYLE.imageStyle(6)} />
    </TouchableOpacity>
  );

  const handleRefreshPress = () => {
    setRefreshBtnPressed(true);
    onPressRefresh();
    setTimeout(() => {
      setRefreshBtnPressed(false);
    }, 3000);
  };

  return (
    <View style={styles.headerViewStyle}>
      <CachedImage
        source={IMAGES.IM_TOKEN_ICON}
        style={[COMMON_STYLE.imageStyle(8), { alignSelf: 'center' }]}
      />
      <View style={styles.headerTitleViewStyle}>
        <Text style={styles.moneyTitleStyle}>{localize('INTERNET_MONEY')}</Text>
      </View>
      { !refreshBtnPressed
        && (
          <TouchableOpacity
            style={styles.refreshValueStyle}
            // eslint-disable-next-line react/jsx-no-bind
            onPress={() => handleRefreshPress()}
          >
            <CachedImage
              style={styles.refreshIconStyle}
              source={IMAGES.REFRESH_ICON}
            />
          </TouchableOpacity>
        )}
      { refreshBtnPressed
        // eslint-disable-next-line max-len
        && <CachedImage source={IMAGES.ROUNDED_LOADING} style={styles.loadingStyle} />}
      {renderHeaderBtns(IMAGES.SUPPORT_ICON, () => {
        navigation.navigate('SUPPORT_SCREEN', {
          supportType: 'contactus',
        });
      })}
    </View>
  );
};

TabHeaderView.propTypes = {
  navigation: PropTypes.any,
  testID: PropTypes.string,
  onPressRefresh: PropTypes.func,
};

export default TabHeaderView;
