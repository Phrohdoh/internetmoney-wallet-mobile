import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  headerViewStyle: {
    flexDirection: 'row',
    paddingHorizontal: Responsive.getWidth(5),
    marginVertical: Responsive.getHeight(2),
  },
  headerTitleViewStyle: {
    marginHorizontal: Responsive.getWidth(3),
    flex: 1,

    justifyContent: 'center',
  },

  moneyTitleStyle: {
    ...COMMON_STYLE.textStyle(28, COLORS.YELLOWFB, 'PIX_BOY'),
  },
  headerBtnStyle: { marginLeft: Responsive.getWidth(3) },
  redDotStyle: {
    position: 'absolute',
    backgroundColor: 'red',
    width: 12,
    height: 12,
    borderRadius: 6,
    right: -6,
  },
  loadingStyle: {
    ...COMMON_STYLE.imageStyle(8),
    alignSelf: 'center',
  },
  refreshIconStyle: {
    ...COMMON_STYLE.imageStyle(6),
    marginRight: 5,
  },
});
