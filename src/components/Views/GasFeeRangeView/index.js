import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

// import components
import {
  GasFeeUpdateView,
  RangeSelectionView,
  RoundedButton,
  CachedImage,
} from '@components';

// import languages
import { localize } from '@languages';

// import style

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';
import {
  IntegerUnits,
  sanitizeFloatingPointString,
  sanitizeIntegerString,
} from '@utils';
import { calculateEstimateFee, calculateMaxFee } from '@web3';
import { styles } from './style';

const GWEI = new IntegerUnits(1000000000, 18);
const GAS_INCREMENT = new IntegerUnits(1, 0);
const FEE_INCREMENT = GWEI.multiply(0.1);

const GasFeeRangeView = (props) => {
  const { gasRange, txnObject, network } = props;
  const [selectedRange, setSelectedRange] = useState(props.selectedRange);
  const [isShowAdvance, setIsShowAdvance] = useState(false);
  const [gas, setGas] = useState(new IntegerUnits(0));
  const [gasText, setGasText] = useState('');
  const [gasPrice, setGasPrice] = useState(new IntegerUnits(0));
  const [gasPriceText, setGasPriceText] = useState('');
  const [maxPrioFee, setMaxPrioFee] = useState(new IntegerUnits(0));
  const [maxPrioFeeText, setMaxPrioFeeText] = useState('');
  const [maxFeePerGas, setMaxFeePerGas] = useState(new IntegerUnits(0));
  const [maxFeePerGasText, setMaxFeePerGasText] = useState('');
  const [minGas, setMinGas] = useState(new IntegerUnits(0));
  const [minMaxPrioFee, setMinMaxPrioFee] = useState(new IntegerUnits(0));
  const [minGasPrice, setMinGasPrice] = useState(new IntegerUnits(0));

  useEffect(() => {
    setGas(txnObject.gas);
    setGasText(txnObject.gas.value.toString());
    setGasPrice(txnObject.gasPrice);
    setGasPriceText(txnObject.gasPrice.divide(GWEI).toPrecision(18).toString(6, true, true));

    if (txnObject.maxPriorityFeePerGas) {
      setMaxPrioFee(txnObject.maxPriorityFeePerGas);
      setMaxPrioFeeText(txnObject.maxPriorityFeePerGas.divide(GWEI).toPrecision(18).toString(6, true, true));
    }

    if (txnObject.maxFeePerGas) {
      setMaxFeePerGas(txnObject.maxFeePerGas);
      setMaxFeePerGasText(txnObject.maxFeePerGas.divide(GWEI).toPrecision(18).toString(6, true, true));
    }

    if (network.txnType != 0) {
      setMinGas(gasRange.gas.minCap);
      setMinMaxPrioFee(gasRange.maxPriorityFeePerGas.minCap);
    }
    if (network.txnType == 0) {
      setMinGas(gasRange.gas.minCap);
      setMinGasPrice(gasRange.gasPrice.minCap);
    }
  }, [gasRange, txnObject]);

  useEffect(() => {
    getGasFeeValue();
  }, [selectedRange]);

  const getGasFeeValue = () => {
    if (network.txnType != 0) {
      const defaultGasRange = gasRange.gas.default;
      const defaultMaxPrioRange = gasRange.maxPriorityFeePerGas.default;
      const defaultMaxFeeRange = gasRange.maxFee.default;

      if (selectedRange === 0) {
        setGas(defaultGasRange.low);
        setGasText(defaultGasRange.low.value.toString());
        setMaxPrioFee(defaultMaxPrioRange.low);
        setMaxPrioFeeText(defaultMaxPrioRange.low.divide(GWEI).toPrecision(18).toString(6, true, true));
        setMaxFeePerGas(defaultMaxFeeRange.low);
        setMaxFeePerGasText(defaultMaxFeeRange.low.divide(GWEI).toPrecision(18).toString(6, true, true));
      } else if (selectedRange === 1) {
        setGas(defaultGasRange.medium);
        setGasText(defaultGasRange.medium.value.toString());
        setMaxPrioFee(defaultMaxPrioRange.medium);
        setMaxPrioFeeText(defaultMaxPrioRange.medium.divide(GWEI).toPrecision(18).toString(6, true, true));
        setMaxFeePerGas(defaultMaxFeeRange.medium);
        setMaxFeePerGasText(defaultMaxFeeRange.medium.divide(GWEI).toPrecision(18).toString(6, true, true));
      } else if (selectedRange === 2) {
        setGas(defaultGasRange.high);
        setGasText(defaultGasRange.high.value.toString());
        setMaxPrioFee(defaultMaxPrioRange.high);
        setMaxPrioFeeText(defaultMaxPrioRange.high.divide(GWEI).toPrecision(18).toString(6, true, true));
        setMaxFeePerGas(defaultMaxFeeRange.high);
        setMaxFeePerGasText(defaultMaxFeeRange.high.divide(GWEI).toPrecision(18).toString(6, true, true));
      }
    }
  };

  const estimateFee = calculateEstimateFee({
    gas,
    gasPrice,
    maxPriorityFeePerGas: maxPrioFee,
  }, network.txnType).toString(6);

  const maxFee = calculateMaxFee({
    gas,
    maxFeePerGas,
    maxPriorityFeePerGas: maxPrioFee,
  }).toString(6);

  const getTimeToSend = () => {
    /* istanbul ignore else */

    if (selectedRange === 0) {
      return localize('LOW_SEND_TIME');
    } if (selectedRange === 1) {
      return localize('MEDIUM_SEND_TIME');
    } if (selectedRange === 2) {
      return localize('HIGH_SEND_TIME');
    }
  };

  const validationTwo = () => maxPrioFee.gte(minMaxPrioFee) && gas.gte(minGas);

  const validationZero = () => gasPrice.gte(minGasPrice) && gas.gte(minGas);

  const onChangeGasText = useCallback((text) => {
    text = sanitizeIntegerString(text);
    setGas(IntegerUnits.fromFloatingPoint(text));
    setGasText(text);
  }, [setGas, setGasText]);

  const onIncrementGas = useCallback(() => {
    const value = gas.add(GAS_INCREMENT);
    setGas(value);
    setGasText(value.value.toString());
  }, [gas, setGas, setGasText]);

  const onDecrementGas = useCallback(() => {
    if (gas.lte(minGas)) {
      return;
    }
    const value = gas.subtract(GAS_INCREMENT);
    setGas(value);
    setGasText(value.value.toString());
  }, [gas, minGas, setGas, setGasText]);

  const onChangeGasPriceText = useCallback((text) => {
    text = sanitizeFloatingPointString(text);
    setGasPrice(IntegerUnits.fromFloatingPoint(text).multiply(GWEI));
    setGasPriceText(text);
  }, [setGasPrice, setGasPriceText]);

  const onIncrementGasPrice = useCallback(() => {
    const value = gasPrice.add(FEE_INCREMENT);
    setGasPrice(value);
    setGasPriceText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [gasPrice, setGasPrice, setGasPriceText]);

  const onDecrementGasPrice = useCallback(() => {
    if (gasPrice.lte(minGasPrice)) {
      return;
    }
    const value = gas.gt(FEE_INCREMENT)
      ? gas.subtract(FEE_INCREMENT)
      : new IntegerUnits(0);
    setGasPrice(value);
    setGasPriceText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [gasPrice, minGasPrice, setGasPrice, setGasPriceText]);

  const onChangeMaxPrioFeeText = useCallback((text) => {
    text = sanitizeFloatingPointString(text);
    setMaxPrioFee(IntegerUnits.fromFloatingPoint(text).multiply(GWEI));
    setMaxPrioFeeText(text);
  }, [setMaxPrioFee, setMaxPrioFeeText]);

  const onIncrementMaxPrioFee = useCallback(() => {
    const value = maxPrioFee.add(FEE_INCREMENT);
    setMaxPrioFee(value);
    setMaxPrioFeeText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [maxPrioFee, setMaxPrioFee, setMaxPrioFeeText]);

  const onDecrementMaxPrioFee = useCallback(() => {
    if (maxPrioFee.lte(minMaxPrioFee)) {
      return;
    }
    const value = maxPrioFee.gt(FEE_INCREMENT)
      ? maxPrioFee.subtract(FEE_INCREMENT)
      : new IntegerUnits(0);
    setMaxPrioFee(value);
    setMaxPrioFeeText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [maxPrioFee, minMaxPrioFee, setMaxPrioFee, setMaxPrioFeeText]);

  const onChangeMaxFeePerGasText = useCallback((text) => {
    text = sanitizeFloatingPointString(text);
    setMaxFeePerGas(IntegerUnits.fromFloatingPoint(text).multiply(GWEI));
    setMaxFeePerGasText(text);
  }, [setMaxFeePerGas, setMaxFeePerGasText]);

  const onIncrementMaxFeePerGas = useCallback(() => {
    const value = maxFeePerGas.add(FEE_INCREMENT);
    setMaxFeePerGas(value);
    setMaxFeePerGasText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [maxFeePerGas, setMaxFeePerGas, setMaxFeePerGasText]);

  const onDecrementMaxFeePerGas = useCallback(() => {
    if (maxFeePerGas.lte(maxPrioFee)) {
      return;
    }
    const value = maxFeePerGas.gt(FEE_INCREMENT)
      ? maxFeePerGas.subtract(FEE_INCREMENT)
      : new IntegerUnits(0);
    setMaxFeePerGas(value);
    setMaxFeePerGasText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [maxFeePerGas, maxPrioFee, setMaxFeePerGas, setMaxFeePerGasText]);

  const renderTypeOneOptions = () => (
    network.txnType == 0 && (
      <React.Fragment>
        <GasFeeUpdateView
          title={localize('GAS_LIMIT')}
          id="gas-limit"
          valueText={gasText}
          isShowError={gas.lt(minGas)}
          error={localize('GAS_FEE_NOT_ALLOW_MSG', [gas, minGas])}
          onClickIncrement={onIncrementGas}
          onClickDecrement={onDecrementGas}
          onChange={onChangeGasText}
        />
        <GasFeeUpdateView
          title={localize('GAS_PRICE')}
          id="gas-price"
          value={gasPrice}
          valueText={gasPriceText}
          isShowError={gasPrice.lt(minGasPrice)}
          error={localize('MIN_GAS_PRICE_MSG')}
          onClickIncrement={onIncrementGasPrice}
          onClickDecrement={onDecrementGasPrice}
          onChange={onChangeGasPriceText}
        />
      </React.Fragment>
    )
  );

  const renderTypeTwoOptions = () => (
    network.txnType != 0 && (
      <React.Fragment>
        <TouchableOpacity
          testID="gas-popup-advance-button"
          style={styles.advanceTitleViewStyle}
          onPress={() => setIsShowAdvance(!isShowAdvance)}
        >
          <Text style={styles.advanceStringStyle}>
            {localize('ADVANCE_OPTION')}
          </Text>
          <CachedImage
            style={COMMON_STYLE.imageStyle(3, COLORS.YELLOWFB)}
            source={IMAGES.DOWN_ARROW}
          />
        </TouchableOpacity>
        {isShowAdvance && (
          <React.Fragment>
            <GasFeeUpdateView
              title={localize('GAS_LIMIT')}
              id="gas-limit"
              valueText={gasText}
              isShowError={gas.lt(minGas)}
              error={localize('GAS_FEE_NOT_ALLOW_MSG', [gas, minGas])}
              onClickIncrement={onIncrementGas}
              onClickDecrement={onDecrementGas}
              onChange={onChangeGasText}
            />
            <GasFeeUpdateView
              title={localize('MAX_PRIO_FEE')}
              id="max-prio-fee"
              value={maxPrioFee}
              valueText={maxPrioFeeText}
              isShowError={maxPrioFee.lt(minMaxPrioFee)}
              error={localize('MAX_PRIO_FEE_NOT_ALLOW_MSG')}
              onClickIncrement={onIncrementMaxPrioFee}
              onClickDecrement={onDecrementMaxPrioFee}
              onChange={onChangeMaxPrioFeeText}
            />
            <GasFeeUpdateView
              title={localize('MAX_FEE')}
              id="max-fee"
              value={maxFeePerGas}
              valueText={maxFeePerGasText}
              isShowError={maxFeePerGas.lt(maxPrioFee)}
              error={localize('MAX_FEE_NOT_ALLOW_MSG')}
              onClickIncrement={onIncrementMaxFeePerGas}
              onClickDecrement={onDecrementMaxFeePerGas}
              onChange={onChangeMaxFeePerGasText}
            />
          </React.Fragment>
        )}
      </React.Fragment>
    )
  );

  return (
    <View style={styles.containerStyle}>
      <Text style={COMMON_STYLE.modalTitleStyle}>
        {localize('ESTIMATED_GAS_FEE_UPPERCASE')}
      </Text>
      <Text
        style={styles.gasFeeStyle}
        numberOfLines={1}
        adjustsFontSizeToFit
      >
        {estimateFee}
        {' '}
        {network.sym}
      </Text>
      {network.txnType != 0 && (
        <Text style={styles.maxFeeStyle}>
          {localize('MAX_FEE:')}
          <Text style={styles.maxFeeTextStyle}>
            {maxFee}
            {' '}
            {network.sym}
          </Text>
        </Text>
      )}
      <Text style={styles.estimateTimeStyle}>{getTimeToSend()}</Text>
      {props.network.txnType != 0 && (
        <RangeSelectionView
          selectedRange={selectedRange}
          onSelectRange={index => setSelectedRange(index)}
        />
      )}
      <View style={styles.sepratorLineStyle} />
      <View style={styles.sepratorLineStyle} />
      {renderTypeTwoOptions()}
      {renderTypeOneOptions()}
      <RoundedButton
        testID="gas-fee-save-button"
        title={localize('SAVE_UPPERCASE')}
        style={styles.saveButtonStyle}
        onPress={() => {
          if (network.txnType != 0) {
            if (validationTwo) {
              props.onPressSave({
                gas,
                maxPrioFee,
                maxFeePerGas,
              });
            }
          } else if (validationZero) {
            props.onPressSave({
              gas,
              gasPrice,
            });
          }
        }}
      />
    </View>
  );
};

GasFeeRangeView.propTypes = {
  gasRange: PropTypes.object,
  txnObject: PropTypes.object,
  network: PropTypes.object,
  selectedRange: PropTypes.number,
  onPressSave: PropTypes.func,
};
export default GasFeeRangeView;
