import {StyleSheet} from 'react-native';

// import helpers
import {Responsive} from '@helpers';

// import themes
import {COLORS, COMMON_STYLE} from '@themes';

export const styles = StyleSheet.create({
  containerStyle: {
    marginHorizontal: Responsive.getWidth(2),
  },
  gasFeeStyle: {
    ...COMMON_STYLE.textStyle(28, COLORS.WHITE, 'BOLD', 'center'),
  },
  maxFeeStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
  },
  maxFeeTextStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BOLD'),
    marginVertical: Responsive.getHeight(0.7),
  },
  estimateTimeStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.GREEN6D, 'BASE', 'center'),
    marginVertical: Responsive.getHeight(0.7),
  },
  saveButtonStyle: {
    marginVertical: Responsive.getHeight(2),
  },
  sepratorLineStyle: {
    height: 1,
    width: '100%',
    backgroundColor: COLORS.GRAY27,
    marginVertical: Responsive.getHeight(0.3),
  },
  advanceTitleViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: Responsive.getHeight(2),
  },
  advanceStringStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.YELLOWFB, 'BASE', 'center'),
  },
});
