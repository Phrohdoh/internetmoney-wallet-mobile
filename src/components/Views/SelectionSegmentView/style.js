import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export default StyleSheet.create({
  segmentContainerStyle: {
    width: '100%',
    borderBottomWidth: 2,
    borderColor: COLORS.LIGHT_OPACITY(0.3),
    height: 40,
    flexDirection: 'row',
    display: 'flex',
  },
  flatlistStyle: {
    position: 'relative',
    flex: 1
  },
  segmentViewStyle: isSelected => {
    return {
      minWidth: Responsive.getWidth(20),
      borderBottomWidth: 2,
      borderColor: isSelected ? COLORS.YELLOWFB : COLORS.TRANSPARENT,
      justifyContent: 'center',
      alignItems: 'center',
      height: 40,
      paddingHorizontal: Responsive.getWidth(4),
      flex: 1
    };
  },
  segmentTextStyle: isSelected => {
    return {
      ...COMMON_STYLE.textStyle(
        11,
        isSelected ? COLORS.YELLOWFB : COLORS.LIGHT_OPACITY(0.3),
        'BOLD',
      ),
    };
  },
  sepratorStyle: { 
    width: 40,
  },
});
