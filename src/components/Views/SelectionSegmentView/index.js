import React, { useRef } from 'react';
import { View, FlatList, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';

const SelectionSegmentView = (props) => {
  const { segmentList, selectedSegment, onSelectSegment } = props;

  const segmentViewRef = useRef(null);

  const renderSegment = ({ item, index }) => {
    const isSelected = selectedSegment === item;

    return (
      <TouchableOpacity
        key={`${item.title}-${index}`}
        testID={`segment-${item.title}-${index}`}
        style={styles.segmentViewStyle(isSelected)}
        onPress={() => {
          onSelectSegment(item);
          segmentViewRef.current.scrollToIndex({
            animated: true,
            index,
          });
        }}
      >
        <Text style={styles.segmentTextStyle(isSelected)}>{item.title}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={[styles.segmentContainerStyle]}>
      <FlatList
        ref={segmentViewRef}
        style={styles.flatlistStyle}
        data={segmentList}
        renderItem={item => renderSegment(item)}
        ItemSeparatorComponent={() => <View style={styles.sepratorStyle} />}
        showsHorizontalScrollIndicator={false}
        horizontal
        keyExtractor={(item, index) => index + item}
      />
    </View>
  );
};

SelectionSegmentView.defaultProps = {
  segmentList: [],
};

SelectionSegmentView.propTypes = {
  segmentList: PropTypes.array,
  selectedSegment: PropTypes.object,
  onSelectSegment: PropTypes.func,
};
export default SelectionSegmentView;
