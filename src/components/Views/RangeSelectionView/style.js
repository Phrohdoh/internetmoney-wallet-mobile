import {StyleSheet} from 'react-native';

// import helpers
import {Responsive} from '@helpers';

// import themes
import {COLORS, COMMON_STYLE} from '@themes';

export const styles = StyleSheet.create({
  rangeContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: Responsive.getHeight(2),
    marginBottom: Responsive.getHeight(2),
  },
  rangeViewStyle: isSelected => {
    return {
      height: Responsive.getHeight(4),
      width: '26%',
      borderRadius: Responsive.getHeight(2),
      borderWidth: 3,
      borderColor: COLORS.GRAY15,
      backgroundColor: isSelected ? COLORS.YELLOWFB : COLORS.GRAYC5,
      alignItems: 'center',
      justifyContent: 'center',
    };
  },
  rangeTextStyle: isSelected => {
    return {
      ...COMMON_STYLE.textStyle(
        12,
        isSelected ? COLORS.BLACK : COLORS.LIGHT_OPACITY(0.6),
        'BOLD',
        'center',
      ),
    };
  },
  lineStyle: {
    height: 3,
    backgroundColor: COLORS.GRAY15,
    flex: 1,
  },
});
