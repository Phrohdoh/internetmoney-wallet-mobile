import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

const RangeSelectionView = (props) => {
  const range = localize('RANGE_LIST');

  const renderRangeTab = (title, index) => (
    <React.Fragment key={index}>
      <TouchableOpacity
        key={`${index + title}btn`}
        testID={`range-selection-${title}-tab-button`}
        style={styles.rangeViewStyle(index === props.selectedRange)}
        onPress={() => props.onSelectRange(index)}
      >
        <Text style={styles.rangeTextStyle(props.selectedRange === index)}>
          {title}
        </Text>
      </TouchableOpacity>
      {index !== range.length - 1 && (
        <View id={`${title + index}view`} style={styles.lineStyle} />
      )}
    </React.Fragment>
  );
  return (
    <View style={styles.rangeContainerStyle}>
      {range.map((item, index) => renderRangeTab(item, index))}
    </View>
  );
};

RangeSelectionView.propTypes = {
  selectedRange: PropTypes.number,
  onSelectRange: PropTypes.func,
};
export default RangeSelectionView;
