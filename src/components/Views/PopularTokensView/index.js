import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { ScrollView, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { ASYNC_KEYS } from '../../../constants';
import { styles } from './style';

import { AlertActionModel, LoadingView, PopularTokenListCell } from '@components';
import { localize } from '@languages';
import { NetworksAction, getPopularTokens } from '@redux';
import { StorageOperation } from '@storage';
import { COLORS, COMMON_STYLE } from '@themes';
import * as Web3Layer from '@web3';
import { parseObject, IntegerUnits } from '@utils';
import { getBalances, getNetwork, getUsdPrices } from '../../../redux/selectors';

/**
 * Popular Tokens View 
 */
const PopularTokensView = props => {
  const { account } = props;
  const accountAddress = account.publicAddress.toLowerCase();

  const dispatch = useDispatch();

  const networks = useSelector(state => state.networks);
  const selectedNetwork = useSelector(getNetwork);

  const popularTokens = useSelector(getPopularTokens)[accountAddress];
  const balances = useSelector(getBalances)[selectedNetwork.chainId][accountAddress];
  const usdPrices = useSelector(getUsdPrices)[selectedNetwork.chainId];

  const [isLoading, setIsLoading] = useState(true);
  const [isShowAddAlert, setIsShowAddAlert] = useState(false);
  const [selectedToken, setSelectedToken] = useState(undefined);
  const [accountPopularTokens, setAccountPopularTokens] = useState([]);

  useEffect(() => {
    if (popularTokens && balances && usdPrices) {
      if (isLoading) setIsLoading(false);

      const tokensWithBalances = popularTokens.map((popularToken) => {
        const address = popularToken.address.toLowerCase();

        const defaultValues = {
          fetching: false,
          value: new IntegerUnits(0),
          lastFetched: Date.now(),
        };

        const balance = balances[address]?.value
          ? {
              ...balances[address],
              value: new IntegerUnits(
                balances[address].value.value,
                balances[address].value.decimals,
              ),
            }
          : defaultValues;

        const usdPrice = usdPrices[address]?.value
          ? {
              ...usdPrices[address],
              value: new IntegerUnits(
                usdPrices[address].value.value,
                usdPrices[address].value.decimals,
              ),
            }
          : defaultValues;
  
        return {
          ...popularToken,
          balance,
          usdPrice,
        };
      });
  
      setAccountPopularTokens(tokensWithBalances);
    }
  }, [popularTokens, balances, usdPrices, isLoading, setIsLoading, setAccountPopularTokens]);

  const addToken = async () => {
    const checkIsValidTokenAddress = await Web3Layer.isTokenAddressValid(
      selectedToken.address,
      selectedNetwork,
    );
    const { tokenDetails } = checkIsValidTokenAddress;
    const networkTokens = networks.tokens ? [...networks.tokens] : [];
    
    const accountTokens = networkTokens
      .find(({ id }) => id.toLowerCase() === accountAddress);
    
    if (accountTokens) {
      const tokenIndex = networkTokens.indexOf(accountTokens);
      const tokenList = [...accountTokens.tokens];
      tokenList.push(selectedToken);
      const newAccountTokens = { ...accountTokens };
      newAccountTokens.tokens = tokenList;
      networkTokens[tokenIndex] = newAccountTokens;
    } else {
      const newAccountTokens = {
        id: account.publicAddress,
        tokens: [{ ...tokenDetails, address: selectedToken.address }]
      };
      networkTokens.push(newAccountTokens);
    }
    
    const { data } = await StorageOperation.getData(ASYNC_KEYS.CHAIN_TOKENS);
    const parsedData = parseObject(data);
    
    const chainTokens = parsedData.success ? parsedData.data : [];
    const chain = chainTokens.find((chainToken) => chainToken.chainId == selectedNetwork.chainId);
    
    if (chain) {
      const chainIndex = chainTokens.indexOf(chain);
      chain.accountTokens = networkTokens;
      chainTokens[chainIndex] = chain;
    } else {
      chainTokens.push({
        chainId: selectedNetwork.chainId,
        accountTokens: networkTokens,
      });
    }

    dispatch(NetworksAction.saveTokens(networkTokens));
    await StorageOperation.setMultiData([
      [ASYNC_KEYS.CHAIN_TOKENS, JSON.stringify(chainTokens)],
    ]);

    setIsShowAddAlert(false);
    setSelectedToken(undefined);

    if (props.onAddPopularTokenSuccess) props.onAddPopularTokenSuccess();
  };

  const renderPopularTokensView = () => (
    <View>
      {!isLoading ? (
        <React.Fragment>
          <ScrollView style={ styles.segmentContainerStyle }>
            { accountPopularTokens.length === 0 && (
              <Text
                style={COMMON_STYLE.textStyle(
                  14,
                  COLORS.WHITE,
                  undefined,
                  'center',
                )}>
                {localize('ADDED_ALL_POPULAR_TOKENS_MSG')}
              </Text>
            )}
            {
              accountPopularTokens.map((popularToken, index) => (
                <PopularTokenListCell
                  key={ index }
                  index={ index }
                  item={ popularToken }
                  networkObj={selectedNetwork}
                  onSelectToken={ (token) => {
                    setIsShowAddAlert(true);
                    setSelectedToken(token);
                  } }
                />
              ))
            }
          </ScrollView>
        </React.Fragment>
      ) : (
        <LoadingView />
      )}
      <AlertActionModel
        alertTitle={localize('ADD_TOKEN')}
        isShowAlert={isShowAddAlert}
        onPressSuccess={() => addToken()}
        successBtnTitle={localize('YES')}
        onPressCancel={() => {
          setIsShowAddAlert(false);
          setSelectedToken(undefined);
        }}
      >
        <Text style={COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BOLD', 'center')}>
          {localize('ADD_TOKEN_MSG', selectedToken && [selectedToken.symbol])}
        </Text>
      </AlertActionModel>
    </View>
  );

  return renderPopularTokensView();
};

PopularTokensView.propTypes = {
  account: PropTypes.object.isRequired,
  onAddPopularTokenSuccess: PropTypes.func,
};

export default PopularTokensView;
