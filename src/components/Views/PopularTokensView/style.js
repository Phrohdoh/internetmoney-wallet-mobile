import { StyleSheet } from 'react-native';

import { COMMON_STYLE, COLORS } from '@themes';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  flatlistStyle: {
    borderRadius: Responsive.getWidth(5),
    borderColor: COLORS.GRAY41,
    borderWidth: 2,
    marginBottom: Responsive.getHeight(2),
  },
  seperatorStyle: { height: 1, backgroundColor: COLORS.LIGHT_OPACITY(0.1) },
  // segmentContainerStyle: { height: Responsive.getHeight(55) },
});
