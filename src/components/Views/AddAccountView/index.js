import React, { useState } from 'react';
import { Text, View, FlatList } from 'react-native';
import PropTypes from 'prop-types';

import { useDispatch, useSelector, useStore } from 'react-redux';
import {
  WalletAction,
  AccountsAction,
  getArchivedAccountList,
  getFullAccountList,
} from '@redux';

// import components
import { RoundedButton, TitleTextInput, CachedImage } from '@components';

// import constants
import { ASYNC_KEYS } from '@constants';

// import languages
import { localize } from '@languages';

// import storage functions
import { StorageOperation } from '@storage';

// import style
import { styles } from './style';

// import themes
import { COMMON_STYLE, IMAGES } from '@themes';

// import Utils
import { Validation } from '@utils';

// import Web3 functions
import * as Web3Layer from '@web3';
import Logger from '../../../utils/logger';
import AccountListCell from '../AccountListCell';
import { getNetwork } from '../../../redux';

const AddAccountView = props => {
  const store = useStore();
  const dispatch = useDispatch();
  const wallet = useSelector(state => state.wallet);
  const accounts = useSelector(state => state.accounts);
  const selectedNetwork = useSelector(getNetwork);

  const [isLoading, setIsLoading] = useState(false);
  const [isShowCreateAccount, setIsShowCreateAccount] = useState(false);
  const [isShowRestoreAccount, setIsShowRestoreAccount] = useState(false);
  const [isShowImportAccount, setIsShowImportAccount] = useState(false);

  const [accountName, onChangeAccountName] = useState({
    id: 'account-name',
    value: '',
    title: 'ACCOUNT_NAME',
    placeholder: 'ENTER_ACCOUNT_NAME',
    extraProps: {
      isError: false,
      errorText: localize('ENTER_VALID_ACCOUNT_NAME_MSG'),
    },
  });

  const [privateKey, onChangePrivateKey] = useState({
    id: 'private-key',
    value: '',
    title: 'PRIVATE_KEY',
    placeholder: 'ENTER_PRIVATE_KEY',
    extraProps: {
      isError: false,
      errorText: localize('ENTER_VALID_PRIVATE_KEY'),
    },
  });

  const archivedAccountList = useSelector(getArchivedAccountList);

  // Action Methods
  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.value = text;
    tempDict.extraProps = { ...state.extraProps, isError: false };
    setFunction(tempDict);
  };

  const changeObj = (obj, state, setFunction) => {
    const tmpObj = { ...state, ...obj };
    setFunction(tmpObj);
  };

  const checkAccountCreateValidation = () => {
    const isEmptyAccountName = Validation.isEmpty(accountName.value);
    const isNameExist = accounts.accountList.find(
      accObj => accObj.name === accountName.value.trim(),
    );
    if (isEmptyAccountName) {
      changeObj(
        {
          extraProps: {
            ...accountName.extraProps,
            isError: true,
            errorText: localize('ENTER_VALID_ACCOUNT_NAME_MSG'),
          },
        },
        accountName,
        onChangeAccountName,
      );
      return false;
    } else if (isNameExist) {
      changeObj(
        {
          extraProps: {
            ...accountName.extraProps,
            isError: true,
            errorText: localize('ACCOUNT_NAME_EXIST_MSG'),
          },
        },
        accountName,
        onChangeAccountName,
      );
      return false;
    } else {
      return true;
    }
  };

  const checkAccountImportValidation = () => {
    const isEmptyAccountName = Validation.isEmpty(accountName.value);
    const isEmptyPrivateKey = Validation.isEmpty(privateKey.value);

    const isNameExist = accounts.accountList.find(
      accObj => accObj.name === accountName.value.trim(),
    );

    Logger.debug('checkAccountImportValidation: results', {
      isEmptyAccountName,
      isEmptyPrivateKey,
      isNameExist,
    });

    if (isEmptyAccountName || isEmptyPrivateKey) {
      changeObj(
        {
          extraProps: {
            ...accountName.extraProps,
            isError: isEmptyAccountName,
          },
        },
        accountName,
        onChangeAccountName,
      );
      changeObj(
        {
          extraProps: {
            ...privateKey.extraProps,
            isError: isEmptyPrivateKey,
            errorText: localize('ENTER_VALID_PRIVATE_KEY'),
          },
        },
        privateKey,
        onChangePrivateKey,
      );
      return false;
    } else if (isNameExist) {
      changeObj(
        {
          extraProps: {
            ...accountName.extraProps,
            isError: true,
            errorText: localize('ACCOUNT_NAME_EXIST_MSG'),
          },
        },
        accountName,
        onChangeAccountName,
      );
      return false;
    } else {
      return true;
    }
  };

  // Create account
  const createNewAccount = async () => {
    if (checkAccountCreateValidation()) {
      setIsLoading(true);
      const newAccountObj = await Web3Layer.createNewAccount(
        wallet.walletDetail.mnemonic,
        wallet.walletDetail.walletObject,
        wallet.password,
        selectedNetwork,
      );

      if (newAccountObj.success) {
        const updatedWallet = { ...wallet.walletDetail };
        updatedWallet.walletObject = newAccountObj.object;
        const accountList = [...accounts.accountList];
        accountList.push({
          publicAddress: newAccountObj.account.publicAddress,
          isHardware: false,
          isImported: false,
          isArchived: false,
          isPopularTokens: true,
          name: accountName.value.trim(),
        });

        dispatch(WalletAction.updateWalletObject(updatedWallet));
        dispatch(AccountsAction.updateAccountObject(accountList));

        await StorageOperation.setMultiData([
          [ASYNC_KEYS.WALLET_DETAIL, JSON.stringify(updatedWallet)],
          [ASYNC_KEYS.ACCOUNT_LIST, JSON.stringify(accountList)],
        ]);

        setTimeout(() => {
          setIsLoading(false);
        }, 200);

        props.createAccountSuccess();
      }
    }
  };

  // Import account
  const importAccount = async () => {
    if (checkAccountImportValidation()) {
      setIsLoading(true);

      const checkAccountImported = await Web3Layer.checkImportAccount(
        wallet.walletDetail.walletObject,
        privateKey.value.trim(),
        selectedNetwork,
      );

      Logger.debug('importAccount: result of Web3Layer.checkImportAccount', { result: JSON.stringify(checkAccountImported)});

      if (!checkAccountImported.success) {
        changeObj(
          {
            extraProps: {
              ...privateKey.extraProps,
              isError: true,
              errorText: checkAccountImported.error,
            },
          },
          privateKey,
          onChangePrivateKey,
        );
        setIsLoading(false);
        return;
      }

      const importAccountObj = await Web3Layer.importAccount(
        wallet.walletDetail.walletObject,
        privateKey.value.trim(),
        wallet.password,
        selectedNetwork,
      );

      Logger.debug('importAccount: success of Web3Layer.importAccount', { success: importAccountObj.success, error: importAccountObj.error });

      if (importAccountObj.success) {
        const updatedWallet = { ...wallet.walletDetail };
        updatedWallet.walletObject = importAccountObj.object;
        const accountList = [...accounts.accountList];
        accountList.push({
          publicAddress: importAccountObj.account.publicAddress,
          isHardware: false,
          isImported: true,
          isArchived: false,
          isPopularTokens: true,
          name: accountName.value.trim(),
        });
        dispatch(WalletAction.updateWalletObject(updatedWallet));
        dispatch(AccountsAction.updateAccountObject(accountList));

        await StorageOperation.setMultiData([
          [ASYNC_KEYS.WALLET_DETAIL, JSON.stringify(updatedWallet)],
          [ASYNC_KEYS.ACCOUNT_LIST, JSON.stringify(accountList)],
        ]);

        setIsLoading(false);

        props.importAccountSuccess();
      } else {
        changeObj(
          {
            extraProps: {
              ...privateKey.extraProps,
              isError: true,
              errorText: importAccountObj.error,
            },
          },
          privateKey,
          onChangePrivateKey,
        );
      }
    }
  };

  // Render Custom components
  const renderInputComponent = (state, setStateFunction) => {
    return (
      <TitleTextInput
        testID={ `add-account-view-${state.id}` }
        inputContainerStyle={ styles.inputContainerStyle }
        value={ state.value }
        title={ localize(state.title) }
        placeholder={ localize(state.placeholder) }
        onChangeText={ text => changeData(text, state, setStateFunction) }
        onEndEditing={ () => {
          if (isShowCreateAccount && state.id === 'account-name') {
            createNewAccount();
          } else if (state.id === 'private-key') {
            importAccount();
          }
        } }
        { ...state.extraProps }
      />
    );
  };

  const renderLoadingView = () => {
    return (
      <CachedImage source={ IMAGES.ROUNDED_LOADING } style={ styles.loadingStyle } />
    );
  };

  // Add account view
  const renderAddAccountView = () => {
    return (
      !isShowCreateAccount &&
      !isShowRestoreAccount &&
      !isShowImportAccount &&
      !isLoading && (
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('ADD_ACCOUNT')}
          </Text>
          <RoundedButton
            testID="add-account-button"
            title={ localize('CREATE_NEW_ACCOUNT') }
            style={ COMMON_STYLE.roundedBtnMargin() }
            onPress={ () => {
              setIsShowCreateAccount(true);
              setIsShowRestoreAccount(false);
              setIsShowImportAccount(false);
            } }
          />
          {
            archivedAccountList.length > 0
              ? (
                <RoundedButton
                  testID="restore-account-button"
                  title={ localize('RESTORE_ACCOUNT') }
                  style={ COMMON_STYLE.roundedBtnMargin() }
                  onPress={ () => {
                    setIsShowCreateAccount(false);
                    setIsShowRestoreAccount(true);
                    setIsShowImportAccount(false);
                  } }
                />
              )
              : null
          }
          <RoundedButton
            testID="import-account-button"
            isBordered
            title={ localize('IMPORT_ACCOUNT') }
            onPress={ () => {
              setIsShowCreateAccount(false);
              setIsShowRestoreAccount(false);
              setIsShowImportAccount(true);
            } }
          />
        </React.Fragment>
      )
    );
  };

  // render Create Account View
  const renderCreateAccountView = () => {
    return (
      isShowCreateAccount && (
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('CREATE_ACCOUNT')}
          </Text>
          {!isLoading ? (
            <React.Fragment>
              {renderInputComponent(accountName, onChangeAccountName)}
              <RoundedButton
                testID={ 'create-account-button' }
                title={ localize('CREATE_NEW_ACCOUNT') }
                style={ COMMON_STYLE.roundedBtnMargin() }
                onPress={ () => createNewAccount() }
              />
            </React.Fragment>
          ) : (
            renderLoadingView()
          )}
        </React.Fragment>
      )
    );
  };

  const renderRestoreAccountView = () => {
    return (
      isShowRestoreAccount && (
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('RESTORE_ACCOUNT')}
          </Text>
          {
            !isLoading
              ? (
                <FlatList
                  style={ styles.flatlistStyle }
                  data={ archivedAccountList }
                  renderItem={ ({ item, index }) => (
                    <AccountListCell
                      key={ index + item + 'accList' }
                      item={ item }
                      index={ index }
                      isShowAddress={ true }
                      isShowBalance={ false }
                      isRemoving={ false }
                      onClick={async () => {
                        dispatch(AccountsAction.restoreAccount(item));
                        await StorageOperation.setData(
                          ASYNC_KEYS.ACCOUNT_LIST,
                          JSON.stringify(getFullAccountList(store.getState())),
                        );
                        props.restoreAccountSuccess();
                      }}
                      { ...props }
                    />
                  ) }
                  ItemSeparatorComponent={ () => <View style={ styles.sepratorStyle } /> }
                  keyExtractor={ (item, index) => index + item }
                />
              )
              : renderLoadingView()
          }
        </React.Fragment>
      )
    );
  };

  // render Create Account View
  const renderImportAccountView = () => {
    return (
      isShowImportAccount && (
        <React.Fragment>
          <Text style={ COMMON_STYLE.modalTitleStyle }>
            {localize('IMPORT_ACCOUNT')}
          </Text>
          {!isLoading ? (
            <React.Fragment>
              <Text style={ styles.importAccountMsgStyle }>
                {localize('IMPORT_ACCOUNT_MSG')}
              </Text>
              {renderInputComponent(accountName, onChangeAccountName)}
              {renderInputComponent(privateKey, onChangePrivateKey)}
              <RoundedButton
                testID="import-account-button-2"
                title={ localize('IMPORT_ACCOUNT') }
                style={ COMMON_STYLE.roundedBtnMargin() }
                onPress={ () => importAccount() }
              />
            </React.Fragment>
          ) : (
            renderLoadingView()
          )}
        </React.Fragment>
      )
    );
  };

  return (
    <View>
      {renderAddAccountView()}
      {renderCreateAccountView()}
      {renderRestoreAccountView()}
      {renderImportAccountView()}
    </View>
  );
};

AddAccountView.propTypes = {
  createAccountSuccess: PropTypes.func,
  restoreAccountSuccess: PropTypes.func,
  importAccountSuccess: PropTypes.func,
};
export default AddAccountView;
