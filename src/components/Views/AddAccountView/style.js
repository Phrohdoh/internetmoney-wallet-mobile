import {StyleSheet} from 'react-native';

// import helpers
import {Responsive} from '@helpers';

// import themes
import {COLORS, COMMON_STYLE} from '@themes';

export const styles = StyleSheet.create({
  inputContainerStyle: {
    marginBottom: Responsive.getHeight(3),
  },
  importAccountMsgStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginBottom: Responsive.getHeight(3),
  },
  loadingStyle: {
    ...COMMON_STYLE.imageStyle(20),
    alignSelf: 'center',
    marginVertical: Responsive.getHeight(5),
  },
});
