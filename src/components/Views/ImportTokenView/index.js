import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { ScrollView, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { styles } from './style';

import { RoundedButton, TitleTextInput, CachedImage } from '@components';
import { ASYNC_KEYS } from '@constants';
import { localize } from '@languages';
import { NetworksAction, getTokensByAddress, getNetwork } from '@redux';
import { StorageOperation } from '@storage';
import { COMMON_STYLE, IMAGES } from '@themes';
import { Validation, parseObject } from '@utils';
import * as Web3Layer from '@web3';

/**
 * Import Token View
 */
const ImportTokenView = props => {
  const { account } = props;

  const dispatch = useDispatch();
  const networks = useSelector(state => state.networks);
  const selectedNetwork = useSelector(getNetwork);
  
  const [isLoading, setIsLoading] = useState(false);
  const [isValidToken, setIsValidToken] = useState(false);
  const [tokenDetail, setTokenDetail] = useState(undefined);

  const [tokenAddress, onChangeTokenAddress] = useState({
    id: 'token-address',
    value: '',
    title: 'TOKEN_ADDRESS',
    placeholder: 'ENTER_HERE',
    extraProps: {
      isError: false,
      errorText: localize('ENTER_VALID_TOKEN_ADDRESS'),
    },
    endEditing: address => validateAddress(address.trim()),
  });

  const [tokenSymbol, onChangeTokenSymbol] = useState({
    id: 'token-symbol',
    value: '',
    title: 'TOKEN_SYMBOL',
    placeholder: 'ENTER_HERE',
    extraProps: {
      editable: true,
      isError: false,
      errorText: localize('TOKEN_SYMBOL_ERROR'),
    },
    endEditing: symbol => { return symbol; }
  });
  const [tokenPrecision, onChangeTokenPrecision] = useState({
    id: 'token-precision',
    value: '',
    title: 'TOKEN_PRECISION',
    placeholder: 'ENTER_HERE',
    extraProps: {
      editable: true,
      isError: false,
      errorText: localize('TOKEN_PRECISION_ERROR'),
      keyboardType: 'numeric'
    },
    endEditing: precision => { return precision; }
  });

  const tokens = useSelector(getTokensByAddress)[account.publicAddress.toLowerCase()];

  // Action Methods
  const changeData = (text, state, setFunction) => {
    const tempDict = { ...state };
    tempDict.value = text;
    tempDict.extraProps = { ...state.extraProps, isError: false };
    setFunction(tempDict);
  };

  const changeObj = (obj, state, setFunction) => {
    const tmpObj = { ...state, ...obj };
    setFunction(tmpObj);
  };

  const checkImportTokenValidation = () => {
    const isEmptyToken = Validation.isEmpty(tokenAddress.value);

    const isInvalidTokenSymbol= (tokenSymbol.value+''.trim()).length > 11;
    const isInvalidTokenPrecision= parseInt(tokenPrecision.value) < 0 || parseInt(tokenPrecision.value) > 36;
    const isDuplicateToken = checkIsTokenImported(tokenAddress.value);

    if (isDuplicateToken) {
      changeObj(
        {
          extraProps: {
            ...tokenAddress.extraProps,
            isError: true,
            errorText: localize('TOKEN_ALREADY_EXIST_MSG'),
          },
        },
        tokenAddress,
        onChangeTokenAddress,
      );
    }

    if (isEmptyToken) {
      changeObj(
        {
          extraProps: {
            ...tokenAddress.extraProps,
            isError: isEmptyToken,
            errorText: localize('ENTER_VALID_TOKEN_ADDRESS'),
          },
        },
        tokenAddress,
        onChangeTokenAddress,
      );
    }

    if (isInvalidTokenSymbol) {
      changeObj(
        {
          extraProps: {
            ...tokenSymbol.extraProps,
            isError: true,
          },
        },
        tokenSymbol,
        onChangeTokenSymbol,
      );
    }

    if (isInvalidTokenPrecision) {
      changeObj(
        {
          extraProps: {
            ...tokenPrecision.extraProps,
            isError: true,
          },
        },
        tokenPrecision,
        onChangeTokenPrecision,
      );
    }
    
    if(isEmptyToken || isInvalidTokenSymbol || isInvalidTokenPrecision || isDuplicateToken){
      return false;
    }
    else {
      return isValidToken;
    }
  };

  // Validate token address
  const validateAddress = async address => {
    if (!checkIsTokenImported(address)) {
      setIsLoading(true);

      const checkIsValidTokenAddress = await Web3Layer.isTokenAddressValid(
        address.trim(),
        selectedNetwork,
      );

      setIsLoading(false);
      setIsValidToken(checkIsValidTokenAddress.success);
      if (checkIsValidTokenAddress.success) {
        setTokenDetail({
          ...checkIsValidTokenAddress.tokenDetails,
          decimals: checkIsValidTokenAddress.tokenDetails.decimals.toString(),
        });
        changeData(
          checkIsValidTokenAddress.tokenDetails.symbol,
          tokenSymbol,
          onChangeTokenSymbol,
        );
        changeData(
          checkIsValidTokenAddress.tokenDetails.decimals.toString(),
          tokenPrecision,
          onChangeTokenPrecision,
        );
      } else {
        setTokenDetail(undefined);
        changeObj(
          {
            extraProps: {
              ...tokenAddress.extraProps,
              isError: true,
              errorText: checkIsValidTokenAddress.error,
            },
          },
          tokenAddress,
          onChangeTokenAddress,
        );
      }
    }
  };

  const checkIsTokenImported = address => {
    return tokens.some(token => token.address.toLowerCase() === address.toLowerCase());
  };

  // Import token
  const importToken = async () => {
    if (checkImportTokenValidation()) {
      let tempTokens = [];

      /* istanbul ignore else */
      if (networks.tokens) {
        tempTokens = [...networks.tokens];
      }

      const tokenObj = tempTokens.find(
        token => token.id.toLowerCase() === account.publicAddress.toLowerCase(),
      );

      if (tokenObj) {
        const tokenIndex = tempTokens.indexOf(tokenObj);
        const tokenList = [...tokenObj.tokens];
        tokenList.push({ ...tokenDetail, address: tokenAddress.value.trim() });
        const tempTokenObj = { ...tokenObj };
        tempTokenObj.tokens = tokenList;
        tempTokens[tokenIndex] = tempTokenObj;
      } else {
        const tempTokenObj = {
          id: account.publicAddress,
          tokens: [{ ...tokenDetail, address: tokenAddress.value.trim() }],
        };
        tempTokens.push(tempTokenObj);
      }

      const { data } = await StorageOperation.getData(ASYNC_KEYS.CHAIN_TOKENS);
      const parseObj = parseObject(data);

      let tempChainTokens = [];
      if (parseObj.success) {
        tempChainTokens = parseObj.data;
      }
      const chainObj = tempChainTokens.find(
        chain => chain.chainId === selectedNetwork.chainId,
      );

      if (chainObj) {
        const chainIndex = tempChainTokens.indexOf(chainObj);
        chainObj.accountTokens = tempTokens;
        tempChainTokens[chainIndex] = chainObj;
      } else {
        tempChainTokens.push({
          chainId: selectedNetwork.chainId,
          accountTokens: tempTokens,
        });
      }
      dispatch(NetworksAction.saveTokens(tempTokens));
      await StorageOperation.setMultiData([
        [ASYNC_KEYS.CHAIN_TOKENS, JSON.stringify(tempChainTokens)],
      ]);

      if (props.onImportTokenSuccess) props.onImportTokenSuccess();
    }
  };

  // Render Custom components
  const renderInputComponent = (state, setStateFunction) => {
    return (
      <TitleTextInput
        testID={ `add-token-view-${state.id}` }
        inputContainerStyle={ styles.inputContainerStyle }
        value={ state.value }
        title={ localize(state.title) }
        placeholder={ localize(state.placeholder) }
        onChangeText={ text => changeData(text, state, setStateFunction) }
        onEndEditing={ event => state.endEditing(event.nativeEvent.text.trim()) }
        { ...state.extraProps }
      />
    );
  };

  const renderLoadingView = () => {
    return (
      <CachedImage source={ IMAGES.ROUNDED_LOADING } style={ styles.loadingStyle } />
    );
  };

  // render Create Account View
  const renderImportTokenView = () => {
    return (
      <React.Fragment>
       {!isLoading ? (
          <ScrollView>
            <Text style={ styles.importTokenMsgStyle }>
              {localize('IMPORT_TOKEN_MSG')}
            </Text>
            {renderInputComponent(tokenAddress, onChangeTokenAddress)}
            {renderInputComponent(tokenSymbol, onChangeTokenSymbol)}
            {renderInputComponent(tokenPrecision, onChangeTokenPrecision)}
            <RoundedButton
              testID="import-token-button"
              title={ localize('IMPORT_TOKEN_UPPERCASE') }
              style={ COMMON_STYLE.roundedBtnMargin() }
              onPress={ () => importToken() }
            />
          </ScrollView>
        ) : (
          renderLoadingView()
        )}
      </React.Fragment>
    );
  };

  return <View>{renderImportTokenView()}</View>;
};

ImportTokenView.propTypes = {
  account: PropTypes.object,
  onImportTokenSuccess: PropTypes.func,
};
export default ImportTokenView;
