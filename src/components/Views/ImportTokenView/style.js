import { StyleSheet } from 'react-native';

import { Responsive } from '@helpers';
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  inputContainerStyle: {
    marginBottom: Responsive.getHeight(3),
  },
  importTokenMsgStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginBottom: Responsive.getHeight(1),
  },
  scamMsgStyle: {
    ...COMMON_STYLE.textStyle(15, COLORS.YELLOWFB, 'BOLD', 'center'),
    marginBottom: Responsive.getHeight(2),
  },
  // loadingStyle: {
  //   ...COMMON_STYLE.imageStyle(20),
  //   alignSelf: 'center',
  //   marginVertical: Responsive.getHeight(5),
  // },
  segmentContainerStyle: {marginVertical: Responsive.getHeight(2)},
  segmentTabsStyle: {
    flex: 1,
  },
  loadingStyle: {
    ...COMMON_STYLE.imageStyle(12),
    flex: 1, 
    justifyContent: 'center',
    alignSelf: 'center',
  },
});
