import {StyleSheet} from 'react-native';

// import helpers
import {Responsive} from '@helpers';

// import themes
import {COLORS, COMMON_STYLE} from '@themes';

export const styles = StyleSheet.create({
  scrollStyle: {
    marginVertical: Responsive.getHeight(2),
  },
  optionViewStyle: {
    flexDirection: 'row',
    paddingVertical: Responsive.getHeight(1.5),
    alignItems: 'center',
  },
  optionTextStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BOLD'),
    marginHorizontal: Responsive.getWidth(2),
  },
  checkBoxStyle: {marginRight: Responsive.getWidth(3)},
  sepratorStyle: {height: 1, backgroundColor: COLORS.GRAY27},
  chooseAccountBtnStyle: {
    height: 50,
    borderRadius: 25,
    borderWidth: 2,
    borderColor: COLORS.GRAY1F,
    backgroundColor: COLORS.GRAY15,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: Responsive.getWidth(5),
    marginTop: Responsive.getHeight(1),
  },
  selectedAccountStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BOLD'),
    flex: 1,
  },
});
