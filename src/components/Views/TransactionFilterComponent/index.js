import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import {
  View,
  Text,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from 'react-native';

import { useSelector } from 'react-redux';

// import components
import { RoundedButton, CheckBox, CachedImage } from '@components';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

const TransactionFilterComponent = props => {
  const { selectedFilterAccount, accountName, selectedFiltersArray } = props;
  const [arrSelectedType, setArrSelectedType] = useState(selectedFiltersArray);

  const [isShowAccountList, setIsShowAccountList] = useState(false);
  const [selectedAccount, setSelectedAccount] = useState(selectedFilterAccount);
  const [selectedAccountName, setSelectedAccountName] = useState(accountName);
  const [uniqueTransAddress, setUniqueTransAddress] = useState([]);

  const accounts = useSelector(state => state.accounts);

  const filterOptions = [
    { title: 'SEND_TRANSACTION', type: 'send' },
    { title: 'SWAP_TRANSACTION', type: 'swap' },
    { title: 'CLAIM_TRANSACTION', type: 'claim' },
    { title: 'DAPP_TRANSACTION', type: 'dapp' },
  ];

  useEffect(() => {
    filterUniqueAccountList();
  }, []);

  const filterUniqueAccountList = () => {
    const uniqueFromAddress = [
      ...new Set(
        props.transactionsList.map(item => item.transactionDetail.fromAddress),
      ),
    ];
    setUniqueTransAddress(uniqueFromAddress);
  };

  const getTransImage = item => {
    if (item.type === 'send') {
      return IMAGES.SEND_ICON;
    } else if (item.type === 'swap') {
      return IMAGES.SWAP_ICON;
    } else if (item.type === 'claim') {
      return IMAGES.TIME_TOKEN_ICON;
    } else if (item.type === 'dapp') {
      return IMAGES.DAPP_ICON;
    } else {
      return IMAGES.RECEIVE_ICON;
    }
  };

  const getAccountName = (item, index) => {
    const nameObj = accounts.accountList.find(
      account => account.publicAddress.toLowerCase() === item.toLowerCase(),
    );
    if (!nameObj) {
      return `${localize('ACCOUNT')} ${index + 1}`;
    } else {
      return nameObj.name;
    }
  };

  const updateSelectedTypes = item => {
    if (arrSelectedType.includes(item.type)) {
      setArrSelectedType(arrSelectedType.filter(x => x !== item.type));
    } else {
      setArrSelectedType([...arrSelectedType, item.type]);
    }
  };
  // render custom component
  const renderOption = item => {
    return (
      <TouchableOpacity
        testID={ `filter-option-${item.type}-btn` }
        style={ styles.optionViewStyle }
        onPress={ () => {
          updateSelectedTypes(item);
        } }>
        <CheckBox
          checkboxStyle={ styles.checkBoxStyle }
          isSelected={ arrSelectedType.includes(item.type) }
          onPress={ () => {
            updateSelectedTypes(item);
          } }
        />
        <CachedImage
          style={ COMMON_STYLE.imageStyle(8) }
          source={ getTransImage(item) }
        />
        <Text style={ styles.optionTextStyle }>{localize(item.title)}</Text>
      </TouchableOpacity>
    );
  };

  const renderAccountListCell = (item, index) => {
    const accName = getAccountName(item, index);
    return (
      <TouchableOpacity
        testID={ `filter-select-account${item}-btn` }
        style={ styles.chooseAccountBtnStyle }
        onPress={ () => {
          setSelectedAccount({ accountName: accName, address: item });
          setSelectedAccountName(accName);
          setIsShowAccountList(false);
        } }>
        <Text style={ styles.selectedAccountStyle }>{accName}</Text>
      </TouchableOpacity>
    );
  };

  const renderAllCell = () => {
    return (
      <TouchableOpacity
        testID={ 'filter-select-all-account-btn' }
        style={ styles.chooseAccountBtnStyle }
        onPress={ () => {
          setIsShowAccountList(false);
          setSelectedAccountName(undefined);
          setSelectedAccount(undefined);
        } }>
        <Text style={ styles.selectedAccountStyle }>{localize('ALL')}</Text>
      </TouchableOpacity>
    );
  };

  const renderSelectedAccountView = () => {
    const accName = selectedAccountName ? selectedAccountName : localize('ALL');
    return (
      <TouchableOpacity
        testID={ 'filter-choose-account-btn' }
        style={ styles.chooseAccountBtnStyle }
        onPress={ () => setIsShowAccountList(true) }>
        <Text style={ styles.selectedAccountStyle }>{accName}</Text>
        <CachedImage style={ COMMON_STYLE.imageStyle(5) } source={ IMAGES.DOWN_ARROW } />
      </TouchableOpacity>
    );
  };

  const renderFilterSelection = () => {
    return (
      <React.Fragment>
        <Text style={ COMMON_STYLE.modalTitleStyle }>
          {localize('FILTER_UPPERCASE')}
        </Text>
        <Text style={ COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6)) }>
          {localize('CHOOSE_ACCOUNT')}
        </Text>
        {renderSelectedAccountView()}
        <ScrollView
          style={ styles.scrollStyle }
          scrollEnabled={ false }
          horizontal
          contentContainerStyle={ { flex: 1 } }>
          <FlatList
            data={ filterOptions }
            scrollEnabled={ false }
            renderItem={ ({ item, _index }) => renderOption(item) }
            ItemSeparatorComponent={ () => <View style={ styles.sepratorStyle } /> }
            keyExtractor={ (item, index) => index + item }
          />
        </ScrollView>
        <RoundedButton
          testID="filter-transection-apply-button"
          title={ localize('APPLY_UPPERCASE') }
          style={ COMMON_STYLE.roundedBtnMargin() }
          onPress={ () =>
            props.onSelectFilter(
              arrSelectedType,
              selectedAccount ? selectedAccount.address : selectedAccount,
              selectedAccountName,
            )
          }
        />
      </React.Fragment>
    );
  };

  const renderAccountSelectionView = () => {
    return (
      <React.Fragment>
        <Text style={ COMMON_STYLE.modalTitleStyle }>
          {localize('CHOOSE_ACCOUNT_UPPERCASE')}
        </Text>

        <ScrollView
          style={ styles.scrollStyle }
          scrollEnabled={ false }
          horizontal
          contentContainerStyle={ { flex: 1 } }>
          <FlatList
            data={ uniqueTransAddress }
            renderItem={ ({ item, index }) => renderAccountListCell(item, index) }
            ListHeaderComponent={ renderAllCell() }
            keyExtractor={ (item, index) => index + item }
          />
        </ScrollView>
      </React.Fragment>
    );
  };

  return (
    <View style={ styles.titleViewStyle }>
      {isShowAccountList
        ? renderAccountSelectionView()
        : renderFilterSelection()}
    </View>
  );
};

TransactionFilterComponent.propTypes = {
  transactionsList: PropTypes.array.isRequired,
  selectedFilterType: PropTypes.string,
  selectedFilterAccount: PropTypes.string,
  accountName: PropTypes.string,
  onSelectFilter: PropTypes.func,
  selectedFiltersArray: PropTypes.array,
};

export default TransactionFilterComponent;
