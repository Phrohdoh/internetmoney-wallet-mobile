import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  cellStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: Responsive.getHeight(1.5),
  },
  networkNameStyle: {
    marginLeft: Responsive.getWidth(2),
    ...COMMON_STYLE.textStyle(12, COLORS.WHITE),
  },
  deleteBtnStyle: {
    width: Responsive.getWidth(12),
    justifyContent: 'center',
    alignItems: 'center',
  },
  swapBadge: {
    backgroundColor: COLORS.YELLOWFB,
    color: COLORS.BLACK,
    overflow: 'hidden',
    marginLeft: Responsive.getWidth(2),
    borderRadius: Responsive.getHeight(0.5),
    paddingHorizontal: Responsive.getWidth(1.5),
    paddingVertical: Responsive.getHeight(0.25),
    fontSize: 10,
  },
  expand: {
    flex: 1,
  }
});
