import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import { CachedImage } from '@components';

// import style

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

// import utils
import { useTokenIcons } from '@utils';
import { useSelector } from 'react-redux';
import { styles } from './style';

const NetworkListCell = (props) => {
  const { item, index, onPressDelete, isRemoveEnable } = props;
  const getTokenIcon = useTokenIcons();
  const swapSupportedNetworks = useSelector(state => (
    state.networks.swapSupportedNetworks
  ));
  const swapSupported = swapSupportedNetworks.includes(item.chainId);

  return (
    <TouchableOpacity
      style={styles.cellStyle}
      testID={`select-network-${index}`}
      onPress={() => props.onSelectNetwork(item)}
    >
      <CachedImage
        style={COMMON_STYLE.imageStyle(6)}
        source={getTokenIcon('', item.chainId)}
      />
      <Text style={styles.networkNameStyle} numberOfLines={1}>
        {item.networkName}
      </Text>
      {swapSupported && (
        <Text style={styles.swapBadge}>Swap</Text>
      )}
      <View style={styles.expand} />
      {item.rpc === props.selectedNetwork.rpc && (
        <CachedImage
          style={COMMON_STYLE.imageStyle(6, COLORS.YELLOWFB)}
          source={IMAGES.check}
        />
      )}
      {isRemoveEnable && (
        <TouchableOpacity
          style={styles.deleteBtnStyle}
          onPress={() => onPressDelete(item)}
        >
          <CachedImage
            source={IMAGES.REMOVE_ICON}
            style={COMMON_STYLE.imageStyle(6, COLORS.ERROR)}
          />
        </TouchableOpacity>
      )}
    </TouchableOpacity>
  );
};

NetworkListCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  onSelectNetwork: PropTypes.func.isRequired,
  selectedNetwork: PropTypes.object.isRequired,
  onPressDelete: PropTypes.func,
  isRemoveEnable: PropTypes.bool,
};

export default NetworkListCell;
