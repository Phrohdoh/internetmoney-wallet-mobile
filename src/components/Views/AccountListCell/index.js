import React, { useCallback, useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { useDispatch, useSelector, useStore } from 'react-redux';
import { localize } from '@languages';
import { IMAGES, COMMON_STYLE, COLORS } from '@themes';
import { displayAddress } from '@utils';
import {
  AccountsAction,
  WalletAction,
  getFullAccountList,
  getWalletDetail,
  getTotalValueByAddress,
} from '@redux';
import { StorageOperation } from '@storage';
import { AlertActionModel, CachedImage } from '@components';
import { styles } from './style';
import { ASYNC_KEYS } from '../../../constants';

const AccountListCell = (props) => {
  const { navigation, item, index, isShowAddress, isShowBalance, isRemoving, onClick } = props;
  const publicAddress = item.publicAddress.toLowerCase();
  const store = useStore();
  const dispatch = useDispatch();
  const [showRemoveModal, setShowRemoveModal] = useState(false);
  const accountBalance = useSelector(getTotalValueByAddress)[publicAddress];

  const onRemove = useCallback(async () => {
    setShowRemoveModal(false);
    dispatch(AccountsAction.removeAccount(item));
    dispatch(WalletAction.removeAccount(item));
    await StorageOperation.setData(
      ASYNC_KEYS.ACCOUNT_LIST,
      JSON.stringify(getFullAccountList(store.getState())),
    );
    await StorageOperation.setData(
      ASYNC_KEYS.WALLET_DETAIL,
      JSON.stringify(getWalletDetail(store.getState())),
    );
  }, [dispatch, item]);

  let handleClick = onClick;
  if (handleClick === undefined && !isRemoving) {
    handleClick = () => {
      navigation.navigate('ACCOUNT_DETAIL_SCREEN', {
        accountDetail: item,
      });
    }
  }

  return (
    <TouchableOpacity
      testID="account-listcell-selection-button"
      style={styles.cellStyle}
      key={`${index + item}view1`}
      onPress={handleClick}
    >
      <View style={{ flex: 1 }} key={`${index + item}view2`}>
        <Text numberOfLines={1} style={styles.nameStyle}>
          {item.name}
        </Text>
        <Text numberOfLines={1} style={styles.statusStyle}>
          {isShowAddress ? displayAddress(item.publicAddress) : '--'}
        </Text>
      </View>
      <View style={{ flex: 1 }} key={`${index + item}view3`}>
        <Text style={styles.valueStyle}>{localize('VALUE')}</Text>
        <Text style={styles.balanceStyle} numberOfLines={1}>
          ${isShowBalance ? accountBalance?.toString() : '--'}
        </Text>
      </View>
      {
        isRemoving
          ? (
            <TouchableOpacity
              style={styles.deleteBtnStyle}
              onPress={() => setShowRemoveModal(true)}
            >
              <CachedImage
                source={IMAGES.REMOVE_ICON}
                style={COMMON_STYLE.imageStyle(7, COLORS.ERROR)}
              />
            </TouchableOpacity>
          )
          : <CachedImage source={IMAGES.RIGHT_ARROW} style={styles.rightArrowStyle} />
      }
      <AlertActionModel
        alertTitle={localize('REMOVE_ACCOUNT')}
        isShowAlert={showRemoveModal}
        successBtnTitle={localize('YES')}
        onPressSuccess={onRemove}
        onPressCancel={() => setShowRemoveModal(false)}
      >
        <Text style={COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BOLD', 'center')}>
          {localize('REMOVE_ACCOUNT_MESSAGE', [item.name])}
        </Text>
      </AlertActionModel>
    </TouchableOpacity>
  );
};

AccountListCell.propTypes = {
  navigation: PropTypes.any,
  item: PropTypes.object,
  index: PropTypes.number,
  isShowAddress: PropTypes.bool,
  isShowBalance: PropTypes.bool,
  isRemoving: PropTypes.bool,
  onClick: PropTypes.func,
};

export default AccountListCell;
