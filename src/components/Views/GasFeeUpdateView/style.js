import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

// import themes
import { COLORS } from '@themes';

export const styles = StyleSheet.create({
  containerStyle: {
    marginVertical: Responsive.getHeight(1),
  },
  titleContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  gasFeeContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: Responsive.getHeight(6),
    borderRadius: Responsive.getHeight(3),
    borderWidth: 2,
    borderColor: COLORS.GRAY15,
    marginTop: Responsive.getHeight(1),
  },
  addBtnStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.GRAY1F,
    height: Responsive.getHeight(4),
    width: Responsive.getHeight(4),
    borderRadius: Responsive.getHeight(2),
    marginHorizontal: Responsive.getWidth(3),
  },
  feeViewStyle: {
    height: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
