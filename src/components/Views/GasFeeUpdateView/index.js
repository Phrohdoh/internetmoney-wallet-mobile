import React from 'react';

import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import { CachedImage } from '@components';
import PropTypes from 'prop-types';

import * as Web3Layer from '@web3';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

const GasFeeUpdateView = props => {

  const onChange = (e) => {
    props.onChange(e.target.value);
  }

  return (
    <View style={ styles.containerStyle }>
      <View style={ styles.titleContainerStyle }>
        <Text style={ COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6)) }>
          {props.title}
        </Text>
        {props.value ? (
          <Text
            style={ COMMON_STYLE.textStyle(
              10,
              COLORS.LIGHT_OPACITY(0.6),
              'BASE',
              'right',
            ) }>
            {localize('ESTIMATE:')}
            <Text
              style={ COMMON_STYLE.textStyle(
                10,
                COLORS.LIGHT_OPACITY(0.6),
                'BOLD',
              ) }>
              {props.value.toString(6)}
            </Text>
          </Text>
        ) : null}
      </View>
      <View style={ styles.gasFeeContainerStyle }>
        <TouchableOpacity
          testID={ `gas-fee-minus-button-${props.id}` }
          style={ styles.addBtnStyle }
          onPress={ () => props.onClickDecrement() }>
          <CachedImage
            source={ IMAGES.MINUS_ICON }
            style={ COMMON_STYLE.imageStyle(6, COLORS.YELLOWFB) }
          />
        </TouchableOpacity>
        <View style={ styles.feeViewStyle }>
          <TextInput
            testID={ `gas-fee-input-${props.id}` }
            style={ [
              COMMON_STYLE.textStyle(11, COLORS.WHITE, 'BOLD', 'center'),
              { flex: 1, width: '100%' },
            ] }
            value={ props.valueText }
            keyboardType={ 'numeric' }
            onChangeText={ props.onChange }
          />
        </View>
        <TouchableOpacity
          testID={ `gas-fee-add-button-${props.id}` }
          style={ styles.addBtnStyle }
          onPress={ () => props.onClickIncrement() }>
          <CachedImage
            source={ IMAGES.ADD_ICON }
            style={ COMMON_STYLE.imageStyle(6, COLORS.YELLOWFB) }
          />
        </TouchableOpacity>
      </View>
      {props.isShowError && (
        <Text style={ COMMON_STYLE.textStyle(8, COLORS.ERROR) }>
          {props.error}
        </Text>
      )}
    </View>
  );
};

GasFeeUpdateView.propTypes = {
  gasRange: PropTypes.object,
  title: PropTypes.string,
  value: PropTypes.any,
  valueText: PropTypes.string,
  id: PropTypes.any,
  isShowError: PropTypes.bool,
  onClickDecrement: PropTypes.func,
  onChange: PropTypes.func,
  onClickIncrement: PropTypes.func,
  error: PropTypes.string,
};

export default GasFeeUpdateView;
