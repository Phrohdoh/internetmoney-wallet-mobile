import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  titleViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: Responsive.getHeight(2),
  },
  hideShowStyle: { width: '32%', height: Responsive.getHeight(3) },
  totalStyle: {
    ...COMMON_STYLE.textStyle(15, COLORS.WHITE, 'BASE', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  valueStyle: {
    ...COMMON_STYLE.textStyle(23, COLORS.YELLOWFB, 'BOLD', 'center'),
    marginTop: Responsive.getHeight(1),
  },
  flatlistStyle: {
    marginTop: Responsive.getHeight(3),
  },
  sepratorStyle: { height: Responsive.getHeight(1.5) },
  addBtnStyle: {
    width: Responsive.getWidth(15),
    height: Responsive.getWidth(15),
    borderRadius: Responsive.getWidth(7.5),
    position: 'absolute',
    right: 0,
    bottom: Responsive.getHeight(3),
  },
});
