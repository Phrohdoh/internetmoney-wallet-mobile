import React, { useState } from 'react';

import { View, Text, FlatList } from 'react-native';
import PropTypes from 'prop-types';

// import components
import {
  RoundedButton,
  AccountListCell,
  ModalContainerView,
  AddAccountView,
} from '@components';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

import {
  getTotalValue,
  getAccountList,
} from '@redux';
import { useSelector } from 'react-redux';

const AccountListComponent = props => {
  const accountList = useSelector(getAccountList);
  const [isShowBalance, setIsShowBalance] = useState(true);
  const [isShowAddAccount, setIsShowAddAccount] = useState(false);
  const [removingAccount, setRemovingAccount] = useState(false);

  const totalValue = useSelector(getTotalValue);

  return (
    <View style={ { flex: 1 } }>
      <Text style={ styles.totalStyle }>{localize('TOTAL_VALUE')}</Text>
      <Text numberOfLines={ 1 } style={ styles.valueStyle }>
        {isShowBalance ? `$${totalValue.toString()}` : '--'}
      </Text>
      <View style={ styles.titleViewStyle }>
        <RoundedButton
          testID={ 'add-import-account-button' }
          style={ styles.hideShowStyle }
          titleStyle={ COMMON_STYLE.textStyle(8, COLORS.BLACK, 'BOLD') }
          title={ localize('ADD_ACCOUNT') }
          onPress={ () => setIsShowAddAccount(true) }
        />
        <RoundedButton
          testID={ 'remove-account-button' }
          style={ styles.hideShowStyle }
          titleStyle={ COMMON_STYLE.textStyle(8, COLORS.BLACK, 'BOLD') }
          title={
            removingAccount
              ? localize('FINISHED_REMOVING_ACCOUNT')
              : localize('REMOVE_ACCOUNT')
          }
          onPress={ () => setRemovingAccount(!removingAccount) }
        />
        <RoundedButton
          testID={ 'acc-list-hideshow-btn' }
          style={ styles.hideShowStyle }
          titleStyle={ COMMON_STYLE.textStyle(8, COLORS.BLACK, 'BOLD') }
          title={ localize(
            isShowBalance ? 'HIDE_BALANCE_UPPERCASE' : 'SHOW_BALANCE_UPPERCASE',
          ) }
          onPress={ () => setIsShowBalance(!isShowBalance) }
        />
      </View>

      <FlatList
        style={ styles.flatlistStyle }
        data={ accountList }
        renderItem={ ({ item, index }) => (
          <AccountListCell
            key={ index + item + 'accList' }
            item={ item }
            index={ index }
            isShowAddress={ isShowBalance }
            isShowBalance={ isShowBalance }
            isRemoving={removingAccount}
            { ...props }
          />
        ) }
        ItemSeparatorComponent={ () => <View style={ styles.sepratorStyle } /> }
        keyExtractor={ (item, index) => index + item }
      />

      <ModalContainerView
        testID={ 'modal-add-account' }
        isVisible={ isShowAddAccount }
        onPressClose={ () => setIsShowAddAccount(false) }>
        <AddAccountView
          createAccountSuccess={ () => setIsShowAddAccount(false) }
          restoreAccountSuccess={ () => setIsShowAddAccount(false) }
          importAccountSuccess={ () => setIsShowAddAccount(false) }
        />
      </ModalContainerView>
    </View>
  );
};

AccountListComponent.propTypes = {
  accountsList: PropTypes.array,
};

export default AccountListComponent;
