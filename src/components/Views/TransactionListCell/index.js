import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { CachedImage } from '@components';
import PropTypes from 'prop-types';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';

// import utils
import { displayAddress } from '@utils';

// import themes
import { COMMON_STYLE, IMAGES } from '@themes';
import { useWalletConnect } from '@contexts/WalletConnectContext';

const TransactionListCell = props => {
  const { navigation, item } = props;
  const { sessions } = useWalletConnect();
  const { transactionDetail } = item;

  let peerMeta;
  if (item.type === 'dapp') {
    const isV2 = typeof transactionDetail.dappTransactionDetail.topic === 'string';
    peerMeta = isV2
      ? sessions.find(s => s.topic === transactionDetail.dappTransactionDetail.topic)?.peer?.metadata
      : transactionDetail.dappTransactionDetail.connection.peerMeta;
    peerMeta = peerMeta ?? {};
  }

  const getTransImage = () => {
    if (item.type === 'send') {
      return IMAGES.SEND_ICON;
    } else if (item.type === 'swap') {
      return IMAGES.SWAP_ICON;
    } else if (item.type === 'claim') {
      return IMAGES.TIME_TOKEN_ICON;
    } else if (item.type === 'dapp') {
      return IMAGES.DAPP_ICON;
    } else if (item.type === 'referral_claim') {
      return IMAGES.EARN_REFERRAL_ICON;
    } else {
      return IMAGES.RECEIVE_ICON;
    }
  };

  const getTransType = () => {
    if (item.type === 'send') {
      return localize('SEND');
    } else if (item.type === 'swap') {
      return localize('SWAP');
    } else if (item.type === 'claim') {
      return localize('CLAIM_DIV');
    } else if (item.type === 'dapp') {
      return localize('DAPP');
    } else if (item.type === 'referral_claim') {
      return localize('REFERRAL_EARNINGS');
    } else {
      return localize('RECEIVE');
    }
  };

  return (
    <TouchableOpacity
      style={ styles.cellStyle }
      onPress={ () =>
        navigation.navigate('TRANSACTION_DETAIL_SCREEN', { transaction: item })
      }>
      <CachedImage source={ getTransImage() } style={ COMMON_STYLE.imageStyle(8) } />
      <View style={ styles.transfarDetailViewStyle }>
        {item.type === 'dapp' ? (
          <Text numberOfLines={ 1 } style={ styles.typeStyle }>
            {getTransType() + ' ' + peerMeta.name}
          </Text>
        ) : item.type === 'referral_claim' ? (
          <Text numberOfLines={ 1 } adjustsFontSizeToFit style={ styles.typeStyle }>
            {getTransType() + ' IM'}
          </Text>
        ) : item.type !== 'claim' ? (
          <Text numberOfLines={ 1 } adjustsFontSizeToFit style={ styles.typeStyle }>
            {getTransType() + ' ' + transactionDetail.fromToken.symbol}
          </Text>
        ) : (
          <Text numberOfLines={ 1 } adjustsFontSizeToFit style={ styles.typeStyle }>
            {getTransType()}
          </Text>
        )}
        {transactionDetail.accountName ? (
          <Text style={ styles.accountStyle } numberOfLines={ 1 }>
            {transactionDetail.accountName}
          </Text>
        ) : (
          <Text style={ styles.accountStyle } numberOfLines={ 1 }>
            {displayAddress(transactionDetail.fromAddress)}
          </Text>
        )}
      </View>
      <View style={ styles.amountViewStyle }>
        <Text numberOfLines={ 1 } adjustsFontSizeToFit style={ styles.idStyle }>
          {transactionDetail.displayAmount.toString(6)}
        </Text>
        {transactionDetail.isSupportUSD && (
          <Text
            numberOfLines={ 1 }
            adjustsFontSizeToFit
            style={ styles.amountStyle }
          >
            ${transactionDetail.displayAmount.multiply(transactionDetail.usdValue).toString(2)}
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

TransactionListCell.propTypes = {
  navigation: PropTypes.any,
  item: PropTypes.object,
};
export default TransactionListCell;
