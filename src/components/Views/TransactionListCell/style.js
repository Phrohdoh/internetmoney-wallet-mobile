import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  cellStyle: {
    height: Responsive.getHeight(7),
    width: '100%',
    borderRadius: Responsive.getHeight(3.5),
    borderColor: COLORS.GRAY18,
    backgroundColor: COLORS.BLACK,
    borderWidth: 2,
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    paddingLeft: Responsive.getWidth(3),
    paddingRight: Responsive.getWidth(4),
  },

  transfarDetailViewStyle: { flex: 1, marginLeft: Responsive.getWidth(3) },
  accountStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.GRAYBF, 'BOLD'),
  },
  typeStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.WHITE, 'BOLD'),
  },
  idStyle: {
    ...COMMON_STYLE.textStyle(14, COLORS.YELLOWFB, 'BOLD', 'right'),
  },
  amountViewStyle: {
    flex: 1,
    justifyContent: 'center',
  },
  amountStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.GRAYBF, 'BOLD', 'right'),
  },
});
