import {StyleSheet} from 'react-native';

// import helpers
import {Responsive} from '@helpers';

// import themes
import {COLORS, COMMON_STYLE} from '@themes';

export const styles = StyleSheet.create({
  cellStyle: {
    height: Responsive.getHeight(5),
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: Responsive.getWidth(5),
  },
  rightArrowStyle: {
    ...COMMON_STYLE.imageStyle(4),
    marginLeft: Responsive.getWidth(3),
  },
  nameStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BOLD'),
    flex: 1,
  },
  addressStyle: {
    ...COMMON_STYLE.textStyle(13, COLORS.YELLOWFB, 'BOLD', 'right'),
    flex: 1,
  },
});
