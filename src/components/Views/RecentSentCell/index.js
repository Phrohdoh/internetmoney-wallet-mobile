import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

// import style
import { styles } from './style';

// import utils
import { displayAddress } from '@utils';

const RecentSentCell = props => {
  const { item, index, accounts } = props;

  const getAccountName = () => {
    const nameObj = accounts.accountList.find(
      account => account.publicAddress.toLowerCase() === item.toLowerCase(),
    );
    if (!nameObj) {
      return '';
    } else {
      return nameObj.name;
    }
  };

  return (
    <TouchableOpacity
      testID={ `recent-account-${index}` }
      style={ styles.cellStyle }
      onPress={ () => props.onSelectRecentAccount(item) }>
      <Text numberOfLines={ 1 } style={ styles.nameStyle }>
        {getAccountName()}
      </Text>
      <Text numberOfLines={ 1 } style={ styles.addressStyle }>
        {displayAddress(item)}
      </Text>
    </TouchableOpacity>
  );
};

RecentSentCell.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  onSelectRecentAccount: PropTypes.func,
  accounts: PropTypes.object,
};

export default RecentSentCell;
