import React from 'react';
import { CachedImage } from '@components';
import { IMAGES } from '@themes';
import { styles } from './style';

/**
 * Loading View
 */
const LoadingView = () => <CachedImage source={IMAGES.ROUNDED_LOADING} style={styles.loadingStyle} />;

export default LoadingView;
