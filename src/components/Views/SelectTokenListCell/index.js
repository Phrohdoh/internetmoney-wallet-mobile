import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { CachedImage } from '@components';

// import style

import { useTokenIcons } from '@utils';

// import themes
import { COMMON_STYLE } from '@themes';
import { styles } from './style';

const SelectTokenListCell = (props) => {
  const { item, index, networkObj } = props;
  const getTokenIcon = useTokenIcons();

  const displayedBalance = item.balance.value.toString(6, true);

  return (
    <TouchableOpacity
      testID={`choose-token-${index}`}
      style={styles.cellStyle}
      key={`${index + item}view1`}
      onPress={() => props.onSelectToken(item, index)}
    >
      <CachedImage
        source={getTokenIcon(item.address, networkObj.chainId)}
        style={COMMON_STYLE.imageStyle(6)}
      />
      <Text style={styles.tokenTitleStyle}>{item.symbol}</Text>
      <Text style={styles.tokenValueStyle} numberOfLines={1}>
        {displayedBalance}
      </Text>
    </TouchableOpacity>
  );
};

SelectTokenListCell.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  networkObj: PropTypes.object,
  onSelectToken: PropTypes.func,
};

export default SelectTokenListCell;
