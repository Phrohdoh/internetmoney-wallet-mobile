import {StyleSheet} from 'react-native';

// import helpers
import {Responsive} from '@helpers';

// import themes
import {COLORS, COMMON_STYLE} from '@themes';

export const styles = StyleSheet.create({
  cellStyle: {
    height: Responsive.getHeight(7),
    width: '100%',
    borderRadius: Responsive.getHeight(3.5),
    borderColor: COLORS.GRAY18,
    borderWidth: 2,
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: Responsive.getWidth(3),
    backgroundColor: COLORS.GRAY11,
  },
  tokenTitleStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BOLD'),
    marginHorizontal: Responsive.getWidth(2),
  },
  tokenValueStyle: {
    ...COMMON_STYLE.textStyle(13, COLORS.YELLOWFB, 'BOLD', 'right'),
    marginHorizontal: Responsive.getWidth(1),
    flex: 1,
  },
});
