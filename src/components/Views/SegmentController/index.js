import React, { useState, useRef } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Animated,
  ScrollView,
} from 'react-native';
import PropTypes from 'prop-types';
import { CachedImage } from '@components';

// import style
import { Validation } from '@utils';
import { styles } from './style';

const SegmentController = (props) => {
  const { segments } = props;

  const translation = useRef(new Animated.Value(0)).current;

  const [scrollWidth, setScrollWidth] = useState(0);

  const changePosition = (index) => {
    Animated.timing(translation, {
      toValue: index === 0 ? 0 : scrollWidth / 2,
      duration: 250,
      useNativeDriver: true,
    }).start();
  };

  const onPageLayout = (event) => {
    const { width } = event.nativeEvent.layout;
    setScrollWidth(width);
  };

  // render custom component
  const renderSegmentTab = (segment, index) => (
    <TouchableOpacity
      key={index + segment.title}
      style={styles.segmentTabStyle}
      testID={`${index + segment.title}-tab`}
      onPress={() => {
        changePosition(index);
        props.onChangeIndex(index);
      }}
    >
      {!Validation.isEmpty(segment.title) && (
        <Text
          style={
            props.isLineSegment
              ? styles.lineSegmentTextStyle(props.currentIndex === index)
              : styles.segmentTextStyle(props.currentIndex === index)
          }
        >
          {segment.title}
        </Text>
      )}
      {segment.image && (
        <CachedImage
          source={segment.image}
          style={[
            styles.segmentImageStyle(props.currentIndex === index),
            props.segmentImageStyle,
          ]}
        />
      )}
    </TouchableOpacity>
  );

  const renderLineSegment = () => (
    <View style={[styles.lineSegmentViewStyle, props.style]}>
      <ScrollView
        contentContainerStyle={{ width: '100%' }}
        horizontal
        onLayout={onPageLayout}
      >
        {segments.map((item, index) => renderSegmentTab(item, index))}
      </ScrollView>
      <Animated.View
        style={[
          styles.lineSegmentStyle(`${100 / segments.length}%`),
          { transform: [{ translateX: translation }] },
        ]}
      />
    </View>
  );

  const renderBorderSegment = () => (
    <View style={[styles.segmentViewStyle, props.style]}>
      <ScrollView
        contentContainerStyle={{ width: '100%' }}
        horizontal
        onLayout={onPageLayout}
      >
        <Animated.View
          style={[
            styles.segmentStyle(`${100 / segments.length}%`),
            { transform: [{ translateX: translation }] },
          ]}
        />
        {segments.map((item, index) => renderSegmentTab(item, index))}
      </ScrollView>
    </View>
  );
  return props.isLineSegment ? renderLineSegment() : renderBorderSegment();
};

SegmentController.defaultProps = {
  currentIndex: 0,
  isLineSegment: false,
};

SegmentController.propTypes = {
  segments: PropTypes.array,
  currentIndex: PropTypes.number,
  segmentImageStyle: PropTypes.any,
  style: PropTypes.any,
  onChangeIndex: PropTypes.func,
};

export default SegmentController;
