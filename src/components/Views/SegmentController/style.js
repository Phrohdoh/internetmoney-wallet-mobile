import { StyleSheet } from 'react-native';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  segmentViewStyle: {
    height: 50,
    width: '100%',
    borderRadius: 25,
    borderColor: COLORS.GRAY18,
    borderWidth: 2,
    flexDirection: 'row',
    overflow: 'hidden',
  },
  lineSegmentViewStyle: {
    height: 50,
    width: '100%',
    borderColor: COLORS.GRAY18,
    borderBottomWidth: 3,
    flexDirection: 'row',
    overflow: 'hidden',
  },
  segmentTabStyle: {
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  segmentTextStyle: isSelected => {
    return {
      ...COMMON_STYLE.textStyle(
        11,
        isSelected ? COLORS.BLACK : COLORS.LIGHT_OPACITY(0.2),
        'BOLD',
      ),
    };
  },
  lineSegmentTextStyle: isSelected => {
    return {
      ...COMMON_STYLE.textStyle(
        11,
        isSelected ? COLORS.YELLOWFB : COLORS.LIGHT_OPACITY(0.2),
        'BOLD',
      ),
    };
  },
  segmentImageStyle: isSelected => {
    return {
      width: '40%',
      aspectRatio: 1,
      tintColor: isSelected ? COLORS.BLACK : COLORS.YELLOWFB,
    };
  },
  segmentStyle: (width: '50%') => {
    return {
      width: width,
      backgroundColor: COLORS.YELLOWFB,
      height: '100%',
      position: 'absolute',
      borderRadius: 25,
    };
  },
  lineSegmentStyle: (width: '50%') => {
    return {
      width: width,
      height: 3,
      position: 'absolute',
      alignSelf: 'flex-end',
      borderRadius: 1.5,
      backgroundColor: COLORS.YELLOWFB,
    };
  },
});
