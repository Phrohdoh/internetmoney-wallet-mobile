import React from 'react';
import { Image } from 'react-native';
import FastImage from 'react-native-fast-image';

const CachedImage = (props) => {
  if (props?.source?.uri?.startsWith('http')) {
    return <FastImage {...props}/>;
  }
  return <Image {...props} />;
};

export default CachedImage;
