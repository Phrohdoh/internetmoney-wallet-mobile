import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  quoteCellStyle: {
    height: Responsive.getHeight(11.5),
    borderRadius: Responsive.getWidth(3),
    backgroundColor: COLORS.GRAY27,
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: Responsive.getWidth(2),
    marginHorizontal: Responsive.getWidth(2),
  },
  quoteCellStyleRecommended: {
    height: Responsive.getHeight(17),
    borderWidth: 1,
    borderColor: COLORS.YELLOWFB,
  },
  containerStyle: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: Responsive.getHeight(1.5),
    height: '100%',
  },
  detailContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: Responsive.getWidth(5),
  },
  recommended: { ...COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BOLD', 'center') },
  titleStyle: { ...COMMON_STYLE.textStyle(11, COLORS.GRAYCA) },
  subTitleStyle: (align = 'left', color = COLORS.WHITE) => {
    return { ...COMMON_STYLE.textStyle(11, color, 'BOLD', align) };
  },
  usdValue: { ...COMMON_STYLE.textStyle(10, COLORS.GRAYCA) },
  rightArrowStyle: {
    ...COMMON_STYLE.imageStyle(4),
    marginLeft: Responsive.getWidth(1),
  },
});
