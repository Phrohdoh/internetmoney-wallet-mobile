import React, { useCallback, useEffect, useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { CachedImage } from '@components';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

// import languages
import { localize } from '@languages';

// import style

// import themes
import { IMAGES, COLORS } from '@themes';
import { styles } from './style';
import { getUSDPrice } from '../../../web3-layer/web3Layer';
import { getNetwork } from '../../../redux/selectors';

const QuoteCell = (props) => {
  const { item, index, selectedToToken, onSelectQuote }
    = props;

  const quoteCellStyle = [styles.quoteCellStyle];

  if (index === 0) {
    quoteCellStyle.push(styles.quoteCellStyleRecommended);
  }
  
  const selectedNetwork = useSelector(getNetwork);

  const [toUSDPrice, setToUSDPrice] = useState();

  const fetchToUSDPrice = useCallback(async () => {
    const priceResult = await getUSDPrice(selectedToToken.address, selectedNetwork);
    if (priceResult.success) {
      setToUSDPrice(priceResult.usdPrice);
    }
  }, [selectedToToken.address]);

  useEffect(() => {
    fetchToUSDPrice();
  }, [fetchToUSDPrice]);

  const selectQuote = useCallback(() => {
    onSelectQuote(item);
  }, [item, onSelectQuote]);

  return (
    <TouchableOpacity
      style={quoteCellStyle}
      testID={`quote-item-${index}`}
      onPress={selectQuote}
    >
      <View style={styles.containerStyle}>
        {
          index === 0
            ? (
              <Text style={styles.recommended}>{localize('RECOMMENDED_OPTION')}</Text>
            )
            : null
        }
        <View style={styles.detailContainerStyle}>
          <View>
            <Text style={styles.titleStyle}>
              {`${localize('RECEIVING')} (${selectedToToken.symbol})`}
            </Text>
            <Text style={styles.subTitleStyle()}>
              {item.amountOut.toString(6)}
            </Text>
            {
              toUSDPrice === undefined
                ? null
                : (
                  <Text style={styles.usdValue}>
                    {localize('USD_UPPERCASE')} {localize('VALUE')}: ${toUSDPrice.multiply(item.amountOut).toString(2)}
                  </Text>
                )
            }
          </View>
          <View>
            <Text style={styles.titleStyle}>{localize('SOURCE')}</Text>
            <Text style={styles.subTitleStyle('right', COLORS.YELLOWFB)}>
              {item.dexName}
            </Text>
          </View>
        </View>
      </View>
      <CachedImage source={IMAGES.RIGHT_ARROW} style={styles.rightArrowStyle} />
    </TouchableOpacity>
  );
};

QuoteCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number,
  onSelectQuote: PropTypes.func.isRequired,
  selectedFromToken: PropTypes.object,
  selectedToToken: PropTypes.object,
  selectedNetwork: PropTypes.object,
};

export default QuoteCell;
