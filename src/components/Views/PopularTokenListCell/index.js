import { useTokenIcons } from '@utils';
import PropTypes from 'prop-types';
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { CachedImage } from '@components';

// import themes
import { COMMON_STYLE } from '@themes';
import { styles } from './style';

const PopularTokenListCell = (props) => {
  const { item, index, networkObj, onSelectToken } = props;
  const getTokenIcon = useTokenIcons();

  const renderCell = () => (
    <View style={styles.cellStyle} key={`${index + item}view1`}>
      <CachedImage
        source={getTokenIcon(item.address, networkObj.chainId)}
        style={COMMON_STYLE.imageStyle(6)}
      />
      <Text style={styles.tokenTitleStyle}>{item.symbol}</Text>
    </View>
  );

  return onSelectToken
    ? (
      <TouchableOpacity
        testID="token-list-cell-select"
        style={styles.cellStyle}
        key={`${index + item}view1`}
        onPress={() => onSelectToken(item)}
      >
        {renderCell()}
      </TouchableOpacity>
    )
    : (
      renderCell()
    );
};

PopularTokenListCell.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  networkObj: PropTypes.object,
  onSelectToken: PropTypes.func,
};
export default PopularTokenListCell;
