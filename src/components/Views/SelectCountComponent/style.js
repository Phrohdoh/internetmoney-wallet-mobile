import {StyleSheet} from 'react-native';

import {COMMON_STYLE, COLORS} from '@themes';
import {Responsive} from '@helpers';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  countContainerStyle: {
    flexDirection: 'row',
    marginVertical: Responsive.getHeight(1),
    alignItems: 'center',
  },

  countViewStyle: isSelected => {
    return {
      minWidth: Responsive.getHeight(7),
      paddingHorizontal: Responsive.getWidth(2),
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: isSelected ? COLORS.YELLOWFB : COLORS.GRAY15,
      borderColor: COLORS.GRAY1F,
      borderWidth: 3,
      borderRadius: Responsive.getHeight(3),
      padding: 10,
    };
  },
  countText: isSelected => {
    return {
      ...COMMON_STYLE.textStyle(
        12,
        isSelected ? COLORS.BLACK : COLORS.GRAYDF,
        'BOLD',
      ),
    };
  },
});
