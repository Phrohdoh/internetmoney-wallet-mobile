import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';

const SelectCountComponent = (props) => {
  const { style } = props;

  const renderCountCell = (count, index) => (
    <TouchableOpacity
      key={`${count}-${index}`}
      testID={`select-count-${index}`}
      style={styles.countContainerStyle}
      onPress={() => props.onPressCount(count)}
    >
      <View style={styles.countViewStyle(props.selectedCount === count)}>
        <Text style={styles.countText(props.selectedCount === count)}>
          {count}
        </Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={[styles.container, style]}>
      {props.wordCounts.map((item, index) => renderCountCell(item, index))}
    </View>
  );
};

SelectCountComponent.defaultProps = {
  wordCounts: [],
};

SelectCountComponent.propTypes = {
  wordCounts: PropTypes.array,
  selectedCount: PropTypes.number,
  style: PropTypes.any,
  onPressCount: PropTypes.func,
};
export default SelectCountComponent;
