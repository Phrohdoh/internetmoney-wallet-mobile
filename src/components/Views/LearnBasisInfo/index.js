import React from 'react';
import { Text, ScrollView, View } from 'react-native';
import { CachedImage } from '@components';
import PropTypes from 'prop-types';

// import style
import { COMMON_STYLE, IMAGES } from '@themes';
import { styles } from './style';

// import themes

const LearnBasisInfo = (props) => {
  const { item } = props;
  return (
    <View style={styles.backgroundStyle}>
      {item.image && (
        <CachedImage
          style={[styles.graphLogoStyle, props.imageStyle]}
          source={IMAGES[item.image]}
        />
      )}
      <ScrollView bounces={false}>
        <View style={styles.containerStyle}>
          <Text style={COMMON_STYLE.infoTitleStyle()}>{item.title}</Text>
          {item.subtitle && (
            <Text style={COMMON_STYLE.infoSubTitleStyle()}>
              {item.subtitle}
            </Text>
          )}
          {item.description && (
            <Text style={COMMON_STYLE.infoDescStyle()}>{item.description}</Text>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

LearnBasisInfo.propTypes = {
  item: PropTypes.object,
  imageStyle: PropTypes.any,
};

export default LearnBasisInfo;
