import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  backgroundStyle: {
    flex: 1,
  },
  graphLogoStyle: {
    width: '100%',
    height: Responsive.getHeight(55),
    alignSelf: 'center',
    borderBottomLeftRadius: Responsive.getWidth(6),
    borderBottomRightRadius: Responsive.getWidth(6),
    marginBottom: Responsive.getHeight(3),
  },
  containerStyle: {
    paddingHorizontal: Responsive.getWidth(6),
  },
});
