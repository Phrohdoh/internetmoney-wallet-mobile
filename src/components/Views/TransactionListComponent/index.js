import React, { useState, useEffect } from 'react';

import { View, FlatList } from 'react-native';
import PropTypes from 'prop-types';

// import components
import {
  RoundedButton,
  TransactionListCell,
  ModalContainerView,
  TransactionFilterComponent,
} from '@components';

// import languages
import { localize } from '@languages';

// import style
import { styles } from './style';
import { exportTransactions } from '@utils/transactions';

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

const TransactionListComponent = props => {
  const [isShowFilter, setIsShowFilter] = useState(false);
  const [filterTypeArray, setFilterTypeArray] = useState([]);
  const [filterAccount, setFilterAccount] = useState(undefined);
  const [filterAccountName, setFilterAccountName] = useState(undefined);
  const [filteredList, setFilteredList] = useState([]);

  useEffect(() => {
    getFilteredList();
  }, [filterTypeArray, filterAccount]);

  const getFilteredList = () => {
    let tempFilteredList = [];
    if (filterTypeArray.length > 0 && filterAccount) {
      tempFilteredList = props.transactionsList.filter(
        item =>
          item.transactionDetail.fromAddress === filterAccount &&
          filterTypeArray.includes(item.type),
      );
    } else if (filterTypeArray.length > 0) {
      tempFilteredList = props.transactionsList.filter(item =>
        filterTypeArray.includes(item.type),
      );
    } else if (filterAccount) {
      tempFilteredList = props.transactionsList.filter(
        item => item.transactionDetail.fromAddress === filterAccount,
      );
    } else {
      tempFilteredList = props.transactionsList;
    }

    const sortArray = tempFilteredList
      .slice()
      .sort((a, b) => parseFloat(b.blockNumber) - parseFloat(a.blockNumber));

    setFilteredList(sortArray);
  };

  return (
    <React.Fragment>
      <View style={ styles.titleViewStyle }>
        <RoundedButton
          testID="transaction-filter_button"
          style={ styles.filterBtnStyle }
          titleStyle={ COMMON_STYLE.textStyle(8, COLORS.BLACK, 'BOLD') }
          title={ localize('FILTER_UPPERCASE') }
          image={ IMAGES.DOWN_ARROW }
          imageStyle={ COMMON_STYLE.imageStyle(3, COLORS.BLACK) }
          onPress={ () => setIsShowFilter(true) }
        />
        <RoundedButton
          testID="transaction-filter_button"
          style={ styles.exportBtnStyle }
          titleStyle={ COMMON_STYLE.textStyle(8, COLORS.BLACK, 'BOLD') }
          title={ localize('EXPORT_TRANSACTION_UPPERCASE') }
          onPress={ () => exportTransactions(filteredList) }
        />
      </View>

      <FlatList
        style={ styles.flatlistStyle }
        data={ filteredList }
        renderItem={ ({ item, _index }) => (
          <TransactionListCell item={ item } { ...props } />
        ) }
        ItemSeparatorComponent={ () => <View style={ styles.sepratorStyle } /> }
        keyExtractor={ (item, index) => index + item }
      />

      <ModalContainerView
        testID={ 'modal-add-account' }
        isVisible={ isShowFilter }
        onPressClose={ () => setIsShowFilter(false) }>
        <TransactionFilterComponent
          transactionsList={ props.transactionsList }
          selectedFiltersArray={ filterTypeArray }
          selectedFilterAccount={ filterAccount }
          accountName={ filterAccountName }
          onSelectFilter={ (typeArray, account, accountName) => {
            setFilterTypeArray(typeArray);
            setFilterAccount(account);
            setFilterAccountName(accountName);
            setIsShowFilter(false);
          } }
        />
      </ModalContainerView>
    </React.Fragment>
  );
};

TransactionListComponent.propTypes = {
  transactionsList: PropTypes.array.isRequired,
};

export default TransactionListComponent;
