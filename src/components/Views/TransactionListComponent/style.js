import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  titleViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  filterBtnStyle: { width: '25%', height: Responsive.getHeight(3) },
  exportBtnStyle: { width: '44%', height: Responsive.getHeight(3) },
  flatlistStyle: {
    marginTop: Responsive.getHeight(3),
  },
  sepratorStyle: { height: Responsive.getHeight(1.5) },
});
