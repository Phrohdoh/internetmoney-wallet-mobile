import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  cellStyle: {
    height: Responsive.getHeight(6.5),
    width: '100%',
    flexDirection: 'row',
    overflow: 'hidden',
    alignItems: 'center',
    paddingHorizontal: Responsive.getWidth(3),
  },
  tokenInfo: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: Responsive.getWidth(2),
  },
  tokenBalanceRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  tokenUsdRow: {
    flex: 1,
    flexDirection: 'row',
  },
  tokenTitleStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BOLD'),
  },
  tokenBalanceStyle: {
    ...COMMON_STYLE.textStyle(11, COLORS.GRAYB5, 'BOLD', 'right'),
    flex: 1,
  },
  tokenPrice: {
    ...COMMON_STYLE.textStyle(10, COLORS.YELLOWFB, 'BOLD'),
    flex: 1,
  },
  balanceViewStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.YELLOWFB, 'BOLD'),
  },
  tokenValueStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.YELLOWFB, 'BOLD', 'right'),
  },
  deleteBtnStyle: {
    height: Responsive.getHeight(6.5),
    width: Responsive.getWidth(12),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
