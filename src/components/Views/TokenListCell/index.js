import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { useTokenIcons } from '@utils';
import { CachedImage } from '@components';

// import themes
import { COMMON_STYLE, IMAGES, COLORS } from '@themes';
import { styles } from './style';

const TokenListCell = (props) => {
  const {
    item,
    index,
    networkObj,
    onSelectToken,
    onPressDelete,
    isRemoveEnable,
  } = props;
  const getTokenIcon = useTokenIcons();

  const usdPrice = item.usdPrice?.value;
  const balance = item.balance?.value;
  const usdValue = (usdPrice === undefined || balance === undefined)
    ? undefined
    : balance.multiply(usdPrice);

  const valueDisplay = usdValue === undefined
    ? null
    : (
      <Text style={styles.tokenValueStyle} numberOfLines={1}>
        {`$${usdValue.toString(2)}`}
      </Text>
    );

  const priceDisplay = usdPrice === undefined
    ? null
    : (
      <Text style={styles.tokenPrice} numberOfLines={1}>
        {`$${usdPrice.toString(2, false, false, 2)}`}
      </Text>
    );

  const renderCell = () =>(
    <View style={styles.cellStyle} key={`${index + item}view1`}>
      <CachedImage
        source={getTokenIcon(item.address, networkObj.chainId)}
        style={COMMON_STYLE.imageStyle(6)}
      />
      <View style={styles.tokenInfo}>
        <View style={styles.tokenBalanceRow}>
          <Text style={styles.tokenTitleStyle}>{item.symbol}</Text>
          <Text style={styles.tokenBalanceStyle} numberOfLines={1}>
            {balance?.toString(6, true) ?? ''}
            {' '}
            {item.symbol}
          </Text>
        </View>
        {
          usdValue === undefined
            ? null
            : (
              <View style={styles.tokenUsdRow}>
                {priceDisplay}
                <View style={styles.balanceViewStyle}>
                  {valueDisplay}
                </View>
              </View>
            )
        }
      </View>
      { isRemoveEnable && !item.isNative && (
        <TouchableOpacity
          style={styles.deleteBtnStyle}
          onPress={() => onPressDelete(item)}
        >
          <CachedImage
            source={IMAGES.REMOVE_ICON}
            style={COMMON_STYLE.imageStyle(7, COLORS.ERROR)}
          />
        </TouchableOpacity>
      )}
    </View>
  );

  return onSelectToken ? (
    <TouchableOpacity
      testID="token-list-cell-select"
      style={styles.cellStyle}
      key={`${index + item}view1`}
      onPress={() => onSelectToken(item)}>
      {renderCell()}
    </TouchableOpacity>
  ) : (
    renderCell()
  );

  // return (
  //   <React.Fragment>
  //     {onSelectToken ? (
  //       <TouchableOpacity
  //         testID="token-list-cell-select"
  //         style={styles.cellStyle}
  //         key={`${index + item}view1`}
  //         onPress={() => onSelectToken(item)}>
  //         {renderCell()}
  //       </TouchableOpacity>
  //     ) : (
  //       renderCell()
  //     )}
  //   </React.Fragment>
  // );
};

TokenListCell.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  networkObj: PropTypes.object,
  onSelectToken: PropTypes.func,
  onPressDelete: PropTypes.func,
  isRemoveEnable: PropTypes.bool,
};
export default TokenListCell;
