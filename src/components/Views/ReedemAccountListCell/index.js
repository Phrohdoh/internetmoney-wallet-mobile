import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

// import style
import { styles } from './style';

const ReedemAccountListCell = (props) => {
  const { item, index } = props;

  return (
    <TouchableOpacity
      testID={`redeem-gift-account-selection-button-${index}`}
      style={styles.cellStyle}
      key={`${index + item}view1`}
      onPress={() => props.onSelectAccount(item, index)}
    >
      <Text style={styles.nameStyle}>{item.name}</Text>
    </TouchableOpacity>
  );
};

ReedemAccountListCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  onSelectAccount: PropTypes.func.isRequired,
};
export default ReedemAccountListCell;
