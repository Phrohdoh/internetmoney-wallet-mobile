import React from 'react';
import { View, Text } from 'react-native';
import { CachedImage } from '@components';

// import languages
import { localize } from '@languages';

// import style
import { IMAGES } from '@themes';
import { styles } from './style';

// import themes

const JailBreakError = () => (
  <View style={styles.containerStyle}>
    <CachedImage style={[styles.imageStyle]} source={IMAGES.ALERT_ICON} />

    <Text style={styles.alertMsgStyle}>
      {localize('JAILBREAK_ERROR_MSG')}
    </Text>
  </View>
);

export default JailBreakError;
