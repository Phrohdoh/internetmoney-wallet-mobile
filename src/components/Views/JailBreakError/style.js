import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  imageStyle: {
    width: Responsive.getWidth(30),
    height: Responsive.getWidth(30),
    resizeMode: 'contain',
  },
  containerStyle: {
    width: Responsive.getWidth(100),
    height:Responsive.getHeight(100),
    borderRadius: Responsive.getWidth(3),
    backgroundColor: COLORS.GRAY1A,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: Responsive.getWidth(5),
    paddingBottom: Responsive.getHeight(5),
    paddingTop: Responsive.getHeight(3),
  },
  alertMsgStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.WHITE, 'BASE', 'center'),
    marginBottom: Responsive.getHeight(3),
  },
});
