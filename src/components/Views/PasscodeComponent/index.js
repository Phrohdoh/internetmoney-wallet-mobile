import React, { useState } from 'react';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';
import PropTypes from 'prop-types';
import { CachedImage } from '@components';

// import style
import { styles } from './style';

// import themes
import { COMMON_STYLE, IMAGES, COLORS } from '@themes';

const numberList = [
  { title: '1', value: '1' },
  { title: '2', value: '2' },
  { title: '3', value: '3' },
  { title: '4', value: '4' },
  { title: '5', value: '5' },
  { title: '6', value: '6' },
  { title: '7', value: '7' },
  { title: '8', value: '8' },
  { title: '9', value: '9' },
  { title: '', value: '' },
  { title: '0', value: '0' },
  { title: 'remove', value: 'remove', image: IMAGES.DELETE_TEXT_ICON },
];
const PasscodeComponent = props => {
  const passcodeCount = 6;
  const [selectedKey, setSelectedKey] = useState(null);

  const onPressKey = item => {
    // setSelectedKey(item.value);
    props.onPressKey(item);
  };

  const renderKey = item => {
    return item.title ? (
      <TouchableOpacity
        key={ 'passcode-' + item.title }
        testID={ 'passcode-' + item.title }
        style={ [
          styles.keyViewStyle,
          {
            backgroundColor:
              selectedKey === item.value ? COLORS.YELLOWFB : 'transparent',
          },
        ] }
        onPress={ () => onPressKey(item) }
        onPressIn={ () => setSelectedKey(item.value) }
        onPressOut={ () => setSelectedKey(null) }>
        {item.image ? (
          <CachedImage source={ item.image } style={ COMMON_STYLE.imageStyle(5) } />
        ) : (
          <Text style={ COMMON_STYLE.textStyle(16, COLORS.WHITE, 'BOLD') }>
            {item.title}
          </Text>
        )}
      </TouchableOpacity>
    ) : (
      <View style={ styles.emptyViewStyle } />
    );
  };

  const renderPasscodeDot = index => {
    return (
      <View
        key={ 'dot-' + index }
        style={ styles.dotStyle(props.passcode.length > index) }
      />
    );
  };
  const header = () => {
    return (
      <View>
        {props.headerComp ? props.headerComp : null}

        <Text style={ styles.titleStyle }>{props.title}</Text>
        <View style={ styles.dotContainerStyle }>
          {[...Array(passcodeCount)].map((item, index) =>
            renderPasscodeDot(index),
          )}
        </View>
      </View>
    );
  };
  return (
    <View style={ styles.containerStyle }>
      <View style={ { flex: 1 } } />
      <FlatList
        key={ 'keyNumberGridView' }
        style={ styles.flatListStyle }
        columnWrapperStyle={ styles.flatListContainerStyle }
        data={ numberList }
        numColumns={ 3 }
        renderItem={ ({ item, index }) => renderKey(item, index) }
        keyExtractor={ (item, index) =>
          index + item.toString() + 'keyboardGridView'
        }
        ItemSeparatorComponent={ () => <View style={ styles.sepratorStyle } /> }
        showsVerticalScrollIndicator={ false }
        ListHeaderComponent={ header }
      />
    </View>
  );
};

PasscodeComponent.defaultProps = {
  passcode: '',
};

PasscodeComponent.propTypes = {
  title: PropTypes.string,
  passcode: PropTypes.string,
  onPressKey: PropTypes.func,
  headerComp: PropTypes.any,
};

export default PasscodeComponent;
