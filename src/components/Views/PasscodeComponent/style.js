import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

// import themes
import { COMMON_STYLE, COLORS } from '@themes';

export const styles = StyleSheet.create({
  containerStyle: { flex: 1 },
  titleStyle: {
    ...COMMON_STYLE.textStyle(10, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
  },
  dotContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: Responsive.getHeight(3),
  },
  dotStyle: isAdded => {
    return {
      width: 14,
      height: 14,
      borderRadius: 7,
      borderWidth: isAdded ? 0 : 3,
      borderColor: COLORS.GRAY1F,
      backgroundColor: isAdded ? COLORS.YELLOWFB : COLORS.GRAY15,
      margin: Responsive.getWidth(2),
    };
  },
  keyViewStyle: {
    width: Responsive.getWidth(14),
    height: Responsive.getWidth(14),
    borderRadius: Responsive.getWidth(7),
    borderColor: COLORS.GRAY1F,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyViewStyle: {
    width: Responsive.getWidth(14),
    height: Responsive.getWidth(14),
  },
  flatListStyle: {
    width: '100%',
  },
  flatListContainerStyle: { justifyContent: 'space-around' },
  sepratorStyle: {
    height: Responsive.getHeight(3),
  },
});
