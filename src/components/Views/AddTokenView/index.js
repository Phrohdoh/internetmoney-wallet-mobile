import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Pressable, Text, View } from 'react-native';
import { useSelector } from 'react-redux';

import { styles } from './style';

import { ImportTokenView, LoadingView, PopularTokensView, SegmentController } from '@components';
import { localize } from '@languages';
import { getTokensByAddress } from '@redux';

const AddTokenView = props => {
  const { account, setIsShowAddToken } = props;

  const segmentList = [
    { title: localize('POPULAR_TOKENS_UPPERCASE') },
    { title: localize('IMPORT_TOKEN_UPPERCASE') },
  ];

  const [currentIndex, setCurrentIndex] = useState(0); // 0 = Popular Tokens, 1 = Import Token

  const tokens = useSelector(getTokensByAddress)[account.publicAddress.toLowerCase()];

  return (
    <React.Fragment>
      <View style={ styles.headerStyle }>
        <Text style={ styles.titleStyle }>{localize('ADD_TOKEN_UPPERCASE')}</Text>
        <Pressable onPress={() => setIsShowAddToken(false)} hitSlop={5}>
          <Text style={ styles.closeIconStyle }>&times;</Text>
        </Pressable>
      </View>
      {tokens ? (
        <React.Fragment>
          <View>
            <SegmentController
              isLineSegment
              style={styles.segmentContainerStyle}
              segments={segmentList}
              currentIndex={currentIndex}
              onChangeIndex={index => {
                setCurrentIndex(index);
              }}
            />
          </View>
          <View style={ styles.segmentTabsStyle }>
            {currentIndex === 0 &&
              <PopularTokensView
                account={account}
                onAddPopularTokenSuccess={props.onAddPopularTokenSuccess}
              />
            }
            {currentIndex === 1 &&
              <ImportTokenView
                account={account}
                tokens={tokens}
                onImportTokenSuccess={props.onImportTokenSuccess}
              />
            }
          </View>
        </React.Fragment>
      ) : (
        <LoadingView />
      )}
    </React.Fragment>
  );
};

AddTokenView.propTypes = {
  account: PropTypes.object.isRequired,
  setIsShowAddToken: PropTypes.func.isRequired,
  onAddPopularTokenSuccess: PropTypes.func,
  onImportTokenSuccess: PropTypes.func,
};

export default AddTokenView;
