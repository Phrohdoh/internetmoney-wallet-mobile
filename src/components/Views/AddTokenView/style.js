import { StyleSheet } from 'react-native';

import { Responsive } from '@helpers';
import { COLORS, COMMON_STYLE, STYLES } from '@themes';

export const styles = StyleSheet.create({
  ...COMMON_STYLE,
  inputContainerStyle: {
    marginBottom: Responsive.getHeight(3),
  },
  importTokenMsgStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.LIGHT_OPACITY(0.6), 'BASE', 'center'),
    marginBottom: Responsive.getHeight(1),
  },
  scamMsgStyle: {
    ...COMMON_STYLE.textStyle(15, COLORS.YELLOWFB, 'BOLD', 'center'),
    marginBottom: Responsive.getHeight(2),
  },
  segmentContainerStyle: { marginVertical: Responsive.getHeight(2) },
  segmentTabsStyle: {
    height: Responsive.getHeight(55),
  },
  loadingStyle: {
    ...COMMON_STYLE.imageStyle(12),
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  headerStyle: {
    flexDirection: 'row',
  },
  titleStyle: {
    ...STYLES.textStyle(12, COLORS.GRAYB5, 'BOLD', 'center'),
    flex: 1,
  },
  closeIconStyle: {
    ...STYLES.textStyle(25, COLORS.GRAYB5, 'BOLD', 'center'),
    position: 'absolute',
    top: -12,
    right: 0,
  },
});
