import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  stepsContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: Responsive.getHeight(2),
  },
  stepCountViewStyle: isSelected => {
    return {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: isSelected ? COLORS.YELLOWFB : COLORS.GRAY15,
      width: Responsive.getWidth(8),
      height: Responsive.getWidth(8),
      borderRadius: Responsive.getWidth(4),
      borderWidth: 3,
      borderColor: isSelected ? COLORS.YELLOWFB : COLORS.GRAY1C,
    };
  },
  stepCountStyle: isSelected => {
    return {
      ...COMMON_STYLE.textStyle(
        13,
        isSelected ? COLORS.BLACK : COLORS.LIGHT_OPACITY(0.2),
        'BOLD',
      ),
    };
  },
  lineStyle: {
    height: 3,
    width: Responsive.getWidth(20),
    backgroundColor: COLORS.GRAY15,
  },
  sepratorLineStyle: (selectedindex, currentIndex) => {
    return {
      height: 3,
      width:
        currentIndex + 1 === selectedindex
          ? '50%'
          : currentIndex + 1 < selectedindex
            ? '100%'
            : '0%',
      borderTopRightRadius: 1.5,
      borderBottomRightRadius: 1.5,
      backgroundColor: COLORS.YELLOWFB,
    };
  },
});
