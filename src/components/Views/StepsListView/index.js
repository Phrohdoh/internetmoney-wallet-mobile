import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';

// import style
import { styles } from './style';

const steps = [1, 2, 3];
const StepsListView = (props) => {
  const { selectedSteps } = props;

  return (
    <View style={styles.stepsContainerStyle}>
      {steps.map((item, index) => (
        <View
          style={styles.stepsContainerStyle}
          key={`${item + index}viewcontainer`}
        >
          <View
            key={`${item + index}count`}
            style={styles.stepCountViewStyle(item <= selectedSteps)}
          >
            <Text style={styles.stepCountStyle(item <= selectedSteps)}>
              {item}
            </Text>
          </View>
          {index < steps.length - 1 && (
            <View key={`${item + index}line`} style={styles.lineStyle}>
              <View style={styles.sepratorLineStyle(selectedSteps, index)} />
            </View>
          )}
        </View>
      ))}
    </View>
  );
};

StepsListView.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  selectedSteps: PropTypes.number,
};
export default StepsListView;
