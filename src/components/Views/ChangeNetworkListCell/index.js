import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { CachedImage } from '@components';

// import style

// import themes
import { COLORS, COMMON_STYLE, IMAGES } from '@themes';

// import utils
import { useTokenIcons } from '@utils';
import { styles } from './style';

const ChangeNetworkListCell = (props) => {
  const { item, index, isShowSwapSupported, swapSupportedList } = props;
  const isSwapSupport = swapSupportedList.includes(item.chainId.toString());
  const getTokenIcon = useTokenIcons();

  return (
    <TouchableOpacity
      style={styles.cellStyle(!isSwapSupport && isShowSwapSupported)}
      testID={`select-network-${index}`}
      onPress={() => {
        if (isShowSwapSupported) {
          if (isSwapSupport) {
            props.onSelectNetwork(item);
          }
        } else {
          props.onSelectNetwork(item);
        }
      }}
      activeOpacity={0}
    >
      <CachedImage
        style={COMMON_STYLE.imageStyle(6)}
        source={getTokenIcon('', item.chainId)}
      />
      <Text style={styles.networkNameStyle} numberOfLines={1}>
        {item.networkName}
      </Text>
      {item.rpc === props.selectedNetwork.rpc && (
        <CachedImage
          style={COMMON_STYLE.imageStyle(6, COLORS.YELLOWFB)}
          source={IMAGES.check}
        />
      )}
    </TouchableOpacity>
  );
};

ChangeNetworkListCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  onSelectNetwork: PropTypes.func.isRequired,
  selectedNetwork: PropTypes.object.isRequired,
  swapSupportedList: PropTypes.array,
  isShowSwapSupported: PropTypes.bool,
};

export default ChangeNetworkListCell;
