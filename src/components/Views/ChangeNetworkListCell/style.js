import { StyleSheet } from 'react-native';

// import helpers
import { Responsive } from '@helpers';

// import themes
import { COLORS, COMMON_STYLE } from '@themes';

export const styles = StyleSheet.create({
  cellStyle: isDisable => {
    return {
      flexDirection: 'row',
      alignItems: 'center',
      padding: Responsive.getHeight(1.5),
      opacity: isDisable ? 0.5 : 1,
    };
  },
  networkNameStyle: {
    ...COMMON_STYLE.textStyle(12, COLORS.WHITE),
    marginLeft: Responsive.getWidth(3),
    flex: 1,
  },
});
