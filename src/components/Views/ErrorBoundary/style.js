import { StyleSheet } from 'react-native';
import { Responsive } from '@helpers';

export const styles = StyleSheet.create({
  errorBoundary: {
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    padding: Responsive.getHeight(2),
    backgroundColor: 'black',
  },
  errorText: {
    color: 'white',
    marginTop: Responsive.getHeight(4),
    paddingTop: Responsive.getHeight(4),
    marginBottom: Responsive.getHeight(2),
    paddingBottom: Responsive.getHeight(2),
  },
  errorMessage: {
    color: 'white',
    marginTop: Responsive.getHeight(2),
    paddingTop: Responsive.getHeight(2),
    marginBottom: Responsive.getHeight(4),
    paddingBottom: Responsive.getHeight(4),
    fontSize: 12,
  },
});
