import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { localize } from '@languages';
import { ScrollView, Text, View } from 'react-native';
import RoundedButton from '../../buttons/RoundedButton';
import { styles } from './style';

const ErrorBoundaryDisplay = ({ errorMessage }) => {
  const [displayError, setDisplayError] = useState(false);
  const toggle = useCallback(() => {
    setDisplayError(!displayError);
  }, [displayError]);
  return (
    <View style={styles.errorBoundary}>
      <ScrollView>
        <Text style={styles.errorText}>{localize('UNEXPECTED_ERROR')}</Text>
        <RoundedButton title={displayError ? localize('HIDE_ERROR') : localize('SHOW_ERROR')} onPress={toggle} />
        {
          displayError
            ? <Text style={styles.errorMessage}>{errorMessage}</Text>
            : null
        }
      </ScrollView>
    </View>
  );
};

ErrorBoundaryDisplay.propTypes = {
  errorMessage: PropTypes.string,
};

export default ErrorBoundaryDisplay;
