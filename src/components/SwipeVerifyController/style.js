/* istanbul ignore file */

import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  sliderContainerStyle: (borderColor, buttonSize) => ({
    borderColor,
    borderWidth: 3,
    borderRadius: buttonSize / 2 + 3,
    overflow: 'hidden',
  }),
  containerStyle: backgroundColor => ({ backgroundColor, justifyContent: 'center' }),
  childViewStyle: {
    position: 'absolute',
    alignSelf: 'center',
  },
  panButtonStyle: (buttonSize, btnColor) => ({
    width: buttonSize,
    height: buttonSize,
    borderRadius: buttonSize / 2,
    backgroundColor: btnColor,
    justifyContent: 'center',
    alignItems: 'center',
  }),
  progressViewStyle: (buttonSize, progressColor) => ({
    height: '100%',
    backgroundColor: progressColor,
    position: 'absolute',
    borderRadius: buttonSize / 2,
  }),
});
