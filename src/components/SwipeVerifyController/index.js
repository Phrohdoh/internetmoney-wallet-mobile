/* istanbul ignore file */

// >>>>>>>>>> TODO <<<<<<<<<<<<<
// >>>>>>>>>> Convert into function component <<<<<<<<<<<<<

import React, { Component } from 'react';
import { View, PanResponder, Animated, UIManager } from 'react-native';
import PropTypes from 'prop-types';
import { COLORS } from '@themes';
import { Responsive } from '@helpers';
import { styles } from './style';

// Enable LayoutAnimation on Android
if (UIManager.setLayoutAnimationEnabledExperimental) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

// default props value
const defaultProps = {
  buttonSize: Responsive.getHeight(7),
  backgroundColor: COLORS.GRAY15,
  buttonColor: COLORS.YELLOWFB,
  borderColor: COLORS.GRAY1F,
  progressColor: COLORS.YELLOW77,
};

class SwipeVerifyController extends Component {
  constructor(props) {
    super(props);

    this.state = {
      drag: new Animated.ValueXY(),
      moving: false,
      verify: false,
      isReset: false,
      percent: 0,
      position: { x: 0, y: 0 },
      dimensions: { width: 0, height: 0 },
      progress: new Animated.Value(50),
    };

    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderGrant: () => {
        const positionXY = this.state.drag.__getValue();
        this.state.drag.setOffset(positionXY);
        this.state.drag.setValue({ x: 0, y: 0 });
      },
      onPanResponderMove: Animated.event([null, { dx: this.state.drag.x }], {
        // limit sliding out of box
        useNativeDriver: false,

        listener: (event, gestureState) => {
          const { buttonSize } = this.props;

          const {
            drag,
            verify,
            dimensions: { width },
          } = this.state;

          const maxMoving = width - buttonSize;

          let toX = gestureState.dx;

          if (toX < 0) {
            toX = 0;
          }
          if (toX > maxMoving) {
            toX = maxMoving;
          }
          const percent = ((toX * 100) / maxMoving).toFixed();
          this.setState({ percent });

          if (verify) {
            drag.setValue({ x: 0, y: 0 });
            return;
          }
          drag.setValue({ x: toX, y: 0 });
        },
      }),
      onPanResponderRelease: () => {
        if (this.state.verify) {
          return;
        }
        if (this.state.percent >= 100) {
          this.props.onVerified();
          if (!this.props.isSetReset) {
            this.setState({ moving: false, verify: true });
            // communicate that the verification was successful
          } else {
            this.reset();
          }
        } else if (!this.state.verify) {
          this.reset();
        }
      },
      onPanResponderTerminate: () => {
        // Another component has become the responder, so this gesture
        // should be cancelled
      },
    });
  }

  reset() {
    this.state.drag.setOffset({ x: 0, y: 0 });

    Animated.parallel([
      Animated.timing(this.state.drag, {
        toValue: { x: 0, y: 0 },
        duration: 300,
        useNativeDriver: false,
      }),
    ]).start(() => {
      this.setState({ percent: 0, isReset: false });
    });

    this.setState({ moving: false, verify: false, isReset: true });
  }

  countProgressWidth() {
    const widthPer = (this.state.dimensions.width / 100) * this.state.percent;

    let extraWidth = 0;
    let duration = 350;
    if (this.state.percent === 0) {
      extraWidth = 0;
    } else if (this.state.percent < 25) {
      duration = 500;
      extraWidth = this.props.buttonSize * 0.75;
    } else if (this.state.percent < 50) {
      extraWidth = this.props.buttonSize * 0.5;
    } else if (this.state.percent < 75) {
      extraWidth = this.props.buttonSize * 0.25;
    } else {
      extraWidth = 0;
    }
    const progressWidth = widthPer + extraWidth;

    if (!this.state.verify) {
      if (!this.state.isReset) {
        Animated.timing(this.state.progress, {
          toValue: progressWidth,
          duration: 0,
          useNativeDriver: false,
        }).start();
      } else {
        Animated.timing(this.state.progress, {
          toValue: 0,
          duration,
          useNativeDriver: false,
        }).start();
      }
    }

    const barWidth = {
      width: this.state.progress,
    };

    return barWidth;
  }

  render() {
    const {
      buttonColor,
      buttonSize,
      borderColor,
      backgroundColor,
      icon,
      style,
      progressColor,
    } = this.props;

    const position = { transform: this.state.drag.getTranslateTransform() };

    return (
      <View
        style={[styles.sliderContainerStyle(borderColor, buttonSize), style]}
      >
        <View
          onLayout={(event) => {
            const { x, y, width, height } = event.nativeEvent.layout;
            this.setState({
              dimensions: { width, height },
              position: { x, y },
            });
          }}
          style={[styles.containerStyle(backgroundColor, buttonSize)]}
        >
          {this.props.children && (
            <View style={styles.childViewStyle}>{this.props.children}</View>
          )}

          <Animated.View
            style={[
              this.countProgressWidth(),
              styles.progressViewStyle(buttonSize, progressColor),
            ]}
          />

          <Animated.View
            {...this._panResponder.panHandlers}
            style={[position, styles.panButtonStyle(buttonSize, buttonColor)]}
          >
            {icon}
          </Animated.View>
        </View>
      </View>
    );
  }
}

SwipeVerifyController.defaultProps = defaultProps;

SwipeVerifyController.propTypes = {
  buttonSize: PropTypes.any,
  backgroundColor: PropTypes.string,
  buttonColor: PropTypes.string,
  borderColor: PropTypes.string,
  progressColor: PropTypes.string,
  style: PropTypes.any,
  icon: PropTypes.any,
  children: PropTypes.any,
  isSetReset: PropTypes.bool,
  onVerified: PropTypes.func,
};
export default SwipeVerifyController;
