// COMOPONENT OR LIBRARY IMPORT
import React from 'react';
import { KeyboardAvoidingView, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { COMMON_DATA } from '@constants';

const KeyboardAvoidScrollView = (props) => {
  const { children } = props;
  return (
    <KeyboardAvoidingView
      style={{ flex: 1 }}
      behavior={COMMON_DATA.DEVICE_TYPE === 'ANDROID' ? null : 'padding'}
    >
      <ScrollView
        bounces={false}
        contentContainerStyle={props.contentContainerStyle}
        keyboardShouldPersistTaps="handled"
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        nestedScrollEnabled
      >
        {children}
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

KeyboardAvoidScrollView.propTypes = {
  children: PropTypes.any,
  contentContainerStyle: PropTypes.any,
};
export default KeyboardAvoidScrollView;
