import React, { createContext, useCallback, useContext, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Logger from '@utils/logger';
import {
  getSessions,
  getAuthRequests,
  getSessionProposals,
  getSessionRequests,
} from '@utils/walletConnect';
import { useSelector } from 'react-redux';
import {
  getCompletedAuthRequestIds,
  getCompletedSessionProposalIds,
  getCompletedSessionRequestIds,
  getRequestVerifications,
} from '@redux';

const WalletConnectContext = createContext({
  sessions: [],
  authRequests: [],
  sessionProposals: [],
  sessionRequests: [],
});

export const useWalletConnect = () => {
  return useContext(WalletConnectContext);
};

const useDeepEqualityState = (initialValue) => {
  const [value, setValue] = useState(initialValue);
  const set = useCallback((nextValue) => {
    if (JSON.stringify(value) !== JSON.stringify(nextValue)) {
      setValue(nextValue);
    }
  }, [value, setValue]);
  return [value, set];
}

export const WalletConnectProvider = ({ children }) => {
  const [sessions, setSessions] = useDeepEqualityState([]);
  const [authRequests, setAuthRequests] = useDeepEqualityState([]);
  const [sessionProposals, setSessionProposals] = useDeepEqualityState([]);
  const [sessionRequests, setSessionRequests] = useDeepEqualityState([]);
  const completedAuthRequestIds = useSelector(getCompletedAuthRequestIds);
  const completedSessionProposalIds = useSelector(getCompletedSessionProposalIds);
  const completedSessionRequestIds = useSelector(getCompletedSessionRequestIds);
  const requestVerifications = useSelector(getRequestVerifications);

  const getData = useCallback(async () => {
    try {
      setSessions(await getSessions());
      setAuthRequests(await getAuthRequests());
      setSessionProposals(await getSessionProposals());
      setSessionRequests(await getSessionRequests());
    } catch (e) {
      // Error while getting wallet connect data
    }
  }, [setSessions, setAuthRequests, setSessionProposals, setSessionRequests]);

  useEffect(() => {
    const interval = setInterval(getData, 100);
    return () => clearInterval(interval);
  }, [getData]);

  const value = {
    sessions,
    authRequests: authRequests
      .filter(authRequest => !completedAuthRequestIds.includes(authRequest.id))
      .map(authRequest => ({
        ...authRequest,
        verification: requestVerifications[authRequest.id] ?? 'UNKNOWN',
      })),
    sessionProposals: sessionProposals
      .filter(sessionProposal => !completedSessionProposalIds.includes(sessionProposal.id))
      .map(sessionProposal => ({
        ...sessionProposal,
        verification: requestVerifications[sessionProposal.id] ?? 'UNKNOWN',
      })),
    sessionRequests: sessionRequests
      .filter(sessionRequest => sessions.some(session => session.topic === sessionRequest.topic))
      .filter(sessionRequest => !completedSessionRequestIds.includes(sessionRequest.id))
      .map(sessionRequest => ({
        ...sessionRequest,
        verification: requestVerifications[sessionRequest.id] ?? 'UNKNOWN',
      })),
  };

  return (
    <WalletConnectContext.Provider value={value}>
      {children}
    </WalletConnectContext.Provider>
  );
};

WalletConnectProvider.propTypes = {
  children: PropTypes.object,
};