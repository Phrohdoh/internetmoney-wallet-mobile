import { Platform } from 'react-native';

export const COMMON_DATA = {
  ENGLISH_LANG: 'en',
  DEVICE_TYPE: Platform.OS.toLocaleUpperCase(),

  // Terms & Privacy
  PRIVACY_POLICY_LINK: 'https://internetmoney.io/privacy-policy',
  TERMS_OF_SERVICE_LINK: 'https://internetmoney.io/terms-of-service',
  ABOUNT_LINK: 'https://internetmoney.io/about',

  // TIME Token
  TIME_LINK: 'https://internetmoney.io/time',

  // Social Media
  TELEGRAM_LINK: 'https://t.me/internetmoneyio',
  REDDIT_LINK: 'https://www.reddit.com/r/internetmoneyio/',
  TWITTER_LINK: 'https://twitter.com/internetmoneyio',
  SITE_LINK: 'https://internetmoney.io/',
};
