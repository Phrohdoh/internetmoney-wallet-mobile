/* istanbul ignore file */
import { NativeModules } from 'react-native';
import { Core } from '@walletconnect/core';
import { Web3Wallet } from '@walletconnect/web3wallet';
import { buildApprovedNamespaces, getSdkError } from '@walletconnect/utils';
import { store, GlobalAction, WalletConnectAction } from '@redux';
import { localize } from '@languages';
import { parseObject } from '@utils';
import {
  ASYNC_KEYS,
  WALLET_CONNECT_PROJECT_ID,
  WALLET_CONNECT_PROJECT_NAME,
  WALLET_CONNECT_PROJECT_DESCRIPTION,
  WALLET_CONNECT_PROJECT_URL,
} from '@constants';
import { StorageOperation } from '@storage';
import Logger from './logger';
import { getSelectedWalletConnectAccount } from '../redux';

const core = new Core({
  projectId: WALLET_CONNECT_PROJECT_ID,
});

let web3WalletPromise;

const listenForVerifyContext = async (web3WalletPromise) => {
  const web3Wallet = await web3WalletPromise;
  web3Wallet.on('session_proposal', verifyRequest);
  web3Wallet.on('session_request', verifyRequest);
  web3Wallet.on('auth_request', verifyRequest);
};

export const getWeb3Wallet = async () => {
  if (web3WalletPromise === undefined) {
    web3WalletPromise = Web3Wallet.init({
      core,
      metadata: {
        name: WALLET_CONNECT_PROJECT_NAME,
        description: WALLET_CONNECT_PROJECT_DESCRIPTION,
        url: WALLET_CONNECT_PROJECT_URL,
        icons: ['https://cdn.sanity.io/images/94xksvt2/production/249e9bbf9a02bf701a327b094e92d1257ae26b9d-600x600.png']
      },
    });
    listenForVerifyContext(web3WalletPromise);
  }
  const web3Wallet = await web3WalletPromise;
  return web3Wallet;
};

export const getSessions = async () => {
  const web3Wallet = await getWeb3Wallet();
  return Object.values(web3Wallet.getActiveSessions());
};

export const getSession = async (topic) => {
  const sessions = await getSessions();
  return sessions.find(session => session.topic === topic);
};

export const getAuthRequests = async () => {
  const web3Wallet = await getWeb3Wallet();
  return web3Wallet.getPendingAuthRequests();
};

export const getAuthRequest = async (id) => {
  const requests = await getAuthRequests();
  return requests.find(request => request.id === id);
};

export const getSessionProposals = async () => {
  const web3Wallet = await getWeb3Wallet();
  return web3Wallet.getPendingSessionProposals();
};

export const getSessionProposal = async (id) => {
  const proposals = await getSessionProposals();
  return proposals.find(proposal => proposal.id === id);
};

export const getSessionRequests = async () => {
  const web3Wallet = await getWeb3Wallet();
  return web3Wallet.getPendingSessionRequests();
};

export const getSessionRequest = async (id) => {
  const requests = await getSessionRequests();
  return requests.find(request => request.id === id);
};

export const openedByDapp = () => {
  store.dispatch(WalletConnectAction.openedByDapp());
};

export const returnToDapp = () => {
  const { openedByDapp } = store.getState().walletConnect;
  if (openedByDapp) {
    store.dispatch(WalletConnectAction.returnToDapp());
    NativeModules.AppControlModule.moveToBackground();
  }
};

const deepLink = 'internetmoney://wc?uri=';
const universalLink = 'https://internetmoney.io/wc?uri=';

export const pair = async (uri) => {
  Logger.info('Pairing with WalletConnect', { uri });
  if (uri.startsWith(deepLink)) {
    uri = decodeURIComponent(uri.split(deepLink)[1]);
  } else if (uri.startsWith(universalLink)) {
    uri = decodeURIComponent(uri.split(universalLink)[1]);
  }
  if (uri.includes('@1')) {
    throw new Error('WalletConnect v1 is no longer supported.');
  }
  if (uri.includes('@2')) {
    const web3Wallet = await getWeb3Wallet();
    await web3Wallet.core.pairing.pair({ uri });
  }
};

export const unpair = async (topic) => {
  Logger.info('Unpairing with WalletConnect');
  const web3Wallet = await getWeb3Wallet();
  await web3Wallet.disconnectSession({
    topic,
    reason: getSdkError('USER_DISCONNECTED'),
  });
};

export const acceptConnection = async (id) => {
  Logger.info('Approving a WalletConnect connection request', { id });
  const web3Wallet = await getWeb3Wallet();
  const proposal = await getSessionProposal(id);
  const namespaces = getApprovedNamespaces(proposal);
  await web3Wallet.approveSession({
    id,
    namespaces,
  });
  store.dispatch(
    GlobalAction.showAlert(
      localize('SUCCESS'),
      localize('WALLET_CONNECT_SUCCESS_MSG'),
    ),
  );
  markSessionProposalAsCompleted(id);
  returnToDapp();
};

export const rejectConnection = async (id) => {
  Logger.info('Rejecting a WalletConnect connection request', { id });
  markSessionProposalAsCompleted(id);
  const web3Wallet = await getWeb3Wallet();
  await web3Wallet.rejectSession({
    id,
    reason: getSdkError('USER_REJECTED_METHODS'),
  });
  returnToDapp();
};

const getApprovedNamespaces = (proposal) => {
  const state = store.getState();
  const { networks } = state.networks;
  const account = getSelectedAccount();
  const chains = networks
    .filter(network => !network.disabled)
    .map(network => `eip155:${network.chainId}`);
  const accounts = networks
    .filter(network => !network.disabled)
    .map(network => `eip155:${network.chainId}:${account}`);
  const supportedMethods = [
    'eth_sendTransaction',
    'eth_signTransaction',
    'eth_sign',
    'personal_sign',
    'eth_signTypedData',
    'eth_signTypedData_v4',
    'wallet_switchEthereumChain',
  ];
  return buildApprovedNamespaces({
    proposal,
    supportedNamespaces: {
        eip155: {
            chains,
            methods: supportedMethods,
            events: ['accountsChanged', 'chainChanged'],
            accounts,
        },
    },
  });
};

export const getSelectedAccount = () => {
  const state = store.getState();
  const selectedAccount = getSelectedWalletConnectAccount(state);
  return selectedAccount.publicAddress;
};

export const getSelectedChainId = () => {
  const state = store.getState();
  const { selectedNetworkObj } = state.networks;
  return selectedNetworkObj.chainId;
};

export const approveTransactionRequest = async (
  transaction,
  transactionHash,
) => {
  Logger.info('Approving a WalletConnect transaction');
  markSessionRequestAsCompleted(transaction.id);
  const web3Wallet = await getWeb3Wallet();
  const response = {
    id: transaction.id,
    jsonrpc: '2.0',
    result: transactionHash,
  };
  await web3Wallet.respondSessionRequest({
    topic: transaction.topic,
    response,
  });
  returnToDapp();
};

export const rejectTransaction = async (transaction) => {
  Logger.info('Rejecting a WalletConnect transaction');
  markSessionRequestAsCompleted(transaction.id);
  if (typeof transaction.iss === 'string') {
    await rejectAuthRequest(transaction);
    return;
  }
  const { id, topic } = transaction;
  const web3Wallet = await getWeb3Wallet();
  const response = {
    id,
    jsonrpc: '2.0',
    error: {
      code: 5000,
      message: 'User rejected.'
    }
  };
  await web3Wallet.respondSessionRequest({ topic, response });
  returnToDapp();
};

export const approveAuthRequest = async(
  id,
  iss,
  signature,
) => {
  Logger.info('Approving a WalletConnect auth_request', { id });
  const web3Wallet = await getWeb3Wallet();
  await web3Wallet.respondAuthRequest(
    {
      id,
      signature: {
        s: signature,
        t: 'eip191',
      },
    },
    iss
  );
  await markAuthRequestAsCompleted(id);
  returnToDapp();
}

export const rejectAuthRequest = async (id, iss) => {
  Logger.info('Rejecting a WalletConnect auth_request', { id });
  const web3Wallet = await getWeb3Wallet();
  await web3Wallet.respondAuthRequest(
    {
      id,
      error: {
        code: 4001,
        message: 'Auth request has been rejected'
      }
    },
    iss
  );
  await markAuthRequestAsCompleted(id);
  returnToDapp();
}

// The Wallet Connect web3wallet object is keeping the auth requests even after we've already
// accepted/rejected them, which causes the modal to incorrectly remain open. Until that bug
// is fixed, we will maintain our own state to keep track of which requests have already been
// accepted/rejected, to filter those from the pending auth request list.
const markAuthRequestAsCompleted = async (id) => {
  const { data } = await StorageOperation.getData(ASYNC_KEYS.WALLET_CONNECT_COMPLETED_AUTH_REQUEST_IDS);
  const parseObj = parseObject(data);
  const completedAuthRequestIds = parseObj.success
    ? parseObj.data
    : [];
  completedAuthRequestIds.push(id);
  await StorageOperation.setData(
    ASYNC_KEYS.WALLET_CONNECT_COMPLETED_AUTH_REQUEST_IDS,
    JSON.stringify(completedAuthRequestIds),
  );
  store.dispatch(WalletConnectAction.completedAuthRequest(id));
};

// The Wallet Connect web3wallet object is keeping the session proposals even after we've already
// accepted/rejected them, which causes the modal to incorrectly remain open. Until that bug
// is fixed, we will maintain our own state to keep track of which requests have already been
// accepted/rejected, to filter those from the pending auth request list.
const markSessionProposalAsCompleted = async (id) => {
  const { data } = await StorageOperation.getData(ASYNC_KEYS.WALLET_CONNECT_COMPLETED_SESSION_PROPOSAL_IDS);
  const parseObj = parseObject(data);
  const completedSessionProposalIds = parseObj.success
    ? parseObj.data
    : [];
  completedSessionProposalIds.push(id);
  await StorageOperation.setData(
    ASYNC_KEYS.WALLET_CONNECT_COMPLETED_SESSION_PROPOSAL_IDS,
    JSON.stringify(completedSessionProposalIds),
  );
  store.dispatch(WalletConnectAction.completedSessionProposal(id));
};


// The Wallet Connect web3wallet object is keeping the session requests even after we've already
// accepted/rejected them, which causes the modal to incorrectly remain open. Until that bug
// is fixed, we will maintain our own state to keep track of which requests have already been
// accepted/rejected, to filter those from the pending session request list.
const markSessionRequestAsCompleted = async (id) => {
  const { data } = await StorageOperation.getData(ASYNC_KEYS.WALLET_CONNECT_COMPLETED_SESSION_REQUEST_IDS);
  const parseObj = parseObject(data);
  const completedSessionRequestIds = parseObj.success
    ? parseObj.data
    : [];
  completedSessionRequestIds.push(id);
  await StorageOperation.setData(
    ASYNC_KEYS.WALLET_CONNECT_COMPLETED_SESSION_REQUEST_IDS,
    JSON.stringify(completedSessionRequestIds),
  );
  store.dispatch(WalletConnectAction.completedSessionRequest(id));
};

const verifyRequest = async (request) => {
  const id = request.id;
  const verification = request.verifyContext?.verified?.validation;
  const { data } = await StorageOperation.getData(ASYNC_KEYS.WALLET_CONNECT_REQUEST_VERIFICATIONS);
  const parseObj = parseObject(data);
  const requestVerifications = parseObj.success
    ? parseObj.data
    : {};
  requestVerifications[id] = verification;
  await StorageOperation.setData(
    ASYNC_KEYS.WALLET_CONNECT_REQUEST_VERIFICATIONS,
    JSON.stringify(requestVerifications),
  );
  store.dispatch(WalletConnectAction.verifyRequest({ id, verification }));
};
