/* global __DEV__ */
export const log = (...data) => {
  /* istanbul ignore else */
  if (__DEV__) {
    console.log(...data);
  } else {
    // will log to a third party service in production
  }
};
