const max = (a, b) => (
  a > b ? a : b
);

const min = (a, b) => (
  a < b ? a : b
);

export const sanitizeIntegerString = (value) => (
  value.replace(/[^0-9]/g, '')
);

export const sanitizeFloatingPointString = (value, enableCommas, precision) => {
  const endsWithPeriod = value.endsWith('.');
  value = value.replace(/[^0-9.]/g, '');

  let decimalPointIncluded = false;
  value = value.replace(/\./g, (decimalPoint) => {
    if (decimalPointIncluded) {
      return '';
    }
    decimalPointIncluded = true;
    return decimalPoint;
  });

  let parts = value.split('.');
  if (parts[1]) {
    parts[1] = parts[1].substring(0, precision);
  }

  if (enableCommas) {
    const integerPart = parts[0];
    let newInt = '';
    let count = 0;
    for (let i = integerPart.length - 1; i >= 0; i--) {
      count += 1;
      newInt = integerPart[i] + newInt;
      if (count % 3 === 0 && i !== 0) {
        newInt = ',' + newInt;
      }
    }

    value = newInt;
    if (parts[1] || endsWithPeriod) {
      value += '.' + (parts[1] || '');
    }
  } else if (endsWithPeriod) {
    if (!value.endsWith('.')) {
      value += '.';
    }
  }
  return value;
};

const addCommas = (str) => str.replace(/\B(?=(\d{3})+(?!\d))/g, ',');

export default class IntegerUnits {

  constructor(value, decimals = 18n) {
    if (
      (typeof value === 'number' && value % 1 === 0) ||
      typeof value === 'string'
    ) {
      value = BigInt(value);
    }
    if (
      (typeof decimals === 'number' && decimals % 1 === 0) ||
      typeof decimals === 'string'
    ) {
      decimals = BigInt(decimals);
    }
    if (typeof value !== 'bigint') {
      throw new TypeError('The `value` parameter must be of type `bigint`.');
    }
    if (typeof decimals !== 'bigint') {
      throw new TypeError('The `decimals` parameter must be of type `bigint`.');
    }
    this.value = value;
    this.decimals = decimals;
    this.isIntegerUnits = true;
    Object.freeze(this);
  }

  checkType = (parameter) => {
    if (!parameter.isIntegerUnits) {
      throw new TypeError('Parameter must be an instance of IntegerUnits.');
    }
  }

  add = (other) => {
    this.checkType(other);
    const decimals = max(this.decimals, other.decimals);
    const left = this.toPrecision(decimals);
    const right = other.toPrecision(decimals);
    return new IntegerUnits(
      left.value + right.value,
      decimals,
    );
  }

  subtract = (other) => {
    this.checkType(other);
    const decimals = max(this.decimals, other.decimals);
    const left = this.toPrecision(decimals);
    const right = other.toPrecision(decimals);
    if (left.value < right.value) {
      throw new Error('Cannot subtract a larger amount from a smaller amount.');
    }
    return new IntegerUnits(
      left.value - right.value,
      decimals,
    );
  }

  multiply = (other) => {
    if (typeof other === 'number') {
      other = IntegerUnits.fromFloatingPoint(other);
      return this.multiply(other).toPrecision(this.decimals);
    }
    if (typeof other === 'bigint') {
      other = new IntegerUnits(other, 0);
    }
    this.checkType(other);
    return new IntegerUnits(
      this.value * other.value,
      this.decimals + other.decimals,
    );
  }

  divide = (other, roundUp = false, precision = 100n) => {
    if (typeof other === 'number' && other % 1 === 0) {
      other = new IntegerUnits(BigInt(other), 0);
    }
    if (typeof other === 'bigint') {
      other = new IntegerUnits(other, 0);
    }
    this.checkType(other);
    const value = (this.value * (10n ** precision))
    let quotient = value / other.value;
    if (roundUp) {
      const remainder = value % other.value;
      if (remainder !== 0n) {
        quotient += 1n;
      }
    }
    const decimals = this.decimals + precision - other.decimals
    return new IntegerUnits(quotient, decimals);
  }

  power = (exponent) => {
    if (typeof exponent === 'number' && exponent % 1 === 0) {
      exponent = BigInt(exponent);
    }
    if (typeof exponent !== 'bigint') {
      throw new TypeError('The `exponent` parameter must be of type `bigint`.');
    }
    return new IntegerUnits(
      this.value ** exponent,
      this.decimals * exponent,
    );
  }

  toPrecision = (targetDecimal, roundUp = false) => {
    if (typeof targetDecimal === 'number' && targetDecimal % 1 === 0) {
      targetDecimal = BigInt(targetDecimal);
    }
    if (typeof targetDecimal !== 'bigint') {
      throw new TypeError('The `targetDecimal` parameter must be of type `bigint`.');
    }

    const difference = targetDecimal - this.decimals;

    if (difference === 0n) {
      return new IntegerUnits(this.value, this.decimals);
    }
    if (difference > 0n) {
      return new IntegerUnits(this.value * (10n ** difference), targetDecimal);
    }
    let quotient = this.value / (10n ** (-difference));
    if (roundUp) {
      const remainder = this.value % (10n ** (-difference));
      if (remainder !== 0) {
        quotient += 1n;
      }
    }
    return new IntegerUnits(quotient, targetDecimal);
  }

  gte = (other) => {
    this.checkType(other);
    const decimals = max(this.decimals, other.decimals);
    const thisValue = this.toPrecision(decimals).value;
    const otherValue = other.toPrecision(decimals).value;
    return thisValue >= otherValue;
  }

  gt = (other) => {
    this.checkType(other);
    const decimals = max(this.decimals, other.decimals);
    const thisValue = this.toPrecision(decimals).value;
    const otherValue = other.toPrecision(decimals).value;
    return thisValue > otherValue;
  }

  lte = (other) => {
    this.checkType(other);
    const decimals = max(this.decimals, other.decimals);
    const thisValue = this.toPrecision(decimals).value;
    const otherValue = other.toPrecision(decimals).value;
    return thisValue <= otherValue;
  }

  lt = (other) => {
    this.checkType(other);
    const decimals = max(this.decimals, other.decimals);
    const thisValue = this.toPrecision(decimals).value;
    const otherValue = other.toPrecision(decimals).value;
    return thisValue < otherValue;
  }

  eq = (other) => {
    this.checkType(other);
    const decimals = max(this.decimals, other.decimals);
    const thisValue = this.toPrecision(decimals).value;
    const otherValue = other.toPrecision(decimals).value;
    return thisValue === otherValue;
  }

  toString = (
    decimals = 2,
    simplifiedZero = false,
    excludeCommas = false,
    significantDigits = 0,
  ) => {
    if (simplifiedZero && this.value === 0n) return '0';

    decimals = Math.min(Number(this.decimals), decimals);

    let stringValue = this.value.toString();
    stringValue = stringValue.padStart(Number(this.decimals) + 1, '0');

    let wholePart = stringValue.slice(0, -Number(this.decimals));
    let fractionalPart = stringValue.slice(-Number(this.decimals));

    if (this.decimals === 0n) {
      wholePart = stringValue.slice(0, stringValue.length - Number(decimals));
      fractionalPart = stringValue.slice(-Number(decimals));
    }

    if (decimals === 0) {
      const shouldRoundUp = parseInt(fractionalPart[0], 10) >= 5;
      wholePart = (parseInt(wholePart, 10) + (shouldRoundUp ? 1 : 0)).toString();
      return excludeCommas ? wholePart : addCommas(wholePart);
    }

    if (wholePart !== '0') {
      significantDigits -= wholePart.length;
    }
    significantDigits = Math.max(significantDigits, 0);
    if (significantDigits > 0) {
      const zeros = fractionalPart.match(/^0+/)?.[0]?.length ?? 0;
      fractionalPart = fractionalPart
        .slice(0, zeros + significantDigits);
      if (wholePart !== '0') {
        fractionalPart = fractionalPart
          .replace(/0+$/, '')
          .padEnd(decimals, '0');
      }
    } else {
      fractionalPart = fractionalPart.slice(0, decimals).padEnd(decimals, '0');
    }
    wholePart = excludeCommas ? wholePart : addCommas(wholePart);

    return `${wholePart}${fractionalPart ? '.' : ''}${fractionalPart}`;
  };

  static fromFloatingPoint = (floatingPoint) => {
    floatingPoint = floatingPoint
      .toString()
      .trim()
      .replaceAll(',', '');
    if (!floatingPoint.includes('.')) {
      return new IntegerUnits(BigInt(floatingPoint), 0);
    }
    const [wholePart, fractionalPart] = floatingPoint.split('.');
    const scale = fractionalPart.length;
    const bigIntValue = BigInt(wholePart + fractionalPart);
    return new IntegerUnits(bigIntValue, scale);
  }

  static fromJSON = (json) => {
    if (json === undefined) {
      return undefined;
    }
    return new IntegerUnits(
      BigInt(json.value),
      BigInt(json.decimals),
    );
  }

  static max = max
  static min = min

  toJSON = () => {
    return {
      value: this.value.toString(),
      decimals: this.decimals.toString(),
    };
  }

  static serialize(value) {
    if (value === null || value === undefined || typeof value !== 'object') {
      return value;
    }

    if (value instanceof IntegerUnits) {
      return value.toJSON();
    }
  
    if (Array.isArray(value)) {
      return value.map(item => IntegerUnits.serialize(item));
    }
  
    const newValue = {};
    for (const key in value) {
      newValue[key] = IntegerUnits.serialize(value[key]);
    }
    return newValue;
  }

  static deserialize(value) {
    if (value === null || value === undefined || typeof value !== 'object') {
      return value;
    }

    if (Array.isArray(value)) {
      return value.map(item => IntegerUnits.deserialize(item));
    }

    const hasValue = typeof value.value === 'string';
    const hasDecimal = typeof value.decimals === 'string';
    if (Object.keys(value).length === 2 && hasValue && hasDecimal) {
      try {
        return new IntegerUnits(BigInt(value.value), BigInt(value.decimals));
      } catch (e) {
        // Failed to parse value; proceed to deserialize as normal object
      }
    }

    const newValue = {};
    for (const key in value) {
      newValue[key] = IntegerUnits.deserialize(value[key]);
    }
    return newValue;
  }

}