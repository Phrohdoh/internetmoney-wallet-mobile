import { Platform } from 'react-native';
import { jsonToCSV } from 'react-native-csv';
import RNFS from 'react-native-fs';
import Share from 'react-native-share';
import { log } from '@utils';
import Toast from 'react-native-toast-message';
import { localize } from '@languages';
import moment from 'moment';

export const exportTransactions = transactions => {
  transactions = JSON.stringify(parseTransactions(transactions));
  const results = jsonToCSV(transactions);

  const fileName = 'intenet-money-wallet-transactions-' + Date.now() + '.csv';
  Platform.OS === 'ios'
    ? downloadIOS(results, fileName)
    : downloadAndroid(results, fileName);
};

const downloadIOS = (content, fileName) => {
  const path = RNFS.DocumentDirectoryPath + '/' + fileName;

  try {
    // write the file
    RNFS.writeFile(path, content, 'utf8')
      .then(function (_success) {
        const options = {
          url: path,
          saveToFiles: true,
        };

        Share.open(options)
          .then(_res => {})
          .catch(err => {
            err && log(err);
          });
      })
      .catch(err => {
        log(err.message);
      });
  } catch (e) {
    log(e);
  }
};

const downloadAndroid = (content, fileName) => {
  const path = RNFS.DownloadDirectoryPath + '/' + fileName;

  Toast.show({
    text1: localize('TRANSACTIONS_EXPORTED'),
  });

  // write the file
  RNFS.writeFile(path, content, 'utf8')
    .then(_success => {})
    .catch(err => {
      log(err.message);
    });
};

const getDateString= (date, format) => {
  try{
    return moment(date).utc().format(format);
  }
  catch(e){
    return '';
  }
};

const getTransactionUrl= (transaction) => {

  if(transaction.transactionDetail?.networkDetail?.explore && transaction.receipt?.transactionHash){
    return transaction.transactionDetail?.networkDetail?.explore + 'tx/' + transaction.receipt?.transactionHash;
  }
  else{
    return '';
  }
};

const parseTransactions= (transactions) => {

  const flatTransactions=[];

  transactions.forEach(transaction => {

    const transactionObject = {
      'Memo': '',
      'Date (UTC)': transaction.transactionDetail?.utcTime ? getDateString(transaction.transactionDetail?.utcTime, 'MM-DD-YYYY') : '',
      'Time (UTC)': transaction.transactionDetail?.utcTime ? getDateString(transaction.transactionDetail?.utcTime, 'hh:mm a') : '',
      'Type': transaction.type ? transaction.type[0].toUpperCase() + transaction.type.slice(1) : '',
      'Amount (Tokens)': transaction.transactionDetail?.displayAmount.toString(6) ?? '',
      'Asset Symbol': transaction.transactionDetail?.fromToken?.symbol ? transaction.transactionDetail.fromToken.symbol : '',
      'Token Swapped Into': transaction.transactionDetail?.toToken?.symbol ? transaction.transactionDetail.toToken.symbol : '', // TO TOKEN SYMBOL
      'USD Value': transaction.transactionDetail?.isSupportUSD
        ? `$${transaction.transactionDetail.displayAmount.multiply(transaction.transactionDetail.usdValue).toString(2)}`
        : '',
      'Wallet Fee': transaction.transactionDetail?.walletFee ? transaction.transactionDetail.walletFee.toString(6) : '',
      // 'Gas Fee': '',
      'From (Address)': transaction.transactionDetail?.fromAddress ? transaction.transactionDetail.fromAddress : '',
      'Account Name': transaction.transactionDetail?.accountName ? transaction.transactionDetail.accountName : '',
      'To (Address)': transaction.transactionDetail?.toAddress ? transaction.transactionDetail.toAddress : '',
      'Network (Blockchain)': transaction.transactionDetail?.networkDetail?.networkName ? transaction.transactionDetail.networkDetail.networkName : '',
      'Transaction Hash': transaction.receipt?.transactionHash ? transaction.receipt.transactionHash : '',
      'Transaction Url': getTransactionUrl(transaction),
      'Status': transaction.receipt?.status ? (transaction.receipt.status ? 'Success' : 'Failed') : '',
      
      // transferedAmount: transaction.transactionDetail?.transferedAmount ? transaction.transactionDetail.transferedAmount : '',
      // supportsUSD: transaction.transactionDetail?.isSupportUSD ? transaction.transactionDetail.isSupportUSD : '',
      // claimableRewards: transaction.transactionDetail?.claimableRewards ? transaction.transactionDetail.claimableRewards : '',
      // claimableDiv: transaction.transactionDetail?.claimableDiv ? transaction.transactionDetail.claimableDiv : '',
      // airdropAddress: transaction.transactionDetail?.airdropAddress ? transaction.transactionDetail.airdropAddress : '',
      // networkChainId: transaction.transactionDetail?.networkDetail?.chainId ? transaction.transactionDetail.networkDetail.chainId : '',
      // networkRpc: transaction.transactionDetail?.networkDetail?.rpc ? transaction.transactionDetail.networkDetail.rpc : '',
      // networkExplore: transaction.transactionDetail?.networkDetail?.explore ? transaction.transactionDetail.networkDetail.explore : '',
    };

    flatTransactions.push(transactionObject);
  });

  return flatTransactions;

};
