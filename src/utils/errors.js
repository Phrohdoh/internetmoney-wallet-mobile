import { localize } from '@languages';

export const getErrorResponse = (errorOrMessage, additionalProps) => {
  const isErrorObj = typeof errorOrMessage === 'object';
  let response = {
    success: false,
    error: isErrorObj ? errorOrMessage.message : errorOrMessage,
  };
  if (additionalProps) {
    response = {
      ...additionalProps,
      ...response,
    };
  }
  return response;
};

const USER_FRIENDLY_ERROR_MESSAGES = [
  {
    tests: [
      'could not replace existing tx',
      'replacement transaction underpriced',
      'ALREADY_EXISTS: already known',
    ],
    replacement: localize('REPLACEMENT_TRANSACTION'),
  }
];

export const getUserFriendlyErrorMessage = (originalMessage) => {
  for (const userFriendlyErrorMessage of USER_FRIENDLY_ERROR_MESSAGES) {
    const { tests, replacement } = userFriendlyErrorMessage;
    for (const test of tests) {
      if (originalMessage.includes(test)) {
        return replacement;
      }
    }
  }
  return originalMessage;
}
