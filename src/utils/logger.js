import { Platform } from 'react-native';
import axios from 'axios';
import crypto from 'crypto';
import Logger from 'eleventh';

import { ASYNC_KEYS, BACKEND_SERVER } from '@constants';
import { StorageOperation } from '@storage';
import packageJson from '../../package.json';

Logger.setLogLevel('debug');

let uniqueIdentifier;

let logsEnabled = false;

export const getLogsEnabled = () => {
  return logsEnabled;
};

export const setLogsEnabled = async (enabled) => {
  logsEnabled = enabled;
  await StorageOperation.setData(
    ASYNC_KEYS.LOGS_ENABLED,
    JSON.stringify(enabled),
  );
};

const loadLogsEnabled = async () => {
  const { data } = await StorageOperation.getData(
    ASYNC_KEYS.LOGS_ENABLED,
  );
  logsEnabled = data === 'true';
}

loadLogsEnabled();

const getIdentifier = async () => {
  if (uniqueIdentifier !== undefined) {
    return uniqueIdentifier;
  }
  const { error, data } = await StorageOperation.getData(
    ASYNC_KEYS.UNIQUE_IDENTIFIER,
  );
  if (!error && typeof data === 'string' && data.length === 32) {
    uniqueIdentifier = data;
    return uniqueIdentifier;
  }
  uniqueIdentifier = crypto.randomBytes(16).toString('hex');
  await StorageOperation.setData(
    ASYNC_KEYS.UNIQUE_IDENTIFIER,
    uniqueIdentifier,
  );
  return uniqueIdentifier;
};

const logUrl = BACKEND_SERVER.BASE_URL + BACKEND_SERVER.POST_LOG;

Logger.subscribe(async (message) => {
  if (!logsEnabled) {
    return;
  }
  const body = {
    uniqueIdentifier: await getIdentifier(),
    message,
  };
  await axios.post(logUrl, body, {
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
  });
});

const addDefaults = logFunction => (message, values) => {
  logFunction(message, {
    platform: Platform.OS,
    version: packageJson.version ?? 'undefined',
    // TODO: add selected network info?
    ...values,
  })
}

export default {
  debug: addDefaults(Logger.debug),
  info: addDefaults(Logger.info),
  warning: addDefaults(Logger.warning),
  error: addDefaults(Logger.error),
  fatal: addDefaults(Logger.fatal),
};
