import { useState, useEffect } from 'react';

export default (inputBoolean, delay = 1000) => {
  const [debouncedBoolean, setDebouncedBoolean] = useState(false);
  const [timer, setTimer] = useState(null);

  useEffect(() => {
    if (timer) {
      clearTimeout(timer);
    }

    if (inputBoolean) {
      setTimer((
        setTimeout(() => {
          setDebouncedBoolean(true);
        }, delay)
      ));
    } else {
      setDebouncedBoolean(false);
    }

    return () => {
      if (timer) {
        clearTimeout(timer);
      }
    };
  }, [inputBoolean]);

  return inputBoolean && debouncedBoolean;
};
