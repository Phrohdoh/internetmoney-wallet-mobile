import * as React from 'react';

export const navigationRef = React.createRef();

export function resetAppNavigation () {
  navigationRef.current?.reset({
    index: 0,
    routes: [
      {
        name: 'SPLASH_SCREEN',
      },
    ],
  });
}
