// import themes
import { IMAGES } from '@themes';

// import Utils
import { Validation, log } from '@utils';
import * as Web3Layer from '@web3';
import { getTokenLogo } from '../web3-layer/web3Layer';

export const parseObject = data => {
  try {
    const parseData = JSON.parse(data);
    if (parseData) {
      return { success: true, data: parseData };
    } else {
      return { success: false };
    }
  } catch (e) {
    log('parsing error >>>>>', e);
    return { success: false };
  }
};

export const getTokenSymbol = async (tokenAddress, chainId) => {
  try {
    const imageList = IMAGES[chainId];
    if (Validation.isEmpty(tokenAddress)) {
      return imageList.NATIVE_ICON;
    }
    if (imageList[tokenAddress.toLowerCase()]) {
      return imageList[tokenAddress.toLowerCase()];
    }
    return {
      uri: await getTokenLogo(chainId, tokenAddress)
    };
  } catch (e) {
    return IMAGES.TOKEN_PLACEHOLDER_ICON;
  }
};

export const displayDecimal = (value, minDecimal) => {
  let decimalSize = value.toString().split('.');

  if (decimalSize.length > 1) {
    decimalSize = Math.min(decimalSize[1].length, minDecimal);
  } else {
    decimalSize = 0;
  }

  const num = Number(value);

  return num.toFixed(Math.min(9, decimalSize));
};

export const displayAddress = value => {
  if (value) {
    const publicAddressPart1 = value.toString().substr(0, 8);
    const publicAddressPart2 = value
      .toString()
      .substr(value.length - 6, value.length);
    return publicAddressPart1 + '...' + publicAddressPart2;
  }
  return '';
};

export const formatExponentialNumber = expoNum => {
  if (Math.abs(expoNum) < 1.0) {
    const e = parseInt(expoNum.toString().split('e-')[1]);
    if (e) {
      expoNum *= Math.pow(10, e - 1);
      expoNum = '0.' + new Array(e).join('0') + expoNum.toString().substring(2);
    }
  } else {
    let e = parseInt(expoNum.toString().split('+')[1]);
    if (e > 20) {
      e -= 20;
      expoNum /= Math.pow(10, e);
      expoNum += new Array(e + 1).join('0');
    }
  }

  return expoNum.toString();
};

export const convertToRemoveDecimal = (value, decimals = 9) => {
  return Number(
    Web3Layer.removeDecimals(formatExponentialNumber(value), decimals),
  );
};

export const convertToAppendDecimal = (value, decimals = 9) => {
  return Number(
    Web3Layer.appendDecimals(formatExponentialNumber(value), decimals),
  );
};


// remove commas from numbers
export const removeCommas = (str) => {
  return str?.replace(/\s*,\s*|\s+,/g, '')
}
