# Wallet Functionality

##### "Wallet/Account Manage Functions"

## 1. generateAccountFromMnemonicOf

Create a new account from the mnemonic.

#### parameters

1. accountIndex - index from a list of accounts.
2. mnemonic - 12/15/18/21/24 group of words (i.e also seed phase).

#### returns

- object -
  - address: public address of the given index.
  - privateKey: original private Key of the given index.

## 2. retrievePrivateKeyOf

Retrieves private key from user provided password.

#### parameters

1. address - public address to get the private key of.
2. walletObject - encrypted wallet object which stores encrypted private keys.
3. password - user provided custom password.

#### returns

- object -
  - success: true/false
  - privateKey: original private Key

## 3. isMnemonicValid

Check if user provided/entered mnemonic is valid.

#### parameters

1. mnemonic - 12/15/18/21/24 group of words (i.e also seed phase).

#### returns

- bool - true/false

## 4. encryptMnemonic

Encrypts the mnemopnic.

#### parameters

1. mnemonic - 12/15/18/21/24 group of words (i.e also seed phase).
2. password - user provided custom password.

#### returns

- object -
  - success: true/false
  - mnemonic: encrypted/chiper mnemonic.

## 5. decryptMnemonic

Decrypts the mnemopnic.

#### parameters

1. cipherMnemonic - encrypted/chiper mnemonic.
2. password - user provided custom password.

#### returns

- object -
  - success: true/false
  - mnemonic: 12/15/18/21/24 group of words (i.e also seed phase).

## 6. generateRandomMnemonic

To generate random valid Mnemonic (i.e "bip39").

#### parameters

1. numberOfWords - number of words to generate
   [ NOTE: Valid only for 12/15/18/21/24 Words ]

#### returns

- object -
  - success: true/false
  - mnemonic: Mnemonic/Seed Phase

## 7. createNewWallet

Basically creates New Wallet for user.
[ NOTE: Called only ONCE during the On-Board. ]

#### parameters

1. intialDetails - Object: The intial properties,
   - mnemonic:
     1. "null" - if User wants to newly generate mnemonic.
     2. "userProvidedMnemonic" - if User wants to Recover accounts from existing mnemonic/seed phase.
   - password: custom password provided by User.
   - numberOfWords: number of words to generate mnemonic.
     [ NOTE: Valid only for 12/15/18/21/24 Words ]
     [ NOTE : if "userProvidedMnemonic" is used - then, "numberOfWords" should match the actual number of words in provided mnemonic ]
2. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success: true/false
  - mnemonic: chiper/encrypted mnemonic.
  - walletObject: encrypted newly created walletObject.

## 8. createNewAccount

.To create new account (i.e from mnemonic generated accounts).

#### parameters

1. mnemonic - chiper/encrypted mnemonic.
2. walletObject - prev encrypted walletObject.
3. password - user provided custom password.
4. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success: false/true
  - walletObject: updated walletObject after creating New account.
    [ NOTE: prev/old walletObject has to be updated with above return walletObject ]
  - account - Object:
    - publicAddress: public address of newly created account.
    - value: balance of account

## 9. importAccount

To import account from private key.

#### parameters

1. walletEncObject - encrypted wallet object which stores encrypted private keys.
2. privateKey - private key of importing account.
3. password - user provided custom password.
4. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success: true/false
  - account - Object:
    - publicAddress: public address of newly created account.
    - value: balance of account
  - walletObject: updated walletObject after creating New account.
    [ NOTE: prev/old walletObject has to be updated with above return walletObject ]

## 10. checkImportAccount

To check if importing account is already imported or NOT.

#### parameters

1. walletEncObject - encrypted wallet object which stores encrypted private keys.
2. privateKey - private key of importing account.

#### returns

- success: true/false

## 11. getAccounts

To fetch all user created & imported accounts.

#### parameters

1. walletObject - encrypted wallet object which stores encrypted private keys.
2. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- accountsDetail - Array (ofObject):
  - publicAddress: public address of account.
  - value: PLS value/balance of respective account.

## 12. getSingleAccount

To fetch each user Account Details.

#### parameters

1. accountAddress - user account/public address.
2. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- accountsDetail - Object:
  - publicAddress: public address of account.
  - value: PLS value/balance of respective account.

## 13. checkPassword

Verify if password provided/given by user is Correct or InCorrect.

#### parameters

1. walletObject - encrypted wallet object which stores encrypted private keys.
2. password - user provided custom password.

#### returns

- bool - TRUE/FALSE

## 14. changePassword

Change the password (i.e oldPassword to newPassword).

#### parameters

1. walletObject - encrypted wallet object which stores encrypted private keys.
2. mnemonic - chiper/encrypted mnemonic.
3. oldPassword - user provided old password.
4. newPassword - user provided custom new password.

#### returns

- object -
  - success: true/false
  - mnemonic: new chipherMnemonic
  - walletObject: new encrypted walletObject

##### "Send Token/Crypto, TO/FROM Address/ENS Validation & OR Code Validation Functions"

## 15. getBaseFee

To get a base fee os any blockchain network.

#### parameters

1. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success: true/false
  - baseFeePerGas: a real time base fee w.r.t the specific network.

## 16. validateAddressFormat

To validate correct formate of any address (i.e token Address/ public address)

#### parameters

1. address - publicAddress/tokenAddress anything.

#### returns

- bool: true/false

## 17. getContractInstance

To get a instance of any contract address to furture make use for interacting with it.

#### parameters

1. contractAddress - any smart contract's address (i.e may be tokenAddress etc..).
2. contractABI - a correct & valid ABI of the Smart Contract.
3. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success: true/false
  - contractInstance: a instance of a contract, which will be used to make txns/interact with it.

## 18. getENSAddress

To get a public address from ens formate.

#### parameters

1. toAddressOrENS - any valid ens to get/fetch it's public address.

#### returns

- object -
  - success: true/false
  - publicAddress: public address of the given ens (i.e if valid ens)

## 19. getGasEstimate

To get a estimate gas consumtion details on any txns.

#### parameters

1. web3 - instance of web3 which help to interact.
2. value - value which will sent over txn.
3. fromAddress - from user's public Address (i.e who will be siging txn).
4. toAddress - tokenAddress/public Address to whom txn will be sent to.
5. data - data sent over a txn.

#### returns

- object -
  - success: true/false.
  - gas: a estimate gas consumed value.

## 20. getFeeDetails

To get a gas fee details on any txns (i.e gas price, prio fee, max fee etc..).

#### parameters

1. address - public address to get the private key of.
2. walletObject - encrypted wallet object which stores encrypted private keys.
3. password - user provided custom password.

#### returns

- object -
  - success: true/false
  - range: different ranges of fee details to speed up the txn.
  - totalPrice: total Price expected for sending txn.
  - baseFee: base fee expected for sending txn.
  - maxPriorityFeePerGas: max Priority Fee expected for sending txn.
  - maxFeePerGas: max fee expected for sending txn.

## 21. getSendTxnObject

To get a transaction Object before sending any token/crypto transactions
& to allow user customize the fees.

#### parameters

1. isToken - true/false (i.e "true" if sending token, else "False" ).
2. tokenAddress - a valid address of the sending token.
3. fromAddress - from user account/public address.
4. toAddress - to user account/public address.
5. amount/value - amount/value of token/crypto that has to be transfered.
6. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - txnObject - Object:
    - Contains all property required to perform transaction
  - range - Array (ofObject):
    - Contains min/max fee range of each property.

## 22. createValidRawTx

To create & valid the transaction before signing transaction.

#### parameters

1. txnType: a type of txns the blockchain network can handle.
2. txnObject - Object:
   Contains all property required to perform transaction

#### returns

- object -
  - success - true/false
  - txnObject - Object:
    - Contains all property required to perform transaction

## 23. signTx

To sign transaction Object generated from createValidRawTx functions & finally perform any token/crypto transactions.

#### parameters

1. txnObject - Array (ofObject):
   Contains all property required to perform transaction
2. walletEncObject - encrypted wallet object which stores encrypted private keys.
3. password - user provided custom password.
4. signingAddress - account/public address which has to sign. (i.e fromAddress)
5. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - receipt - Array (ofObject):
    - The receipt after transaction is sent on blockchain & the status of it.

## 24. isTokenAddressValid

To check if user provided token address is valid or not.

#### parameters

1. accountAddress - user account/public address in which token is imported.
2. tokenAddress - a valid address of the sending token.
3. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - tokenDetails - Array (ofObject):
    - symbol - symbol of the token address.
    - decimals - decimal points of the token address.
    - balance - token balance of user account.

## 25. checkValidENSOrToAddress

To check if user provided toAddress/ENS is valid or not.

#### parameters

1. toAddressOrENS - user provided ENS name or account/public address.

#### returns

- object -
  - success - true/false.
  - address - public address of the provided ENS name.

##### "Get Account Balance & USD Price Functions"

## 26. getPriceDexSrcAPI

To get price of any token address through dexScreener API.

#### parameters

1. tokenAddress - any valid token address.

#### returns

- object -
  - success - true/false.
  - price - price of the token in USD.

## 27. getPLSChainPrice

To get price of any token address on PLS network through graphOL.

#### parameters

1. tokenAddress - any valid token address.

#### returns

- object -
  - success - true/false.
  - PLS - price of the token in PLS.
  - USD - price of the token in USD.

## 28. canGetUSDPrice

To check if able to fetch/get the price of any token address.

#### parameters

1. tokenAddress - any valid token address.
2. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false.
  - price - price of the token in USD.

## 29. getBalanceOf

To get the token balance of any account.

#### parameters

1. accountAddress - user account/public address in which token is imported.
2. tokenAddress - a valid address of the sending token.
3. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false.
  - tokenValues - Array (ofObject):
    - balance - token balance of the account.
    - usdPrice - usd price of the token address. (i.e per token)

##### "Multiple Networks Managing/Related Functions"

## 30. addNewBlockchainNetwork

To add a New Custome Network from user provided rpc url.

#### parameters

1. networkName - user provided name of the blockchain network.
2. networkRPC - a user provided valid rpc url of the blockchain network.
3. chainId - user provided chainID of the blockchain network.
4. symbol - user provided native currency symbol of the blockchain network.
5. networkExplorer - a valid explore url of the blockchain network.

#### returns

- object -
  - success - true/false.
  - networkDetails - Object: validated networkDetails Object of custom blockchain network provided by user,
    - txnType: a type of txns the blockchain network can handle
    - rpc: a valid rpc url of the blockchain network
    - chainId: chainID of the blockchain network
    - sym: a native currency symbol
    - explore: a valid explore url of the blockchain network

##### "Swap Functions"

##### "Approve Feature --------"

## 31. checkAllowance

To check the allowance of the token before Swaping.

#### parameters

1. amount - how much amount of allowance.
2. fromAddress - from Address (i.e owner)
3. spenderAddress - spender address (i.e toAddress)
4. tokenAddress - any valid token address.
5. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success: true/false
  - status: true/false

## 32. getTokenApproveTxnObj

To get a transaction Object before sending approve txn.

#### parameters

1. spenderAddress - spender address (i.e toAddress).
2. signingAddress - from user account/public address (i.e who will be signing txn).
3. tokenAddress - any valid token address.
4. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - txnObject - Object:
    - Contains all property required to perform transaction
  - range - Array (ofObject):
    - Contains min/max fee range of each property.
  - approveAmount - amount of token which was approved.

##### "--------"

## 33. getBestPath

To fetch the best path for swaping b/w any token tokens.

#### parameters

1. chainId - network Chain Id.
2. dexId - dex Exchange id.
3. amountIn - token amount to swap.
4. fromToken - valid FROM/IN token Address.
5. toToken - valid TO/OUT token Address.
6. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network
7. routerAddress - a dex Exchange routerAddress.

#### returns

- object -
  - success: true/false
  - bestPath: a best path that can be used to swap.

## 34. getQuotes

To get the different quotes from all different dex Exchanges.

#### parameters

1. nativeTokenAddress - native Token Address (i.e wrapper Address)
2. swapAddress - swap smart contract address.
3. tokenIN - valid FROM/IN token Address.
4. amountIN - token amount to swap.
5. tokenOUT - valid TO/OUT token Address.
6. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success: true/false
  - quotes: Array of dex object
    - dex (Object):
      - id
      - dexName
      - tokenIN
      - tokenOUT
      - path
      - amountIn
      - amountOut

## 35. getSwapTxnObject

To get a transaction Object before making/sending Swap txn.

#### parameters

1. swapType - type 0/1/2.
2. swapAddress - swap smart contract address.
3. quoteObj - qhich quoteObj.
4. slippage - slippage for swap.
5. fromAddress - from user account/public address (i.e who will be signing txn).
6. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - txnObject - Object:
    - Contains all property required to perform transaction
  - range - Array (ofObject):
    - Contains min/max fee range of each property.

##### "Display/Read Functions - Transactions"

## 36. getTransactionDetails

To get a complete details of any transaction.

#### parameters

1. transactionHash - a hash of the transaction to fetch it's details.
2. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false.
  - txnDetails - Object:
    - Contains all details required to display of the provided transaction hash.

##### "WD Token Functions"

## 37. claimWDTxnObj

To get a transaction Object before making/sending Claim WD txn.

#### parameters

1. wdAddress - WD Token Address.
2. fromAddress - from user account/public address (i.e who will be signing txn).
3. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - txnObject - Object:
    - Contains all property required to perform transaction
  - range - Array (ofObject):
    - Contains min/max fee range of each property.

## 38. getWDStats

To get the statistics of WD Token.

#### parameters

1. wdAddress - WD Token Address.
2. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success: true/false
  - stats: Stats Details (Object)
    - name
    - sym
    - decimals
    - totalSupply
    - totalDistributed

## 39. getUserDividendsInfo

To get a User dividends info/Stats on WD Token.

#### parameters

1. wdAddress - WD Token Address.
2. accountAddress - user'c account address.
3. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success: true/false
  - stats: Stats Details (Object)
    - accountAddress
    - totalSupply
    - balance
    - claimableDiv
    - totalEarnedDiv
    - lastClaimed

##### "TO GET any Smart Contract Send Txn Object"

## 40. getSmartContractFunctionTxnObject

To get a transaction Object before making/sending any Smart Contract txns.

#### parameters

1. fromAddress - from user account/public address.
2. toAddress - to user account/public address.
3. data - data sent over a txn.
4. amount/value - amount/value of token/crypto that has to be transfered.
5. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - txnObject - Object:
    - Contains all property required to perform transaction
  - range - Array (ofObject):
    - Contains min/max fee range of each property.

##### "DAPP/Wallet Connect - Handling different Web3 Functions"

## 41. ethSendTransactionPayloadObject

To get a transaction Object before making/sending any txns through Dapp.

#### parameters

1. payload - a txn payload received from dapp.
2. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -

  - success - true/false
  - txnObject - Object:
    - Contains all property required to perform transaction
  - range - Array (ofObject):
    - Contains min/max fee range of each property.
  - method: "txn"/"sign"

## 42. ethSignTransactionPayloadObject

To sign a transaction from the payload recieved from Dapp.

#### parameters

1. payload - a txn payload received from dapp.
2. walletEncObject - encrypted wallet object which stores encrypted private keys.
3. password - user provided custom password.
4. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -

  - success - true/false
  - signResult - signedResult
  - method: "txn"/"sign"

## 43. ethSignTypedDataPayloadObject

To sign a data from the payload recieved from Dapp. type - "ethSignTypedDataPayloadObject"

#### parameters

1. payload - a txn payload received from dapp.
2. fromAddress - from user account/public address.
3. walletEncObject - encrypted wallet object which stores encrypted private keys.
4. password - user provided custom password.
5. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - signResult - signedResult
  - method: "txn"/"sign"

## 44. personalSignPayloadObject

To sign a personal data from the payload recieved from Dapp. type - "personalSignPayloadObject"

#### parameters

1. payload - a txn payload received from dapp.
2. walletEncObject - encrypted wallet object which stores encrypted private keys.
3. password - user provided custom password.
4. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - signResult - signedResult
  - method: "txn"/"sign"

## 45. ethSignPayloadObject

To sign a data from the payload recieved from Dapp. type - "ethSignPayloadObject"

#### parameters

1. payload - a txn payload received from dapp.
2. walletEncObject - encrypted wallet object which stores encrypted private keys.
3. password - user provided custom password.
4. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - signResult - signedResult
  - method: "txn"/"sign"

## 45. ethSignPayloadObject

To sign a transaction from the payload recieved from Dapp. - type - "ethSignPayloadObject"

#### parameters

1. payload - a txn payload received from dapp.
2. walletEncObject - encrypted wallet object which stores encrypted private keys.
3. password - user provided custom password.
4. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - signResult - signedResult
  - method: "txn"/"sign"

## 46. handleDappTxnObject

In Genearal to handle all type of payload recieved from Dapp.

#### parameters

1. payload - a txn payload received from dapp.
2. fromAddress - from user account/public address.
3. walletEncObject - encrypted wallet object which stores encrypted private keys.
4. password - user provided custom password.
5. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - signResult - signedResult
  - method: "txn"/"sign"

## 47. getSignMsg

To sign a msg from the payload recieved from Dapp. - type - "ethSignPayloadObject"

#### parameters

1. payload - a txn payload received from dapp.
2. networkDetails - Object: on which blockchain network,
   - txnType: a type of txns the blockchain network can handle
   - rpc: a valid rpc url of the blockchain network
   - chainId: chainID of the blockchain network
   - sym: a native currency symbol
   - explore: a valid explore url of the blockchain network

#### returns

- object -
  - success - true/false
  - signResult - signedResult
  - method: "txn"/"sign"
