const { argon2d, argon2Verify } = require("hash-wasm");

const config = require("./config.json");
import Logger from "../utils/logger.js";
import { getShaHash } from "./web3Layer.js";

export const getHash = async (password) => {
  return await argon2d({
    password: getShaHash(password),
    salt: config.ARGON2D_SALT,
    parallelism: 1,
    iterations: 512,
    memorySize: 512,
    hashLength: 32,
    outputType: "encoded",
  });
};

export const verifyPassword = async (password, passwordHash) => {
  try {
    return argon2Verify({
      password: getShaHash(password),
      hash: passwordHash,
    });
  } catch (error) {
    Logger.error('Error in verifyPassword');
    console.error(err);
    return false;
  }
};
