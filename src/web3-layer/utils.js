import BigNumber from 'bignumber.js';
import IntegerUnits from '@utils/IntegerUnits'
import pricingSupportedChainIDs from "./dexScreenerNetworkChainIds.js";
import { Web3 } from 'web3';
import {
  DEFAULT_FEE_MULTIPLIER,
  DEFAULT_GAS_MULTIPLIER,
  DEFAULT_GAS_PRICE_MULTIPLIER,
  FEE_MULTIPLIERS,
  GAS_MULTIPLIERS,
  GAS_PRICE_MULTIPLIERS,
} from '@constants';

export const retry = async (callback, limit = 3, delay = 100) => {
  for (let attempt = 0; attempt < limit; attempt += 1) {
    try {
      return await callback();
    } catch (e) {
      if (attempt === limit - 1) {
        throw e;
      }
      await new Promise(resolve => setTimeout(resolve, delay));
    }
  }
};

export const parseIntCustom = (value, radix = 10) => (
  BigNumber(value, radix).dividedToIntegerBy(1).toFixed(0)
);

export const pad32Bytes = (data) => {
  const l = data.length;
  let s = String(data).slice(2, l);
  while (s.length < (64 || 2)) {
    s = "0" + s;
  }
  return s;
};

export const dynamicSort = (property) => {
  let sortOrder = 1;
  if (property[0] === "-") {
    sortOrder = -1;
    property = property.substr(1);
  }
  return function (a, b) {
    const result =
      a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
    return result * sortOrder;
  };
};

export const filterDexScrNetworkSpecificQueries = (
  chainId,
  queries,
  tokenAddress
) => {
  const prefixNetworkName = pricingSupportedChainIDs[parseIntCustom(chainId)];
  if (prefixNetworkName == undefined) {
    return [];
  }
  const filteredQueries = [];
  let tokenPrice = new IntegerUnits(0, 0);
  tokenAddress = tokenAddress.toLowerCase();

  for (let pair = 0; pair < queries.length; pair++) {
    if (queries[pair].url.includes(prefixNetworkName)) {
      const baseToken = queries[pair].baseToken.address.toLowerCase();
      const quoteToken = queries[pair].quoteToken.address.toLowerCase();
      const priceUsd = queries[pair].priceUsd;
      const priceNative = queries[pair].priceNative;
      if (baseToken === tokenAddress && priceUsd) {
        tokenPrice = IntegerUnits.fromFloatingPoint(priceUsd).toPrecision(18);
      } else if (quoteToken === tokenAddress && priceUsd && priceNative) {
        tokenPrice = IntegerUnits.fromFloatingPoint(priceUsd)
          .divide(IntegerUnits.fromFloatingPoint(priceNative))
          .toPrecision(18);
      } else {
        continue;
      }

      if (queries[pair].liquidity) {
        filteredQueries.push({
          priceUSD: tokenPrice,
          liquidity: queries[pair].liquidity.usd,
          url: queries[pair].url,
        });
      }
    }
  }

  return filteredQueries.sort(dynamicSort("-liquidity"));
};

export const getErrorCorrection = (chainId) => {
  switch (parseIntCustom(chainId)) {
    case 1:
      return 1.04;
    case 56:
      return 1.04;
    case 941:
      return 1.1;
    default:
      return 0;
  }
};

/**
 * This helper function is used to omit the transactions, sender of which is not an EOA [Externally Owner Account].
 *
 * @param {Object[]} aTransfers Array of all receive transactions
 *
 * @returns Array of direct receive transactions
 */
export const getDirectTransfers = (aTransfers) => {
  return aTransfers.filter(
    (oTransfer) => oTransfer.sender.smartContract.contractType === null
  );
};

/**
 * This helper function is used to get Ether unit for given decimal value
 *
 * @param {number} nDecimals - Number of decimals to get unit for
 * @returns Respective Ether unit for the {nDecimals}
 */
export const getDecimalUnit = (nDecimals) => (
  {
    3: 'kwei',
    6: 'mwei',
    9: 'gwei',
    12: 'szabo',
    15: 'finney',
    18: 'ether',
  }[nDecimals] ?? ''
);

/**
 * This function is used to append token decimals to the number
 *
 * @param {string} sAmount - The amount of tokens to append the decimals to
 * @param {number} nDecimals - Number of decimal places to append
 * @returns String representation of the number after appending the decimal
 */
export const appendDecimals = (sAmount, nDecimals) => {
  try {
    if (sAmount.indexOf('.') > 0) {
      let [sNumberPart, sDecimalPart] = sAmount.split('.');

      if (sDecimalPart.length > nDecimals)
        sDecimalPart = sDecimalPart.slice(0, nDecimals);

      sAmount = `${sNumberPart}.${sDecimalPart}`;
    }

    const sDecimalUnit = getDecimalUnit(nDecimals);

    if (!sDecimalUnit) return (Number(sAmount) * 10 ** nDecimals).toString();

    return Web3.utils.toWei(sAmount, sDecimalUnit);
  } catch (err) {
    console.error(err);
    return 0;
  }
};

/**
 * This function is used to remove token decimals from the number
 *
 * @param {string} sAmount - The amount of tokens with decimals
 * @param {number} nDecimals - Number of decimal places to remove
 * @returns String representation of the number after removing the token decimal
 */
export const removeDecimals = (sAmount, nDecimals) => {
  try {
    const sDecimalUnit = getDecimalUnit(nDecimals);

    if (!sDecimalUnit) return (Number(sAmount) / 10 ** nDecimals).toString();

    return Web3.utils.fromWei(sAmount, sDecimalUnit);
  } catch (err) {
    console.error(err);
    return 0;
  }
};

export const displayCommas = (number) => {
  try {
    const splitedNumber = number.split('.');
    const firstPart = BigNumber(splitedNumber[0])
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    const finalNumber =
      splitedNumber.length == 1
        ? firstPart
        : firstPart + '.' + splitedNumber[1];
    return finalNumber;
  } catch (err) {
    console.error(err);
    return err.message;
  }
};

export const displayDecimalWithCommas = (
  value,
  minDecimal,
  isCommas = false,
  isDecimal = false,
  decimals = 18,
  nFixedDecimals = 0
) => {
  try {
    if (isDecimal) {
      decimals = parseIntCustom(decimals);
      value = BigInt(value).toLocaleString().split(',').join('');

      const valueSize = value.toString().length - decimals;

      if (valueSize <= 0) {
        value = '0.' + '0'.repeat(valueSize * -1) + value;
      } else {
        value =
          value.toString().slice(0, valueSize) +
          '.' +
          value.toString().slice(valueSize, valueSize + minDecimal + 1);
      }
    }

    let resValue;

    if (!value) value = 0;

    if (value.toString().includes('e')) {
      value = Number(value).toFixed(minDecimal);
    }

    const bigN = value.toString().split('.')[1] || '0'.repeat(minDecimal);
    const valueNum = BigInt(parseIntCustom(value)).toString();

    resValue =
      Number(bigN.toString().slice(0, minDecimal)) === 0
        ? valueNum
        : valueNum + '.' + bigN.toString().slice(0, minDecimal);

    if (resValue.indexOf('.') !== -1) {
      let nTrailingZeros = 0;
      const sDecimalValue = resValue.split('.')[1];
      for (
        let nCharIndex = sDecimalValue.length - 1;
        nCharIndex >= 0;
        nCharIndex--
      ) {
        if (sDecimalValue[nCharIndex] == '0') nTrailingZeros++;
        else break;
      }

      if (sDecimalValue.length - nTrailingZeros < nFixedDecimals)
        nTrailingZeros -=
          nFixedDecimals - (sDecimalValue.length - nTrailingZeros);

      if (nTrailingZeros)
        resValue = resValue.slice(0, resValue.length - nTrailingZeros);
      const nNumberOfZerosToAppend = Math.abs(nTrailingZeros);
      if (nNumberOfZerosToAppend && nFixedDecimals)
        resValue += '0'.repeat(nNumberOfZerosToAppend);
    } else {
      if (nFixedDecimals) resValue += '.' + '0'.repeat(nFixedDecimals);
    }

    if (isCommas) {
      resValue = displayCommas(resValue);
    }
    
    return resValue;
  } catch (err) {
    console.error(err);
    return nFixedDecimals ? '0.' + '0'.repeat(nFixedDecimals) : '0';
  }
};

export const calculateEstimateFee = (txnObject, txnType) => {
  const { gas, gasPrice, maxPriorityFeePerGas } = txnObject;

  return txnType == 0
    ? gasPrice.multiply(gas)
    : gasPrice.add(maxPriorityFeePerGas).multiply(gas);
};

export const calculateMaxFee = (txnObject) => {
  const maxPriorityFeePerGas = txnObject.maxPriorityFeePerGas ?? new IntegerUnits(0);
  const totalMaxFeePerGas = txnObject.maxFeePerGas.add(maxPriorityFeePerGas);
  return totalMaxFeePerGas.multiply(txnObject.gas);
};

export const getFeeMultiplier = networkDetails => {
  return FEE_MULTIPLIERS[networkDetails.chainId] ?? DEFAULT_FEE_MULTIPLIER;
};

export const getGasMultiplier = networkDetails => {
  return GAS_MULTIPLIERS[networkDetails.chainId] ?? DEFAULT_GAS_MULTIPLIER;
};

export const getGasPriceMultiplier = networkDetails => {
  return (
    GAS_PRICE_MULTIPLIERS[networkDetails.chainId] ??
    DEFAULT_GAS_PRICE_MULTIPLIER
  );
};
