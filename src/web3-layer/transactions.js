import { ASYNC_KEYS } from '@constants';
import { localize } from '@languages';
import { StorageOperation } from '@storage';
import { IntegerUnits, Validation, parseObject } from '@utils';
import { createValidRawTx, signTx } from '@web3';
import Logger from '../utils/logger';
import { getErrorResponse } from '../utils/errors';

export const WalletTransactionTypes = {
  SWAP: 'swap',
  SEND: 'send',
  CLAIM: 'claim',
  DAPP: 'dapp',
};

/**
 * Create & Send Transaction
 * @param {*} txnObject Transaction data
 * @param {*} wallet Sender's wallet
 * @param {*} fromAccount Sender's account data
 * @param {*} fromAccountBalance Sender's account balance
 * @param {*} selectedNetwork Current network
 * @param {*} additionalParams Extra transaction parameters
 * @returns Transaction receipt
 */
export const createAndSendTxn = async (
  type,
  txnObject,
  wallet,
  fromAccount,
  fromAccountBalance,
  selectedNetwork,
  additionalParams = {},
) => {
  try {
    const { gas, maxPriorityFeePerGas, maxFeePerGas } = txnObject;
    const gasPrice = maxFeePerGas
      ? IntegerUnits.min(txnObject.gasPrice, maxFeePerGas)
      : txnObject.gasPrice;

    let estimateFee =
      selectedNetwork.txnType == 0
        ? gasPrice.multiply(gas)
        : gasPrice.add(maxPriorityFeePerGas).multiply(gas);

    if (
      type === (WalletTransactionTypes.SEND || WalletTransactionTypes.SWAP) &&
      Validation.isEmpty(additionalParams.address)
    ) {
      estimateFee = estimateFee.add(additionalParams.transferAmount);
    }

    const isFeeValid = estimateFee.lte(fromAccountBalance);
    if (!isFeeValid) {
      throw new Error(localize('ESTIMATE_FEE_ERROR_MSG', [selectedNetwork.sym,]))
    };

    let txnReceipt = null;

    const validateTx = createValidRawTx(selectedNetwork.txnType, txnObject);

    if (validateTx.error) throw new Error(validateTx.error);

    txnReceipt = await signTx(
      validateTx.txnObject,
      wallet.walletDetail.walletObject,
      wallet.password,
      fromAccount.publicAddress,
      selectedNetwork,
      additionalParams.isApprove
    );

    if (txnReceipt.error) throw new Error(txnReceipt.error);

    return txnReceipt;
  } catch (err) {

    Logger.error('Error in createAndSendTxn', {
      txnType: type,
      isHardwareWallet: fromAccount.isHardware,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

/**
 * Update Transactions List
 * @param {*} newTxn Transaction to add
 * @param {*} existingTxns Existing transactions list
 * @param {*} selectedNetwork Current network
 * @returns Updated Transactions lists
 */
export const updateTxnLists = async (newTxn, existingTxns, selectedNetwork) => {
  try {
    const tempTxnsList = [...existingTxns.transactionsList, newTxn];
  
    const { data } = await StorageOperation.getData(
      ASYNC_KEYS.TRANSACTIONS,
    );
    const parseObj = parseObject(data);
  
    let parseTransactions;
    if (parseObj.success) {
      parseTransactions = parseObj.data;
    }
  
    let tempTxns = [];
    if (parseTransactions) {
      tempTxns = parseTransactions;
    }
    const transObj = tempTxns.find(
      chain => chain.chainId === selectedNetwork.chainId,
    );
  
    if (transObj) {
      const transIndex = tempTxns.indexOf(transObj);
      transObj['transactions'] = tempTxnsList;
      tempTxns[transIndex] = transObj;
    } else {
      tempTxns.push({
        chainId: selectedNetwork.chainId,
        transactions: tempTxnsList,
      });
    }
  
    return {
      tempTxns,
      tempTxnsList,
    };
  } catch (err) {
    console.error(err);
    throw err;
  }
};
