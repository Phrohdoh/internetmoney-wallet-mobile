import LocalizedStrings from 'react-native-localization';
import { Validation } from '@utils';
import { english } from './en';

export const Strings = new LocalizedStrings({
  en: english,
});

// Get Localize string of selected language
export const localize = (key, params = []) => {
  if (!Validation.isEmpty(key)) {
    if (params.length) {
      return Strings.formatString(Strings[key], ...params);
    }
    return Strings[key];
  }
  return '';
};
