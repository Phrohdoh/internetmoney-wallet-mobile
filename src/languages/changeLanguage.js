// Change Language as pr localization language or set as a default selected language for localize strings

import { Strings } from '@languages';

export const changeLanguage = (params) => {
  Strings.setLanguage(params);
};
