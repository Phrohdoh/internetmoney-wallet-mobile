export const english = {
  'APP_NAME': 'Internet Money',

  // Splash Screen
  'INTERNET': 'INTERNET',
  'MONEY': 'MONEY',
  'SPLASH_MSG': 'The Future of\nMoney',

  // Start Screens
  'WELCOME_TITLE': 'Welcome to',
  'INTERNET_MONEY': 'Internet Money',
  'WELCOME_MSG': 'The Future of Finance\nBy the people, for the people',
  'NEW_CRYPTO_UPPERCASE': 'NEW TO CRYPTO',
  'EXPERIENCE_CRYPTO_UPPERCASE': 'EXPERIENCED WITH CRYPTO',
  'ACKNOWLEDGE_MSG': 'I acknowledge that I have read and accept the {0} and {1}',
  'CHECK_ACKNOWLEDGE_MSG': 'Please accept the terms and conditions to continue.',

  // LearnBasicScreen
  'CONTINUE_UPPERCASE': 'CONTINUE',
  'LEARN_BASIC_INFO_LIST': [
    {
      image: 'GET_READY_BG',
      title: 'Get Ready',
      subtitle: 'to embark on a journey of financial freedom',
      description: `Our mission is to simplify the crypto space and make it more accessible.

There will be plenty of resources available to help you along the way.`,
    },
    {
      image: 'UNDERSTAND_BASIC_BG',
      title: 'Understanding The Basics',
      subtitle: 'What is a Wallet?',
      description: `This app is your wallet. With this wallet, you can create accounts. Those accounts store
information about and allow you to interact with your cryptocurrency assets.

It’s kind of like your “crypto bank account”.`,
    },
    {
      title: 'Understanding The Basics',
      subtitle: 'Seed Phrase',
      description: `In a couple of moments you will generate your “Seed Phrase”.

Understanding your seed phrase is one of the most important components of your crypto journey.

Your seed phrase is 24 words that are unique to you. These 24 words are used to store and recover your crypto assets. With these 24 words, someone can steal all of your crypto assets. It is very important to store these words safely.

NEVER share this seed phrase with anyone.

If someone asks for your seed phrase, consider it a scam`,
    },
    {
      title: 'Understanding The Basics',
      subtitle: 'Public & Private Keys',
      description: `When your seed phrase is generated, an account will also be generated.

Your account has a public key or “public address”. Anyone can view your public address. It is completely safe. This is how people send you crypto assets.

With each public key you generate, a private key is also generated. If someone knows this private key, they can steal your crypto.

It is very important to NEVER share this private key with anyone.

If someone asks for your private key, consider it a scam.

First rule of crypto, “Not your keys, not your crypto”.
`,
    },
    {
      image: 'PASS_SETUP_BG',
      title: 'Password/Passcode Setup',
      description:
        'Make this something you will remember or can safely store. As this password/passcode cannot be recovered if lost. If you lose this, the only way to access your crypto assets is with your seed phrase.',
    },
  ],
  'UNDERSTAND_BASICS': 'I understand the basics',
  'UNDERSTAND': 'I Understand',
  'CHECK_UNDERSTAND_MSG':
    'Please confirm you understand the basics in order to continue.',
  'CHECK_UNDERSTAND_MSG_2': 'Please confirm you understand in order to continue.',
  'SETUP_PASSWORD': 'SETUP PASSWORD',
  'SETUP_PASSCODE': 'SETUP PASSCODE',

  // Setup Password Screen
  'PASS_SETUP': 'Password Setup',
  'PASS_SETUP_MSG': 'Password must contain a minimum of 10 characters.',
  'PASSWORD': 'Password',
  'CONF_PASSWORD': 'Confirm Password',
  'ENTER_PASSWORD': 'Enter Password',
  'ENTER_CONF_PASSWORD': 'Enter Confirm Password',
  'GET_START': 'Get Started',
  'GET_START_UPPERCASE': 'GET STARTED',

  // Setup Passcode screen
  'PASSCODE_SETUP': 'Passcode Setup',
  'PASSCODE': 'Passcode',
  'CONF_PASSCODE': 'Confirm Passcode',
  'ENTER_VALID_PASSCODE_MSG': 'Your passcode must be 6 digits long.',
  'PASSCODE_NOT_MATCH_MSG':
    'Passcode and confirm passcode do not match, please try again.',

  // ExperiencedCryptoScreen
  'THANK_YOU_FOR_CHOOSE': 'Thank you for choosing us',
  'EXPERIENCE_CRYPTO_MSG': `We know there a lot of crypto wallets to choose from. We appreciate you giving us the opportunity to earn your trust.

The Internet Money Wallet is a multi-chain EVM wallet that supports all Ethereum-based blockchains.

Security is our top priority. Followed by innovation. Inside the app you will find many amazing tools and features to support your crypto journey. We are always building and striving to continue to enhance your experience.

Make sure you learn about the T.I.M.E Dividend (TIME) token. The first token of its kind.

By holding this token, you will earn passive income from transactions that occur in the Internet Money Wallet.`,
  'CREATE_NEW_WALLET_UPPERCASE': 'CREATE NEW WALLET',
  'IMPORT_WALLET': 'Import Wallet',
  'IMPORT_WALLET_UPPERCASE': 'IMPORT WALLET',

  // Create wallet screen
  'CREATE_WALLET': 'Creating Your',
  'WALLET': 'Wallet',
  'WALLET_UPPERCASE': 'WALLET',
  'WALLET_CREATE_STRINGS': 'generating...',

  // Create wallet success screen
  'CONGRATULATIONS_UPPERCASE': 'CONGRATULATIONS!',
  'WALLET_CREATE_SUCCESS_MSG': 'Wallet Successfully\nCreated!',
  'ALMOST_THERE': 'YOU ARE ALMOST THERE',
  'CONTINUE_INFO_MSG':
    'We know this can be overwhelming. Don’t sweat it. In the app, you will find plenty of resources to help you along the way.',

  // Import Wallet Screen
  'IMPORT_WALLET_MSG': 'Select the number of words in your BIP39 seed phrase, then enter the words one at a time with a space after each word.',
  'ENTER_PHRASE_MSG': 'Enter Seed Phrase',
  'PHRASE_VALIDATION_MSG':
    'The number of words entered does not match with your selected count.',
  'IMPORT_WALLET_ERROR':
    'Your secret recovery phrase is invalid, please try again.',

  // Secret Recovery Phrase Screen
  'SEED_PHRASE': 'Seed Phrase',
  'RECOVERY_PHRASE_MSG':
    'Reminder, your seed phrase needs to be stored securely. With these 24 words, anyone can access your crypto assets. We HIGHLY recommend using a “hardware wallet” to store your seed phrase. Until then, write them down and store them in a secure location.',
  'REVEAL_SEED_PHRASE_UPPERCASE': 'REVEAL SEED PHRASE',
  'HIDE_SEED_PHRASE_UPPERCASE': 'HIDE SEED PHRASE',
  'SHOW_SEED_PHRASE_UPPERCASE': 'SHOW SEED PHRASE',
  'ALERT_MSG':
    'Scammers may try to steal your seed phrase or private keys by posing as support. No individual or support will ever ask for this information. Only import your seed phrase and private keys into reputable and trusted applications.',
  'SEED_PHRASE_SCAM_MSG_UPPERCASE': 'SCAMMERS MAY TRY TO STEAL YOUR SEED PHRASE OR BY POSING AS SUPPORT. NO INDIVIDUAL OR SUPPORT WILL EVER ASK FOR THIS INFORMATION. ONLY IMPORT YOUR SEED PHRASE INTO REPUTABLE AND TRUSTED APPLICATIONS. IT IS RECOMMENDED TO AVOID TAKING SCREENSHOTS OF THIS PAGE, AS SCREENSHOTS MAY BE ACCESSED BY MALICIOUS APPS.',
  'SEED_PHRASE_24_WORDS_MSG': 'Make sure to save your 24-word seed phrase in the exact order it is displayed below.',
  'SEED_PHRASE_PASSCODE_VERIFICATION_MSG': 'Enter your passcode to reveal your seed phrase:',
  'SEED_PHRASE_PASSWORD_VERIFICATION_MSG': 'Enter your password to reveal your seed phrase:',

  // Password Verification Screen
  'PASSWORD_VERIFICATION_UPPERCASE': 'PASSWORD VERIFICATION',
  'PASSCODE_VERIFICATION_UPPERCASE': 'PASSCODE VERIFICATION',

  // TODO: remove these two
  'PASSCODE_VERIFICATION_MSG': 'Enter your passcode to reveal your private key',
  'PASSWORD_VERIFICATION_MSG': 'Enter your password to reveal your private key',

  'PRIVATE_KEY_PASSCODE_VERIFICATION_MSG': 'Enter your passcode to reveal your private key:',
  'PRIVATE_KEY_PASSWORD_VERIFICATION_MSG': 'Enter your password to reveal your private key:',

  // Your Secret Recovery Phrase Screen
  'YOUR_SEED_PHRASE_UPPERCASE': 'YOUR SEED PHRASE',
  'RECOVERY_24_WORD_MSG': 'Your Secret Recovery Phrase with 24 Words',
  'NEVER_SHARE_WITH_ANYONE': 'Never share this with ANYONE',
  'COPY_CLIPBOARD_UPPERCASE': 'COPY TO CLIPBOARD',
  'HIDE_KEY_UPPERCASE': 'HIDE PRIVATE KEY',
  'SHOW_KEY_UPPERCASE': 'SHOW PRIVATE KEY',
  'PRIVATE_KEY_SCAM_MSG_UPPERCASE': 'SCAMMERS MAY TRY TO STEAL YOUR PRIVATE KEYS BY POSING AS SUPPORT. NO INDIVIDUAL OR SUPPORT WILL EVER ASK FOR THIS INFORMATION. ONLY IMPORT YOUR PRIVATE KEYS INTO REPUTABLE AND TRUSTED APPLICATIONS. IT IS RECOMMENDED TO AVOID TAKING SCREENSHOTS OF THIS PAGE, AS SCREENSHOTS MAY BE ACCESSED BY MALICIOUS APPS.',
  'REVEAL_KEY_MSG': `If someone knows this private key, they can steal your crypto. It is very important to NEVER share this private key with anyone.

If someone asks you for your private key, consider it a scam.`,
  'CONFIRM_MSG': '"I confirm that I have securely stored my seed phrase"',
  'SLIDE_TO_CONFIRM_UPPERCASE': 'SLIDE TO CONFIRM',
  'CHECK_CONFIRM_MSG':
    'Please confirm you have stored your secret recovery phrase in order to continue',

  // Get start info screen
  'INTERNET_MONEY_WALLET': 'INTERNET MONEY WALLET',
  'GET_START_INFO_MSG': `Be sure to check the resources tab. There you’ll find many of the tools to help you on your crypto journey.

Join our communities for updates and questions! Our Telegram chat is amazing. You can get most questions answered there. Otherwise, feel free to submit a support ticket or give us feedback by hitting the bubble in the top right corner of the app.`,
  'GET_START_THANKYOU_MSG':
    'Thank you for giving us the opportunity to be your crypto wallet!',

  // Wallet dashboard screen
  'ACCOUNTS': 'Accounts',
  'TRANSACTIONS': 'Transactions',
  'ACCOUNTS_UPPERCASE': 'ACCOUNTS',
  'TRANSACTION_UPPERCASE': 'TRANSACTIONS',
  'HIDE_BALANCE_UPPERCASE': 'HIDE BALANCES',
  'SHOW_BALANCE_UPPERCASE': 'SHOW BALANCES',
  'EXPORT_TRANSACTION_UPPERCASE': 'EXPORT TRANSACTIONS',
  'TOTAL_VALUE': 'Total Value',
  'VALUE': 'Value',
  'FILTER_UPPERCASE': 'FILTER',
  'ACCOUNT': 'Account',
  'NO_ACCOUNTS': 'There are no accounts in this wallet. Please use the Add Account button.',

  // Add Account component
  'ADD_ACCOUNT': 'ADD ACCOUNT',
  'REMOVE_ACCOUNT': 'REMOVE ACCOUNT',
  'REMOVE_ACCOUNT_MESSAGE': 'Are you sure you want to remove the account "{0}"?',
  'FINISHED_REMOVING_ACCOUNT': 'FINISHED REMOVING',
  'CREATE_NEW_ACCOUNT': 'CREATE NEW ACCOUNT',
  'IMPORT_ACCOUNT': 'IMPORT ACCOUNT',
  'CREATE_ACCOUNT': 'CREATE ACCOUNT',
  'RESTORE_ACCOUNT': 'RESTORE ACCOUNT',
  'ACCOUNT_NAME': 'Account Name',
  'ENTER_ACCOUNT_NAME': 'Enter Account Name',
  'PRIVATE_KEY': 'Private Key',
  'ENTER_PRIVATE_KEY': 'Enter Private Key',
  'IMPORT_ACCOUNT_MSG':
    'NOTE: IMPORTED ACCOUNTS ARE NOT RECOVERABLE VIA THE SEED PHRASE GENERATED BY THE INTERNET MONEY WALLET.',
  'ENTER_VALID_ACCOUNT_NAME_MSG': 'Please enter a valid account name.',
  'ENTER_VALID_PRIVATE_KEY': 'Please enter a valid private key',
  'ACCOUNT_NAME_EXIST_MSG':
    'Account name already exists. Please use a different account name.',

  // Account detail screen
  'DAY_FILTER_LIST': ['HOUR', 'DAY', 'WEEK', 'MONTH', 'YEAR', 'ALL'],
  'SEND_UPPERCASE': 'SEND',
  'RECEIVE_UPPERCASE': 'RECEIVE',
  'SWAP_UPPERCASE': 'SWAP',
  'SEND': 'Send',
  'SWAP': 'Swap',
  'DAPP': 'Dapp',
  'CLAIM': 'Claim',
  'RECEIVE': 'Receive',
  'ADD_TOKEN_UPPERCASE': '+ ADD TOKEN',
  'EDIT_UPPERCASE': 'EDIT',
  'HIDE_CHART': 'Hide Chart',
  'VIEW_CHART': 'View Chart',
  'DELETE_UPPERCASE': 'DELETE',
  'REMOVE_TOKEN': 'Remove Token',
  'REMOVE_TOKEN_MSG': 'Are you sure you want to remove {0}?',
  'REMOVE_TOKEN_UPPERCASE': 'REMOVE TOKEN',

  // Add Token view
  'ADD_TOKEN_UPPERCASE': 'ADD TOKEN',
  'ADD_TOKEN_MSG': 'Are you sure you want to add {0}?',
  'ADDED_ALL_POPULAR_TOKENS_MSG': 'You\'ve already added the most popular tokens!',

  // Popular Tokens view
  'POPULAR_TOKENS_UPPERCASE': 'POPULAR TOKENS',

  // Import Token view
  'IMPORT_TOKEN_UPPERCASE': 'IMPORT TOKEN',
  'IMPORT_TOKEN_MSG':
    'Note: Anyone can create a token, including creating a fake version of an existing token.',
  'TOKEN_ADDRESS': 'Token Address',
  'ENTER_HERE': 'Enter Here...',
  'TOKEN_SYMBOL': 'Token Symbol',
  'TOKEN_PRECISION': 'Token Precision',
  'TOKEN_SYMBOL_ERROR': 'Token symbol must be 11 characters or fewer.',
  'TOKEN_PRECISION_ERROR': 'Token Precision must be at least 0, and not over 36.',
  'ENTER_VALID_TOKEN_ADDRESS': 'Please enter a valid token address.',
  'TOKEN_ALREADY_EXIST_MSG':
    'This token is already imported! Please use a different token.',

  // Send token screen
  'SEND_TOKEN_UPPERCASE': 'SEND TOKENS',
  'CURRENT_NETWORK': 'Current Network',
  'NEXT_UPPERCASE': 'NEXT',
  'CHOOSE_SEND_FROM_ACCOUNT': 'Choose the account to send from:',
  'ENTER_SEND_TO_ACCOUNT': 'Enter the account to send to:',
  'RECENT_ACCOUNT': 'Recent accounts you\'ve sent to',
  'ENTER_PUBLIC_ADDRESS_PLACEHOLDER': 'Public address',
  'BALANCE:': 'Balance: ',
  'BALANCE': 'Balance',
  'TRANSFER_BETWEEN_ACCOUNTS_UPPERCASE': 'TRANSFER BETWEEN ACCOUNTS',
  'CHOOSE_ACCOUNTS_UPPERCASE': 'CHOOSE ACCOUNT',
  'ENTER_PUBLIC_ADDRESS_MSG': 'Please enter a public address.',
  'CHOOSE_TOKEN': 'Choose token to send:',
  'ENTER_AMOUNT': 'Enter the amount to send:',
  'ACCOUNT_SELECT_ERROR_MSG':
    'This account does not contain any tokens.  Please add some tokens before sending.',
  'USD_UPPERCASE': 'USD',
  'VALUE:': ' Value: ',
  'SEND_AMOUNT_ERROR_MSG':
    'Your balance is not sufficient to send the entered amount. Please enter a valid amount.',
  'SWAP_AMOUNT_ERROR_MSG':
    'Your balance is not sufficient to transfer the entered amount. Please enter a valid amount.',
  'ENTER_AMOUNT_MSG': 'Please enter the amount to send',
  'ESTIMATE_FEE_ERROR_MSG': 'Not enough {0} to pay for the transaction fees.',
  'MIN_GAS_PRICE_MSG': 'Gas Price is Underpriced',
  'MAX_UPPERCASE': 'MAX',
  'SLIPPAGE': 'Slippage (%):',
  'SLIPPAGE_ERROR_MSG': 'Please enter a valid slippage value.',

  // Transfer between accounts
  'CHOOSE_TRANSFER_TO_ACCOUNT': 'Choose the account to transfer to:',
  'CHOOSE_DIFFERENT_ACCOUNT_MSG':
    'You cannot transfer to the same account. Please select a different account.',

  // Confirm transaction screen
  'CONFIRM_TRANSACTION_UPPERCASE': 'CONFIRM TRANSACTION',
  'FAILED_TRANSACTION_UPPERCASE': 'Failed transaction',
  'PENDING_TRANSACTION_UPPERCASE': 'Pending Transaction',
  'SUBMITTING_TRANSACTION_UPPERCASE': 'Submitting the Transaction...',
  'EXPEDITE_TRANSACTION_NOTICE': 'If your transaction has been pending for an extended period, consider resubmitting it with a higher gas fee to expedite processing.',
  'AMOUNT': 'Amount',
  'FROM:': 'From:',
  'TO': 'To:',
  'ESTIMATE_GAS_FEE': 'Estimated gas fee:',
  'LOW_SEND_TIME': 'Likely more then > 30 Seconds',
  'MEDIUM_SEND_TIME': 'Likely in < 30 Seconds',
  'HIGH_SEND_TIME': 'Likely in 15 Seconds',
  'MAX_FEE:': 'Max Fee: ',
  'GAS_PRICE': 'Gas Price',
  'TOTAL': 'Total',
  'SLICE_TO_SEND_TOKEN_UPPERCASE': 'SLIDE TO SEND TOKEN',

  // GasFee popup
  'ESTIMATED_GAS_FEE_UPPERCASE': 'ESTIMATED GAS FEE',
  'SAVE_UPPERCASE': 'SAVE',
  'RANGE_LIST': ['LOW', 'MEDIUM', 'HIGH'],
  'ADVANCE_OPTION': 'Advanced Options',
  'GAS_LIMIT': 'Gas Limit',
  'MAX_PRIO_FEE': 'Max Priority Fee',
  'MAX_FEE': 'Max Fee',
  'ESTIMATE:': 'Estimate: ',
  'GAS_FEE_NOT_ALLOW_MSG': 'Gas limit is too low. Given {0}, need at least {1}.',
  'MAX_PRIO_FEE_NOT_ALLOW_MSG':
    'Max priority fees cannot be less than minimum priority value.',
  'MAX_FEE_NOT_ALLOW_MSG': 'Max fee cannot be less than max priority fee. Please try your transaction again. This error usually resolves itself.',

  // Receive Token Screen
  'RECEIVE_TOKENS_UPPERCASE': 'RECEIVE TOKENS',
  'RECEIVE_TOKENS_MSG_UPPERCASE':
    'SCAN OR CLICK COPY TO SHARE YOUR PUBLIC ADDRESS',

  'COPY_UPPERCASE': 'COPY',
  'SHARE_UPPERCASE': 'SHARE',

  // Transaction filter component
  'APPLY_UPPERCASE': 'APPLY',
  'CHOOSE_ACCOUNT_UPPERCASE': 'CHOOSE ACCOUNT',
  'CHOOSE_ACCOUNT': 'Choose Account',
  'SEND_TRANSACTION': 'Send Transactions',
  'RECEIVE_TRANSACTION': 'Receive Transactions',
  'SWAP_TRANSACTION': 'Swap Transactions',
  'CLAIM_TRANSACTION': 'Claim Transactions',
  'DAPP_TRANSACTION': 'Dapp Transactions',
  'REFERRAL_EARNINGS': 'Referral Earnings', // Keep this for historical transaction records
  'ALL': 'All',

  // Swap Token select token screen
  'SWAP_TOKEN_UPPERCASE': 'SWAP TOKENS',
  'CHOOSE_TOKEN_UPPERCASE': 'CHOOSE TOKEN',
  'CHOOSE_SWAP_ACCOUNT': 'Choose the account to swap with:',
  'ENTER_SWAP_TOKEN_ADDRES': 'Enter token address here...',
  'CHOOSE_FROM_TOKEN_TO_SWAP': 'Choose asset to swap out of:',
  'ENTER_AMOUNT_TO_SWAP': 'Enter the amount to swap:',
  'CHOOSE_TO_TOKEN_TO_SWAP': 'Choose an asset to swap into:',
  'CHOOSE_DIFFER_TOKEN_MSG':
    'You can not swap the same token. Please choose a different token to swap.',
  'ENTER_SWAP_AMOUNT_MSG': 'Please enter the amount to swap',
  'WALLET_FEE:': 'Wallet fee:',

  // swap token get quote screen
  'RECEIVE_TOKEN_MSG': 'Here are quotes for',
  'RECOMMENDED_OPTION': 'Recommended Option',
  'RECEIVING': 'Receiving',
  'DEX_FEES': 'Dex Fees',
  'WALLET_FEES': 'Wallet Fees',
  'SOURCE': 'Source',
  'SWAP_NOT_ALLOW_MSG': 'Swap amount not approved. Please approve to get quotes.',
  'APPROVE_REQUEST': 'Approve Request',
  'GIVE_ALLOWANCES_MSG': 'Give Allowance to Access {0} Token',
  'APPROVE': 'Approve',
  'FEE_DETAILS_TITLE': 'Internet Money Swap Fee',
  'FEE_DETAILS': 'Get the best price from the top liquduity sources every time. The fee of 0.729% is automatically calculated based on this quote and paid in {NATIVE_TOKEN}. 100% of this fee is distributed to holders of the TIME token.',
  'FEE_FOOTNOTE': 'A fee of 0.729% is calculated based on this quote',

  // swap approvel confirmation screen
  'APPROVE_TRANSACTION_UPPERCASE': 'APPROVE TRANSACTION',
  'GIVE_PERMISSION_MSG': 'Give permission to access your {0}?',
  'GRANT_MSG':
    'By granting permission, you are allowing the following contract to access your funds.',
  'IM_SWAP_ROUTER': 'Internet Money Swap Router',
  'APPROVAL_AMOUNT': 'Approval amount',
  'SLIDE_TO_APPROVE_UPPERCASE': 'SLIDE TO APPROVE',
  'PERMISSON_REQUEST': 'Permission Request',
  'SWAP_PERMISSION_INFO':
    'InternetMoney wallet may access and spend up to this max amount',
  'APPROVED_AMOUNT': 'Approved Amount',
  'GRANTED_TO': 'Granted to',
  'CONTRACT': 'Contract',
  'DATA': 'Data',
  'FUNCTION': 'Function',
  'EIDT_PERMISSON': 'EDIT PERMISSON',
  'SPEND_LIMIT_PERMISSON': 'Spend limit permission',
  'ALLOW_AMOUNT':
    'Allow Internet Money Wallet to withdraw and spend up to the following amount:',
  'PROPOSED_LIMIT': 'Proposed Approval Limit',
  'SPEND_LIMIT_REQUEST': 'Spend limit requested by Internet Money Wallet',
  'DRIP_UPPERCASE': 'DRIP',
  'CUSTOM_SPEND': 'Custom Spend Limit',
  'ENTER_MAX_SPEND': 'Enter Max Spend Limit',
  'CANCEL_UPPERCASE': 'CANCEL',
  'CUSTOM_LIMIT_NOT_VALID': 'Amount is less than the Swap Amount.',

  // swap confirmation screen
  'SLIDE_TO_SWAP_TOKEN_UPPERCASE': 'SLIDE TO SWAP TOKEN',
  'SWAP_SUCCESS_MSG': `Successfully submitted transaction:
  Swap from {0} to {1}`,
  'EXPECTED': 'Expected',
  'VOLATILITY_ERROR': 'This quote is no longer valid due to price volatility. Consider increasing the maximum slippage, and try again.',

  // Rename account screens
  'ACCOUNT_SETTING_UPPERCASE': 'ACCOUNT SETTING',
  'RENAME_ACCOUNT_MSG':
    'Below you can rename your account or reveal your private key.',
  'RENAME_ACCOUNT': 'Rename Account',
  'REVEAL_PRIVATE_KEY_UPPERCASE': 'REVEAL PRIVATE KEY',
  'SAVE_CHANGES_UPPERCASE': 'SAVE CHANGES',

  // Reveal private key screen
  'PRIVATE_KEY_UPPERCASE': 'PRIVATE KEY',
  'REVEAL_KEY_INFO_MSG': `The private key is a key that allows you to access this single account.

Anyone who has access to the private key can transfer and gain access to all of your funds for this account.`,
  'VERIFICATION_UPPERCASE': 'VERIFICATION',
  'ACCOUNT_PRIVATE_KEY_UPPERCASE': '{0} PRIVATE KEY',

  // Transaction detail screen
  'GAS_LIMIT_FEE': 'Gas Limit (Units)',
  'GAS_USE_FEE': 'Gas Used (Units)',
  'BASE_FEE': 'Base Fee',
  'PRIO_FEE': 'Priority Fee',
  'TOTAL_FEE': 'Total Gas Fee',
  'MAX_FEE_PER_GAS': 'Max Fee Per Gas',
  'WALLET_FEE': 'Wallet Fee',
  'GWEI': 'GWEI',
  'VIEW_BLOCK_EXPLORER_UPPERCASE': 'VIEW ON BLOCK EXPLORER',
  'COPY_TRANS_ID_UPPERCASE': 'COPY TRANSACTION ID',
  'NONCE': 'Nonce',
  'CLAIMED_AMOUNT': 'Claimed Dividends',
  'SWEEPED_AMOUNT': 'Sweeped Dividends',
  'CLAIM_DIV': 'Claim Dividend',
  'SWEEP_DIV': 'Sweep Dividend',
  'TRANSACTION_CONFIRM_UPPERCASE': 'TRANSACTION CONFIRMED',

  // Login Password Verification Screen
  'LOGIN_PASSWORD_VERIFY_UPPERCASE': 'VERIFY PASSWORD TO LOGIN',
  'LOGIN_PASSCODE_VERIFY_UPPERCASE': 'VERIFY PASSCODE TO LOGIN',

  // Network List screen
  'NETWORK_UPPERCASE': 'NETWORKS',
  'ADD_NETWORK_UPPERCASE': 'ADD NETWORK',
  'NETWORK_SWITCH': 'Network Switch',
  'NETWORK_SWITCH_MSG': 'Are you sure you want to switch to ',
  'NETWORK_SWITCH_TITLE': 'Network Switched',
  'NETWORK_CHANGE_SUCCESS_MSG': 'Successfully switched to ',
  'CHANGE_NETWORK': 'Change Network',
  'CHANGE_NETWORK_TO': 'Change Network to ',
  'REMOVE_NETWORK_UPPERCASE': 'REMOVE NETWORK',
  'DONE_UPPERCASE': 'DONE',

  // Add Network screen
  'ADD_NETWORK_MSG':
    'Note: A Malicious network provider can lie about the state of the blockchain and record your network activity. Only add custom networks you trust',
  'NETWORK_NAME': 'Network Name',
  'RPC_URL': 'RPC Url',
  'NEW_RPC_NETWORK': 'New RPC Network',
  'CHAIN_ID': 'Chain ID',
  'SYMBOL': 'Symbol',
  'BLOCK_EXPLOR_URL': 'Block Explorer URL',
  'BLOCK_EXPLOR_URL_OPTIONAL': 'Block Explorer URL (Optional)',
  'ENTER_NETWORK_NAME_MSG': 'Please enter a network name.',
  'ENTER_RPC_URL_MSG': 'Please enter an RPC Url',
  'ENTER_CHAIN_ID_MSG': 'Please enter a chainid.',
  'ENTER_SYMBOL_NAME_MSG': 'Please enter a symbol name.',
  'ENTER_VALID_RPC_MSG': 'Please enter a valid RPC Url.',
  'ENTER_VALID_BLOCK_URL_MSG': 'Please enter a valid block url',
  'RPC_ALREADY_EXIST_MSG': 'This rpc url already exists. Please use another one.',
  'NETWORK_ALREADY_EXIST_MSG': 'This network already exists. Please enter a new chain ID or remove the existing one.',

  // setting screens
  'SETTINGS_UPPERCASE': 'SETTINGS',
  'GENERAL': 'General',
  'ABOUT_IM': 'About IM',
  'ABOUT': 'About',
  'CHOOSE_LANGUAGE': 'Choose Language',
  'LOGOUT': 'Logout',
  'REFER_FRIEND': 'Refer a Friend',
  'THEMES': 'Themes',
  'DATA_COLLECTION': 'Send Logs & Error Reports',
  'SECURITY_PRIVACY': 'Security & Privacy',
  'SECRET_RECOVERY_PHRASE': 'Secret Recovery Phrase',
  'WALLET_CONNECT': 'Wallet Connect',
  'VIEW_CONNECTED_APPS': 'View Connected Apps',
  'SCAN_QR_CODE': 'Scan QR Code',
  'SUPPORT': 'Support',
  'FEATURE_REQUEST': 'Feature Request',
  'SUBMIT_BUG': 'Submit a Bug',
  'SUBMIT_UPPERCASE': 'SUBMIT',
  'CONTACT_US': 'Contact Us',
  'LEGAL': 'Legal',
  'TERMS_AND_CONDITIONS': 'Terms and Conditions',
  'AND': 'and',
  'PRIVACY_POLICY': 'Privacy Policy',
  'APP_VERSION': 'App Version',

  // Change Language  screens
  'CHANGE_LANGUAGE_UPPERCASE': 'CHANGE LANGUAGE',
  'CURRENT_LANGUAGE_UPPERCASE': 'CURRENT LANGUAGE',
  'CHOOSE_LANGUAGE_MSG':
    'At this time, we only support English. Additional languages will be supported in future versions.',
  'LANGUAGE_LIST': [{ title: 'English', key: 'en' }],
  'ENTER_VALID_WALLET_URL': 'Wallet url is not valid. Please enter valid URL.',

  // Scan QR screen
  'CAMERA_PERMISSION_DENIED': 'Camera permission has been denied. To proceed, enable the camera permissions for Internet Money in your phone\'s settings.',
  'OPEN_PERMISSION_SETTINGS': 'Open Settings',

  // Collect Data screen
  'SENDING_LOGS_UPPERCASE': 'SENDING LOGS',
  'LOGS_AND_ERROR_REPORTING': 'Logs and Error Reporting',
  'DATA_COLLECTION_MSG': 'You may opt in to send logs and error reports to help the Internet Money team in debugging found issues. NO PERSONAL OR IDENTIFYING DATA IS COLLECTED.',
  'DATA_COLLECTION_LIST': [
    { title: 'Do not send logs', key: 'disable' },
    { title: 'Send logs', key: 'enable' },
  ],

  // About Screen
  'ABOUT_UPPERCASE': 'ABOUT',

  // Change Password screen
  'PASSWORD_SETTING_UPPERCASE': 'PASSWORD SETTINGS',
  'PASSCODE_SETTING_UPPERCASE': 'PASSCODE SETTINGS',

  'CHANGE_PASSWORD_UPPERCASE': 'CHANGE PASSWORD',
  'CHANGE_PASSCODE_UPPERCASE': 'CHANGE PASSCODE',
  'CHANGE_PASSWORD': 'Change Password',
  'CHANGE_PASSCODE': 'Change Passcode',

  'CHANGE_PASSWORD_INFO_MSG':
    'Use a password to access the wallet. If you lose this, you will need to use the secret recovery phrase to recover the wallet.',
  'CHANGE_PASSCODE_INFO_MSG':
    'Use a passcode to access the wallet. If you lose this, you will need to use the secret recovery phrase to recover the wallet.',

  'OLD_PASSWORD': 'Old Password',
  'NEW_PASSWORD': 'New Password',

  'OLD_PASSCODE': 'Old Passcode',
  'NEW_PASSCODE': 'New Passcode',

  'ENTER_OLD_PASSWORD_MSG': 'Please enter the old password',
  'ENTER_NEW_PASSWORD_MSG': 'Please enter the new password',
  'CHANGE_PASS_ERROR_MSG': 'Something went wrong, please try again.',

  'ENTER_OLD_PASSCODE_MSG': 'Please enter valid old passcode',
  'ENTER_NEW_PASSCODE_MSG': 'Please enter valid new passcode',

  'INCORRECT_OLD_PASSWORD': 'Incorrect old Password',

  // TIME Claim screen
  'TIME_UPPERCASE': 'TIME',
  'TIME_BALANCE': 'TIME Balance',
  'WALLET_DIVIDENDS_UPPERCASE': 'T.I.M.E DIVIDENDS',
  'SWEEPABLE_DIVIDENDS': 'Sweepable Dividends',
  'TOTAL_DIVIDENDS_CLAIMED': 'Total Dividends Claimed',
  'CLAIMABLE_DIVIDENDS': 'Claimable Dividends',
  'LAST_CLAIMED': 'Last Claimed',
  'NEVER_CLAIMED': 'No Dividends Claimed',
  'NETWORK': 'Network',
  'TOKEN': 'Token',
  'TOTAL_DIVIDENDS_DISTRIBUTED': 'Total Dividends Distributed',
  'CLAIM_DIVIDENDS_UPPERCASE': 'CLAIM DIVIDENDS',
  'SWEEP_DIVIDENDS_UPPERCASE': 'SWEEP DIVIDENDS',
  'TO_TIME_ADDRESS:': 'TO TIME Address:',
  'DIVIDENDS_AVAILABLE_TO_CLAIM': 'Dividends Available to Claim',
  'SLIDE_TO_CLAIM_DIVIDENDS_UPPERCASE': 'SLIDE TO CLAIM DIVIDENDS',
  'CLAIM_ZERO_BALANCE_ERROR_MSG':
    'Claimable dividend is not large enough to claim.',
  'SWEEP_ZERO_BALANCE_ERROR_MSG':
    'Sweepable dividend amount is too low.',
  'AVAILABLE_CLAIM': 'Available to Claim',
  'TIME_NOT_SUPPORTED': 'T.I.M.E. Dividend (TIME) is not yet supported on this network.',
  'WHAT_IS_TIME': 'What is TIME? ',
  'LEARN_MORE_HERE': 'Learn More Here',
  'FEATURE_NOT_READY': 'This feature is not available yet. We\'re working on it.',

  // Support Screen
  'SUPPORT_UPPERCASE': 'SUPPORT',
  'SUPPORT_TYPES': [
    {
      title: 'Send Feedback',
      subTitle: 'Feature Request',
      type: 'sendfeedback',
      detailTitle: 'GOT A COOL FEATURE IN MIND?',
      detailMsg: 'Let us know what you would like to see in future versions',
    },
    {
      title: 'Submit a Bug',
      subTitle: 'Report a Bug',
      type: 'submitbug',
      detailTitle: 'FOUND A BUG?',
      detailMsg:
        'Leaving your contact information is optional. If you want us to follow up, please leave a way to contact you!',
    },
    {
      title: 'Contact Us',
      subTitle: 'Get In Touch',
      type: 'contactus',
      detailTitle: 'WE WOULD LOVE TO HEAR FROM YOU!',
      detailMsg:
        'Leaving your contact information is optional. If you want us to follow up, please leave a way to contact you!',
    },
  ],

  'NAME': 'Name',
  'NAME_PLACEHOLDER': 'Name (Optional)',
  'EMAIL_OPT': 'Email (Optional)',
  'PHONE_NUMBER_OPT': 'Phone Number (Optional)',
  'SOCIAL_OPT': 'Social (Optional)',
  'YOUR_MESSAGE': 'Your Message *',
  'ENTER_VALID_EMAIL': 'Please enter a valid email address.',
  'ENTER_MESSAGE': 'Please enter your message.',
  'HOW_DO_REACH': 'How do we reach you?',
  'SUBJECT': 'Subject',
  'ENTER_SUBJECT': 'Enter your subject here',
  'TELL_ISSUE': 'Tell us your issue',
  'PROVIDE_DETAIL': 'Please provide as much detail as possible',
  'WHAT_YOU_LIKE': 'What would you like to see',
  'TELL_ABOUT_IT': 'Tell us about it',
  'DESCRIBE_ISSUE': 'Describe your issue below',
  'ENTER_SUBJECT_MSG': 'Please enter subject',
  'ENTER_NAME_MSG': 'Please enter your name.',

  // Wallet connect screen
  'ENTER_WALLET_URL': 'Enter WalletConnect url',
  'REJECT': 'Reject',
  'DISCONNECT': 'Disconnect',
  'VIEW': 'View',
  'WALLET_CONNECT_SUCCESS_MSG': 'Wallet connected successfully.',

  // sign transaction confirmation screen
  'SIGN_REQUEST_UPPERCASE': 'SIGN REQUEST',
  'CONFIRM_SIGNATURE_UPPERCASE': 'Confirm Signature',
  'ORIGIN': 'Origin',
  'YOU_ARE_SIGNING': 'You are signing:',
  'MESSAGE': 'Message:',
  'CANCEL': 'Cancel',
  'SIGN': 'Sign',

  // Send Transaction confirmation screen
  'DAPP_TRANSACTION_UPPERCASE': 'DAPP TRANSACTION',
  'SLIDE_TO_SEND_TRANSACTION_UPPERCASE': 'SLIDE TO SEND DAPP',

  // Referral claim screen
  'RECEVING_REWARDS': 'Receiving Rewards', // Keep this for historical transaction records

  // Force upgrade alert
  'MINOR_UPGRADE_TITLE': 'New Update Available',
  'MAJOR_UPGRADE_TITLE': 'New Update Required',
  'UPDATE_MSG': 'Internet Money Wallet has a new version available.',
  'UPDATE_UPPERCASE': 'UPDATE',
  'SKIP_NOW_UPPERCASE': 'SKIP FOR NOW',

  // Wallet Connect Modals
  'WC_SESSION_REQUEST_TITLE': 'New Connection Request',
  'WC_SESSION_REQUEST_SELECT_ACCOUNT': 'Select Account to Connect',
  'WC_SESSION_REQUEST_ACCEPT': 'Connect',
  'WC_SESSION_REQUEST_REJECT': 'Reject',
  'WC_SESSION_REQUEST_ERROR': 'An error occurred while connecting with Wallet Connect. Please reject the request and try again.',
  'WC_CHAIN_REQUEST_TITLE': 'Chain Switch Request',
  'WC_CHAIN_REQUEST_ACCEPT': 'Switch',
  'WC_CHAIN_REQUEST_REJECT': 'Reject',
  'WC_CHAIN_REQUEST_DISMISS': 'Dismiss',
  'WC_CHAIN_REQUEST_CHAIN_ID': 'Chain ID',
  'WC_CHAIN_REQUEST_UNABLE_TO_PARSE': 'Unable to parse the requested chain.',
  'WC_CHAIN_REQUEST_UNSUPPORTED_CHAIN': 'Requested chain is not currently enabled in your wallet.',
  'WC_TRANSACTION_REQUEST_TITLE': 'New Transaction Request',
  'WC_TRANSACTION_REQUEST_VIEW': 'View',
  'WC_TRANSACTION_REQUEST_REJECT': 'Reject',

  // Tab bar strings
  'NETWORK_DASHBOARD_TAB': 'NETWORKS',
  'CLAIM_DASHBOARD_TAB': 'DIVIDENDS',
  'WALLET_DASHBOARD_TAB': 'WALLET',
  'SETTING_DASHBOARD_TAB': 'SETTINGS',
  'SWAP_DASHBOARD_TAB': 'SWAP',

  // Common strings
  'OK': 'Ok',
  'SUCCESS': 'Success',
  'ERROR': 'Error',
  'YES': 'Yes',
  'NO': 'No',
  'OPPS': 'Opps!',
  'TRY_AGAIN': 'Try Again',
  'FINISHED_UPPERCASE': 'FINISHED',

  'GO_BACK_BUTTON_UPPERCASE': 'GO BACK',

  'UNEXPECTED_ERROR': 'We encountered an unexpected error. Please close the wallet and then reopen it. If the problem persists, contact our support team for assistance.',
  'SHOW_ERROR': 'Show Error',
  'HIDE_ERROR': 'Hide Error',

  // Error & Alert Messages
  'ENTER_PASS_MSG':
    'You can not have an empty password, please enter a password.',
  'ENTER_VALID_PASS_MSG': 'The password must have a minimum of 10 characters',
  'ENTER_CONF_PASS_MSG': 'Please confirm your password.',
  'MATCH_PASS_MSG':
    'The password and confirm password fields are not matching, please ensure the entries match in order to continue.',
  'PASS_NOT_MATCH_MSG':
    'Your password is invalid. Please enter the correct password.',
  'PASSCODE_NOT_MATCH_WITH_PREVIOUS_MSG':
    'Your passcode is invalid. Please enter the correct passcode',
  'LOGIN_VERIFY_PASSWORD_NOT_MATCH_MSG':
    'Your password is invalid. Please enter the correct password.',
  'LOGIN_VERIFY_PASSCODE_NOT_MATCH_MSG':
    'Your passcode is invalid. Please enter the correct passcode.',
  'ENTER_VALID_NEW_PASS_MSG':
    'The new password must be at least 10 characters long.',
  'MATCH_NEW_PASS_MSG':
    'The new password and confirm password fields are not matching, please ensure the entries match in order to continue.',
  'ENTER_VALID_AMOUNT_MSG': 'Please enter a valid amount.',
  'COPIED_SUCCESS': 'Copied successfully',
  'ADDRESS_COPIED_SUCCESS': 'Account address copied successfully',
  'SEED_PHRASE_COPIED_SUCCESS': 'Seed phrase copied successfully',
  'PRIVATE_KEY_COPIED_SUCCESS': 'Private key copied successfully',
  'TRANSACTION_HASH_COPIED_SUCCESS': 'Transaction hash copied successfully',
  'FIRST_CLAIM_FREE_PLS_MSG': 'You have to claim free PLS to refer friend.',
  'TRANSACTIONS_EXPORTED': 'Transactions exported successfully!',
  'ENTER_VALID_ACCOUNT_ADDRESS': 'Enter the valid address',
  'PLEASE_ENTER_TO_ADDRESS': 'Please enter to address',
  'JAILBREAK_ERROR_MSG':
    'You can not access the application with a jailbroken / rooted device.',
  'SCREENSHOT_WARNING': 'Taking a screenshot of your seed phrase is not recommended.',
  'SCREENSHOT_PRIVATE_KEY_WARNING': 'Taking a screenshot of your private key is not recommended.',
  'TRANSACTION_SUCCESS': 'Transaction successful!',
  'SUBMIT_SUCCESS': 'Transaction submitted!',
  'REPLACEMENT_TRANSACTION': 'A pending transaction already exists. Please wait for it to confirm, or consider resubmitting it with a higher gas fee to expedite processing.',
  'SOMETHING_WENT_WRONG': 'Something went wrong!',
  'AVAILABLE_TO_CLAIM_INFO': 'Available = Sweepable + Claimable Dividends',
  'SWEEP_DIVIDENDS_INFO': 'By sweeping, all dividends that have accrued are brought into the TIME smart contract and made available to claim. This is an optional transaction, as when one person sweeps, dividends are distributed for everyone',
  'CLAIM_DIVIDENDS_INFO': 'By claiming, you are transferring all earned dividends to your account holding TIME.',
  'INSUFFICIENT_FUNDS': 'Not enough {0} to pay for the transaction.',
};
