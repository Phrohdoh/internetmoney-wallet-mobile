import { Buffer } from 'buffer';
import process from 'process';
import { TextEncoder, TextDecoder } from 'text-encoding';
import { decode, encode } from 'base-64';
import 'react-native-get-random-values';
import 'react-native-url-polyfill/auto';
import { version } from '../package.json';

const setGlobal = ([key, value]) => {
  global[key] = value;
};

const setGlobals = (values) => {
  Object.entries(values)
    .forEach(setGlobal);
};

const setGlobalDefaults = (values) => {
  Object.entries(values)
    .filter(([key]) => global[key] === undefined)
    .forEach(setGlobal);
};

setGlobalDefaults({
  btoa: encode,
  atob: decode,
  TextEncoder,
  TextDecoder,
});

setGlobals({
  Buffer,
  process,
  NODE_ENV: __DEV__ ? 'development' : 'production',
  APP_VERSION: version,
});
