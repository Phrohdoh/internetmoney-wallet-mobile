const sharp = require('sharp');
const fs = require('fs');
const path = require('path');

const resizeAndReplace = async (newImage, imageToReplace) => {
  try {
    // Get the dimensions of the target image
    const { width, height } = await sharp(imageToReplace).metadata();

    // Resize the original image to the target dimensions
    await sharp(newImage)
      .resize(width, height)
      .toFile('temporaryImage.png');

    // Replace the imageToReplace file with the resized original image
    fs.rename('temporaryImage.png', imageToReplace, (err) => {
      if (err) throw err;
      console.log('Image successfully resized and replaced');
    });
  } catch (err) {
    console.error('Error:', err);
  }
};

const main = async () => {
  const iconDirectory = path.join(__dirname, '../ios/ImCryptoWallet/Images.xcassets/AppIcon.appiconset')
  const newIcon = path.join(__dirname, './internet-money-wallet-logo.png');
  const files = await fs.promises.readdir(iconDirectory);
  const icons = files.filter(file => file.endsWith('.png'));
  const iconPaths = icons.map(icon => path.join(iconDirectory, icon));
  for (let i = 0; i < iconPaths.length; i += 1) {
    console.log(`Resizing ${newIcon} and replacing ${iconPaths[i]}...`);
    await resizeAndReplace(newIcon, iconPaths[i]);
  }
  console.log('Done!');
}

main();