import mockClipboard from '@react-native-clipboard/clipboard/jest/clipboard-mock.js';
import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';
import { Crypto } from '@peculiar/webcrypto';
import { version } from '../package.json';

// jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter.js', () => {
//     const { EventEmitter } = require('events');
//     return EventEmitter;
//   });

jest.mock('react-native-argon2', () => ({
  argon2: jest.fn(() => Promise.resolve('mocked_value')),
}));

jest.mock('@react-native-clipboard/clipboard', () => mockClipboard);
jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);
global.APP_VERSION = version;
global.crypto = new Crypto();
