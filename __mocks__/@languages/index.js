import LocalizedStrings from 'react-native-localization';
import { english } from './../../src/languages/en';

export const Strings = new LocalizedStrings({
  en: english,
});

// Get Localize string of selected language
export const localize = (key, _params = []) => {
  return Strings[key];
};
