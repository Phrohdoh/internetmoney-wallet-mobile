import React from 'react';
import {Button} from 'react-native';
export default SwipeVerifyController = props => {
  return (
    <Button
      key="swiper"
      title="Swipe"
      testID="swiper"
      onPress={ () => props.onVerified() }
    />
  );
};
