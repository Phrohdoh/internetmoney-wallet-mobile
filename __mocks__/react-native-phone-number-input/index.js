import React, {PureComponent} from 'react';
import {TextInput} from 'react-native';

export default class PhoneInput extends PureComponent {

  constructor (props) {
    super(props);

    this.state = {
      code: '91',
      number: '9466888803',
      modalVisible: false,
      countryCode: props.defaultCode ? props.defaultCode : 'IN',
      disabled: props.disabled || false,
    };
  }

  
  isValidNumber = (number) => {
    if(number==='9466888803')
      return true;

    return false;  
  };

  render () {
    const {testID, onChangeText}= this.props;
    return (
      <TextInput key="phone-input" testID={ testID } onChangeText={ text => onChangeText(text) }/>
    );
  }
}
