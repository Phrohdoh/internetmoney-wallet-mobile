import React from 'react';
import {Button} from 'react-native';
export default QRCode = () => {
  return <Button key="qr-code" title="qr code" testID="qr-code" />;
};
