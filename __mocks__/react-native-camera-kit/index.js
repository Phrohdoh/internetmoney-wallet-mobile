import React from 'react';
import { Pressable } from 'react-native';

export const CameraScreen = (props) => {
  return <Pressable testID={ 'bar-code-camera' } onPress={ () => props.onReadCode({nativeEvent: {codeStringValue: 'etherium.eth'}}) } ></Pressable>;
};

