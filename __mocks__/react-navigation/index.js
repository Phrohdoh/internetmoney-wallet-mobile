export const Navigation = {
  events: jest.fn(() => {
    return {
      bindComponent: jest.fn(),
      registerAppLaunchedListener: jest.fn(),
      registerComponentDidAppearListener: jest.fn(),
      registerComponentDidDisappearListener: jest.fn(),
      registerNavigationButtonPressedListener: jest.fn(),
    };
  }),
  registerComponentWithRedux: jest.fn(),
  registerComponent: (name, callback) => callback(),
  setRoot: jest.fn(),
  push: jest.fn(),
  pop: jest.fn(),
  setDefaultOptions: jest.fn(),
  showOverlay: jest.fn(),
  dismissOverlay: jest.fn(),
  showModal: jest.fn(),
  mergeOptions: jest.fn(),
  dismissAllModals: jest.fn(),
  navigate: jest.fn(),
};
