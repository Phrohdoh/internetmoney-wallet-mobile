# Internet Money Mobile App

## Publishing a new build

### Preparation

First, ensure that you have the lastest code from the `development` branch:

```bash
git checkout development
git fetch
git reset origin/development --hard
git checkout -b build-xx
```

Install the dependencies:

```bash
yarn
```

Install Ruby, version 2.7.5.

Run the command `gem install bundler` to install the Bundler gem if you haven't installed it already.

Run the command `bundle install` to install the dependencies listed in `Gemfile`.

Enter the `ios` directory and run `pod install` to install the dependencies listed in `ios/Podfile`.

Install Xcode.

### Version Bumping

For every build, there is an internal build version number that must be bumped in two places (e.g. `141` => `142`):
* `versionCode` in `android/app/build.gradle`
* `CURRENT_PROJECT_VERSION` (TWICE!) in `ios/ImCryptoWallet.xcodeproj/project.pbxproj`

After a public release (and only after a public release), you will need to bump the public release version number in two places (e.g. `1.16` => `1.17`):
* `versionName` in `android/app/build.gradle`
* `MARKETING_VERSION` (TWICE!) in `ios/ImCryptoWallet.xcodeproj/project.pbxproj`

For every build, bump the external build version number (e.g. `1.16.2` => `1.17.0` after a public release, otherwise `1.16.2` => `1.16.3`):
* `version` in `package.json`

### Android 

Build the bundle:

```bash
yarn build:android:release
```

Now, locate the bundle file `app-release.aab` located at `android/app/build/outputs/bundle/release/app-release.aab`. This file will need to be uploaded for internal testing.

Navigate to the [Internal testing](https://play.google.com/console/u/1/developers/5190766265125238697/app/4974489493353081102/tracks/internal-testing) page in Google Play Console, and click "Create new release" (or "Edit release" if a draft has already been started).

Upload `app-release.aab` as the App bundle. Give it a release name and notes, and click "Next". Then, click "Start rollout to Internal testing" to begin publishing the new release for internal testing.

After verifying that the app works as expected, you can promote it on the [Open testing](https://play.google.com/console/u/1/developers/5190766265125238697/app/4974489493353081102/tracks/open-testing) page. Follow the same process to create a new release for our open testing users.

Lastly, the app can be promoted for the general public by again following the same release process on the [Production](https://play.google.com/console/u/1/developers/5190766265125238697/app/4974489493353081102/tracks/production) page.

### iOS

In Xcode, open the project file in `ios/ImCryptoWallet.xcworkspace`. In the Signing & Capabilities tab, ensure that you select the "Decentralized Innovations, LLC" team and check "Automatically manage signing". For accessing this team, you will need to be invited to the team in App Store Connect and granted the "Certificates, Identifiers & Profiles" and "Cloud Managed Distribution Certificate" permissions by an admin.

In the top bar of Xcode, ensure that you have "Any iOS Device (arm64)" selected as the target for builds. Then, click Product -> Archive to build the archive file of the app which will be uploaded.

When the archive completes, it will be listed in a new popup window of archives. Select it and click Distribute App. Select "App Store Connect", then "Upload". Ensure that both "Upload your app's symbols" and "Manage Version and Build Number" are checked. Choose "Automatically manage signing", and then Upload.

## Troubleshooting

### Running with Xcode gives `'value' is unavailable: introduced in iOS 12.0` error

The reasons for this error are not fully understood at the moment.

Solution(s):

- Find-and-replace `IPHONEOS_DEPLOYMENT_TARGET = 11.0` with `IPHONEOS_DEPLOYMENT_TARGET = 12.4` in the file `ios/Pods/Pods.xcodeproj/project.pbxproj`.

See [this GitHub comment](https://github.com/facebook/react-native/issues/34106#issuecomment-1188225587) for more information.

### SSL cert errors when pulling from CocoaPods repo

This issue was encountered during `pod install`, and may be related to which version of `openssl` your system is pointing at.

Solution(s):

- Make sure the version of `openssl` you want to use is first in your `$PATH`.
- Make sure you've set your `LDFLAGS` env var to point to that `openssl` version.

See [this StackOverflow thread](https://stackoverflow.com/questions/62195898/openssl-still-pointing-to-libressl-2-8-3) for more information.

### Failures when installing Ruby 2.x.x on a Mac M1

Both `asdf` and `rbenv` are affected by this issue.

Solution(s):

- Prepend `RUBY_CFLAGS="-w"` to your Ruby installation command.

See [this GitHub comment](https://github.com/asdf-vm/asdf-ruby/issues/255#issuecomment-1269236118) for more information.

### `pod install` fails when installing Hermes pod

NOTE: This solution will change your `Podfile.lock`. As this problem is more than likely local to a dev's machine, we shouldn't push the updated file in case it has unforeseen effects. However, more testing needs to be done on that point. 

Solution(s):

- Run `pod update hermes-engine --no-repo-update`, then try `pod install` again.

See [this GitHub comment](https://github.com/facebook/react-native/issues/36945#issuecomment-1515325778) for more information.

### Can't find NodeJS when running from Xcode

This is specific to the Metro program launcher that runs when starting the app via Xcode.

Solution(s):

- In the `ios` folder, copy `_xcode.env` to `.xcode.env.local` and manually set the path to your Node executable.